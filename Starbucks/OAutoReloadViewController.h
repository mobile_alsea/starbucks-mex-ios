//
//  OAutoReloadViewController.h
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"
#import "DYHorizontalStretchButton.h"

@interface OAutoReloadViewController : OBaseViewController <UITableViewDataSource,UITableViewDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
    UIImageView *mBackGroundimageView;
    UISwitch *mStatusAutoReloadSwitch;
    UILabel *mStatusAutoReloadLabel;
}
@property (strong, nonatomic) IBOutlet UIImageView *BackGroundimageView;
@property (strong, nonatomic) IBOutlet UISwitch *statusAutoReloadSwitch;
@property (strong, nonatomic) IBOutlet UILabel *statusAutoReloadLabel;
@property (strong, nonatomic) IBOutlet UIView *viewPaymentSettings;
@property (strong, nonatomic) IBOutlet DYHorizontalStretchButton *SaveButton;
@property (strong, nonatomic) IBOutlet UITableView *TypePaymentTableView;
- (IBAction)SaveButtonPressed:(id)sender;
- (IBAction)AutomaticallyReloadAmountPressed:(id)sender;
- (IBAction)BalanceFallReloadPressed:(id)sender;
- (IBAction)PaymentTypeContinuePressed:(id)sender;
- (IBAction)valueChanged:(id)sender;
- (IBAction)AutoReloadButtonPressed:(id)sender;
@end
