//
//  ODrinkDescriptionCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinkDescriptionCell.h"

@implementation ODrinkDescriptionCell

@synthesize nameLabel = mNameLabel;
@synthesize availableLabel = mAvaliableLabel;
@synthesize userLabel = mUserLabel;
@synthesize user = mUser;
@synthesize name = mName;
@synthesize available = mAvailable;
@synthesize propertyOfLabel = mPropertyOfLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
