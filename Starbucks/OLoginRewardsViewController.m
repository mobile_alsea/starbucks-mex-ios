//
//  OLoginRewardsViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OLoginRewardsViewController.h"
#import "ORewardsViewController.h"
#import "ORememberPasswordViewController.h"
#include "OQueryManager.h"
#import <DYCore/categories/dictionary/NSDictionary_dictionary.h>
#import <DYCore/categories/hash/NSData_hash.h>
#import "OInitRewardsViewController.h"
#import <DYCore/files/DYFileNameManager.h>
@interface OLoginRewardsViewController ()

@end

@implementation OLoginRewardsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.navigationItem setTitle:NSLocalizedString(@"Iniciar Sesión", nil)];
	

    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma markIBActions methods

-(IBAction)enterButtonPressed:(id)sender
{
	
    NSString * password = [mPasswordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSString * uid = @"12345678912345678912345";
    NSString * usuario = [mUSerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mUSerTextField.text = usuario;
	
    if( [usuario length] <= 0 || [password length] <= 0 )
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"El Correo Electrónico y Contraseña no pueden estar vacios." delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
		
		[alertView show];
	}
	else
	{
		
		[mUSerTextField resignFirstResponder];
		[mPasswordTextField resignFirstResponder];
		
		NSString * result = @"<?xml version=\"1.0\" encoding=\"utf-8\"?><soapenv:Envelope xmlns:impl=\"http://impl.service.replicador.alsea.com.mx\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Header><wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsse:UsernameToken wsu:Id=\"UsernameToken-22\"><wsse:Username>bob</wsse:Username><wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">bobPW</wsse:Password><wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">moDQbMH0JeBhfD9y1HR/g==</wsse:Nonce><wsu:Created>2012-09-26T05:47:08.758Z</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><impl:";
		

		
		
		result = [result stringByAppendingString:kMethod_Login];
		result = [result stringByAppendingString:@">"];
		
		//Body Zone
		result = [result stringByAppendingString:@"<"];
		result = [result stringByAppendingString:kParamUserId];
		result = [result stringByAppendingString:@">"];
		result = [result stringByAppendingString:usuario];
		result = [result  stringByAppendingString:@"</"];
		result = [result stringByAppendingString:kParamUserId];
		result = [result stringByAppendingString:@">"];
		
		result = [result stringByAppendingString:@"<"];
		result = [result stringByAppendingString:kParamPassword];
		result = [result stringByAppendingString:@">"];
		result = [result stringByAppendingString:password];
		result = [result  stringByAppendingString:@"</"];
		result = [result stringByAppendingString:kParamPassword];
		result = [result stringByAppendingString:@">"];
		
		
		result = [result stringByAppendingString:@"<"];
		result = [result stringByAppendingString:kParamUid];
		result = [result stringByAppendingString:@">"];
		result = [result stringByAppendingString:uid];
		result = [result  stringByAppendingString:@"</"];
		result = [result stringByAppendingString:kParamUid];
		result = [result stringByAppendingString:@">"];
		
		result = [result stringByAppendingString:@"</impl:"];
		result = [result stringByAppendingString:kMethod_Login];
		result = [result stringByAppendingString:@"></soapenv:Body></soapenv:Envelope>"];

		NSData* part=[result dataUsingEncoding:NSUTF8StringEncoding];
		[OQueryManager queryServiceWithData:kMethod_Login params:part delegate:self];
		
		/*NSDictionary * body =[NSDictionary dictionaryWithObjectsAndKeys:usuario,kParamUserId, nil];
		[OQueryManager queryService:kMethod_Login params:body delegate:self];*/
	}

}
-(IBAction)rememberPasswordPressed:(id)sender
{
	ORememberPasswordViewController *rememberViewController = [[ORememberPasswordViewController alloc] initWithNibName:@"ORememberPasswordViewController" bundle:nil];
	[self.navigationController pushViewController:rememberViewController animated:YES];

}


#pragma mark - SOAPMAnagerDelegate
- (void) didReceiveResponseFromFunction:(NSString *)_function dataXML: (NSDictionary *) _response
{
	
	
	
	if([_function isEqualToString:kMethod_Login])
	{
       
        
        
        
		
		if(_response!=nil)
		{
			NSString *UserName=mUSerTextField.text;
            NSString *Password=mPasswordTextField.text;
            
			mUSerTextField.text = @"";
			mPasswordTextField.text = @"";
			
			[mUSerTextField resignFirstResponder];
			[mPasswordTextField resignFirstResponder];
	

            
            //PRIMERO BUSCAMOS SI SE RECIBIO RESPUESTA OK
            NSString * starDateCheck  = [[_response objectForKey:@"ax224:startDate"]objectForKey:@"value"];
            NSString * estrellasCheck  = [[_response objectForKey:@"ax222:estrellas1"]objectForKey:@"value"]; 
            if([starDateCheck length]<=0 && [estrellasCheck length]<=0 )
			{
                
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Disponible" message:@"Servicio no Disponible." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				//OInitRewardsViewController  *regrsa=[[OInitRewardsViewController alloc]initWithNibName:nil bundle:nil];
                //[self.navigationController pushViewController:regrsa animated:YES];
				[alertView show];
			}

            else
            {
            
            //BUSCAMOS ERROR EN LOGIN Y CONTRASEÑA
			NSString * error =[[_response objectForKey:@"ax224:errorMessage"]objectForKey:@"value"];
            if([error length]>0)
			{
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Correo y/o contraseña incorrectos." delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
				
				[alertView show];
                
			
			}
			else
			{
			
				if([[[_response objectForKey:@"ax222:estrellas1"]objectForKey:@"value"] isEqualToString:@""] ||[[[_response objectForKey:@"ax224:estrellas1"]objectForKey:@"value"] isEqualToString:@""] )
				{
				
					UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Nombre de usuario o contraseña incorrectos. Asegurese de estar conectado a Internet e inténtelo más tarde." delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
				
					[alertView show];
			
				}
				else
				{
                    NSString *pListPath = [DYFileNameManager pathForFile:@"RewardsUserInfo.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
                    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
                    
                    if(dictionary!=nil)
                    {
                        [dictionary setObject:[UserName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"UserName"];
                        [dictionary setObject:[Password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"Password"];
                        [dictionary setObject:[[_response objectForKey:@"ax224:estrellas1"]objectForKey:@"value"] forKey:@"Stars"];
                        [dictionary setObject:[[_response objectForKey:@"ax224:estrellas2"]objectForKey:@"value"] forKey:@"StarsGold"];
                        [dictionary setObject:[[_response objectForKey:@"ax224:startDate"]objectForKey:@"value"] forKey:@"DateMember"];
                        [dictionary writeToFile:pListPath atomically:YES];
                        
                        
                    }
                    
                    
                    
                    
					ORewardsViewController *rewardViewController = [[ORewardsViewController alloc] initWithNibName:@"ORewardsViewController" bundle:nil];
			
			
					rewardViewController.numberOfStars = [[_response objectForKey:@"ax224:estrellas1"]objectForKey:@"value"];

                    rewardViewController.numberOfStarsGold = [[_response objectForKey:@"ax224:estrellas2"]objectForKey:@"value"];
                    
					rewardViewController.dateMember = [[_response objectForKey:@"ax224:startDate"]objectForKey:@"value"];
			
                    
				
					SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:rewardViewController];
					[navigationController.navigationBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"NavBar.png"]]];
					[self presentModalViewController:navigationController animated:YES];
				}
			}
            }
		}
	}
	
	
	
	
}

- (void) soapCallFailedForFunction:(NSString *)_function withErrorCode:(NSString *)_errorCode
{
}




@end
