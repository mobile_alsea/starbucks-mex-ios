//
//  OAutoReloadViewController.m
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OAutoReloadViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OCreditCardCell.h"
#import "OSelectPaymentViewController.h"

@interface OAutoReloadViewController ()

@end

@implementation OAutoReloadViewController
@synthesize statusAutoReloadLabel = mStatusAutoReloadLabel;
@synthesize BackGroundimageView = mBackGroundimageView;
@synthesize statusAutoReloadSwitch = mStatusAutoReloadSwitch;
@synthesize TypePaymentTableView;
@synthesize viewPaymentSettings;
@synthesize SaveButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAutoReloadViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Mis Tarjetas", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Recarga Automática", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Dismiss:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        mBackGroundimageView.layer.cornerRadius = 5.0;
        mBackGroundimageView.layer.masksToBounds = YES;
        mBackGroundimageView.clipsToBounds = YES;
        
        viewPaymentSettings.frame = CGRectMake(0, -146, 320, 192);
        SaveButton.frame = CGRectMake(10, 65, 300, 37);
        [self.view addSubview:viewPaymentSettings];
        [viewPaymentSettings setHidden:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AUTORELOAD"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AUTORELOAD"] == YES) {
            mStatusAutoReloadLabel.text = @"Recarga Automática Activada";
            mStatusAutoReloadSwitch.on = YES;
            
        }else{
            mStatusAutoReloadLabel.text = @"Recarga Automática Desactivada";
            mStatusAutoReloadSwitch.on = NO;
        }
        //[self valueChanged:nil];
        self.TypePaymentTableView.delegate = self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/ORecentTransactionsViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)valueChanged:(id)sender
{
    if (mStatusAutoReloadSwitch.on == YES) {
        [viewPaymentSettings setHidden:NO];
        [UIView animateWithDuration:0.3
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             [viewPaymentSettings setHidden:NO];
                             CGRect frame ;
                             frame = viewPaymentSettings.frame;
                             frame.origin.y = 60;
                             viewPaymentSettings.frame = frame;
                             viewPaymentSettings.alpha = 1.0;
                             
                             
                             frame=SaveButton.frame;
                             frame.origin.y=70;
                             SaveButton.frame=frame;
                             SaveButton.alpha = 1.0;
                             
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        
    }else{
        NSLog(@"AUTORELOAD DESACTIVADO");
        [UIView animateWithDuration:0.3
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGRect frame ;
                             frame = viewPaymentSettings.frame;
                             frame.origin.y = -160;
                             
                             CGRect frame2 ;
                             frame2 = SaveButton.frame;
                             frame2.origin.y = 60;
                             
                             viewPaymentSettings.frame = frame;
                             viewPaymentSettings.alpha = 1.0;
                             SaveButton.frame = frame2;
                             SaveButton.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             [viewPaymentSettings setHidden:YES];
                         }];
        
    }
}

- (IBAction)AutoReloadButtonPressed:(id)sender {
}

#pragma mark DismissViewController

-(IBAction)Dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    switch (_tableView.tag) {
		case 0:
			return 1;
			break;
		default:
			break;
	}
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    
	@try{
        switch (_tableView.tag) {
            case 0:
            {
                
                static NSString *CellIdentifier = @"OCreditCardCell";
                
                OCreditCardCell *cell = (OCreditCardCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil)
                {
                    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                    cell.lblTitulo.text = @"Tarjeta de Crédito: 9799";
                }
                
                [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
                return cell;
            }
                break;
            default:
                break;
        }
        
        return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAutoReloadViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
        [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
        
        OSelectPaymentViewController *detailViewController = [[OSelectPaymentViewController alloc] initWithNibName:@"OSelectPaymentViewController" bundle:nil];
        ;
        //detailViewController.delegate = self;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
	
}

- (IBAction)SaveButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    if (mStatusAutoReloadSwitch.on == YES) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AUTORELOAD"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AUTORELOAD"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)AutomaticallyReloadAmountPressed:(id)sender {
}

- (IBAction)BalanceFallReloadPressed:(id)sender {
}

- (IBAction)PaymentTypeContinuePressed:(id)sender {
}
@end
