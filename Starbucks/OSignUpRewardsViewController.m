//
//  OSignUpRewardsViewController.m
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OSignUpRewardsViewController.h"
#import "MBProgressHUD.h"
#import "UIDevice+IdentifierAddition.h"

@interface OSignUpRewardsViewController ()
@property (strong, nonatomic) UIBarButtonItem *sendUserInformation;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSString *birthdayString;
@property (strong, nonatomic) NSDictionary *userInformationDictionary;
-(BOOL)emailValidate:(NSString *)email;
-(BOOL)passwordValidate:(NSString *)password;
@end

@implementation OSignUpRewardsViewController
@synthesize sView;
@synthesize viewSignUpRegister;
@synthesize sendUserInformation;
@synthesize HUD;
@synthesize userInformationDictionary;
//ui controls
@synthesize NameErrorLabel;
@synthesize nameTextField;
@synthesize lastNameTextField;

//error label
@synthesize emailErrorLabel;

@synthesize emailTextField;
@synthesize usernameLabel;

@synthesize passwordTextField;
@synthesize confirmPasswordTextField;
@synthesize passwordInstructionsLabel;

//error label
@synthesize passwordsErrorLabel;
@synthesize dateButton;
@synthesize BirthdayLabel;
@synthesize addressTextField;
@synthesize secondAddressTextField;
@synthesize cityTextField;
@synthesize stateTextField;
@synthesize postalCodeTextField;
@synthesize newsletterSwitch;
@synthesize dateMonthPickerView;
@synthesize CountryTextField;
@synthesize toolBar;
@synthesize birthdayString;
//error labels



//
//view
@synthesize nameErrorView;
@synthesize nameView;
@synthesize emailErrorView;
@synthesize emailView;
@synthesize passwordErrorView;
@synthesize passwordView;
@synthesize birthdayView;
@synthesize moreInfoView;
@synthesize suscribeView;
//

@synthesize SignUpButton;

bool existsVerifyInputErrorNameLastName = NO;

bool existsVerifyInputErrorEmail = NO;

bool existsVerifyInputErrorPasswords = NO;


int heightPointer;


-(BOOL)passwordValidate:(NSString *)password
{
    NSString *passwordRegex =   @"(?=.*?[a-z])(?=.*?[A-Z])(?=.*\d)(?=.*?[^a-zA-Z]).{6,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordTest evaluateWithObject:password];
    
}


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


#pragma  mark Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            /*
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
            */
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSignUpRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        int intTop = 0;
        [self.navigationItem setTitle:NSLocalizedString(@"Registro", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancelar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(dissmissController:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        
        sendUserInformation = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Continuar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(sendUserInformation:)];
        [self.navigationItem setRightBarButtonItem:sendUserInformation];
        //
        [sView addSubview:viewSignUpRegister];
        intTop = intTop + viewSignUpRegister.frame.size.height;
        
        nameView.frame = CGRectMake(0, intTop, 320, nameView.frame.size.height);
        intTop = intTop + nameView.frame.size.height;
        [sView addSubview:nameView];
        
        
        emailView.frame = CGRectMake(0, intTop, 320, emailView.frame.size.height);
        intTop = intTop + emailView.frame.size.height;
        [sView addSubview:emailView];
        
        passwordView.frame = CGRectMake(0, intTop, 320, passwordView.frame.size.height);
        intTop = intTop + passwordView.frame.size.height;
        [sView addSubview:passwordView];
        
        birthdayView.frame = CGRectMake(0, intTop, 320, birthdayView.frame.size.height);
        intTop = intTop + birthdayView.frame.size.height;
        [sView addSubview:birthdayView];
        
        moreInfoView.frame = CGRectMake(0, intTop, 320, moreInfoView.frame.size.height);
        intTop = intTop + moreInfoView.frame.size.height;
        [sView addSubview:moreInfoView];
        
        suscribeView.frame = CGRectMake(0, intTop, 320, suscribeView.frame.size.height);
        intTop = intTop + suscribeView.frame.size.height;
        [sView addSubview:suscribeView];
        
        [sView setContentSize:CGSizeMake(320, intTop + 100)];
        //
        heightPointer = 0;
        
        [dateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [dateButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        dateMonthPickerView.frame = CGRectMake(0.0f,screenRect.size.height - 300.0, 320.0f, 252.0f);
        toolBar.frame = CGRectMake(0.0f, screenRect.size.height - 344.0f, 320.0f, 44.0f);
        
        //
        NSArray *arrayTextfields = [[NSArray alloc] initWithObjects:nameTextField,lastNameTextField,emailTextField,passwordTextField,confirmPasswordTextField,addressTextField,secondAddressTextField,cityTextField,stateTextField,postalCodeTextField,nil];
        
        for(int x = 0; x < [arrayTextfields count]; x++){
            UITextField *v = (UITextField *)[arrayTextfields objectAtIndex:x];
            v.delegate = self;
            [v addTarget:self action:@selector(verificarTexto:) forControlEvents:UIControlEventEditingChanged];
        }
        
        
        SignUpButton.enabled = FALSE;
        
        intErrorEmail = 0;
        intErrorName = 0;
        intErrorPassword = 0;
        
        //
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OSignUpRewardsViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(void)verificarTexto:(id)sender{
    int empty = 0;
    for(id x in [viewSignUpRegister subviews]){
        if([x isKindOfClass:[UITextField class]]){
            UITextField *v = (UITextField *)x;
            if (v.text.length == 0 ) {
                empty++;
            }
        }
        
    }
    
    if (empty == 0) {
        if (postalCodeTextField.text.length == 5 && intErrorEmail == 0 && intErrorName == 0 && intErrorPassword == 0) {
            
            //falta la validacion de la contraseña y email
            SignUpButton.enabled = YES;
            NSLog(@"correcto");
        }
        else{
            SignUpButton.enabled = FALSE;
            NSLog(@"incorrecto");
        }
    }
    else{
        //Campos vacios
        SignUpButton.enabled = FALSE;
    }
    
    //return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if ([textField isEqual:lastNameTextField] || [textField isEqual:nameTextField] ) {
        if (nameTextField.text.length == 0 && lastNameTextField.text.length == 0 ) {
            NameErrorLabel.text = @"Por favor ingresa tu nombre y apellido.";
            intErrorName = 1;
        }
        else if(nameTextField.text.length == 0 && lastNameTextField.text.length > 0){
            NameErrorLabel.text = @"Por favor ingresa tu nombre.";
            intErrorName = 1;
        }
        else if(nameTextField.text.length > 0 && lastNameTextField.text.length == 0){
            NameErrorLabel.text = @"Por favor ingresa tu apellido.";
            intErrorName = 1;
        }
        else{
            intErrorName = 0;
        }
    }
    
    
    
    
    if ([textField isEqual:emailTextField]) {
        if (emailTextField.text.length == 0) {
            emailErrorLabel.text = @"Por favor ingresa un Email válido.";
            intErrorEmail = 1;
        }
        else {
            if([self emailValidate:emailTextField.text]){
                intErrorEmail = 0;
            }
            else{
                intErrorEmail = 1;
            }
        }
    }
    
    
    
    if ([textField isEqual:confirmPasswordTextField]) {
        if (passwordTextField.text.length < 6 || confirmPasswordTextField.text.length < 6) {
            passwordsErrorLabel.text = @"Por favor ingresa una contraseña de por lo menos 6 caracteres de largo.";
            intErrorPassword = 1;
        }
        else if ((passwordTextField.text.length == 6 && confirmPasswordTextField.text.length == 6) && (![passwordTextField.text isEqualToString: confirmPasswordTextField.text])) {
            passwordsErrorLabel.text = @"La contraseña no coincide, intente nuevamente.";
            intErrorPassword = 1;
        }
        else {
            if ([self passwordValidate:passwordTextField.text] && [self passwordValidate:confirmPasswordTextField.text] && (![passwordTextField.text isEqualToString: confirmPasswordTextField.text])) {
                passwordsErrorLabel.text = @"La contraseña no coincide, intente nuevamente.";
                intErrorPassword = 1;
            }
            else{
                intErrorPassword = 0;
            }
        }
    }
    
    [self moveElementsSignUpWithError];
    [self verificarTexto:textField];
    
    return YES;
}


-(void)moveElementsSignUpWithError{
    int intTop = 0;
    intTop = intTop + viewSignUpRegister.frame.size.height;
    
    if (intErrorName == 1) {
        [UIView animateWithDuration:0.3 animations:^{
            nameErrorView.frame = CGRectMake(0, intTop, 320, nameErrorView.frame.size.height);
            [sView addSubview:nameErrorView];
        }];
        intTop = intTop + nameErrorView.frame.size.height;
    }
    else{
        [nameErrorView removeFromSuperview];
    }
    intTop = [self moveViewAnimation:nameView height:intTop];
    
    if (intErrorEmail == 1) {
        [UIView animateWithDuration:0.3 animations:^{
            emailErrorView.frame = CGRectMake(0, intTop, 320, emailErrorView.frame.size.height);
            [sView addSubview:emailErrorView];
        }];
        intTop = intTop + emailErrorView.frame.size.height;
    }
    else{
        [emailErrorView removeFromSuperview];
    }
    intTop = [self moveViewAnimation:emailView height:intTop];
    
    if (intErrorPassword == 1) {
        [UIView animateWithDuration:0.3 animations:^{
            passwordErrorView.frame = CGRectMake(0, intTop, 320, passwordErrorView.frame.size.height);
            [sView addSubview:passwordErrorView];
        }];
        intTop = intTop + passwordErrorView.frame.size.height;
    }
    else{
        [passwordErrorView removeFromSuperview];
    }
    intTop = [self moveViewAnimation:passwordView height:intTop];
    intTop = [self moveViewAnimation:birthdayView height:intTop];
    intTop = [self moveViewAnimation:moreInfoView height:intTop];
    intTop = [self moveViewAnimation:suscribeView height:intTop];
    
    [sView setContentSize:CGSizeMake(320, intTop + 315)];
}

-(int)moveViewAnimation:(UIView *)view height:(int)top{
    [UIView animateWithDuration:0.3 animations:^{
        view.frame = CGRectMake(0, top, 320, view.frame.size.height);
    }];
    top = top + view.frame.size.height;
    return top;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    if ([textField isEqual:postalCodeTextField]) {
        NSUInteger newLength = [postalCodeTextField.text length] + [string length] - range.length;
        return (newLength <= 5);
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)dissmissController:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


- (void) OAddRewardsCardViewControllerDidCancel:(OAddRewardsCardViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
    
}

- (IBAction)ValueChangedNewsletterSwitch:(id)sender {
    if (newsletterSwitch.on == YES) {
        
    }else{
        
    }
}

- (IBAction)SelectBirthday:(id)sender {
    [toolBar setHidden:YES];
    [dateMonthPickerView setHidden:YES];
    dateButton.selected = NO;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MMM/yyyy"];
    birthdayString = [format stringFromDate:dateMonthPickerView.date];
    [dateButton setTitle:birthdayString forState:UIControlStateNormal];
    
    NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
    [format2 setDateFormat:@"yyyy/MM/dd"];
    [dateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [dateButton setBackgroundColor:[UIColor whiteColor]];
}

- (IBAction)OpenBirthdayPickerView:(id)sender {
    //
    NSArray *arrayTextfields = [[NSArray alloc] initWithObjects:nameTextField,lastNameTextField,emailTextField,passwordTextField,confirmPasswordTextField,addressTextField,secondAddressTextField,cityTextField,stateTextField,postalCodeTextField,nil];
    
    for(int x = 0; x < [arrayTextfields count]; x++){
        UITextField *v = (UITextField *)[arrayTextfields objectAtIndex:x];
        [v resignFirstResponder];
    }
    //
    dateMonthPickerView.backgroundColor = [UIColor whiteColor];
    [toolBar setHidden:NO];
    [dateMonthPickerView setHidden:NO];
    dateButton.selected = YES;
    [dateButton setBackgroundColor:[UIColor colorWithRed:0.0/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0]];
}

- (IBAction)ValueChangedBirthdayPickerView:(id)sender {
    
    [dateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [dateButton setBackgroundColor:[UIColor colorWithRed:0.0/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0]];
    dateButton.selected = YES;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MMM/yyyy"];
    birthdayString = [format stringFromDate:dateMonthPickerView.date];
    
    [dateButton setTitle:birthdayString forState:UIControlStateSelected];
    
}

//
-(IBAction)sendUserInformation:(id)sender
{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.detailsLabelText = @"Espere por favor...";
    HUD.labelText = @"Registrando cuenta";
    [HUD show:YES];
    
    _receivedData = [[NSMutableData alloc] init];
    NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
    validateCardSbx = [validateCardSbx stringByAppendingString:@"login"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:validateCardSbx]];
    [request setHTTPMethod:@"POST"];
    
    NSNumber *newsletterSuscribe = [NSNumber numberWithBool:newsletterSwitch.isOn];
    
    NSString *registerAccountPostString = [NSString stringWithFormat: @"idPreregistro=%@&udid=%@&nombre=%@&apellidos=%@&email=%@&contrasenia=%@&fechaNacimiento=%@&colonia=%@&calleyNumero=%@&ciudad=%@&estado=%@&pais=%@&codigoPostal=%@&suscripcion=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"idPreregistro"],[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"],nameTextField.text,lastNameTextField.text,emailTextField.text,passwordTextField.text,BirthdayLabel,secondAddressTextField.text,addressTextField.text,cityTextField.text,stateTextField.text,CountryTextField.text,postalCodeTextField.text,newsletterSuscribe];
    
    NSArray *userObjects = [NSArray arrayWithObjects:nameTextField.text,lastNameTextField.text,emailTextField.text,passwordTextField.text,BirthdayLabel.text,secondAddressTextField.text,addressTextField.text,cityTextField.text,stateTextField.text,CountryTextField.text,postalCodeTextField.text,nil];
    NSArray *userKeys = [NSArray arrayWithObjects:@"primerNombre",@"apellidos",@"email",@"contrasenia",@"fechaNacimiento",@"colonia",@"calleNumero",@"ciudad",@"estado",@"pais",@"codigoPostal",nil];
    
    userInformationDictionary = [[NSDictionary alloc] initWithObjects:userObjects forKeys:userKeys];
    
    NSData *registerAccountSbxData = [registerAccountPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postRegisterAccountSbxLength = [NSString stringWithFormat:@"%d", [registerAccountSbxData length]];
    [request setValue:postRegisterAccountSbxLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:registerAccountSbxData];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    [connection start];
}



#pragma mark - NSURL Connection
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [_receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView *dialogo = [[UIAlertView alloc] init];
    [dialogo setDelegate:self];
    [dialogo setTitle:@"¡Lo sentimos!"];
    [dialogo setMessage:[error localizedDescription]];
    [dialogo addButtonWithTitle:@"OK"];
    [dialogo show];
}

// success conectivity
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [HUD hide:YES];
    NSError *error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:_receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    int idRespuesta = [[results objectForKey:@"id"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        NSArray *userCardsArray = (NSArray *)[results objectForKey:@"tarjetas"];
        if ([userCardsArray count] > 0) {
            [[NSUserDefaults standardUserDefaults] setObject:(NSArray *)[results objectForKey:@"tarjetas"] forKey:@"userCardsArray"];
        }
        
        NSString *token = (NSString *)[results objectForKey:@"token"];
        NSString *idUsuario = (NSString *)[results objectForKey:@"idUsuario"];
        
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:idUsuario forKey:@"idUsuario"];
        [[NSUserDefaults standardUserDefaults] setObject:userInformationDictionary forKey:@"userInformation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
//
@end
