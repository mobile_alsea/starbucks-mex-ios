//
//  OSelectPaymentViewController.h
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"

@interface OSelectPaymentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
@property (strong, nonatomic) IBOutlet UITableView *PaymentTypeTableView;
@property (strong, nonatomic) IBOutlet UIImageView *AddCardImageView;
@property (strong, nonatomic) IBOutlet UILabel *AddCardLabel;
@property (strong, nonatomic) IBOutlet UIButton *AddCreditCardButton;
- (IBAction)AddCardButtonPressed:(id)sender;

@end
