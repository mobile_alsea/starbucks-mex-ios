//
//  OAddCreditCardViewController.h
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"
#import "MBProgressHUD.h"
#import "DYHorizontalStretchButton.h"

@interface OAddCreditCardViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate, NSURLConnectionDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
    NSDateFormatter *dateFormatter;
    
}
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UIView *viewCreditCardInformation;
@property (strong, nonatomic) IBOutlet UITableView *CreditCardBillingTableView;
@property (strong, nonatomic) IBOutlet UIPickerView *CreditCardExpireDatePickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *CreditCardTypePickerView;
@property (strong, nonatomic) IBOutlet UISwitch *CreditCardSwitch;
@property (strong, nonatomic) IBOutlet UIToolbar *CreditCardToolbar;

@property (strong, nonatomic) IBOutlet UITextField *creditCardNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *creditCardSecurityNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *creditCardNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *creditCardAliasTextField;
@property (strong, nonatomic) NSString *cardNumber;
@property (strong, nonatomic) NSString *amountOnReload;
@property (strong, nonatomic) IBOutlet UILabel *ExpirationDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *CardTypeLabel;
@property (strong, nonatomic) IBOutlet DYHorizontalStretchButton *ContinueButton;

@property (strong, nonatomic) NSMutableData *receivedData;

- (IBAction)ToolbarCreditCardDonePressed:(id)sender;
- (IBAction)ToolbarCreditCardCancelPressed:(id)sender;
- (IBAction)SelectCreditCardType:(id)sender;
- (IBAction)SelectCreditCardExpiryDate:(id)sender;
- (IBAction)SelectSaveCreditCard:(id)sender;
- (IBAction)SetSaveCreditCardButtonPressed:(id)sender;
@end
