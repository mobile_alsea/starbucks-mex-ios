//
//  OSelectPaymentViewController.m
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OSelectPaymentViewController.h"
#import "OAddCreditCardViewController.h"
#import "OCreditCardCell.h"

@interface OSelectPaymentViewController ()

@end

@implementation OSelectPaymentViewController
@synthesize AddCardImageView;
@synthesize AddCardLabel;
@synthesize AddCreditCardButton;
@synthesize PaymentTypeTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSelectPaymentViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Medio de Pago", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Recargar Tarjeta", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Dismiss:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        
        UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Editar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Edit:)];
        [self.navigationItem setRightBarButtonItem:btnEdit];
        PaymentTypeTableView.delegate = self;
        
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OAddCreditCardViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)Dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)Edit:(id)sender
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)AddCardButtonPressed:(id)sender {
    
    OAddCreditCardViewController *detailViewController = [[OAddCreditCardViewController alloc] initWithNibName:@"OAddCreditCardViewController" bundle:nil];
    ;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    switch (_tableView.tag) {
		case 0:
			return 1;
			break;
		default:
			break;
	}
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    
	@try{
        switch (_tableView.tag) {
            case 0:
            {
                
                static NSString *CellIdentifier = @"OCreditCardCell";
                
                OCreditCardCell *cell = (OCreditCardCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil)
                {
                    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                    //cell.lblTitulo.text = @"Tarjeta de Crédito: 9799";
                }
                
                //[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
                return cell;
            }
                break;
            default:
                break;
        }
        
        return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OPaymentTypeCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}





@end
