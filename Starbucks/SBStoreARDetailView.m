//
//  SBStoreARDetailView.m
//  Starbucks
//
//  Created by Ironbit Ninja on 19/07/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import "SBStoreARDetailView.h"

@interface SBStoreARDetailView ()
@property (strong, nonatomic) UIImage *openImage;
@property (strong, nonatomic) UIImage *closeImage;
@end

@implementation SBStoreARDetailView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
	self.openImage = [UIImage imageNamed:@"compass_tienda_abierto"];
	self.closeImage = [UIImage imageNamed:@"compass_tienda_cerrado"];
	self.isOpen = NO;
	
	self.storeNameLabel.text = @"";
	self.storeAddressLabel.text = @"";
	self.storeDistanceLabel.text = @"";
}

+ (id)getView
{
	NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"SBStoreARDetailView" owner:nil options:nil];
	return [nibViews objectAtIndex:0];
}

#pragma mark - Custom Setters

- (void)setIsOpen:(BOOL)isOpen
{
	_isOpen = isOpen;
	self.openImageView.image = _isOpen?self.openImage:self.closeImage;
}

@end
