//
//  SettingsPagaTodo.h
//  Starbucks
//
//  Created by Mobile on 11/20/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsPagaTodo : NSObject

+ (NSString *)getURLWSSmart;
+ (NSString *)getURLWSRewards;
+ (NSString *)getURLSwitch;
+ (NSString *)getCertWSSmart;
+ (NSString *)getCertWSRewards;
+ (NSString *)getCertKM;
+ (NSString *)getURLAppStore;
+ (NSString *)getDateSettings;

@end
