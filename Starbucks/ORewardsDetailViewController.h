//
//  ORewardsDetailViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface ORewardsDetailViewController : OBaseViewController
{
	IBOutlet UILabel * mDateLabel;
	IBOutlet UILabel * mNumberOfStarsLabel;
	IBOutlet UILabel * mActualLevelLabel;
	IBOutlet UILabel * mNextLevelLabel;
	IBOutlet UILabel * mTotalLabel;
	IBOutlet UILabel * mFaltanLabel;
	IBOutlet UILabel * mDateMemberLabel;
	IBOutlet UILabel * mDetailLabel;
	NSString * mDateMember;
	NSString * mNumberOfStars;
    NSString * mNumberOfStarsGold;
	NSString * mActualLevel;
    IBOutlet UIImageView * mImagenWelcome;
    IBOutlet UIImageView * mImagenGreen;
    IBOutlet UIImageView * mImagenGold;
    IBOutlet UILabel * mColorNivel;
    IBOutlet UIImageView * mColorStar;
    
    
    


}
@property(nonatomic, strong) NSString * numberOfStars;
@property(nonatomic, strong) NSString * numberOfStarsGold;
@property(nonatomic, strong) NSString * dateMember;
@property(nonatomic, strong) NSString * actualLevel;
@end
