//
//  DYDOMParser.h
//  DYXML
//
//  Created by Dylvian on 31/08/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYXML/xml/DYXMLParserDelegate.h>

/**
 @addtogroup	DYDOMParser
 @{
 
 @class		DYDOMParser
 
 @brief		Builds a DOM (document object model) of a XML representation. The resulting DOM is implemented as a dictionary of items
 */
/// @}
@interface DYDOMParser : NSObject <DYXMLParserDelegate>
{
	NSMutableDictionary *mDom;
}

/**
 @property	NSMutableDictionary *dom
 
 Holds the resulting DOM
 */
@property (nonatomic, readonly)	NSMutableDictionary *dom;

/**
 @param[in]	_keyArray	Array containing the keys we want to get from DOM
 
 @return	A dictionary holding an array of items for each key retrieved from DOM
 */
- (NSDictionary *) dictionaryWithKeys:(NSArray *)_keyArray;

@end
