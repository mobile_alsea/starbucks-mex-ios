//
//  DYSOAPManagerPreprocessDelegate.h
//  hofmann
//
//  Created by Juƒan Antonio Romero on 19/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DYSOAPManagerPreprocessDelegate <NSObject>

@optional

- (NSString*) responseKeyForErrorCode;

- (NSData*) preprocessData:(NSData*)_data forQuery:(NSString*)_query;

- (NSDictionary*) preprocessResponse:(NSDictionary*)_response forQuery:(NSString*)_query;

- (BOOL) shouldCacheResponse:(NSDictionary*)_response forQuery:(NSString*)_query;

@end
