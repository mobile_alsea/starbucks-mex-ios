//
//  DYSOAPManager.h
//  hofmann
//
//  Created by Juan Antonio Romero on 19/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DYCore/singleton/DYSingletonObject.h>
#import "DYSOAPManagerDelegate.h"
#import "DYSOAPManagerDataSource.h"
#import "DYSOAPManagerPreprocessDelegate.h"

@interface DYSOAPManager : DYSingletonObject
{
	NSCache *mCache;
	
	id<DYSOAPManagerDataSource> mDataSource;
	id<DYSOAPManagerPreprocessDelegate> mPreprocessDelegate;
}

@property (nonatomic, retain) id<DYSOAPManagerDataSource> dataSource;
@property (nonatomic, retain) id<DYSOAPManagerPreprocessDelegate> preprocessDelegate;

+ (void) callFunction:(NSString *) _fuction params:(NSDictionary *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate;
+ (void) callFunction:(NSString *) _function data:(NSData *) _data delegate:(id<DYSOAPManagerDelegate>)_delegate;

- (NSString *) requestBodyForFunction:(NSString *)_function params:(NSDictionary *)_params;

@end
