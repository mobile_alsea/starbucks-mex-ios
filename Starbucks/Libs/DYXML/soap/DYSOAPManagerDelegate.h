//
//  DYSOAPManagerDelegate.h
//  hofmann
//
//  Created by Juan Antonio Romero on 19/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DYSOAPManagerDelegate <NSObject>

@required

- (void) didReceiveResponseFromFunction:(NSString *)_function dataXML: (NSDictionary *) _response;

- (void) soapCallFailedForFunction:(NSString*)_function withErrorCode:(NSString*)_errorCode;

@end
