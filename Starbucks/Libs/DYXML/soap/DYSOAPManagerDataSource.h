//
//  DYSOAPManagerDataSource.h
//  hofmann
//
//  Created by Juan Antonio Romero on 19/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DYSOAPManagerDataSource <NSObject>

@required

- (NSString*) SOAPBaseURLForQuery:(NSString*)_query;

- (NSString*) SOAPQueryFormatForQuery:(NSString*)_query;

@end
