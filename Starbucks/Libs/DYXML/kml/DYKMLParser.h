//
//  DYKMLParser.h
//  DYXML
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import <DYXML/xml/DYXMLParserDelegate.h>

typedef enum RouteMode { RouteModeByCar = 0, RouteModeOnFoot } RouteMode;

/**
 @addtogroup	DYKMLParser
 @{
 
 @class		DYKMLParser
 
 @brief		Parses a KML file to obtain a route between two points
 */
/// @}
@interface DYKMLParser : NSObject <DYXMLParserDelegate>
{
	NSMutableArray	*mpPointArray;
	NSMutableArray	*mpKeyPointArray;
	NSMutableArray	*mpIndicationArray;
	
	NSString		*mpDescription;
	
	//NSMutableDictionary	*mpPlacemark;
}

/**
 @property	NSArray *pointArray
 
 Array of points parsed from the KML file. Points are stored as a CLLocationCoordinate2D encapsulated in a NSValue object.
 */
@property	(nonatomic, readonly)	NSArray		*pointArray;
/**
 @property	NSArray *keyPointArray
 
 Array of route points parsed from the KML file. These are only the key points, where you must \"turn left\", \"go to north\", etc. Points are stored as dictionarys containing location (similar to pointArray)
 */
@property	(nonatomic, readonly)	NSArray		*keyPointArray;
/**
 @property	NSArray *indicationArray
 
 Array of route indications parsed from the KML file. These are only the indications, such as \"turn left\", \"go to north\", etc.
 */
@property	(nonatomic, readonly)	NSArray		*indicationArray;
/**
 @property	NSString *description
 
 Description of the KML parsed
 */
@property	(nonatomic, readonly)	NSString	*description;

/**
 @param[in]	_from	Origin
 @param[in]	_to	Destination
 
 @return	A string with the URL of Google maps with the route from origin to destination
 */
+ (NSString *)	routeFrom:(CLLocationCoordinate2D)_from to:(CLLocationCoordinate2D)_to;
/**
 @param[in]	_from	Origin
 @param[in]	_to	Destination
 @param[in] _passPoint	Array of strings or NSValue encoded CLLocationCoordinate2D representing pass points
 
 @return	A string with the URL of Google maps with the route from origin to destination, stopping in pass points
 */
+ (NSString *)	routeFrom:(CLLocationCoordinate2D)_from to:(CLLocationCoordinate2D)_to via:(NSArray *)_passPoints;
/**
 @param[in]	_fromAddress	Origin
 @param[in]	_toAddress	Destination
 
 @return	A string with the URL of Google maps with the route from origin to destination
 */
+ (NSString *)	routeFromAddress:(NSString *)_fromAddress toAddress:(NSString *)_toAddress;
/**
 @param[in]	_fromAddress	Origin
 @param[in]	_toAddress	Destination
 @param[in] _passPoint	Array of strings or NSValue encoded CLLocationCoordinate2D representing pass points
 
 @return	A string with the URL of Google maps with the route from origin to destination, stopping in pass points
 */
+ (NSString *)	routeFromAddress:(NSString *)_fromAddress toAddress:(NSString *)_toAddress via:(NSArray *)_passPoints;
/**
 @param[in]	_from	Origin
 @param[in]	_to	Destination
 @param[in] _mode	Route mode (In car, by foot ...)
 
 @return	A string with the URL of Google maps with the route from origin to destination
 */
+ (NSString *)	routeFrom:(CLLocationCoordinate2D)_from to:(CLLocationCoordinate2D)_to mode:(RouteMode)_mode;
/**
 @param[in]	_from	Origin
 @param[in]	_to	Destination
 @param[in] _mode	Route mode (In car, by foot ...)
 @param[in] _passPoint	Array of strings or NSValue encoded CLLocationCoordinate2D representing pass points
 
 @return	A string with the URL of Google maps with the route from origin to destination, stopping in pass points
 */
+ (NSString *)	routeFrom:(CLLocationCoordinate2D)_from to:(CLLocationCoordinate2D)_to mode:(RouteMode)_mode via:(NSArray *)_passPoints;
/**
 @param[in]	_fromAddress	Origin
 @param[in]	_toAddress	Destination
 @param[in] _mode	Route mode (In car, by foot ...)
 
 @return	A string with the URL of Google maps with the route from origin to destination
 */
+ (NSString *)	routeFromAddress:(NSString *)_fromAddress toAddress:(NSString *)_toAddress mode:(RouteMode)_mode;
/**
 @param[in]	_fromAddress	Origin
 @param[in]	_toAddress	Destination
 @param[in] _mode	Route mode (In car, by foot ...)
 @param[in] _passPoint	Array of strings or NSValue encoded CLLocationCoordinate2D representing pass points
 
 @return	A string with the URL of Google maps with the route from origin to destination, stopping in pass points
 */
+ (NSString *)	routeFromAddress:(NSString *)_fromAddress toAddress:(NSString *)_toAddress mode:(RouteMode)_mode via:(NSArray *)_passPoints;
/**
 @param[in]	_coord	Coordinate
 
 @return	A string representing the address for _coord
 */
+ (NSString *)	addressFromCoordinate:(CLLocationCoordinate2D)_coord;

/**
 @brief	Remove all points, leaving the parser as initialized
 */
- (void)	removeAllPoints;

@end
