#pragma once

/*
 *  DYXMLParserDelegate.h
 *
 *  Created by Dylvian
 */

/**
 @addtogroup	DYXMLParser
 @{
 @protocol		DYXMLParserDelegate
 
 @brief		Delegate for XML events
 */
/// @}

@class DYXMLParser;

@protocol DYXMLParserDelegate <NSObject>
@required

/**
 @param[in]	_parser	Parser in which the event happened
 @param[in]	_elementName	Name of the element that started
 @param[in]	_attributeDictionary	Dictionary of attributes of the element
 
 @brief	Called by the parser when a tag is opened
 */
- (void) xmlParser:(DYXMLParser *)_parser didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDictionary;
/**
 @param[in]	_parser	Parser in which the event happened
 @param[in]	_elementName	Name of the element that was closed
 @param[in]	_text	Text contained in the tag
 
 @brief	Called by the parser when a tag is closed
 */
- (void) xmlParser:(DYXMLParser *)_parser didEndElement:(NSString *)_elementName text:(NSString *)_text;

/**
 @param[in]	_parser	Parser in which the event happened
 @param[in]	_error	Error detected
 
 @brief	Called by the parser when an error is detected
 */
- (void) xmlParser:(DYXMLParser *)_parser didFail:(NSError *)_error;

@optional

/**
 @param[in]	_parser	Parser starting to parse the document
 
 @brief	Called by the parser when parsing of the document is started
 */
- (void) xmlParserDidStartDocument:(DYXMLParser *)_parser;
/**
 @param[in]	_parser	Parser that ended to parse the document
 
 @brief	Called by the parser when parsing of the document is finished
 */
- (void) xmlParserDidEndDocument:(DYXMLParser *)_parser;

@end