#pragma once

/*
 *  DYXMLParser.h
 *
 *  Created by Dylvian
 */

#import <DYCore/containers/DYMutableStack.h>
#import <DYXML/xml/DYXMLParserDelegate.h>

/**
 @addtogroup	DYXMLParser
 @{
 @class		DYXMLParser
 
 @brief		Event-based XML Parser
 
 Implements a XML parser driven by events, instead of creating a tree-like structure in memory.
 */
///@}
@interface DYXMLParser: NSXMLParser <NSXMLParserDelegate>
{
	DYMutableStack	*mStack;
			
	BOOL			mIsAborted;
	BOOL			mIsTreatingAttributesAsValues;
	
	id<DYXMLParserDelegate>	mXMLDelegate;
}

/**
 @property	BOOL	treatAttributesAsValues
 
 Default NO. The default behaviour is to notify of attributes in every tag start callback. If you set treatAttributesAsValues to YES, attributes are notified as a new tag start callback instead of regular attributes
 */
@property (nonatomic)								BOOL	treatAttributesAsValues;

/**
 @property	BOOL	aborted
 
 You can abort XML parsing in every moment. This property holds if parsing has been aborted
 */
@property (readonly, nonatomic, getter=isAborted)	BOOL	aborted;

/**
 @property	id<DYXMLParserDelegate>	xmlDelegate
 
 Sets the delegate for receiving notifications of parse events
 */
@property (nonatomic, assign)	id<DYXMLParserDelegate>	xmlDelegate;

/**
 @param[in]	_data		data to parse
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the XML file.
 */
+ (BOOL)	parseXMLData:(NSData *)_data delegate:(id<DYXMLParserDelegate>)_delegate;

/**
 @param[in]	_data						data to parse
 @param[in]	_delegate					Delegate which will receive notifications of XML events
 @param[in]	_treatAttributesAsElements	Indicates if the parser should treat attributes as elements
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the XML file.
 */
+ (BOOL)	parseXMLData:(NSData *)_data delegate:(id<DYXMLParserDelegate>)_delegate treatAttributesAsElements:(BOOL)_treatAttributesAsElements;

/**
 @param[in]	_filename	String containing the name of the file to parse
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the XML file.
 */
+ (BOOL)	parseXMLFile:(NSString *)_filename delegate:(id<DYXMLParserDelegate>)_delegate;

/**
 @param[in]	_filename	String containing the name of the file to parse
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 @param[in]	_treatAttributesAsElements	Indicates if the parser should treat attributes as elements
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the XML file.
 */
+ (BOOL)	parseXMLFile:(NSString *)_filename delegate:(id<DYXMLParserDelegate>)_delegate treatAttributesAsElements:(BOOL)_treatAttributesAsElements;

/**
 @param[in]	_remoteUrl	URL of the remote file to parse
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the remote XML file.
 */
+ (BOOL)	parseXMLURL:(NSURL *)_remoteUrl delegate:(id<DYXMLParserDelegate>)_delegate;

/**
 @param[in]	_remoteUrl	URL of the remote file to parse
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 @param[in]	_treatAttributesAsElements	Indicates if the parser should treat attributes as elements
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the remote XML file.
 */
+ (BOOL)	parseXMLURL:(NSURL *)_remoteUrl delegate:(id<DYXMLParserDelegate>)_delegate treatAttributesAsElements:(BOOL)_treatAttributesAsElements;

/**
 @param[in]	_string	String to parse
 @param[in] _encoding	String encoding
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the XML string.
 */
+ (BOOL)	parseXMLString:(NSString *)_string encoding:(NSStringEncoding)_encoding delegate:(id<DYXMLParserDelegate>)_delegate;

/**
 @param[in]	_string	String to parse
 @param[in] _encoding	String encoding
 @param[in]	_delegate	Delegate which will receive notifications of XML events
 @param[in]	_treatAttributesAsElements	Indicates if the parser should treat attributes as elements
 
 @return	YES if parsing was OK, NO if there were errors
 
 Parses the XML string.
 */
+ (BOOL)	parseXMLString:(NSString *)_string encoding:(NSStringEncoding)_encoding delegate:(id<DYXMLParserDelegate>)_delegate treatAttributesAsElements:(BOOL)_treatAttributesAsElements;

/**
 Aborts the parsing of the XML string
 */
- (void)	Abort;

/**
 @param[in]	_indexInStack	Level of the parsing element we want to obtain
 
 @return	Element name of the selected element
 
 This method is provided for you to know the tag hierarchy. _indexInStack 0 refers to the tag being processed, _indexInStack 1 refers to the parent tag, and so on.
 */
- (NSString *)	processingElement:(NSUInteger) _indexInStack;

/**
 @return	The parent tag of the tag being processed
 
 This is a convenience method which invokes GetProcessingElement:1
 */
- (NSString *)	parentElement;

/**
 @param[in]	_indexInStack	Level of the parsing element we want to obtain
 
 @return	Selected element
 
 This method is provided for you to know the tag hierarchy and you should not need to access it directly. _indexInStack 0 refers to the tag being processed, _indexInStack 1 refers to the parent tag, and so on.
 */
- (NSMutableDictionary *) processingDictionary:(NSUInteger)_indexInStack;


@end