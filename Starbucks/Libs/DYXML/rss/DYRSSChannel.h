//
//  DYRSSChannel.h
//
//  Created by Dylvian on 28/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYXML/rss/DYRSSCloud.h>
#import <DYXML/rss/DYRSSImage.h>
#import <DYXML/rss/DYRSSTextInput.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
	@class DYRSSChannel
 
	@brief Channel or feed, for RSS 2.0 feeds
 
	@note iTunes extensions and Yahoo! media extensions are not read by DYRSSParser, only by DYExtendedRSSParser
 */
///@}
@interface DYRSSChannel : NSObject
{
	// Mandatory
	NSMutableString		*mpTitle;
	NSMutableString		*mpLink;
	NSMutableString		*mpDescription;
		
	// Optional
	NSMutableString		*mpLanguage;
	NSMutableString		*mpCopyright;
	NSMutableString		*mpManagingEditor;
	NSMutableString		*mpWebMaster;
	NSDate				*mpPubDate;
	NSDate				*mpLastBuildDate;
	NSMutableArray		*mpCategoryVector;
	NSMutableString		*mpGenerator;
	NSMutableString		*mpDocLink;

	DYRSSCloud			*mpCloudInfo;

	NSInteger			mTtl;
	DYRSSImage			*mpImageInfo;
	NSMutableString		*mpRating;
	DYRSSTextInput		*mpTextInputInfo;
	NSInteger			mSkipHours;
	NSInteger			mSkipDays;
	
	// iTunes extensions
	NSMutableString		*mpItunesAuthor;
	BOOL				mItunesBlock;
	NSMutableArray		*mpItunesCategoryVector;
	BOOL				mItunesExplicit;
	NSMutableArray		*mpItunesKeywordVector;
	NSMutableString		*mpItunesOwnerEmail;
	NSMutableString		*mpItunesOwnerName;
	NSMutableString		*mpItunesSubtitle;
	NSMutableString		*mpItunesSummary;
	NSMutableString		*mpItunesNewFeedURL;
	NSDate				*mpItunesPubDate;
	NSMutableString		*mpItunesImage;
}

/**
 @property	NSMutableString	*title
 
 Feed title
 */
@property	(nonatomic, readonly)	NSMutableString *title;
/**
 @property	NSMutableString	*link
 
 Feed URL, as string
 */
@property	(nonatomic, readonly)	NSMutableString *link;
/**
 @property	NSMutableString	*description
 
 Feed description
 */
@property	(nonatomic, readonly)	NSMutableString *description;

/**
 @property	NSMutableString	*language
 
 Feed language
 */
@property	(nonatomic, readonly)	NSMutableString *language;
/**
 @property	NSMutableString	*copyright
 
 Feed copyright message
 */
@property	(nonatomic, readonly)	NSMutableString *copyright;
/**
 @property	NSMutableString	*managingEditor
 
 Email address for person responsible for editorial content of the feed
 */
@property	(nonatomic, readonly)	NSMutableString *managingEditor;
/**
 @property	NSMutableString	*webMaster
 
 Email address for person responsible for technical issues relating to channel
 */
@property	(nonatomic, readonly)	NSMutableString *webMaster;
/**
 @property	NSDate	*pubDate
 
 Feed publication date
 */
@property	(nonatomic, copy)	NSDate *pubDate;
/**
 @property	NSDate *lastBuildDate
 
 Last date that the content of the channel was changed
 */
@property	(nonatomic, copy)	NSDate *lastBuildDate;
/**
 @property	NSMutableArray *categoryVector
 
 Array of categories that the channel belongs  to
 */
@property	(nonatomic, readonly)	NSMutableArray *categoryVector;
/**
 @property	NSMutableString	*generator
 
 Program used to generate the channel
 */
@property	(nonatomic, readonly)	NSMutableString *generator;
/**
 @property	NSMutableString	*docLink
 
 URL of documentation for the format used
 */
@property	(nonatomic, readonly)	NSMutableString *docLink;
/**
 @property	DYRSSCloud	*cloudInfo
 
 According to http://cyber.law.harvard.edu/rss/rss.html :
 Allows processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds.
 More info http://cyber.law.harvard.edu/rss/rss.html#ltcloudgtSubelementOfLtchannelgt.
 */
@property	(nonatomic, readonly)	DYRSSCloud *cloudInfo;
/**
 @property NSInteger	ttl
 
 Number of minutes that a channel can be cached before refreshing from the source
 */
@property	(nonatomic)	NSInteger ttl;
/**
 @property DYRSSImage *imageInfo
 
 Image info for this feed
 */
@property	(nonatomic, readonly)	DYRSSImage *imageInfo;
/**
 @property NSMutableString *rating
 
 Feed PICS rating (http://www.w3.org/PICS/)
 */
@property	(nonatomic, readonly)	NSMutableString *rating;
/**
 @property DYRSSTextInput *textInputInfo
 
 Text input for feed. Most aggregators ignore it.
 */
@property	(nonatomic, readonly)	DYRSSTextInput *textInputInfo;
/**
 @property	NSInteger	skipHours
 
 How many hours an aggregator can skip
 */
@property	(nonatomic)	NSInteger skipHours;
/**
 @property	NSInteger	skipDays
 
 How many days an aggregator can skip
 */
@property	(nonatomic)	NSInteger skipDays;

// iTunes extensions
/**
 @property NSMutableString *iTunesAuthor
 
 Author of the feed or podcast
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesAuthor;
/**
 @property BOOL iTunesBlock
 
 If YES, indicates that the feed or podcast should not show up in the ITMS
 */
@property	(nonatomic)				BOOL			iTunesBlock;
/**
 @property	NSMutableArray *iTunesCategoryVector
 
 Array of categories that the feed or podcast will show up when browsing by category
 */
@property	(nonatomic, readonly)	NSMutableArray	*iTunesCategoryVector;
/**
 @property BOOL iTunesExplicit
 
 Whether feed or podcast may contain explicit content
 */
@property	(nonatomic)				BOOL			iTunesExplicit;
/**
 @property NSMutableArray *iTunesKeywordVector
 
 Array of strings used when searching for pocasts in the ITMS
 */
@property	(nonatomic, readonly)	NSMutableArray	*iTunesKeywordVector;
/**
 @property NSMutableString *iTunesOwnerEmail
 
 Email of the author of the feed or podcast
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesOwnerEmail;
/**
 @property NSMutableString *iTunesOwnerName
 
 Name of the author of the feed or podcast
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesOwnerName;
/**
 @property NSMutableString *iTunesSubtitle
 
 Subtitle of the feed or podcast
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesSubtitle;
/**
 @property NSMutableString *iTunesSummary
 
 Summary of the feed or podcast
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesSummary;
/**
 @property NSMutableString *iTunesNewFeedURL
 
 New URL for the feed or podcast, as string
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesNewFeedURL;
/**
 @property NSDate *iTunesPubDate
 
 Date of publication of the feed or podcast
 */
@property	(nonatomic, copy)		NSDate			*iTunesPubDate;
/**
 @property NSMutableString *iTunesImage
 
 Image of the feed or podcast
 */
@property	(nonatomic, readonly)	NSMutableString	*iTunesImage;

/**
 Removes all information from a channel
 */
- (void)	Clear;

@end
