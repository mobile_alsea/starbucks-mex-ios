//
//  DYRSSItemGuid.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSItemGuid
 
 @brief Unique identifier for an item in a feed. Usually this is the item's URL
 */
///@}
@interface DYRSSItemGuid : NSObject 
{
	BOOL		mIsPermalink;
	NSMutableString	*mpGuid;
}

/**
 @property	BOOL	isPermalink
 
 YES if guid is a permanent link to the item, NO otherwise
 */
@property (nonatomic)	BOOL	isPermalink;
/**
 @property	NSMutableString	*guid
 
 Unique identifier of the item
 */
@property (nonatomic, readonly)	NSMutableString *guid;

/**
 Removes all information from the instance
 */
- (void) Clear;

@end
