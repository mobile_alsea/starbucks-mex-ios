//
//  DYRSSMediaGroup.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYXML/rss/DYRSSMediaContent.h>
#import <DYXML/rss/DYRSSMediaDescription.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSMediaGroup
 
 @brief Groups media objects and descriptions
 */
///@}
@interface DYRSSMediaGroup : NSObject 
{
	DYRSSMediaContent		*mpMediaContent;
	NSMutableString			*mpMediaThumbnailUrl;
	DYRSSMediaDescription	*mpMediaDescription;
	
	UIImage		*mpThumbnail;
	
	NSMutableArray	*mpThumbnailUrlArray;
	NSMutableArray	*mpThumbnailArray;
}

/**
 @property	DYRSSMediaContent *mediaContent
 
 Media object description
 */
@property (nonatomic, readonly)	DYRSSMediaContent *mediaContent;
/**
 @property	NSMutableString *mediaThumbnailUrl
 
 URL of an image to use as thumbnail, as string
 */
@property (nonatomic, readonly)	NSMutableString *mediaThumbnailUrl;
/**
 @property	DYRSSMediaDescription *mediaDescription
 
 Description of the media object
 */
@property (nonatomic, readonly)	DYRSSMediaDescription *mediaDescription;
/**
 @property	UIImage	*thumbnail
 
 Thumbnail image, if loaded
 */
@property (nonatomic, retain)	UIImage		*thumbnail;

/**
 Removes all information from the instance
 */
- (void) Clear;
/**
 @return	Pointer to the thumbnail image
 */
- (UIImage **) GetThumbnailPtr;

/**
 @param[in]	_pUrl	URL of a thumbnail image, as string
 
 Apart from mediaThumbnailUrl, several thumbnail URLs may be specified. This is not a part of the standard <media:group> tag.
 */
- (void) AddThumbnailURL:(NSString *)_pUrl;
/**
 @param[in]	_index	Index of the thumbnail to get
 
 @return	URL of the thumbnail at _index, as string
 */
- (NSString *) GetThumbnailUrlForIndex:(int)_index;
/**
 @param[in]	_thumbnail	Thumbnail image to add
 @param[in]	_index	Index of the thumbnail to store
 
 Stores a thumbnail image at _index
 */
- (void) SetThumbnail:(UIImage *)_thumbnail ForIndex:(int)_index;
/**
 @param[in]	_index	Index of the thumbnail to get
 
 @return	The thumbnail image at _index
 */
- (UIImage *) GetThumbnailForIndex:(int)_index;

/**
 @return	How many thumbnails are stored in a instance
 */
- (int) GetThumbnailCount;

/**
 @return A serialized representation of thumbnail array
 */
- (NSString *) GetSerializedThumbnail;
/**
 @param[in]	_string A serialized representation of the thumbnail array
 */
- (void) SetSerializedString:(NSString *)_string;

@end
