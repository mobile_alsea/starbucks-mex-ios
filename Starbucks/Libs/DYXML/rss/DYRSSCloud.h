//
//  DYRSSCloud.h
//
//  Created by Dylvian on 28/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSCloud
 
 @brief Allows processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds.More info http://cyber.law.harvard.edu/rss/rss.html#ltcloudgtSubelementOfLtchannelgt.
 
 It specifies a web service that supports the rssCloud interface which can be implemented in HTTP-POST, XML-RPC or SOAP 1.1.
 
 Its purpose is to allow processes to register with a cloud to be notified of updates to the channel, implementing a lightweight publish-subscribe protocol for RSS feeds.
 
 @code
 <cloud domain="rpc.sys.com" port="80" path="/RPC2" registerProcedure="myCloud.rssPleaseNotify" protocol="xml-rpc" />
 @endcode
 
 In this example, to request notification on the channel it appears in, you would send an XML-RPC message to rpc.sys.com on port 80, with a path of /RPC2. The procedure to call is myCloud.rssPleaseNotify.
 
 A full explanation of this element and the rssCloud interface is http://cyber.law.harvard.edu/rss/soapMeetsRss.html#rsscloudInterface.
 */
///@}
@interface DYRSSCloud : NSObject
{
	NSString	*mpDomain;
	NSString	*mpPort;
	NSString	*mpPath;
	NSString	*mpRegisterProcedure;
	NSString	*mpProtocol;
}

/**
 @property NSString *domain
 
 Domain of the remote procedure server, as string
 */
@property	(nonatomic, copy)	NSString	*domain;
/**
 @property NSString *port
 
 Port of the remote procedure server
 */
@property	(nonatomic, copy)	NSString	*port;
/**
 @property NSString *path
 
 Path of the remote procedure
 */
@property	(nonatomic, copy)	NSString	*path;
/**
 @property NSString *registerProcedure
 
 Procedure to call
 */
@property	(nonatomic, copy)	NSString	*registerProcedure;
/**
 @property NSString *registerProcedure
 
 Protocol of the procedure to call
 */
@property	(nonatomic, copy)	NSString	*protocol;

/**
 Removes all information from the instance
 */
- (void)	Clear;

@end
