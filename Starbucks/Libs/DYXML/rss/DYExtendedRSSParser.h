//
//  DYExtendedRSSParser.h
//
//  Created by Dylvian on 18/03/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DYXML/rss/DYRSSParser.h>

#import	<DYXML/rss/DYRSSItunesCategory.h>

/**
 @addtogroup DYRSSParser
 @{
	@class	DYExtendedRSSParser
 
	@brief	Enhances parsing of RSS files adding support for Yahoo media: extensions and iTunes extensions
 */
///@}
@interface DYExtendedRSSParser : DYRSSParser
{
	DYRSSItunesCategory *mpTempItunesCategory;
}

@end
