//
//  DYRSSItunesCategory.h
//  DYXML
//
//  Created by Dylvian on 13/11/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSItunesCategory
 
 @brief Category info. This is part of the iTunes extensions and it's only read by DYExtendedRSSParser
 */
///@}
@interface DYRSSItunesCategory : NSObject 
{
	NSMutableString *mpName;
	NSMutableArray *mpSubcategoryArray;
}

/**
 @property	NSMutableString *name
 
 Name of the category
 */
@property (nonatomic, readonly)	NSMutableString *name;
/**
 @property	NSMutableArray *subcategoryArray
 
 Array of subcategories for this category
 */
@property (nonatomic, readonly) NSMutableArray *subcategoryArray;

/**
 Removes all information from the instance
 */
- (void) Clear;

@end
