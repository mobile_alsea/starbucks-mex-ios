//
//  DYRSSItem.h
//
//  Created by Dylvian on 28/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYXML/rss/DYRSSItemEnclosure.h>
#import <DYXML/rss/DYRSSItemGuid.h>
#import <DYXML/rss/DYRSSItemSource.h>
#import <DYXML/rss/DYRSSMediaGroup.h>
#import <DYXML/rss/DYRSSItunesCategory.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSItem
 
 @brief An item in a channel or feed, for RSS 2.0 feeds
 
 @note iTunes extensions and Yahoo! media extensions are not read by DYRSSParser, only by DYExtendedRSSParser
 */
///@}
@interface DYRSSItem : NSObject 
{
	NSMutableString		*mpTitle;
	NSMutableString		*mpLink;
	NSMutableString		*mpDescription;
		
	NSMutableString		*mpAuthor;
	NSMutableArray		*mpCategoryVector;
	NSMutableString		*mpCommentsLink;
	DYRSSItemEnclosure	*mpEnclosure;
	DYRSSItemGuid		*mpGuid;
	NSDate				*mpPubDate;
	DYRSSItemSource		*mpSource;
		
	DYRSSMediaGroup		*mpMediaGroup;		// yahoo extensions
	
	id					mpExtendedData;
	
	NSMutableArray		*mpEnclosureVector;	// maybe there's more than one enclosure per item.
	// iTunes extensions
	NSMutableString		*mpItunesAuthor;
	BOOL				mItunesBlock;
	NSInteger			mItunesDuration;
	BOOL				mItunesExplicit;
	NSMutableArray		*mpItunesKeywordsVector;
	NSMutableString		*mpItunesSubtitle;
	NSMutableString		*mpItunesSummary;
	NSDate				*mpItunesPubDate;
	NSMutableString		*mpItunesImage;
	
	DYRSSItunesCategory *mpItunesCategory;
}

/**
 @property NSMutableString *title
 
 Title of the item
 */
@property (nonatomic, readonly)	NSMutableString *title;
/**
 @property NSMutableString *link
 
 Link of the item, as string
 */
@property (nonatomic, readonly)	NSMutableString *link;
/**
 @property NSMutableString *description
 
 Description of the item
 */
@property (nonatomic, readonly)	NSMutableString *description;

/**
 @property NSMutableString *author
 
 Author of the item
 */
@property (nonatomic, readonly)	NSMutableString *author;
/**
 @property	NSMutableArray *categoryVector
 
 Array of categories that the item belongs  to
 */
@property (nonatomic, readonly)	NSMutableArray *categoryVector;
/**
 @property NSMutableString *commentsLink
 
 URL of a page for comments relating to the item, as string
 */
@property (nonatomic, readonly)	NSMutableString *commentsLink;
/**
 @property DNRSSItemEnclosure *enclosure
 
 Information of a media object. It may be a movie clip, an audio sample, an image, etc.
 */
@property (nonatomic, readonly)	DYRSSItemEnclosure *enclosure;
/**
 @property DNRSSItemGuid *guid
 
 Unique identifier for the item
 */
@property (nonatomic, readonly)	DYRSSItemGuid *guid;
/**
 @property NSDate *pubDate
 
 Date of publication of the item
 */
@property (nonatomic, copy)	NSDate *pubDate;
/**
 @property DNRSSItemSource *source
 
 Source where this item came from
 */
@property (nonatomic, readonly)	DYRSSItemSource *source;

/**
 @property DYRSSMediaGroup *mediaGroup
 
 Information about a group of media object. This is a Yahoo! extension
 
 @bug	According to Yahoo! Media Extensions documentation, this should be an array instead of a single instance
 */
@property (nonatomic, readonly)	DYRSSMediaGroup *mediaGroup;

/**
 @property id extendedData
 
 Used to store an arbitrary object in the item
 */
@property (nonatomic, retain)	id extendedData;

/**
 @property NSMutableArray *enclosureVector
 
 Array of enclosure items. RSS 2.0 standards indicate only one enclosure per item. This relaxes the restriction allowing several enclosures to be specified per item, but it's only read with DNExtendedRSSParser.
 */
@property (nonatomic, readonly)	NSMutableArray *enclosureVector;
// iTunes extensions
/**
 @property NSMutableString *iTunesAuthor
 
 Author of the item or episode
 */
@property (nonatomic, readonly)	NSMutableString	*iTunesAuthor;
/**
 @property BOOL iTunesBlock
 
 If YES, indicates that the item or episod should not show up in the ITMS
 */
@property (nonatomic)			BOOL			iTunesBlock;
/**
 @property NSInteger iTunesDuration
 
 Duration of the episode, in seconds
 */
@property (nonatomic)			NSInteger		iTunesDuration;
/**
 @property BOOL iTunesExplicit
 
 Whether item or episode may contain explicit content
 */
@property (nonatomic)			BOOL			iTunesExplicit;
/**
 @property NSMutableArray *iTunesKeywordVector
 
 Array of strings used when searching for pocasts in the ITMS
 */
@property (nonatomic, readonly) NSMutableArray	*iTunesKeywordVector;
/**
 @property NSMutableString *iTunesSubtitle
 
 Subtitle of the item or episode
 */
@property (nonatomic, readonly) NSMutableString *iTunesSubtitle;
/**
 @property NSMutableString *iTunesSummary
 
 Summary of the item or episode
 */
@property (nonatomic, readonly) NSMutableString *iTunesSummary;
/**
 @property NSDate *iTunesPubDate
 
 Date of publication of the item or episode
 */
@property (nonatomic, copy)	NSDate			*iTunesPubDate;
/**
 @property NSMutableString *iTunesImage
 
 URL of the iTunes image
 */
@property (nonatomic, readonly) NSMutableString *iTunesImage;

/**
 @property DYRSSItunesCategory *iTunesCategory;
 
 URL of the iTunes category
 */
@property (nonatomic, retain) DYRSSItunesCategory *iTunesCategory;

/**
 Removes all information from an item
 */
- (void) Clear;

/**
 @return An HTML string representing the item, with a default format. This string may contain HTML links.
 
 This is a convenience method to save time formatting output.
 
 @sa - (NSString *) htmlStringWithFormat:(NSString *)_pFormatString;
 */
- (NSString *) HTMLString;
/**
 @param[in]	_pFormatString	String with format for the item.
 
 @return An HTML string representing the item. This string may contain HTML links.
 
 This is a convenience method to save time formatting output.
 
 @sa - (NSString *) HTMLStringWithFormat:(NSString *)_pFormatString;
 */
- (NSString *) HTMLStringWithFormat:(NSString *)_pFormatString;
/**
 @param[in]	_pFormatString	String with format for the item.
 @param[in] _processor	Object to invoke when a %X tag is found
 @param[in] _selector	Selector to invoke on _processor when a %X tag is found
 
 @return An HTML string representing the item. This string may contain HTML links.
 
 This is a convenience method to save time formatting output. Supported format:
 
 @code
 %T: title
 %L: link
 %D: description
 
 %A: author
 %C: commentsLink
 
 %EU: enclosure.url
 %EL: enclosure.length
 %ET: enclosure.type
 
 %G: guid
 %P(DateFormat): pubDate
 
 %SU: source.url
 %SS: source.source
 
 %MGMCU: mediaGroup.mediaContent.url
 %MGMCS: mediaGroup.mediaContent.filesize
 %MGMCT: mediaGroup.mediaContent.type
 %MGT: mediaGroup.mediaThumbnailUrl
 %MGMDT: mediaGroup.mediaDescription.type
 %MGMDD: mediaGroup.mediaDescription.description
 %MGMDG: mediaGroup.mediaDescription.genre
 %MGMDS: mediaGroup.mediaDescription.subTitle
 %MGMDE: mediaGroup.mediaDescription.detail
 
 %X: extendedData
 @endcode
 */
- (NSString *) HTMLStringWithFormat:(NSString *)_pFormatString extendedProcessor:(id)_processor processWithSelector:(SEL)_selector;

@end