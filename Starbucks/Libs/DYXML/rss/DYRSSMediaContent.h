//
//  DYRSSMediaContent.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSMediaContent
 
 @brief Identifies a media object. This is one of the Yahoo! media extensions, and it's only read by DYExtendedRSSParser
 */
///@}
@interface DYRSSMediaContent : NSObject 
{
	NSMutableString *mpUrl;
	NSMutableString *mpFilesize;
	NSMutableString *mpType;
}

/**
 @property	NSMutableString *url
 
 URL of the object, as string
 */
@property (nonatomic, readonly)	NSMutableString *url;

/**
 @property	NSMutableString *filesize
 
 Size of the media object in bytes, as string
 */
@property (nonatomic, readonly)	NSMutableString *filesize;

/**
 @property	NSMutableString *type
 
 MIME type of the object
 */
@property (nonatomic, readonly)	NSMutableString *type;

/**
 Removes all information from the instance
 */
- (void) Clear;

@end
