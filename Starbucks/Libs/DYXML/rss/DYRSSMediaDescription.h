//
//  DYRSSMediaDescription.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSMediaDescription
 
 @brief Additional description for an item. This is one of the Yahoo! media extensions, and it's only read by DYExtendedRSSParser
 */
///@}
@interface DYRSSMediaDescription : NSObject 
{
	NSMutableString *mpType;
	NSMutableString *mpDescription;
	
	// non-standard fields introduced by Prisacom!
	NSMutableString *mpGenre;
	NSMutableString *mpSubTitle;
	NSMutableString *mpDetail;
}

/**
 @property	NSMutableString	*type
 
 Type of description. Possible values are 'plain' or 'html'
 */
@property (nonatomic, readonly)	NSMutableString *type;

/**
 @property	NSMutableString	*description
 
 Description by itself
 */
@property (nonatomic, readonly)	NSMutableString *description;

/**
 @property	NSMutableString *subTitle
 
 Additional subtitle for item. This is not a part of the standard Media Yahoo! Extensions, it was introduced by ourselves. The <media:description> tag may contain a <subtitle> tag, only parsed by DNExtendedRSSParser.
 */
@property (nonatomic, readonly)	NSMutableString *subTitle;
/**
 @property	NSMutableString *detail
 
 Additional detail for item. This is not a part of the standard Media Yahoo! Extensions, it was introduced by ourselves. The <media:description> tag may contain a <detail> tag, only parsed by DNExtendedRSSParser.
 */
@property (nonatomic, readonly)	NSMutableString *detail;
/**
 @property	NSMutableString *genre
 
 Genre for item. This is not a part of the standard Media Yahoo! Extensions, it was introduced by ourselves. The <media:description> tag may contain a <genre> tag, only parsed by DNExtendedRSSParser.
 */
@property (nonatomic, readonly)	NSMutableString *genre;

/**
 Removes all information from the instance
 */
- (void) Clear;

@end
