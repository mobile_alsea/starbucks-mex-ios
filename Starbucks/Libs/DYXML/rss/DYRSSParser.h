//
//  DNRSSParser.h
//  LotteryParser
//
//  Created by M2GRP Murcia on 28/01/09.
//  Copyright 2009 M2GRP. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYXML/xml/DYXMLParserDelegate.h>

#import <DYXML/rss/DYRSSChannel.h>
#import <DYXML/rss/DYRSSItem.h>

/**
 @addtogroup DYRSSParser
 @{
 @class	DYRSSParser
 
 @brief	Parser for RSS 2.0 feeds
 */
///@}
@interface DYRSSParser : NSObject <DYXMLParserDelegate>
{	
	NSMutableString	*mpVersion;
	DYRSSChannel	*mpChannel;
	NSMutableArray	*mpItemVector;
	
	// ---- Allow non-fully standards compliant files (IE mode)
	BOOL			mRelaxChecks;
	BOOL			mVerboseMode;
	
	DYRSSItem		*mpTempItem;
	
	BOOL			mIsRssRead;
	BOOL			mIsChannelRead;
	
	BOOL			mIsChannelTitleRead;
	BOOL			mIsChannelLinkRead;
	BOOL			mIsChannelDescriptionRead;
	
	BOOL			mIsImageUrlRead;
	BOOL			mIsImageTitleRead;
	BOOL			mIsImageLinkRead;
	
	BOOL			mIsTextInputTitleRead;
	BOOL			mIsTextInputDescriptionRead;
	BOOL			mIsTextInputNameRead;
	BOOL			mIsTextInputLinkRead;
}

/**
 @property	NSMutableString	*version
 
 Channel version
 */
@property (nonatomic, readonly) NSMutableString *version;
/**
 @property	DYRSSChannel *channel
 
 Channel read from RSS
 */
@property (nonatomic, readonly) DYRSSChannel *channel;

/**
 @param[in]	_relaxedChecks	Pass NO to make parser abort if it finds anything that doesn't comply with standard. Pass YES to allow processing feeds which are not fully standards compliant.
 @param[in]	_verboseMode	If set to YES and _relaxedChecks is YES, it will make parser report violation of standards.
 
 @return	The new allocated parser
 */
- (id) initWithRelaxedChecks:(BOOL)_relaxedChecks verbose:(BOOL)_verboseMode;	// default: NO, NO

// ---- Accessors
/**
 @return	Number of item tags parsed from feed
 */
- (NSUInteger)	GetNumItems;
/**
 @param[in]	_index	Item to get
 
 @return	Item representing the item tag at _index
 */
- (DYRSSItem *)	ItemAt:(NSUInteger)_index;

// ---- Misc
/**
 @param[in]	_relaxedChecks	Pass NO to make parser abort if it finds anything that doesn't comply with standard. Pass YES to allow processing feeds which are not fully standards compliant.
 @param[in]	_verboseMode	If set to YES and _relaxedChecks is YES, it will make parser report violation of standards.
 
 Use this method to override values set when parser was created
 */
- (void)	SetRelaxedChecks:(BOOL) _relaxedChecks verbose:(BOOL) _verboseMode;

// ---- Funciones auxiliares
/**
 Removes all items and channel info from the parser
 */
- (void)	Clear;

@end
