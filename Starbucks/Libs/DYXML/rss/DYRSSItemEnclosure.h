//
//  DYRSSItemEnclosure.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSItemEnclosure
 
 @brief Describes a media object for an item
 */
///@}
@interface DYRSSItemEnclosure : NSObject 
{
	NSMutableString	*mpUrl;
	NSMutableString	*mpLength;
	NSMutableString	*mpType;
	
	UIImage		*mpImage;
}

/**
 @property	NSMutableString *url
 
 URL of the resource, as string
 */
@property (nonatomic, readonly)	NSMutableString	*url;
/**
 @property	NSMutableString *length
 
 Length of the resource in bytes, as string
 */
@property (nonatomic, readonly)	NSMutableString	*length;
/**
 @property	NSMutableString *type
 
 MIME type of the media object, such as x-image/png or x-movie/avi, etc.
 */
@property (nonatomic, readonly)	NSMutableString	*type;
/**
 @property UIImage *image
 
 If media object is an image, may contain the actual image loaded from URL
 */
@property (nonatomic, retain)	UIImage		*image;

/**
 Removes all information from the instance
 */
- (void) Clear;

/**
 @return	A pointer to the image object
 */
- (UIImage **) GetImagePtr;

@end
