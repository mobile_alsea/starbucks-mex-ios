//
//  DYRSSItemCategory.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSItemCategory
 
 @brief Category for an item, optionally with a domain
 */
///@}
@interface DYRSSItemCategory : NSObject 
{
	NSMutableString	*mpDomain;
	NSMutableString	*mpCategory;
}

/**
 @property NSMutableString *domain
 
 Domain for the category, as string
 */
@property (nonatomic, readonly)	NSMutableString *domain;
/**
 @property NSMutableString *category
 
 Category text
 */
@property (nonatomic, readonly)	NSMutableString *category;

/**
 Removes all information from the instance
 */
- (void) Clear;

@end
