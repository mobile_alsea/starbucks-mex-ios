//
//  DYRSSTextInput.h
//
//  Created by Dylvian on 28/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSTextInput
 
 @brief Information for creating a text input for the channel
 */
///@}
@interface DYRSSTextInput : NSObject
{
	NSMutableString	*mpTitle;
	NSMutableString	*mpDescription;
	NSMutableString	*mpName;
	NSMutableString	*mpLink;
}

/**
 @property	NSMutableString	*title
 
 The label of the Submit button in the text input area
 */
@property (nonatomic, readonly)	NSMutableString *title;
/**
 @property	NSMutableString	*description
 
 Explains the text input area
 */
@property (nonatomic, readonly)	NSMutableString *description;
/**
 @property	NSMutableString	*name
 
 The name of the text object in the text input area
 */
@property (nonatomic, readonly)	NSMutableString *name;
/**
 @property	NSMutableString *link
 
 The URL of the script that processes text input requests, as string
 */
@property (nonatomic, readonly)	NSMutableString *link;

/**
 Removes all information from an item
 */
- (void) Clear;

@end
