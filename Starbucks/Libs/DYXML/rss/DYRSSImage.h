//
//  DYRSSImage.h
//
//  Created by Dylvian on 28/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSImage
 
 @brief Information of an image in a RSS 2.0 feed
 */
///@}
@interface DYRSSImage : NSObject 
{
	// Mandatory
	NSMutableString	*mpUrl;
	NSMutableString	*mpTitle;
	NSMutableString	*mpLink;
		
	// Optional
	NSInteger	mWidth;
	NSInteger	mHeight;
	NSMutableString	*mpDescription;
}

/**
 @property	NSMutableString *url
 
 URL of the image, as string
 */
@property	(nonatomic, readonly)	NSMutableString *url;
/**
 @property	NSMutableString *title
 
 Title of the image
 */
@property	(nonatomic, readonly)	NSMutableString *title;
/**
 @property	NSMutableString *link
 
 URL to jump when the user clicks on the image, as string
 */
@property	(nonatomic, readonly)	NSMutableString *link;
/**
 @property	NSInteger width
 
 Width of the image, in pixels
 */
@property	(nonatomic)	NSInteger width;
/**
 @property	NSInteger height
 
 Height of the image, in pixels
 */
@property	(nonatomic)	NSInteger height;
/**
 @property	NSMutableString *description
 
 Description of the image
 */
@property	(nonatomic, readonly)	NSMutableString *description;

/**
 Removes all information from the instance
 */
- (void)	Clear;

@end
