//
//  DYRSSItemSource.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class NSObject
 @addtogroup	DYRSSParser
 @{
 @class DYRSSItemSource
 
 @brief Feed channel where the item came from
 */
///@}
@interface DYRSSItemSource : NSObject 
{
	NSMutableString	*mpUrl;
	NSMutableString	*mpSource;
}

/**
 @property	NSMutableString	*url
 
 URL linking to the feed XML
 */
@property (nonatomic, readonly)	NSMutableString *url;

/**
 @property	NSMutableString *source
 
 Title of the feed channel
 */
@property (nonatomic, readonly)	NSMutableString *source;

/**
 Removes all information from the instance
 */
- (void) Clear;

@end
