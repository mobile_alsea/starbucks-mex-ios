//
//  DYWiktionaryParser.h
//  DYXML
//
//  Created by Dylvian on 26/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DYWiktionaryEntry.h"

/**
 @class NSObject
 @addtogroup DYWiktionaryParser
 @{
 @class	DYWiktionaryParser
 
 @brief	Parser for wiktionary entries
 */
///@}
@interface DYWiktionaryParser : NSObject 
{
	NSString *mLanguage;
}

/**
 @property NSString *language	Wiktionary language
 */
@property (nonatomic, readonly) NSString *language;

/**
 @param[in]	_language	Wiktionary language. Currently, only ES is supported
 
 @return An object initialized with the selected language
 */
- (id) initWithLanguage:(NSString *)_language;

/**
 @param[in]	_word	Word to lookup
 
 @return	A wiktionary entry, or nil if no definition has been found
 */
- (DYWiktionaryEntry *) lookupWord:(NSString *)_word;

@end
