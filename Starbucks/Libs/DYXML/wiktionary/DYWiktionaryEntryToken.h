//
//  DYWiktionaryEntryToken.h
//  DYXML
//
//  Created by Dylvian on 26/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @enum DYWiktionaryEntryTokenType	Token type used for parsing
 */
typedef enum DYWiktionaryEntryTokenType { DYWiktionaryEntryTokenTypePlainText = 0,
	DYWiktionaryEntryTokenTypeLanguage, 
	DYWiktionaryEntryTokenTypePronunciation, 
	DYWiktionaryEntryTokenTypeEthimology,
	DYWiktionaryEntryTokenTypeSection,
	DYWiktionaryEntryTokenTypeUnorderedList,
	DYWiktionaryEntryTokenTypeOrderedList,
	DYWiktionaryEntryTokenTypeBlank,
	DYWiktionaryEntryTokenTypeOther } DYWiktionaryEntryTokenType;

/**
 @class NSObject
 @addtogroup DYWiktionaryParser
 @{
 @class	DYWiktionaryEntryToken
 
 @brief	Token in a entry in a wiktionary
 */
///@}
@interface DYWiktionaryEntryToken : NSObject 
{
	DYWiktionaryEntryTokenType mType;
	
	NSInteger mIndentLevel;
	
	NSString *mPlainString;
	NSString *mHTMLString;
		
	NSInteger mSectionLevel;
	
	NSInteger mUnorderedLevel;
	NSInteger mOrderedLevel;
}

/**
 @property DYWiktionaryEntryTokenType type	token type
 */
@property (nonatomic, readonly) DYWiktionaryEntryTokenType type;
/**
 @property NSInteger indentLevel	indent level
 */
@property (nonatomic, readonly) NSInteger indentLevel;
/**
 @property NSString *plainString	string in plain format
 */
@property (nonatomic, readonly)	NSString *plainString;
/**
 @property NSString *htmlString	string in HTML format
 */
@property (nonatomic, readonly) NSString *htmlString;
/**
 @property NSInteger sectionLevel	section level
 */
@property (nonatomic, readonly) NSInteger sectionLevel;
/**
 @property NSInteger unorderedLevel	level in an unordered list
 */
@property (nonatomic, readonly) NSInteger unorderedLevel;
/**
 @property NSInteger orderedLevel	level in an ordered list
 */
@property (nonatomic, readonly) NSInteger orderedLevel;

/**
 @param[in]	_string	String to parse
 
 @return An entry token initialized with _string
 */
- (id) initWithString:(NSString *)_string;

@end
