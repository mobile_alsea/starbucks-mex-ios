//
//  DYWiktionaryEntry.h
//  DYXML
//
//  Created by Dylvian on 26/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @enum DYWiktionaryEntryParserState	Parser state
 */
typedef enum DYWiktionaryEntryParserState { DYWiktionaryEntryParserStateInitial = 0, DYWiktionaryEntryParserStateSection, DYWiktionaryEntryParserStateSubsection } DYWiktionaryEntryParserState;
										
/**
 @class NSObject
 @addtogroup DYWiktionaryParser
 @{
 @class	DYWiktionaryEntry
 
 @brief	Entry in a wiktionary. That is, a word definition from the wiktionary
 */
///@}
@interface DYWiktionaryEntry : NSObject 
{
	NSString *mPronunciation;
	NSString *mEthimology;
	
	NSMutableDictionary *mSectionDictionary;
	
	DYWiktionaryEntryParserState mParserState;
}

/**
 @param[in]	_wiktionaryString	String read from wiktionary containing the definition
 
 @return An object initialized with the wiktionary string
 */
- (id) initWithString:(NSString *)_wiktionaryString;

/**
 @param[in]	_wiktionaryString	String read from wiktionary containing the definition
 
 Parses _wiktionaryString and generates parsed strings
 */
- (void) setString:(NSString *)_wiktionaryString;

/**
 @return	The parsed string for this entry in HTML format
 */
- (NSString *) htmlString;
/**
 @return	The parsed string for this entry in plain format
 */
- (NSString *) plainString;

@end
