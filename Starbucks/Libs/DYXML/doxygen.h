/*
 *  doxygen.h
 *  DYXML
 *
 *  Created by Dylvian on 13/01/10.
 *  Copyright 2010 Dylvian. All rights reserved.
 *
 */

/**
 @defgroup	DYXML	DYXML - XML Parsers
 
 @defgroup	DYXMLParser	DYXMLParser - XML Parser
 @ingroup	DYXML
 
 @defgroup	DYDOMParser	DYDOMParser - XML to dictionary
 @ingroup	DYXML
 
 @defgroup	DYKMLParser	DYKMLParser - Google route parsing
 @ingroup	DYXML
 
 @defgroup	DYRSSParser	DYRSSParser - RSS Format parsers
 @ingroup	DYXML
 
 @defgroup	DYWiktionaryParser	DYWiktionaryParser - Parse wiktionary entries
 @ingroup	DYXML
 */
