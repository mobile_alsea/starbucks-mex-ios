//
//  DYLog.h
//  DYLog
//
//  Created by Dylvian on 18/12/09.
//  Copyright 2009 Dylvian. All rights reserved.
// 
#import <Foundation/Foundation.h>

#import <DYLog/log/DYLogLogger.h>

typedef enum DYLogLevel { DYLogLevelNone = -1, DYLogLevelCritical = 0, DYLogLevelHigh = 1, DYLogLevelMedium = 5, DYLogLevelLow = 10, DYLogLevelInfo = 20, DYLogLevelAll } DYLogLevel;
typedef enum DYLogCategory { DYLogCategoryApplication = 0, DYLogCategoryLibrary } DYLogCategory;

#define	DYLogLevelDefault	DYLogLevelCritical

#define DYLogLevelDebug		DYLogLevelLow
#define DYLogLevelWarning	DYLogLevelMedium
#define DYLogLevelError		DYLogLevelHigh

/// @class NSObject
/**
 @addtogroup	DYLog	DYLog - Log system
 @{
 
 @class		DYLog
 
 @brief		Log System
 
 This class improves logging, allowing users to set log levels. This way, logs can be turned off when application is finished, so no debug messages are sent to device's console. 
 Log system differentiates between library logs and application logs. Library logs should only be used by internal libraries, application logs may be used as needed
 */
/// @}
@interface DYLog : NSObject <DYLogLogger>
{
	NSInteger	mApplicationLogLevel;
	NSInteger	mLibraryLogLevel;
	
	BOOL		mDefaultEnabled;
	
	NSMutableDictionary *mLoggerDictionary;
}

@property (nonatomic)	BOOL	defaultEnabled;

/**
 @param[in]	_logLevel	Log level threshold. Only messages with level below this threshold will be reported
 @param[in]	_logCategory	Category in which log will be reported, framework or application
 
 Sets log level for selected category
 */
+ (void)	setLogLevel:(NSInteger)_logLevel forCategory:(DYLogCategory)_logCategory;
/**
 @param[in]	_logLevel	Log level threshold. Only messages with level below this threshold will be reported
 
 Sets log level for framework logs
 */
+ (void)	setLibraryLogLevel:(NSInteger)_logLevel;
/**
 @param[in]	_logLevel	Log level threshold. Only messages with level below this threshold will be reported
 
 Sets log level for application and framework logs
 */
+ (void)	setLogLevel:(NSInteger)_logLevel;

/**
 @param[in]	_logCategory	Category in which log will be reported, framework or application
 @param[in]	_level	Level for this message. It will only be reported if _level is lower than log level for this category
 @param[in] _pString	Message string
 @param[in]	...	Variable arguments for message formatting, as in NSLog or printf
 
 Logs a message in selected category
 */
+ (void)	logTo:(DYLogCategory)_logCategory withLevel:(NSInteger)_level message:(NSString *)_pString, ...;
/**
 @param[in]	_level	Level for this message. It will only be reported if _level is lower than log level for this category
 @param[in] _pString	Message string
 @param[in]	...	Variable arguments for message formatting, as in NSLog or printf
 
 Logs a message in application category
 */
+ (void)	logWithLevel:(NSInteger)_level message:(NSString *)_pString, ...;
/**
 @param[in]	_level	Level for this message. It will only be reported if _level is lower than log level for this category
 @param[in] _pString	Message string
 @param[in]	...	Variable arguments for message formatting, as in NSLog or printf
 
 Logs a message in framework category
 */
+ (void)	libraryLogWithLevel:(NSInteger)_level message:(NSString *)_pString, ...;
/**
 @param[in] _pString	Message string
 @param[in]	...	Variable arguments for message formatting, as in NSLog or printf
 
 Logs a message in framework category with low level
 */
+ (void)	libraryLog:(NSString *)_pString, ...;
/**
 @param[in] _pString	Message string
 @param[in]	...	Variable arguments for message formatting, as in NSLog or printf
 
 Logs a message in application category with low level
 */
+ (void)	log:(NSString *)_pString, ...;

/**
 @param[in]	_enabled	Whether default logger (NSLog) is enabled or disabled
 
 Enables or disables default logging to NSLog
 */
+ (void)	setDefaultLoggerEnabled:(BOOL)_enabled;

/**
 @param[in]	_logger	Logger that will receive log messages
 
 Adds a new logger that will receive log messages from all categories
 */
+ (void)	addLogger:(id<DYLogLogger>)_logger;

/**
 @param[in]	_logger	Logger that will receive log messages
 @param[in]	_category	Category which _logger will receive log messages from
 
 Adds a new logger that will receive log messages from selected category
 */
+ (void)	addLogger:(id<DYLogLogger>)_logger toCategory:(DYLogCategory)_category;

/**
 @param[in]	_logger	Logger that will stop receiving log messages
 
 Removes this logger from all categories
 */
+ (void)	removeLogger:(id<DYLogLogger>)_logger;

/**
 @param[in]	_logger	Logger that will stop receiving log messages
 @param[in] _category	Category from which this logger will be removed
 
 Removes this logger from selected categories
 */
+ (void)	removeLogger:(id<DYLogLogger>)_logger fromCategory:(DYLogCategory)_category;

@end
