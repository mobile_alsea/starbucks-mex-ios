//
//  DYLogLogger.h
//  DYLog
//
//  Created by Dylvian on 18/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DYLogLogger
@required

- (void) logMessage:(NSString *)_message;

@end
