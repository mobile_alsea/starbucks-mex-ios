//
//  DYFileLogger.h
//  DYLog
//
//  Created by Dylvian on 18/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYLog/log/DYLogLogger.h>

@interface DYFileLogger : NSObject <DYLogLogger>
{
	NSString *mFileName;
}

@property (nonatomic, readonly)	NSString *fileName;

+ (DYFileLogger *) fileLoggerToFile:(NSString *)_fileName;

- (id) initWithFile:(NSString *)_fileName;

@end
