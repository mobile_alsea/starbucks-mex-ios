//
//  DYFileNameManager.h
//
//  Created by Dylvian on 26/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class	NSObject
 @addtogroup	DYFileNameManager
 @{
 
 @class		DYFileNameManager
 
 @brief		Simple file name helper
 
 This is a class which we can use to get the right file paths without dealing with bundles. It "translates" the name from a file name which can be an absolute path or a name like bundle:filename to specify
 we must search inside the application bundle and the documents folder of the application
 */
/// @}
@interface DYFileNameManager : NSObject 
{
	
}

/**
 @param[in]	_filePath	Name of the file we want to access. It may start with 'bundle:' to set where we want to search
 @param[in]	_mode	String representing the mode in which we want to open the file, like the fopen function mode parameter
 
 @return Full file path to access the actual file
 
 The special case where you want to access a file for writing and that file only exists in the read-only bundle part of the application is solved creating a copy of the read-only file in the writable documents path of the
 application. Afterwards, only the writable file is accessed.
 
 @code
 NSString *httpURL = [DNFileNameManager PathForFile:@"http://www.server.com/sample.txt" mode:@"r"];	// return "http://www.server.com/sample.txt"
 NSString *readOnlyFile = [DNFileNameManager PathForFile:@"bundle://audio.mp3" mode:@"r"];	// return "[non-writable-app-folder]/audio.mp3"
 NSString *readWriteFile = [DNFileNameManager PathForFile:@"bundle://database.sqlite" mode:@"rw"];	// return "[document-writable-app-folder]/database.sqlite"
 // In this last case, if database.sqlite exists in the read-only folder of the application, it is copied to the writable folder of the application
 @endcode
 */
+ (NSString *) pathForFile:(NSString *)_filePath mode:(NSString *)_mode; 

/** 
 @return Application document path
 
 @code
 NSString *writablePath = [DNFileNameManager applicationDocumentsDirectory];
 NSString *logFilePath = [writablePath stringByAppendingString:@"report.log"];
 
 [myData writeToFile:logFilePath atomically:YES];
 @endcode
*/
+ (NSString *) applicationDocumentsDirectory;

+ (NSString *) pathForCacheFile:(NSString *)_filePath mode:(NSString *)_mode;

+ (NSString *) pathForFile:(NSString *)_filePath directory:(NSSearchPathDirectory)_directory mode:(NSString *)_mode copyFromBundle:(BOOL)_copy;

@end
