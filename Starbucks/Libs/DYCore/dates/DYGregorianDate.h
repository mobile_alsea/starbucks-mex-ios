//
//  DYGregorianDate.h
//
//  Created by Dylvian on 29/01/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class	NSObject
 @addtogroup	DYGregorianDate
 @{
 @class		DYGregorianDate
 
 @brief		Class to work with a gregorian calendar
 
 All NSDate interpretation functions must work against a calendar. DYGregorianDate provides all functions with a gregorian calendar, simplifying amount of work to be done
 */
/// @}
@interface DYGregorianDate : NSObject 
{

}

/**
 @param[in]	_year	Year part of date to create
 @param[in]	_month	Month part of date to create
 @param[in]	_day	Day part of date to create
 @param[in]	_hour	Hour part of date to create
 @param[in]	_minute	Minute part of date to create
 @param[in]	_second	Month part of date to create
 
 @return A date object created from parameters.
 
 This function is thread-safe
 */
+ (NSDate *) dateWithYear:(NSUInteger)_year month:(NSUInteger)_month day:(NSUInteger)_day hour:(NSUInteger)_hour minute:(NSUInteger)_minute second:(NSUInteger)_second;

/**
 @param[in]	_date	A string representing a date in Internet format, according to http://asg.web.cmu.edu/rfc/rfc822.html
 
 @return A date object created from string.
 
 This function is thread-safe
 */
+ (NSDate *) dateWithInternetString:(NSString *)_date;

/**
 @param[in]	_date	An NSDate object to represent in Internet format, according to http://asg.web.cmu.edu/rfc/rfc822.html
 
 @return A string representing _date
 
 This function is thread-safe
 */
+ (NSString *) internetStringWithDate:(NSDate *)_date;

/**
 @param[in]	_date	An NSDate object to split in components
 
 @return Components of _date
 
 This function is thread-safe
 */
+ (NSDateComponents *) componentsOfDate:(NSDate *)_date;

/**
 @param[in]	smaller	Unit to count
 @param[in]	larger	Unit containing smaller units
 @param[in]	date	Date to check
 
 @return How many smaller units are there in larger unit in date
 
 This function is useful to check how many days are there in the month of a concrete date. This function is thread-safe.
 
 @code
 NSRange smallRange = [DNGregorianDate rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:[NSDate now]];
 int daysInMonth = smallRange.length;
 @endcode
 */
+ (NSRange)rangeOfUnit:(NSCalendarUnit)smaller inUnit:(NSCalendarUnit)larger forDate:(NSDate *)date;

/**
 @param[in]	_units	How many steps to add to date
 @param[in]	_unit	Unit to measure _units (days, hours, etc.)
 @param[in]	date	Base date
 
 @return A date by adding _units _unit to date.
 
 This function is thread-safe
 */
+ (NSDate *) dateByAdding:(NSInteger)_units unit:(NSCalendarUnit)_unit toDate:(NSDate *)date;

/**
 @param[in]	_pDate	Date to represent
 @param[in]	_pFormat	Date format, as used in NSDateFormatter
 
 @return An string with _pDate formatted according to _pFormat. The string returned is retained, so it must be released by the user
 
 This function is thread-safe
 */
+ (NSString *) stringForDate:(NSDate *)_pDate format:(NSString *)_pFormat;

/**
 @param[in]	_pDateString	String representing a date
 @param[in]	_pFormat	Date format, as used in NSDateFormatter
 
 @return A date created from _pDateString
 
 This function is thread-safe
 */
+ (NSDate *) dateFromString:(NSString *)_pDateString format:(NSString *)_pFormat;

+ (NSInteger) unitsOfUnit:(NSInteger)_unit forDate:(NSDate *)_date;
+ (NSInteger) leapYearsUntilYear:(NSInteger)_year;
+ (BOOL) isLeapYear:(NSInteger)_year;

@end
