//
//  DYFetchDelegate.h
//  DYCore
//
//  Created by José Servet on 12/04/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DYFetchResource.h"

@protocol DYFetchDelegate <NSObject>
@required

- (void) didStartFetchingResource:(DYFetchResource *)_resource;
- (void) didFailFetchingResource:(DYFetchResource *)_resource;
- (void) didFinishFetchingResource:(DYFetchResource *)_resource;

@optional

- (void) didCancelFetchingResource:(DYFetchResource *)_resource;
- (void) didDownloadProgress:(double)_progress resource:(DYFetchResource *)_resource;

@end
