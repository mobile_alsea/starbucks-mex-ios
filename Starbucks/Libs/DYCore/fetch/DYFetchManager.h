//
//  DYFetchManager.h
//  DYCore
//
//  Created by José Servet on 12/04/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYCore/singleton/DYSingletonObject.h>

#import "DYFetchDelegate.h"
#import "DYFetchResource.h"

@interface DYFetchManager : DYSingletonObject
{
	NSMutableDictionary *mResourceDictionary;	// each value is an array of delegates
	NSMutableDictionary *mConnectionDictionary;	// each key is a connection, each value is a resource
	
	NSCache *mFetchedResourcesCache;	// temporary cache, resource-resource
}

+ (void) fetchUrl:(NSString *)_url delegate:(id<DYFetchDelegate>)_delegate;
+ (void) fetchResource:(DYFetchResource *)_resource delegate:(id<DYFetchDelegate>)_delegate;
+ (void) fetchResource:(DYFetchResource *)_resource group:(NSString *)_group delegate:(id<DYFetchDelegate>)_delegate;
+ (void) cancelResource:(DYFetchResource *)_resource;
+ (void) cancelPendingResources;
+ (void) cancelPendingResourcesInGroup:(NSString *)_group;

- (void) bindConnection:(NSURLConnection *)_connection toResource:(DYFetchResource *)_resource;

@end

@interface DYFetchManager (NSURLConnection) <NSURLConnectionDelegate>
@end