//
//  DYFetchResourceData.h
//  DYCore
//
//  Created by José Servet on 12/04/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DYFetchDelegate.h"
#import "DYFetchResource.h"

@interface DYFetchResourceData : NSObject
{
	NSMutableArray *mDelegateArray;
	NSURLConnection *mConnection;
	
	NSMutableArray *mGroupArray;
}

@property (nonatomic, readonly) NSMutableArray *delegateArray;
@property (nonatomic, readonly) NSURLConnection *connection;

- (id) initWithFetchResource:(DYFetchResource *)_resource;

- (void) startConnectionToResource:(DYFetchResource *)_resource delegate:(id<NSURLConnectionDelegate>)_delegate;

- (BOOL) containsDelegate:(id<DYFetchDelegate>)_delegate;
- (void) addDelegate:(id<DYFetchDelegate>)_delegate;

- (void) addToGroup:(NSString *)_group;
- (void) removeFromGroup:(NSString *)_group;
- (BOOL) isInGroup:(NSString *)_group;
- (BOOL) isInAnyGroup;

@end
