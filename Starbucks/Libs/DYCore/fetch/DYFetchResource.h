//
//  DYFetchResource.h
//  DYCore
//
//  Created by José Servet on 12/04/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum DYFetchMethod { DYFetchMethodGet = 0, DYFetchMethodPost } DYFetchMethod;

@interface DYFetchResource : NSObject
{
	NSURL *mUrl;
	DYFetchMethod mMethod;
	NSMutableArray *mParameterArray;
	NSData *mBody;
	
	NSMutableDictionary *mCustomHeaderDictionary;
	
	NSURLResponse *mResponse;
	NSMutableData *mTemporaryData;
	NSData *mData;
	
	long long mContentLength;
	double mProgress;
}

@property (nonatomic, copy) NSURL *url;
@property (nonatomic) DYFetchMethod method;
@property (nonatomic, readonly) NSMutableArray *parameterArray;
@property (nonatomic, copy) NSData *body;

@property (nonatomic, readonly) NSMutableDictionary *customHeaderDictionary;

@property (nonatomic, readonly) NSURLResponse *response;
@property (nonatomic, readonly) NSData *data;

@property (nonatomic, readonly) double progress;

- (id) initWithUrl:(NSURL *)_url;
- (id) initWithMethod:(DYFetchMethod)_method;
- (id) initWithUrl:(NSURL *)_url method:(DYFetchMethod)_method;
- (id) initWithUrl:(NSURL *)_url method:(DYFetchMethod)_method parameters:(NSArray *)_parameterArray;
- (id) initWithUrl:(NSURL *)_url method:(DYFetchMethod)_method parameters:(NSArray *)_parameterArray body:(NSData *)_body;

- (void) addParameter:(NSString *)_parameter value:(NSString *)_value;
- (void) addCustomHeader:(NSString *)_header value:(NSString *)_value;

- (void) setURLResponse:(NSURLResponse *)_response;

- (NSURLRequest *) urlRequest;

- (void) appendTemporaryData:(NSData *)_data;
- (void) markAsFinished;

@end
