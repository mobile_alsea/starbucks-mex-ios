/*
 DYMutableStack
 
 A simple array-based stack
*/

#import <Foundation/Foundation.h>

/**
 @addtogroup	DYMutableStack
 @{
 
 @class		DYMutableStack
 
 @brief		Implements a stack of objects
 
 A stack of objects is a LIFO queue of objects. You can push items on the top of the stack, and the last object pushed will be the first to be popped
 */
/// @}
@interface DYMutableStack : NSObject
{
	NSMutableArray	*mArray;
}

/**
 @param[in]	_object	The object to be stacked
 
 Pushes _object in the stack
 */
- (void) pushObject:(id) _object;

/**
 @return	The object on the top of the stack
 
 Removes the object on the top of the stack and returns it. The object is retained, must be released by the programmer
 */
- (id) popObject;

/**
 @return	The object on the top of the stack
 
 Returns the object on the top of the stack without removing it. Similar to peek() in other languages.
 */
- (id) top;

/**
 @param[in]	_index	Index of the object we want to get
 @return	The object at the specified index
 
 Index 0 is the top of the stack, and index height-1 is the bottom of the stack
 */
- (id) objectAtIndex:(NSUInteger)_index;

/**
 Clears the stack by removing all objects
 */
- (void)	removeAllObjects;

/**
 @return	Number of objects in the queue
 */
- (NSInteger)	count;


@end
