//
//  DYNib.h
//  DYCore
//
//  Created by Dylvian on 21/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

//#define HAS_UINIB

/**
 @class	NSObject
 @addtogroup	DYNib
 @{
 @class		DYNib
 
 @brief		Class to instantiate NIB files
 
 This class is useful when several objects are instantiated from the same NIB file, for example cells in a tableview
 */
/// @}
@interface DYNib : NSObject
{
	NSMutableDictionary *mNibDictionary;
}

//#if defined(HAS_UINIB)
/**
 @param[in]	name	name of the nib file.
 @param[in]	bundleOrNil	bundle containing the nib file. If this parameter is nil, the main bundle is used
 
 @return A UINib object for the selected nib file, or nil if some error occured
 */
+ (UINib *) nibWithNibName:(NSString *)name bundle:(NSBundle *)bundleOrNil;
/**
 @param[in]	data	data representing the nib file
 @param[in]	bundleOrNil	bundle containing the nib file. If this parameter is nil, the main bundle is used
 
 @return A UINib object representing the data, or nil if some error occured
 */
+ (UINib *) nibWithData:(NSData *)data bundle:(NSBundle *)bundleOrNil;
//#endif

/**
 @param[in]	name	name of the nib file.
 @param[in]	bundleOrNil	bundle containing the nib file. If this parameter is nil, the main bundle is used
 @param[in] ownerOrNil	object that will become the file owner declared in the nib file, or nil if it's not necessary
 @param[in] optionsOrNil	dictionary of nib instantiating options, or nil if they are not necessary
 
 @return An array containing the instantiated objects from the nib file
 */
+ (NSArray *) instantiateNib:(NSString *)_name bundle:(NSBundle *)_bundleOrNil owner:(id)ownerOrNil options:(NSDictionary *)optionsOrNil;
/**
 @param[in]	data	data representing the nib file
 @param[in]	bundleOrNil	bundle containing the nib file. If this parameter is nil, the main bundle is used
 @param[in] ownerOrNil	object that will become the file owner declared in the nib file, or nil if it's not necessary
 @param[in] optionsOrNil	dictionary of nib instantiating options, or nil if they are not necessary
 
 @return An array containing the instantiated objects from the data
 */
+ (NSArray *) instantiateNibData:(NSData *)_data bundle:(NSBundle *)_bundleOrNil owner:(id)ownerOrNil options:(NSDictionary *)optionsOrNil;

/**
 @param[in]	name	name of the nib file.
 @param[in]	bundleOrNil	bundle containing the nib file. If this parameter is nil, the main bundle is used
 @param[in] ownerOrNil	object that will become the file owner declared in the nib file, or nil if it's not necessary
 @param[in] optionsOrNil	dictionary of nib instantiating options, or nil if they are not necessary
 @param[in]	_class	class of the object to instantiate from the nib file
 
 @return The first object of the required class instantiated from the nib file. 
 
 If there aren't any objects of the selected class in the nib, this method returns nil.
 If more than one object of the required class is present in the nib file, this method returns the first of them in the object array.
 */
+ (id) instantiateNib:(NSString *)_name bundle:(NSBundle *)_bundleOrNil owner:(id)ownerOrNil options:(NSDictionary *)optionsOrNil class:(Class)_class;
/**
 @param[in]	data	data representing the nib file.
 @param[in]	bundleOrNil	bundle containing the nib file. If this parameter is nil, the main bundle is used
 @param[in] ownerOrNil	object that will become the file owner declared in the nib file, or nil if it's not necessary
 @param[in] optionsOrNil	dictionary of nib instantiating options, or nil if they are not necessary
 @param[in]	_class	class of the object to instantiate from the nib file
 
 @return The first object of the required class instantiated from the data
 
 If there aren't any objects of the selected class in the data, this method returns nil.
 If more than one object of the required class is present in the data file, this method returns the first of them in the object array.
 */
+ (id) instantiateNibData:(NSData *)_data bundle:(NSBundle *)_bundleOrNil owner:(id)ownerOrNil options:(NSDictionary *)optionsOrNil class:(Class)_class;

+ (id) instantiateNib:(NSString *)_name class:(Class)_class;
+ (id) instantiateNibData:(NSData *)_data class:(Class)_class;

@end
