//
//  DYOperationQueue.h
//  DYCore
//
//  Created by M2GRP Murcia on 01/10/09.
//  Copyright 2009 M2GRP. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class	NSOperationQueue
 @addtogroup	DYOperationQueue
 @{
	@class DYOperationQueue
 
	@brief	A wrapper around NSOperationQueue and NSOperation to simplify working with them
 */
/// @}
@interface DYOperationQueue : NSOperationQueue 
{
}

/**
 @return	Default operation queue
 */
+ (DYOperationQueue *) sharedInstance;
/**
 @param[in]	_pKey	key of the operation queue we want to access
 
 @return	Operation queue for _pKey
 
 This way, we can have several operation queues, per thread or per job, and all of them are managed the same way
 */
+ (DYOperationQueue *) sharedInstanceForKey:(NSString *)_pKey;

/**
 @param[in]	_pOperation	operation to perform
 @param[in]	_pKey	key of the operation queue we want to access
 
 Enqueues _pOperation in the operation queue identified by _pKey
 */
+ (void) addOperation:(NSOperation *)_pOperation forKey:(NSString *)_pKey;

/**
 @param[in]	_pOperation	operation to perform
 
 Enqueues _pOperation in the default operation queue
 */
+ (void) addOperation:(NSOperation *)_pOperation;

+ (void) addTarget:(id)_pTarget selector:(SEL)_pSelector;
+ (void) addTarget:(id)_pTarget selector:(SEL)_pSelector object:(id)_object;

/**
 @param[in]	_pTarget	Object that will execute the action
 @param[in]	_pSelector	Method to execute
 @param[in] _pArgArray	Array of arguments for target selector. May be nil. A single parameter must be stored in an array
 
 @brief	Creates an operation to execute selector in target, passing arguments in _pArgArray in the default operation queue
 */
+ (void) addTarget:(id)_pTarget selector:(SEL)_pSelector arguments:(NSArray *)_pArgArray;

/**
 @param[in]	_pTarget	Object that will execute the action
 @param[in]	_pSelector	Method to execute
 @param[in] _pArgArray	Array of arguments for target selector. May be nil. A single parameter must be stored in an array
 @param[in] _pKey		Identifier of the queue in which execute operation
 
 @brief	Creates an operation to execute selector in target, passing arguments in _pArgArray in the desired operation queue
 */
+ (void) addTarget:(id)_pTarget selector:(SEL)_pSelector arguments:(NSArray *)_pArgArray forKey:(NSString *)_pKey;

/**
 @param[in]	_pTarget	Object that will execute the action
 @param[in]	_pSelector	Method to execute
 @param[in] _pArgArray	Array of arguments for target selector. May be nil. A single parameter must be stored in an array
 
 @brief	Creates an operation to execute selector in target, passing arguments in _pArgArray
 */
- (void) addTarget:(id)_pTarget selector:(SEL)_pSelector arguments:(NSArray *)_pArgArray;

@end
