//
//  DYDispatchQueue.h
//  DYCore
//
//  Created by José Servet on 30/04/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DYDispatchQueue : NSObject
{
	dispatch_queue_t mQueue;
}

@property (nonatomic, readonly) dispatch_queue_t queue;

- (id) initWithName:(NSString *)_name;

@end
