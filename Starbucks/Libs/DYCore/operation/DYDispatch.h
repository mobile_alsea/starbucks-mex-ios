//
//  DYDispatch.h
//  DYCore
//
//  Created by José Servet Font on 27/04/12.
//  Copyright (c) 2012 José Servet. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <DYCore/singleton/DYSingletonObject.h>

@interface DYDispatch : DYSingletonObject
{
	NSMutableDictionary *mSerialQueueDictionary;
}

+ (void) dispatchInBackground:(dispatch_block_t)_block;
+ (void) dispatchInMainThread:(dispatch_block_t)_block;
+ (void) dispatchInMainThreadAsync:(dispatch_block_t)_block;

+ (void) dispatchObject:(id)_obj criticalBlock:(dispatch_block_t)_block;
+ (void) dispatchObject:(id)_obj criticalBlockAsync:(dispatch_block_t)_block;

+ (void) dispatchMutex:(NSString *)_mutexName criticalBlock:(dispatch_block_t)_block;

+ (void) dispatchAfterDelay:(NSTimeInterval)_delay block:(dispatch_block_t)_block;

@end
