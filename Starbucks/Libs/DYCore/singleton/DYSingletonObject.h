//
//  DYSingletonObject.h
//  DYCore
//
//  Created by José Servet on 02/11/11.
//  Copyright (c) 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DYSingletonObject : NSObject

+ (id) sharedInstance;

@end
