/*
 *  doxygen.h
 *  DYCore
 *
 *  Created by Dylvian on 12/01/10.
 *  Copyright 2010 Dylvian. All rights reserved.
 *
 */

/**
 @defgroup	DYCore	DYCore - Base classes and utilities

 @defgroup	DYOperationQueue	DYOperationQueue - Operation queue
 @ingroup	DYCore
 
 @defgroup	DYMutableStack	DYMutableStack - Mutable stack with push and pop operations
 @ingroup	DYCore
 
 @defgroup	NSDictionary	NSDictionary - Categories for dictionaries
 @ingroup	DYCore
 
 @class	NSDictionary
 @brief	Foundation NSDictionary
 
 @defgroup	NSMutableDictionary	NSMutableDictionary - Categories for mutable dictionaries
 @ingroup	DYCore
 
 @class	NSMutableDictionary
 @brief	Foundation NSMutableDictionary
 
 @defgroup	NSData	NSData - Categories for data
 @ingroup	DYCore
 
 @class	NSData
 @brief	Foundation NSData
 
 @defgroup	NSString	NSString - Categories for strings
 @ingroup	DYCore
 
 @class	NSString
 @brief	Foundation NSString
 
 @defgroup	NSMutableString	NSMutableString - Categories for mutable strings
 @ingroup	DYCore
 
 @class	NSMutableString
 @brief	Foundation NSMutableString
 
 @defgroup	NSArray	NSArray - Categories for arrays
 @ingroup	DYCore
 
 @class	NSArray
 @brief	Foundation NSArray

 @defgroup	NSMutableArray	NSMutableArray - Categories for mutable arrays
 @ingroup	DYCore
 
 @class	NSMutableArray
 @brief	Foundation NSMutableArray
  
 @defgroup	UIApplication	UIApplication - Categories for applications
 @ingroup	DYCore
 
 @class	UIApplication
 @brief	UIKit UIApplication
 
 @defgroup	DYNib	DYNib - Nib loading options
 @ingroup	DYCore
 
 @defgroup	NSSet	NSSet - Categories for sets
 @ingroup	DYCore
 
 @class	NSSet
 @brief	Foundation NSSet
 
 @defgroup	NSSortDescriptor	NSSortDescriptor - Categories for sort descriptors
 @ingroup	DYCore
 
 @class NSSortDescriptor
 @brief	Foundation NSSortDescriptor
 
 @defgroup	DYGregorianDate	DYGregorianDate - Work with gregorian calendars
 @ingroup	DYCore
 
 @defgroup	DYFileNameManager	DYFileNameManager - Work with filepaths
 @ingroup	DYCore
 */