//
//  NSData_compress.h
//  DYCore
//
//  Created by Dylvian on 07/10/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DYCORE_USE_ZLIB		// Uncomment for ZLib compression
// #define DYCORE_USE_BZ2LIB	// Uncomment for BZ2Lib compression (unpublished by Apple, use at your own risk)

#warning Si usas este fichero debes añadir el framework de la ZLib: add -> existing frameworks -> libz.dylib

typedef enum ECompressRate { ECR_WORST = 1, ECR_AVERAGE = 4, ECR_BEST = 9 } ECompressRate;

/**
 @addtogroup	NSData
 @{
 @class NSData_compress
 @brief		Allows compression in NSData. libbz2 may be enabled, but it's an unpublished library in Apple's iPhone SDK, so use at your own risk. See header file NSData_compress.h for functions.
 
 @note	If zlib compression is enabled, you need to link libz (from SDK)
 @note If bzip2 compression is enabled, you need to link libbz2 (unpublished in Apple's iPhone SDK)
 
 <hr>
 
 @section Programming_Guide	Programming Guide
 
 @code
 {
 ...
 
 NSData *remoteData = [NSData dataWithContentsOfURL:[NSURL urlWithString:@"http://www.server.com/resource.txt"]];
 
 NSData *zlibData = [remoteData compressWithZLib:ECR_BEST];	// if compiled with ZLib support (default:YES)
 NSData *bzlibData = [remoteData compressWithBZip2:ECR_AVERAGE];	// if compiled with libBZ2 support (default:NO as it's an unpublished library in Apple's iPhone SDK)
 NSData *originalData = [zlibData decompressWithZLib];	// restore original data
 
 ...
 }
 @endcode
 */
/// @}
@interface NSData(compress)

#if defined DYCORE_USE_BZ2LIB
/**
 @param[in]	_compressRate	The compress rate to apply: ECR_WORST - faster, less compression, ECR_BEST - slower, more compression
 
 @return	Compressed data using bz2
 
 @brief		Compresses current data with the desired rate
 */
- (NSData *) compressWithBZip2:(ECompressRate)_compressRate;
/**
 @return	Data decompressed using bz2. Current data should be bz2 compressed.
 */
- (NSData *) decompressWithBZip2;
#endif

#if defined DYCORE_USE_ZLIB
/**
 @param[in]	_compressRate	The compress rate to apply: ECR_WORST - faster, less compression, ECR_BEST - slower, more compression
 
 @return	Compressed data using zlib
 
 @brief		Compresses current data with the desired rate
 */
- (NSData *) compressWithZLib:(ECompressRate)_compressRate;
/**
 @return	Data decompressed using zlib. Current data should be bz2 compressed.
 */
- (NSData *) decompressWithZLib;
#endif

@end
