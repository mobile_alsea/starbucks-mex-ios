//
//  UIApplication_misc.h
//  DNCore
//
//  Created by Dylvian on 24/11/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum DeviceModel { DeviceModelUnknown = -1, DeviceModeliPhone = 0, DeviceModeliPod, DeviceModeliPad } DeviceModel;

/**
 @addtogroup	UIApplication
 @{
 @class UIApplication_misc
 @brief		Miscellaneous functions on UIDevice. See header file UIDevice_misc.h for functions.
 */
/// @}
@interface UIApplication(misc)

/**
 @brief	Shows network activity indicator in status bar. Several calls may be nested.
 */
+ (void) ShowNetworkActivity;
/**
 @brief	Hides network activity indicator from status bar if needed, as several calls may be nested.
 */
+ (void) HideNetworkActivity;

/**
 @return	A constant indicating the device model
 */
+ (DeviceModel) deviceModel;

+ (BOOL) isiPhone;
+ (BOOL) isiPad;

+ (NSString *) macAddress;
+ (NSString *) uniqueIdentifier;

@end
