//
//  NSArray_misc.h
//  FleetClient
//
//  Created by Dylvian on 28/10/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSArray
 @{
 @class NSArray_misc
 @brief		Miscellaneous functions on arrays. See header file NSArray_misc.h for functions.
 */
/// @}
@interface NSArray(misc)

/**
 @param[in]	_pArray	Array to merge with current array
 @param[in] _duplicates	Whether resulting array may contain duplicates or not
 
 @return	An array containing elements from both arrays
 
 @brief	If _duplicates is YES, the resulting array contains the elements in _pArray appended to current array. If _duplicates is NO, this method is significantly slower because every object in _pArray must be tested in current array
 */
- (NSArray *) arrayMergedWithArray:(NSArray *)_pArray allowDuplicates:(BOOL)_duplicates;	// joins 2 arrays

/**
 @param[in]	_pArray	Array to intersect with current array
 
 @return	An array containing only common elements in both arrays, without duplicates
 */
- (NSArray *) arrayIntersectedWithArray:(NSArray *)_pArray;									// array with common items

@end
