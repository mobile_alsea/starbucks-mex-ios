//
//  NSString_misc.h
//  DYCore
//
//  Created by Dylvian on 19/07/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSString
 @{
 @class NSString_misc
 @brief		Provide functions to work with strings. See header file NSString_misc.h for functions.
 */
/// @}
@interface NSString(misc)

/**
 @return A string converted to ascii encoding, removing all diacritic information of the string
 */
- (NSString *) asciiString;

/**
 @return The string's range
 */
- (NSRange) range;

- (NSString *) stringByLeftPaddingToLength:(NSInteger)_newLength withString:(NSString *)_padString;
- (NSString *) stringByRightPaddingToLength:(NSInteger)_newLength withString:(NSString *)_padString;

- (BOOL) containsString:(NSString *)_string;
- (BOOL) containsCaseInsensitiveString:(NSString *)_string;

@end
