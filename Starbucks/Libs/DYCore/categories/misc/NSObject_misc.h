//
//  DYReusable.h
//  DYWidgets
//
//  Created by Juan Antonio Romero on 31/05/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DYReusable <NSObject>

@property (nonatomic, retain) NSString *reuseIdentifier;

@end
