//
//  NSString_html.h
//  DYCore
//
//  Created by Dylvian on 23/09/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSString
 @{
 @class NSString_html
 @brief		Provide functions to work with HTML strings. See header file NSString_html.h for functions.
 */
/// @}
@interface NSString(html)

/**
 @return	A string with some tags and entities replaced.
 
 For example, <p> and </p> tags are replaced by a carriage return, so do <br> tags. Entities like &eacute; are replaced by their actual value (é).
 */
- (NSString *) stringByReplacingHTMLentities;
/**
 @return	A string without html tags
 
 All html tags are removed. A tag is any string enclosed between '<' and '>'
 */
- (NSString *) stringByRemovingHTMLTags;

@end
