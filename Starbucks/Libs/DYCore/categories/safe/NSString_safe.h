//
//  NSString_safe.h
//
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSString
 @{
 @class NSString_safe
 @brief		Improves safety in NSString methods by checking nil in certain circumstances. See header file NSString_safe.h for functions.
 */
/// @}
@interface NSString (safe)

/**
 @param[in]	_aString	First string to compare
 @param[in] _bString	Second string to compare
 
 @return	YES if both strings are equal, NO otherwise
 
 Two strings are considered to be equal if they are both nil or isEqualToString returns YES 
 */
+ (BOOL) safeIsEqualString:(NSString *)_aString ToString:(NSString *)_bString;
/**
 @param[in]	_aString	First string to compare
 @param[in] _bString	Second string to compare
 
 @return	NSOrderedAscending if _aString is nil and _bString is not nil, or compare returns NSOrderedAscending
			NSOrderedDescending if _aString is not nil and _bString is nil, or compare returns NSOrderedDescending
			NSOrderedSame if both string are nil or equal
 */
+ (NSComparisonResult) safeCompareString:(NSString *)_aString ToString:(NSString *)_bString;

@end
