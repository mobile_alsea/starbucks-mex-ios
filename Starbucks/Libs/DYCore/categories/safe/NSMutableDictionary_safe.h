//
//  NSMutableDictionary_safe.h
//
//  Created by Dylvian on 15/04/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSMutableDictionary
 @{
 @class NSMutableDictionary_safe
 @brief		Improves safety in NSMutableDictionary methods by checking certain circumstances. See header file NSMutableDictionary_safe.h for functions.
 */
/// @}
@interface NSMutableDictionary(safe)

/**
 @param[in] anObject	Object to insert
 @param[in] aKey	Key for object
 
 @brief	If anObject is not nil and aKey is not nil, anObject is inserted for aKey in dictionary
 */
- (void)setSafeObject:(id)anObject forKey:(id)aKey;
/**
 @param[in] anObject	Object to insert
 @param[in] aKey	Key for object
 
 @brief	If anObject is nil, it is replaced by [NSNull null]. If aKey is nil, it is replaced by [NSNull null]
 */
- (void)setSafeOrNullObject:(id)anObject forKey:(id)aKey;

@end
