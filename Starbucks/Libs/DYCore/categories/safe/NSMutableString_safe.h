//
//  NSMutableString_safe.h
//
//  Created by Dylvian on 14/04/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSMutableString
 @{
 @class NSMutableString_safe
 @brief		Improves safety in NSMutableString methods by checking certain circumstances. See header file NSMutableString_safe.h for functions.
 */
/// @}
@interface NSMutableString (safe)

/**
 @param[in]	aString	String to replace current string with
 
 @brief	If aString is nil, current string is replaced by an empty string
 */
- (void)setSafeString:(NSString *)aString;
/**
 @param[in]	aString	String to append to current string
 
 @brief	Appends aString to current string only if aString is not nil
 */
- (void)appendSafeString:(NSString *)aString;

@end
