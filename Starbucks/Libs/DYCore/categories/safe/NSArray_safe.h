//
//  NSArray_safe.h
//
//  Created by Dylvian on 07/05/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSArray
 @{
 @class NSArray_safe
 @brief		Improves safety in NSArray methods by checking certain circumstances. See header file NSArray_safe.h for functions.
 */
/// @}
@interface NSArray(safe) 

/**
 @param[in] _index
 
 @return	Object at _index, or nil if _index is out of bounds
 */
- (id) objectSafeAtIndex:(NSUInteger)_index;

@end
