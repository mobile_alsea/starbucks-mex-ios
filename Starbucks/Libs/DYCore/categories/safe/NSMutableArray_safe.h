//
//  NSMutableArray_safe.h
//
//  Created by Dylvian on 15/04/09.
//  Copyright 2009 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSMutableArray
 @{
 @class NSMutableArray_safe
 @brief		Improves safety in NSMutableArray methods by checking certain circumstances. See header file NSMutableArray_safe.h for functions.
 */
/// @}
@interface NSMutableArray(safe)

/**
 @param[in]	anObject	Object to insert
 
 Inserts anObject in array if it's not nil
 */
- (void)addSafeObject:(id)anObject;
/**
 @param[in]	anObject	Object to insert
 @param[in] index	Position to insert object
 
 Inserts anObject in array if it's not nil and index is not out of bounds
 */
- (void)insertSafeObject:(id)anObject atIndex:(NSUInteger)index;
/**
 @param[in]	anObject	Object to insert
 
 Inserts anObject in array if it's not nil, otherwise inserts [NSNull null]
 */
- (void)addSafeOrNullObject:(id)anObject;
/**
 @param[in]	anObject	Object to insert
 @param[in] index	Position to insert object
 
 Inserts anObject in array if it's not nil and index is not out of bounds, otherwise inserts [NSNull null]
 */
- (void)insertSafeOrNullObject:(id)anObject atIndex:(NSUInteger)index;
/**
 @param[in] _index	Position to insert object
 @param[in]	_pObject	Object to insert
 
 Replaces object at position _index with _pObject if it's not nil and index is not out of bounds
 */
- (void)replaceSafeObjectAtIndex:(NSUInteger)_index withObject:(id)_pObject;

@end
