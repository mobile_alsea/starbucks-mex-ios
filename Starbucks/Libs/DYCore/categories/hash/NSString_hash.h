//
//  NSString_hash.h
//  DNResources
//
//  Created by M2GRP Murcia on 01/06/09.
//  Copyright 2009 M2GRP. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSString
 @{
 @class NSString_hash
 @brief		Allows several hashes to be performed on strings. See header file NSString_hash.h for functions.
 
 <hr>
 
 @section Programming_Guide	Programming Guide
 
 @code
 {
 ...
 
 NSString *remoteString = [NSString stringWithContentsOfURL:[NSURL urlWithString:@"http://www.server.com/resource.txt"]];
 
 NSString *md5 = [remoteString MD5];	// lowercase MD5 hasg
 NSString *sha256 = [remoteString SHA256];	// lowercase SHA256
 NSString *sha512 = [remoteString SHA512];	// lowercase SHA512
 NSString *ripemd160 = [remoteString RipeMD160];	// lowercase RipeMD-160
 
 ...
 }
 @endcode
 */
/// @}
@interface NSString(hash)

/**
 @return	A string with the lowercase MD5 hash of current string
 */
- (NSString *) MD5;
- (NSData *) MD5Data;
/**
 @return	A string with the lowercase SHA256 hash of current string
 */
- (NSString *) SHA256;
/**
 @return	A string with the lowercase SHA512 hash of current string
 */
- (NSString *) SHA512;
/**
 @return	A string with the lowercase RipeMD-160 hash of current string
 */
- (NSString *) RipeMD160;

@end
