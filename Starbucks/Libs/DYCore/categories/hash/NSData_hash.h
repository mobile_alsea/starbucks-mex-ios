//
//  NSData_hash.h
//  DNResources
//
//  Created by M2GRP Murcia on 01/06/09.
//  Copyright 2009 M2GRP. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSData
 @{
 @class NSData_hash
 @brief		Allows several hashes to be performed on data. See header file NSData_hash.h for functions.
 
 <hr>
 
 @section Programming_Guide	Programming Guide
 
 @code
 {
 ...
 
 NSData *remoteData = [NSData dataWithContentsOfURL:[NSURL urlWithString:@"http://www.server.com/resource.txt"]];
 
 NSString *hash = [remoteData MD5];	// return an uppercase MD5 hash of remoteData
 
 ...
 }
 @endcode
 */
/// @}
@interface NSData(hash)

/**
 @return	A string with the uppercase MD5 hash of current data
 */
- (NSString *) MD5;
- (NSData *) MD5Data;

/**
 @return	Data Base64 encoded
 */
- (NSData *) base64Encode;
/**
 @return	Data decoded from Base64. Current data should be Base64 encoded
 */
- (NSData *) base64Decode;

- (NSData *) AESEncryptWithKey:(NSString *)_key;
- (NSData *) AESDecryptWithKey:(NSString *)_key;

- (NSData *) AESDecryptWithKeyData:(NSData *)_key initializationVector:(NSData *)_iv;

- (NSData *) TripleDESEncryptWithKey:(NSString *)_key;
- (NSData *) TripleDESDecryptWithKey:(NSString *)_key;

- (NSData *) TripleDESEncryptWithKeyData:(NSData *)_key;
- (NSData *) TripleDESDecryptWithKeyData:(NSData *)_key;

- (NSData *) DESDecryptWithKeyData:(NSData *)_key initializationVector:(NSData *)_iv;

@end
