//
//  NSDictionary_dictionary.h
//  DYCore
//
//  Created by Dylvian on 31/08/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSDictionary
 @{
 @class NSDictionary_dictionary
 @brief		Simplification of tasks in dictionaries. See header file NSDictionary_dictionary.h for functions.
 */
/// @}
@interface NSDictionary(dictionary)

/**
 @param[in]	_key	key of the object we want
 
 @return	An array with the contents of this key. If the object stored in this key is already an array, the object is returned unmodified. Otherwise, this method returns an array
 containing the object associated with this key.
 */
- (NSArray *) arrayForKey:(id)_key;
/**
 @param[in] _key	key of the object we want
 
 @return	If the object associated with _key responds to the copy selector, the object returned is a copy of the associated object that must be released by the caller. Otherwise,
 nil is returned.
 */
- (id) copyOfKey:(id)_key;

@end
