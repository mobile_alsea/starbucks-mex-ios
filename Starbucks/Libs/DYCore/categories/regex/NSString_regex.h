//
//  NSString_regex.h
//  DYCore
//
//  Created by Dylvian on 24/05/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (regex)

- (NSArray *) matchRegex:(NSString *)_regex;
- (NSArray *) matchRegex:(NSString *)_regex options:(NSRegularExpressionOptions)_options;

- (BOOL) isInteger;

- (BOOL) isValidEmail;

- (BOOL) isValidNIF;

- (BOOL) isValidNIE;

- (BOOL) isValidCIF;

@end
