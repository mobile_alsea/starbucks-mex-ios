//
//  NSSortDescriptor_kernel.h
//  RandomHouse
//
//  Created by Dylvian on 27/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSSortDescriptor
 @{
 @class NSSortDescriptor_kernel
 @brief		Provides common methods to access NSSortDescriptor objects from universal projects in which device iOS version is different. See header file NSSortDescriptor_kernel.h for functions.
 */
/// @}
@interface NSSortDescriptor(kernel)

/**
 @param[in]	_keyPath	name of the property to sort
 @param[in]	_ascending	sort order. If _ascending is YES, sort from lowest to highest, otherwise sort from highest to lowest
 
 @return	A sort descriptor with the specified options
 */
+ (NSSortDescriptor *) descriptorWithKey:(NSString *)_keyPath ascending:(BOOL)_ascending;

@end
