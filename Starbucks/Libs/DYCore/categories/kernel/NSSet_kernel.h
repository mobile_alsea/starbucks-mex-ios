//
//  NSSet_kernel.h
//  RandomHouse
//
//  Created by Dylvian on 27/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @addtogroup	NSSet
 @{
 @class NSSet_kernel
 @brief		Provides common methods to access NSSet objects from universal projects in which device iOS version is different. See header file NSSet_kernel.h for functions. 
 */
/// @}
@interface NSSet(kernel)

/**
 @param[in]	_sortDescriptors	array of sort descriptors to sort the objects in this set
 
 @return	an array with the objects in this set sorted according to _sortDescriptors
 */
- (NSArray *) arrayUsingDescriptors:(NSArray *)_sortDescriptors;

@end
