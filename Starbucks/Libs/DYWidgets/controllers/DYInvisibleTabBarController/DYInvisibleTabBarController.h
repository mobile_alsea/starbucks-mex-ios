//
//  DYInvisibleTabBarController.h
//  DYWidgets
//
//  Created by Dylvian on 29/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class	UITabBarController
 @addtogroup	DYInvisibleTabBarController
 @{
 @class DYInvisibleTabBarController
 
 @brief	TabBar controller with invisible tab bar
 
 When loaded, hides tab bar and expands the content view to match the controller frame. This is an easy way to get tab bar behaviour with "no tabBar" user interfaces
 */
/// @}
@interface DYInvisibleTabBarController : UITabBarController 
{  
}

@end
