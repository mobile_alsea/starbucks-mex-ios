//
//  DYMenuDockViewDataSource.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYMenuDockView;
@class DYMenuDockItem;

/**
 @enum DYMenuDockItemTransition	Transition type for the items in a menu panel
 */
typedef enum DYMenuDockItemTransition { DYMenuDockItemTransitionFlip = 0, DYMenuDockItemTransitionFade } DYMenuDockItemTransition;

/**
 @class NSObject
 @addtogroup	DYMenuView
 @{
 @protocol	DYMenuDockViewDataSource
 
 @brief	Protocol that a class must implement to provide items for a menu panel
 */
@protocol DYMenuDockViewDataSource <NSObject>
@required

/**
 @param[in]	_dockView	The menu panel asking for data
 
 @return	Implementor must return the number of items that _dockView will contain
 */
- (NSInteger) numberOfItemsInMenuDockView:(DYMenuDockView *)_dockView;
/**
 @param[in]	_dockView	The menu panel asking for data
 @param[in]	_index	Index of the item asked
 
 @return	Implementor must return the menu dock item for the item at _index
 */
- (DYMenuDockItem *) menuDockView:(DYMenuDockView *)_dockView itemAtIndex:(NSInteger)_index;

/**
 @param[in]	_dockView	The menu panel asking for data
 
 @return	Implementor must return the size of the items in _dockView
 */
- (CGSize) sizeOfItemsInMenuDockView:(DYMenuDockView *)_dockView;
/**
 @param[in]	_dockView	The menu panel asking for data
 
 @return	Implementor must return how many pixels images will overlap. Width stand for horizontal items overlapping, while height stands for vertical items overlapping
 
 Possitive sizes make the objects overlap, come closer to each other. Negative sizes increment the spacing between items.
 */
- (CGSize) sizeOfItemOverlapInMenuDockView:(DYMenuDockView *)_dockView;	// allow overlapping

/**
 @param[in]	_dockView	The menu panel asking for data
 
 @return	Implementor must return the transition type for the items in this menu panel
 */
- (DYMenuDockItemTransition) transitionOfItemsInMenuDockView:(DYMenuDockView *)_dockView;

@end

///@}