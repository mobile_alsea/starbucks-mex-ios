//
//  DYMenuDockViewDelegate.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class NSObject
 @addtogroup	DYMenuView
 @{
 @protocol	DYMenuDockViewDelegate
 
 @brief	Protocol that a class must implement to receive events from the items in a menu panel
 */
@protocol DYMenuDockViewDelegate
@required

/**
 @param[in]	_menuDockView	The menu panel which originated the event
 @param[in]	_selected	Whether the item is now selected or not
 @param[in]	_indexPath	Index path representing the item that changed
 
 Called in response to an action that changed the selection status
 */
- (void) menuDockView:(DYMenuDockView *)_menuDockView selectionDidChange:(BOOL)_selected indexPath:(NSIndexPath *)_indexPath;

@end

///@}