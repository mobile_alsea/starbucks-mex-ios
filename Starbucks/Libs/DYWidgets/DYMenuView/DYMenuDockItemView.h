//
//  DYMenuDockItemView.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYMenuDockItem;

/**
 @enum DYMenuDockItemViewTransition	Transition type, from selected to non-selected or from non-selected to selected
 */
typedef enum DYMenuDockItemViewTransition { DYMenuDockItemViewTransitionToSelected = 0, DYMenuDockItemViewTransitionToNonSelected } DYMenuDockItemViewTransition;

/**
 @class NSObject
 @addtogroup	DYMenuView 
 @{
 @protocol	DYMenuDockItemView
 
 @brief	Protocol that a class must implement to be a menu item 
 */
@protocol DYMenuDockItemView <NSObject>
@required

/**
 @param[in]	_item	Source item associated to this menu item
 
 Called so the implementing class can be configured with the item data
 */
- (void) setMenuDockItem:(DYMenuDockItem *)_item;
/**
 @param[in]	_selected	Whether the item is selected or not
 @param[in]	_animated	Whether the transition to _selected should be animated or not
 
 Called when a state transition should happen, that is, when the user taps on the menu item 
 */
- (void) setSelected:(BOOL)_selected animated:(BOOL)_animated;
/**
 @return	You must return whether the item is selected or not
 */
- (BOOL) isSelected;

@end

///@}