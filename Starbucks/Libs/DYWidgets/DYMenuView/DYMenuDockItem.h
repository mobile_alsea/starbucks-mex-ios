//
//  DYMenuDockItem.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 @class	NSObject
 @addtogroup	DYMenuView
 @{
 @class DYMenuDockItem
 
 @brief	An item in a menu panel. Any item can have subitems to create a menu hierarchy.
 
 @note Currently, only top level menu items can have subitems. Deeper subitems are not taken into account
 */
/// @}
@interface DYMenuDockItem : NSObject 
{
	UIImage *mNormalImage;
	UIImage *mSelectedImage;
	
	NSMutableArray *mChildArray;	// submenus
}

/**
 @property	UIImage	*normalImage	Image for the non-selected state of the menu item
 */
@property (nonatomic, readonly) UIImage *normalImage;
/**
 @property	UIImage	*selectedImage	Image for the selected state of the menu item
 */
@property (nonatomic, readonly) UIImage *selectedImage;
/**
 @property	NSMutableArray *childArray	Array of subitems
 */
@property (nonatomic, readonly) NSMutableArray *childArray;

/**
 @param[in]	_normalImage	Image for the non-selected state of the menu item
 @param[in]	_selectedImage	Image for the selected state of the menu item
 
 @return	A menu item initialized with the specified images
 */
- (id) initWithNormalImage:(UIImage *)_normalImage selectedImage:(UIImage *)_selectedImage;
/**
 @param[in]	_normalImage	Image for the non-selected state of the menu item
 @param[in]	_selectedImage	Image for the selected state of the menu item
 
 @return	A menu item initialized with the specified images
 */
+ (id) menuDockItemWithNormalImage:(UIImage *)_normalImage selectedImage:(UIImage *)_selectedImage;

/**
 @param[in]	_childItem	Subitem to add to this item hierarchy
 */
- (void) addChild:(DYMenuDockItem *)_childItem;

@end
