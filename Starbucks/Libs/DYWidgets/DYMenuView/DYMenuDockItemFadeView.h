//
//  DYMenuDockItemFadeView.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYAnimatableView/DYAnimatableView.h>

#import "DYMenuDockItemView.h"

/**
 @class	DYAnimatableView
 @protocol DYMenuDockItemView
 @addtogroup	DYMenuView
 @{
 @class DYMenuDockItemFadeView
 
 @brief	Class that represent menu items whose transition is a fade between images. This class is used internally
 */
/// @}
@interface DYMenuDockItemFadeView : DYAnimatableView <DYMenuDockItemView>
{
	BOOL mIsSelected;
	
	UIImageView *mNormalView;
	UIImageView *mSelectedView;
	
	DYMenuDockItemViewTransition mTransition;
}

@end
