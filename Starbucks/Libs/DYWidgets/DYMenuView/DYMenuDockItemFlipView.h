//
//  DYMenuDockItemFlipView.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DYMenuDockItemView.h"

#import <DYWidgets/DYAnimatableView/DYTransitionView.h>

/**
 @class	DYTransitionView
 @protocol DYMenuDockItemView
 @addtogroup	DYMenuView
 @{
 @class DYMenuDockItemFlipView
 
 @brief	Class that represent menu items whose transition is a flip of the images. This class is used internally
 */
/// @}
@interface DYMenuDockItemFlipView : DYTransitionView <DYMenuDockItemView>
{
	BOOL mIsSelected;
}

@end
