//
//  DYMenuView.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYMenuDockView.h"

/**
 @class	UIView
 @addtogroup	DYMenuView
 @{
 @class DYMenuView
 
 @brief	A view that can contain four menu panels: top, bottom, left and right.
 
 By default, a menu panel frame will be defined by this view frame and the item size and number of children. If a menu is too big for the screen, items will be placed in a scroll view, so all of the
 items can be selected.
 */
/// @}
@interface DYMenuView : UIView 
{
	NSMutableDictionary *mDockViewDictionary;
}

/**
 @param[in]	_position	Position of the desired menu panel
 
 @return	The menu panel at _position. If it doesn't exist yet, a menu panel is created and returned empty
 */
- (DYMenuDockView *) dockViewAtPosition:(DYMenuDockViewPosition)_position;

@end
