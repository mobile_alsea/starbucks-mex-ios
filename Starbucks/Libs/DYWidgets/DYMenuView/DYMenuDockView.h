//
//  DYMenuDockView.h
//  iressa
//
//  Created by Dylvian on 01/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DYMenuDockViewDataSource.h"
#import "DYMenuDockViewDelegate.h"

#import <DYWidgets/DYAnimatableView/DYAnimatableView.h>

/**
 @enum	DYMenuDockViewPosition	Panel position: top, right, bottom or left
 */
typedef enum DYMenuDockViewPosition {	DYMenuDockViewPositionUp = 1 << 0, 
										DYMenuDockViewPositionRight = 1 << 1,
										DYMenuDockViewPositionBottom = 1 << 2,
										DYMenuDockViewPositionLeft = 1 << 3 } DYMenuDockViewPosition;

/**
 @enum	DYMenuDockViewBehaviour	Behaviour of the items in a menu panel: toggle selection or always select
 */
typedef enum DYMenuDockViewBehaviour {	DYMenuDockViewBehaviourAlwaysSelect = 1 << 0,
										DYMenuDockViewBehaviourToggle = 1 << 1 } DYMenuDockViewBehaviour;

/**
 @class	DYAnimatableView
 @addtogroup	DYMenuView
 @{
 @class DYMenuDockView
 
 @brief	Each instance of this class is a menu panel that contains menu items
 */
/// @}
@interface DYMenuDockView : DYAnimatableView 
{
	UIScrollView *mScrollView;	// base scrollview
	
	DYMenuDockViewPosition mPosition;	// used for show and hide
	DYMenuDockViewBehaviour mBehaviour;
	
	CGSize mPadding;
	BOOL mAllowsMultipleSelection;
	NSMutableArray *mSelectionArray;
	
	id<DYMenuDockViewDataSource> mDataSource;
	id<DYMenuDockViewDelegate> mDelegate;
	// fast access
	NSInteger mIconCount;
	NSInteger mChildIconCount;
	CGSize mIconSize;
	CGSize mIconOverlapSize;
	CGSize mMenuSize;
	
	NSMutableDictionary *mViewDictionary;
	
	UITapGestureRecognizer *mTapGestureRecognizer;
	BOOL mIsVisible;
}

/**
 @property	id<DYMenuDockViewDataSource> dataSource	Object that will provide items to this menu panel
 */
@property (nonatomic, assign)	IBOutlet id<DYMenuDockViewDataSource> dataSource;
/**
 @property	id<DYMenuDockViewDelegate> delegate	Object that will be notified of events in the menu panel
 */
@property (nonatomic, assign)	IBOutlet id<DYMenuDockViewDelegate> delegate;

/**
 @property	DYMenuDockViewPosition position	Menu panel position
 */
@property (nonatomic)	DYMenuDockViewPosition position;
/**
 @property	DYMenuDockViewBehaviour behaviour	Menu panel item selection behaviour
 */
@property (nonatomic)	DYMenuDockViewBehaviour behaviour;
/**
 @property	CGSize padding	Blank space between the items and the border of the menu panel
 */
@property (nonatomic)	CGSize padding;
/**
 @property	BOOL allowsMultipleSelection	If multiple selection is allowed, several menu items can be selected simultaneously. Otherwise, only one item can be selected. If the user selects an item
 while other item is selected, the previously selected item changes to non-selected.
 */
@property (nonatomic)	BOOL allowsMultipleSelection;

@property (nonatomic, readonly) NSMutableArray *selectionArray;

/**
 @param[in]	_selectionArray	Array of NSIndexPath of selected items
 
 This method is used to set the selected state of the objects at the index paths specified in _selectedArray
 */
- (void) setSelectedArray:(NSArray *)_selectionArray;
/**
 @param[in]	_index	Index path of the item to select
 
 Set the selected state for the object at the index path specified by _index
 */
- (void) addSelection:(NSIndexPath *)_index;
/**
 @brief	Toggle all items to non-selected state, that is, deselect all items
 */
- (void) clearSelection;

/**
 @brief	Force an item refresh by asking the data source for the items in this menu panel
 */
- (void) reloadData;

/**
 @param[in]	_visible	Whether this menu panel should be visible or not
 @param[in]	_animated	Whether the transition should be animated or not
 
 Shows or hides this menu panel according to parameters
 */
- (void) setVisible:(BOOL)_visible animated:(BOOL)_animated;

@end
