//
//  DYPageControl.h
//  DYWidgets
//
//  Created by Dylvian on 07/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class	UIControl
 @addtogroup	DYPageControl
 @{
 @class DYPageControl
 
 @brief	Customizable page control
 */
/// @}
@interface DYPageControl : UIControl 
{
	NSInteger	mCurrentPage;
	NSInteger	mNumberOfPages;
	BOOL		mHidesForSinglePage;
	
	UIImage *mActiveImage;
	UIImage *mInactiveImage;
	
	NSInteger mDotWidth;
	NSInteger mDotHeight;
	NSInteger mDotMargin;
	
	UIColor *mActiveColor;
	UIColor *mInactiveColor;
	
	UITapGestureRecognizer *mTapGestureRecognizer;
}

/**
 @property	NSInteger currentPage	Current page, pages start at 0
 */
@property (nonatomic)	NSInteger currentPage;
/**
 @property	NSInteger numberOfPages	Number of pages
 */
@property (nonatomic)	NSInteger numberOfPages;
/**
 @property	BOOL hidesForSinglePage	Don't draw any dot if is set to YES and there is only one page
 */
@property (nonatomic)	BOOL hidesForSinglePage;

/**
 @property UIImage *activeImage	Image for the current page
 */
@property (nonatomic, retain)	UIImage *activeImage;
/**
 @property UIImage *inactiveImage	Image for all pages except the current page
 */
@property (nonatomic, retain)	UIImage *inactiveImage;

/**
 @property NSInteger dotWidth	width of each dot in pixels
 */
@property (nonatomic)	NSInteger dotWidth;
/**
 @property NSInteger dotHeight	height of each dot in pixels
 */
@property (nonatomic)	NSInteger dotHeight;
/**
 @property NSInteger dotMargin	space between the actual dot and the dot frame
 */
@property (nonatomic)	NSInteger dotMargin;

/**
 @property UIColor *activeColor	Color of the current page, if no image is set
 */
@property (nonatomic, retain)	UIColor *activeColor;
/**
 @property UIColor *inactiveColor	Color of all pages except the current page, if no image is set
 */
@property (nonatomic, retain)	UIColor *inactiveColor;

- (void) setCurrentPage:(NSInteger)currentPage sendActions:(BOOL)_sendActions;

@end
