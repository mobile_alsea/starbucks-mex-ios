//
//  DYImageCache.h
//  DYWidgets
//
//  Created by Dylvian on 06/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 @class	NSObject
 @addtogroup	DYImageView
 @{
 @class DYImageCache
 
 @brief	Image cache to reuse previously retrieved images
 */
/// @}
@interface DYImageCache : NSCache 
{
	//NSMutableDictionary *mImageCacheDictionary;
	NSMutableDictionary *mLockDictionary;
}

/**
 @return	The singleton shared instance
 */
+ (DYImageCache *) sharedInstance;

/**
 @param[in]	_url	URL of the image
 
 @return	An UIImage initialized with the contents of _url, or nil if the URL is not valid or is not an image, or some error occurred
 */
+ (UIImage *) imageForURL:(NSURL *)_url;

/**
 @param[in]	_image	Image
 @param[in]	_url	URL associated with _image
 
 Stores _image in cache, as if it had been downloaded from _url
 */
+ (void) setImage:(UIImage *)_image forURL:(NSURL *)_url;
/**
 @param[in]	_image	Image
 @param[in]	_url	URL associated with _image
 @param[in] _size	Size in bytes of the image
 
 Stores _image in cache, as if it had been downloaded from _url
 */
+ (void) setImage:(UIImage *)_image forURL:(NSURL *)_url size:(NSInteger)_size;

//+ (void) clear;

+ (void) markStartForURL:(NSURL *)_url;
+ (void) markEndForURL:(NSURL *)_url;

@end
