//
//  DYImageView.h
//  DYWidgets
//
//  Created by Dylvian on 06/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @def DYImageViewDidFailLoadingNotification	Notification to be sent when an image failed loading
 */
#define DYImageViewDidFailLoadingNotification	@"DYImageViewDidFailLoadingNotification"
/**
 @def DYImageViewDidFinishLoadingNotification	Notification to be sent when an image finished loading successfully
 */
#define DYImageViewDidFinishLoadingNotification	@"DYImageViewDidFinishLoadingNotification"

/**
 @enum DYImageViewAnimationStyle	style of the activity indicator shown while loading
 */
typedef enum DYImageViewAnimationStyle
{
	DYImageViewAnimationStyleNone = 0,
	DYImageViewAnimationStyleGray,
	DYImageViewAnimationStyleWhite,
	DYImageViewAnimationStyleWhiteLarge
}DYImageViewAnimationStyle;

/**
 @class	UIImageView
 @addtogroup	DYImageView
 @{
 @class DYImageView
 
 @brief	Image view showing an activity indicator while the actual image is being fetched from Internet
 */
/// @}
@interface DYImageView : UIImageView 
{
	UIActivityIndicatorView *mActivityIndicator;
	
	NSURL *mUrl;
	
	DYImageViewAnimationStyle mAnimationStyle;
	
	UIImage *mFailureImage;
	NSString *mLockGroup;
}

/**
 @property	NSURL url	URL of the image
 */
@property (nonatomic, retain)	NSURL	*url;
/**
 @property	DYImageViewAnimationStyle animationStyle	style of the activity indicator shown while loading
 */
@property (nonatomic)			DYImageViewAnimationStyle animationStyle;

@property (nonatomic, readonly)	UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) UIImage *failureImage;

@property (nonatomic, copy) NSString *lockGroup;

@end
