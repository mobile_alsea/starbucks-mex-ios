//
//  DYTextView.h
//

#import <UIKit/UIKit.h>
#import <DYWidgets/DYTextView/DYEventTextView.h>

/**
 @class	UIView
 @protocol UIScrollViewDelegate
 @addtogroup	DYTextView
 @{
 @class DYTextView
 
 @brief	Improved text view with header and footer support
 */
/// @}
@interface DYTextView : UIView <UIScrollViewDelegate>
{
	UIView *mHeaderView;
	UIView *mFooterView;
	
	UIView *mSpecialTouchView;
	CGRect mOriginalFrame;
	
	DYEventTextView *mTextView;
	//UIView	*mpCaptureView;
	
	// Insets
	NSUInteger	mHeaderInset;
	NSUInteger	mFooterPosition;
}

/**
 @property UIView *specialTouchView	view that will receive touch events
 */
@property (nonatomic, retain) IBOutlet UIView *specialTouchView;

/**
 @param[in]	_headerView	view to be used as a header
 */
- (void) setHeaderView:(UIView *)_headerView;
/**
 @param[in]	_footerView	view to be used as a footer
 */
- (void) setFooterView:(UIView *)_footerView;

/**
 @param[in]	_text	Text in the text view
 */
- (void) setText:(NSString *)_text;

/**
 @brief	Reset text view
 */
- (void) resetTextView;
/**
 @brief	Moves text view to the origin
 */
- (void) scrollToTop;

/**
 @param[in]	_textColor	Color of the text
 */
- (void) setTextColor:(UIColor *)_textColor;
/**
 @param[in]	_font	Font of the text to be used
 */
- (void) setFont:(UIFont *)_font;

@end
