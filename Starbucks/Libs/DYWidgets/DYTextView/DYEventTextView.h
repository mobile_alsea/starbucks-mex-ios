//
//  DYEventTextView.h
//

#import <UIKit/UIKit.h>

/**
 @class	UITextView
 @addtogroup	DYTextView
 @{
 @class DYEventTextView
 
 @brief	Text view capturing touch events
 */
/// @}
@interface DYEventTextView : UITextView 
{
	UIView *mHeaderView;
	UIView *mFooterView;
	UIView *mSpecialTouchView;
	CGRect mOriginalFrame;
	
	bool	mHeaderTouchDown;
	bool	mFooterTouchDown;
	
	CGPoint	mBeginningTouchPoint;
}

/**
 @property	UIView *headerView	view to be used as header
 */
@property	(nonatomic, retain)	UIView	*headerView;
/**
 @property	UIView *footerView	view to be used as footer
 */
@property	(nonatomic, retain)	UIView	*footerView;
/**
 @property	UIView *specialTouchView	view that will intercept touches
 */
@property	(nonatomic, retain) UIView	*specialTouchView;

@end
