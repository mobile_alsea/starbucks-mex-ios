//
//  DYPDFPageViewSelection.h
//  pruebaDYPDFDocument
//
//  Created by Dylvian on 25/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

/**
 @enum	DYPDFPageViewSelectionMode	Selection behaviour: replace selections or add selections
 */
typedef enum DYPDFPageViewSelectionMode { DYPDFPageViewSelectionModeReplace = 0, DYPDFPageViewSelectionModeAdd } DYPDFPageViewSelectionMode;

/**
 @class	NSObject
 @addtogroup	DYPDFPageView
 @{
 @class DYPDFPageViewSelection
 
 @brief	Represent a selection in a PDF page
 */
/// @}
@interface DYPDFPageViewSelection : NSObject 
{
	CGRect mSelectionRect;
	CGRect mEnclosingRect;
	
	UIColor *mColor;
}

/**
 @property	CGRect selection	Selection frame in PDF space
 */
@property (nonatomic, readonly) CGRect selection;
/**
 @property	CGRect enclosing	Enclosing frame of the selection in PDF space
 */
@property (nonatomic, readonly) CGRect enclosing;
/**
 @property	UIColor *color	Selection color
 */
@property (nonatomic, readonly) UIColor *color;

/**
 @param[in]	_selection	frame of the selection in PDF space
 @param[in] _enclosing	enclosing frame of the selection in PDF space
 @param[in]	_color	selection color
 
 @return	An object initialized with the specified parameters
 */
- (id) initWithSelection:(CGRect)_selection enclosing:(CGRect)_enclosing color:(UIColor *)_color;

@end
