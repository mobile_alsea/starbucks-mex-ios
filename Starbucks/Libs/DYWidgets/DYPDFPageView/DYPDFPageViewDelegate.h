//
//  DYPDFPageViewDelegate.h
//  pruebaDYPDFDocument
//
//  Created by Dylvian on 21/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYDocument/PDF/DYPDFImage.h>
#import <DYDocument/PDF/DYPDFAnnotation.h>

@class DYPDFPageView;
@class DYPDFPageViewSelection;

/**
 @class NSObject
 @addtogroup	DYPDFPageView
 @{
 @protocol	DYPDFPageViewDelegate
 
 @brief	Protocol that a class must implement to receive events from PDF page view
 */
@protocol DYPDFPageViewDelegate <NSObject>
@optional

/**
 @param[in]	_pageView	PDF page view that caused the event
 @param[in]	_image	Image that was double tapped in the PDF page view
 @param[in]	_imageFrame	Frame of the image in PDF space
 
 Called in response to a double tap on a image in a PDF page
 */
- (void) pageView:(DYPDFPageView *)_pageView didTapImage:(DYPDFImage *)_image frame:(CGRect)_imageFrame;
/**
 @param[in]	_pageView	PDF page view that caused the event
 @param[in]	_annotation	Annotation that was double tapped in the PDF page view
 @param[in]	_annotationFrame	Frame of the annotation in PDF space
 
 Called in response to a double tap on an annotation in a PDF page
 */
- (void) pageView:(DYPDFPageView *)_pageView didTapAnnotation:(DYPDFAnnotation *)_annotation frame:(CGRect)_annotationFrame;

/**
 @param[in]	_pageView	PDF page view that caused the event
 @param[in]	_text	Text in the selection
 @param[in]	_selection	Selection
 
 Called in response to a selection in a PDF page view
 */
- (void) pageView:(DYPDFPageView *)_pageView didSelectText:(NSString *)_text selection:(DYPDFPageViewSelection *)_selection;

@end

///@}