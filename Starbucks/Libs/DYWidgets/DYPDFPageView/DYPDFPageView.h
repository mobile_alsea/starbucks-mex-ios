//
//  DYPDFPageView.h
//  pruebaDYPDFDocument
//
//  Created by Dylvian on 20/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DYDocument/PDF/DYPDFPage.h>

#import "DYPDFPageViewDelegate.h"
#import "DYPDFPageViewSelection.h"

/**
 @class	NSObject
 @addtogroup	DYPDFPageView
 @{
 @class DYPDFPageView
 
 @brief	A view that draws PDF pages with selections and annotations
 */
/// @}
@interface DYPDFPageView : UIView 
{
	DYPDFPage *mPage;
	
	NSMutableString *mSearchString;
	NSMutableArray *mSearchArray;
	UIColor *mSearchColor;
	
	BOOL mNightMode;
	
	BOOL mHighlightMode;
	BOOL mShowHighlights;
	UIColor *mHighlightColor;
	NSMutableArray *mHighlightArray;
	
	BOOL mShowAnnotations;
	UIColor *mAnnotationColor;
	NSMutableArray *mAnnotationArray;
	
	NSMutableArray *mImageArray;
	
	id<DYPDFPageViewDelegate> mDelegate;
	
	UITapGestureRecognizer *mTapRecognizer;
	BOOL mScaleToFill;
	
	// highlight
	CGPoint mBeginPoint;
	CGPoint mEndPoint;
	DYPDFPageViewSelectionMode mSelectionMode;
	BOOL mIsHighlighting;
}

/**
 @property	UIColor *searchColor	Color of the search results
 */
@property (nonatomic, retain)	UIColor *searchColor;
/**
 @property	UIColor *highlightColor	Color of the highlights (selections)
 */
@property (nonatomic, retain)	UIColor *highlightColor;
/**
 @property	UIColor *annotationColor	Color of the annotations
 */
@property (nonatomic, retain)	UIColor *annotationColor;

/**
 @property	BOOL isNightMode	View is in night mode
 */
@property (nonatomic, readonly)	BOOL	isNightMode;
/**
 @property	BOOL isHighlighting	View is in selection mode
 */
@property (nonatomic, readonly) BOOL	isHighlighting;
/**
 @property	BOOL isShowingHighlights	View is drawing selections
 */
@property (nonatomic, readonly) BOOL	isShowingHighlights;
/**
 @property	BOOL isShowingAnnotations	View is drawing annotations
 */
@property (nonatomic, readonly) BOOL	isShowingAnnotations;

/**
 @property	id<DYPDFPageViewDelegate> delegate	Delegate that will receive notifications of events in a PDF page view
 */
@property (nonatomic, assign)	id<DYPDFPageViewDelegate> delegate;

/**
 @param[in]	_page	Page to show
 
 Draws the new page and clears the search results
 */
- (void) setPage:(DYPDFPage *)_page;	// clear search
/**
 @param[in]	_page	Page to show
 @param[in]	_searchString	String to search in the page
 
 Draws the new page and then searches for _searchString in the page
 */
- (void) setPage:(DYPDFPage *)_page search:(NSString *)_searchString;
/**
 @param[in]	_searchString
 
 Searches _searchString in the current page
 */
- (void) setSearch:(NSString *)_searchString;
/**
 @return	Current drawn page
 */
- (DYPDFPage *) page;

/**
 @param[in]	_nightMode	Night mode status
 
 Toggle night mode
 */
- (void) setNightMode:(BOOL)_nightMode;
/**
 @param[in]	_highlightActive	Selection mode. If YES, touch and drag select text in the PDF page
 
 Toggle selection mode
 */
- (void) setHighlightMode:(BOOL)_highlightActive;
/**
 @param[in]	_showAnnotations	Show annotation status
 
 Toggle annotation visibility
 */
- (void) setShowAnnotations:(BOOL)_showAnnotations;
/**
 @param[in]	_showHighlights	Show selection status
 
 Toggle selection visibility
 */
- (void) setShowHighlights:(BOOL)_showHighlights;
/**
 @param[in]	_selectionMode	Selection mode
 
 Set selection mode
 */
- (void) setSelectionMode:(DYPDFPageViewSelectionMode)_selectionMode;

/**
 @brief	Clear selections in this page
 */
- (void) clearHighlights;
/**
 @param[in]	_rect	Selection to highlight
 
 Clear current selections and select the specified selection
 */
- (void) highlightSelection:(DYPDFPageViewSelection *)_rect;
/**
 @param[in]	_rect	Selection to highlight
 
 Add the specified selection to the page current selections
 */
- (void) addHighlightingSelection:(DYPDFPageViewSelection *)_rect;
/**
 @param[in]	_rectArray	Array of DYPDFPageViewSelection selections to select
 */
- (void) highlightTextInArray:(NSArray *)_rectArray;

/**
 @param[in]	_scaleToFill	Whether PDF should be scaled to fill the whole view, in case view space is larger than PDF space
 */
- (void) setScaleToFill:(BOOL)_scaleToFill;
/**
 @return Whether the PDF page is being scaled up
 */
- (BOOL) isScalingToFill;

/**
 @return	A matrix to transform from PDF space to view space
 */
- (CGAffineTransform) pdfSpaceToViewSpaceMatrix;

/**
 @return	Whether the page has selections
 */
- (BOOL) hasSelections;

@end
