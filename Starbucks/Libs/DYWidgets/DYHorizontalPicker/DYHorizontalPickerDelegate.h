//
//  DYHorizontalPickerDelegate.h
//  DYWidgets
//
//  Created by Dylvian on 07/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYHorizontalPicker;

/**
 @class NSObject
 @addtogroup	DYHorizontalPicker
 @{
 @protocol	DYHorizontalPickerDelegate
 
 @brief	Protocol to receive events from an horizontal picker
 */
@protocol DYHorizontalPickerDelegate <NSObject>
@required

@optional

/**
 @param[in]	_picker	Horizontal picker that caused the event
 @param[in]	_page	Selected item
 
 Called when a page goes offscreen to allow memory recycling
 */
- (void) DYHorizontalPicker:(DYHorizontalPicker *)_picker didHidePage:(NSInteger)_page;

/**
 @param[in]	_picker	Horizontal picker that caused the event
 @param[in]	_page	Selected item
 
 Called when a different item is selected in an horizontal picker
 */
- (void) DYHorizontalPicker:(DYHorizontalPicker *)_picker didChangeToSection:(NSInteger)_page;
- (void) DYHorizontalPicker:(DYHorizontalPicker *)_picker didChangeToSection:(NSInteger)_page isReloading:(BOOL)_isReloading;

@end

///@}