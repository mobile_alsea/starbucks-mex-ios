//
//  DYHorizontalPicker.h
//  DYWidgets
//
//  Created by Dylvian on 07/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYHorizontalPicker/DYHorizontalPickerDataSource.h>
#import <DYWidgets/DYHorizontalPicker/DYHorizontalPickerDelegate.h>

/**
 @class	UIScrollView
 @protocol UIScrollViewDelegate
 @addtogroup	DYHorizontalPicker
 @{
 @class DYHorizontalPicker
 
 @brief	Horizontal picker. Paged scrollview with selectable options
 */
/// @}
@interface DYHorizontalPicker : UIScrollView <UIScrollViewDelegate>
{
	id<DYHorizontalPickerDataSource>	mDataSource;
	id<DYHorizontalPickerDelegate>		mDelegate;

	NSUInteger mViewCount;
	NSInteger mCurrentPage;
	//NSMutableArray *mViewArray;
	NSMutableDictionary *mViewDictionary;
	
	CGFloat	mViewWidth;
	
	CGFloat mVisibleWidth;
	CGFloat mMargin;
	
	NSInteger mLeftPage;
	NSInteger mRightPage;
	
	UIPanGestureRecognizer *mPanGestureRecognizer;
	UISwipeGestureRecognizer *mSwipeGestureRecognizer;
	UIGestureRecognizer *mOtherGestureRecognizer;
}

@property (nonatomic, readonly) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, readonly) UISwipeGestureRecognizer *swipeGestureRecognizer;
@property (nonatomic, readonly) UIGestureRecognizer *otherGestureRecognizer;

/**
 @property id<DYHorizontalPickerDataSource>	dataSource	data source of the horizontal picker
 */
@property (nonatomic, assign)	IBOutlet	id<DYHorizontalPickerDataSource>	dataSource;
/**
 @property id<DYHorizontalPickerDelegate>	delegate	object that will receive notifications of events in the horizontal picker
 */
@property (nonatomic, assign)	IBOutlet	id<DYHorizontalPickerDelegate, UIScrollViewDelegate>		delegate;

/**
 @brief	Asks the data source for the picker items
 */
- (void) reloadData;

/**
 @brief	Selects the item to the left of the current item
 */
- (void) scrollLeft;
/**
 @brief	Selects the item to the right of the current item
 */
- (void) scrollRight;

/**
 @param[in]	_width	Visible area of the horizontal picker

 As only the visible items are shown in the page and the visible area may be larger than the scrollview bounds, this value is used to calculate how many items need to be fetched from the data source
 */
- (void) setVisibleWidth:(CGFloat)_width;

- (NSInteger) currentPage;
- (void) setCurrentPage:(NSInteger)_page animated:(BOOL)_animated;

@end
