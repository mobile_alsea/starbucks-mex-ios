//
//  DYHorizontalPickerDataSource.h
//  DYWidgets
//
//  Created by Dylvian on 07/10/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYHorizontalPicker;

/**
 @class NSObject
 @addtogroup	DYHorizontalPicker
 @{
 @protocol	DYHorizontalPickerDataSource
 
 @brief	Protocol to configure the items in an horizontal picker
 */
@protocol DYHorizontalPickerDataSource <NSObject>
@required

/**
 @param[in]	_picker	Horizontal picker asking for data
 
 @return	Implementor must return the number of items in the horizontal picker
 */
- (NSUInteger) numberOfSectionsInPicker:(DYHorizontalPicker *)_picker;
/**
 @param[in]	_picker	Horizontal picker asking for data
 
 @return	Implementor must return the width of each item in pixels
 */
- (CGFloat) widthOfSectionsInPicker:(DYHorizontalPicker *)_picker;

@optional

/**
 @param[in]	_picker	Horizontal picker asking for data
 @param[in] _index	item index.
 
 @return	Implementor should return a title for the item at _index
 
 Implementors may implement this method to create text label items.
 */
- (NSString *) DYHorizontalPicker:(DYHorizontalPicker *)_picker titleForSectionAtIndex:(NSUInteger)_index;
/**
 @param[in]	_picker	Horizontal picker asking for data
 
 @return	Implementor should return the color of the labels in a picker
 
 This method is only called if data is retrieved with - (NSString *) DYHorizontalPicker:(DYHorizontalPicker *)_picker titleForSectionAtIndex:(NSUInteger)_index;
 */
- (UIColor *) textColorInPicker:(DYHorizontalPicker *)_picker;
/**
 @param[in]	_picker	Horizontal picker asking for data
 
 @return	Implementor should return the font of the labels in a picker
 
 This method is only called if data is retrieved with - (NSString *) DYHorizontalPicker:(DYHorizontalPicker *)_picker titleForSectionAtIndex:(NSUInteger)_index;
 */
- (UIFont *) fontInPicker:(DYHorizontalPicker *)_picker;

/**
 @param[in]	_picker	Horizontal picker asking for data
 @param[in] _index	item index.
 
 @return	Implementor should return a view for the item at _index
 
 Implementors may implement this method to create customized items.
 */
- (UIView *) DYHorizontalPicker:(DYHorizontalPicker *)_picker viewForSectionAtIndex:(NSUInteger)_index;

@end

///@}