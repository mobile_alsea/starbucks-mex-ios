//
//  DYDialogViewDelegate.h
//  privalia
//
//  Created by Dylvian on 10/05/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYDialogView;

@protocol DYDialogViewDelegate <NSObject>

- (void) dialogViewDidCancel:(DYDialogView *)_dialog;
- (void) dialogViewDidAccept:(DYDialogView *)_dialog;

@end
