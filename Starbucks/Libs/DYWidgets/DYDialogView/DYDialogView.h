//
//  DYDialogView.h
//  privalia
//
//  Created by Dylvian on 10/05/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYView/DYView.h>
#import <DYWidgets/DYLayoutView/DYVerticalLayoutView.h>
#import <DYWidgets/DYHorizontalStretchButton/DYHorizontalStretchButton.h>

#import "DYDialogViewDelegate.h"

@interface DYDialogView : DYView 
{
    UIImageView *mBackgroundImageView;
	
	DYVerticalLayoutView *mLayoutView;
	
	DYHorizontalStretchButton *mCancelButton;
	DYHorizontalStretchButton *mAcceptButton;
	
	id<DYLayoutViewDataSource>	mLayoutDataSource;
	id<DYDialogViewDelegate>	mDelegate;
}

@property (nonatomic, retain)	IBOutlet	UIImageView *backgroundImageView;
@property (nonatomic, retain)	IBOutlet	DYVerticalLayoutView *layoutView;
@property (nonatomic, retain)	IBOutlet	DYHorizontalStretchButton *cancelButton;
@property (nonatomic, retain)	IBOutlet	DYHorizontalStretchButton *acceptButton;
@property (nonatomic, retain)	IBOutlet	id<DYLayoutViewDataSource> layoutDataSource;
@property (nonatomic, retain)	IBOutlet	id<DYDialogViewDelegate> delegate;

- (void) setBackgroundImage:(UIImage *)_image;

@end
