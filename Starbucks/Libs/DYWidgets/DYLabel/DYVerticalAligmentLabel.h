//
//  DYVerticalAligmentLabel.h
//
//  Created by Alvaro Villares Fernandez on 08/02/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

// http://discussions.apple.com/thread.jspa?messageID=8322622

#import <UIKit/UIKit.h>

/**
 @enum DYVerticalAlignment	Vertical alignment position
 */
typedef enum DYVerticalAlignment 
{
    DYVerticalAlignmentTop, // Default
    DYVerticalAlignmentMiddle,
    DYVerticalAlignmentBottom,

} DYVerticalAlignment;

/**
 @class	UILabel
 @addtogroup	DYLabel
 @{
 @class DYVerticalAlignmentLabel
 
 @brief	UILabel with vertical alignment
 */
/// @}
@interface DYVerticalAligmentLabel : UILabel 
{
	DYVerticalAlignment mVerticalAlignment;
}

/**
 @property DYVerticalAlignment verticalAlignment	Vertical alignment position
 */
@property (nonatomic) DYVerticalAlignment verticalAlignment;

@end
