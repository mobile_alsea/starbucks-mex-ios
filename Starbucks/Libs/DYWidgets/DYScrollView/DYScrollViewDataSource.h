//
//  DYScrollViewDataSource.h
//  DYWidgets
//
//  Created by Dylvian on 28/09/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYScrollView;

/**
 @class NSObject
 @addtogroup	DYScrollView
 @{
 @protocol	DYScrollViewDataSource
 
 @brief	Protocol to configure the items in a scroll view
 */
@protocol DYScrollViewDataSource <NSObject>
@required

/**
 @param[in]	_scrollView	Scroll view that caused the event
 
 @return Implementor must return the number of items in this scroll view
 */
- (NSUInteger) numberOfPagesInScrollView:(DYScrollView *)_scrollView;

/**
 @param[in]	_scrollView	Scroll view that caused the event
 @param[in]	_page	Index of the item to retrieve
 
 @return Implementor must return the view for the item at _page in _scrollView
 */
- (UIView *) scrollView:(DYScrollView *)_scrollView viewForPage:(NSUInteger)_page;
/**
 @param[in]	_scrollView	Scroll view that caused the event
 @param[in]	_page	Index of the item to retrieve
 
 @return Implementor must return the size of the view for the item at _page in _scrollView
 */
- (CGSize) scrollView:(DYScrollView *)_scrollView preferredSizeForPage:(NSUInteger)_page;

/**
 @param[in]	_scrollView	Scroll view that caused the event
 @param[in]	_page	Index of the item to retrieve
 
 @return Implementor must return the view for the item at _page in _scrollView used when view is scaled
 */
- (UIView *) scrollView:(DYScrollView *)_scrollView viewForZoomingInPage:(NSUInteger)_page;
/**
 @param[in]	_scrollView	Scroll view that caused the event
 @param[in]	_page	Index of the item to retrieve
 
 @return Implementor must return the minimum zoom scale for the view for the item at _page in _scrollView
 */
- (CGFloat) scrollView:(DYScrollView *)_scrollView minimumZoomScaleForPage:(NSUInteger)_page;
/**
 @param[in]	_scrollView	Scroll view that caused the event
 @param[in]	_page	Index of the item to retrieve
 
 @return Implementor must return the maximum zoom scale for the view for the item at _page in _scrollView
 */
- (CGFloat) scrollView:(DYScrollView *)_scrollView maximumZoomScaleForPage:(NSUInteger)_page;

@end

///@}