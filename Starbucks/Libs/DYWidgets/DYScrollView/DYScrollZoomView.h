//
//  DYScrollZoomView.h
//  DYWidgets
//
//  Created by Dylvian on 28/09/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYScrollView;

/**
 @class	UIScrollView
 @protocol UIScrollViewDelegate
 @addtogroup	DYScrollView
 @{
 @class DYScrollZoomView
 
 @brief	View used internally to show views in a DYScrollView
 */
/// @}
@interface DYScrollZoomView : UIScrollView <UIScrollViewDelegate>
{
	UIView *mTopView;
	UIView *mZoomView;
	
	CGSize mPreferredSize;
	
	UITapGestureRecognizer *mTapGestureRecognizer;
	
	DYScrollView *mParentScroll;
}

/**
 @property DYScrollView *parentScroll	DYScrollView view containing this view
 */
@property (nonatomic, assign)	DYScrollView *parentScroll;

@property (nonatomic)			CGSize preferredSize;

/**
 @brief reset the view
 */
- (void) reset;

/**
 @param[in]	_topView	View to show in this view
 */
- (void) setTopView:(UIView *)_topView;
/**
 @param[in]	_topView	View to show in this view
 @param[in]	_preferredSize	Size of _topView
 */
- (void) setTopView:(UIView *)_topView preferredSize:(CGSize)_preferredSize;
/**
 @param[in]	_topView	View to show in this view
 @param[in]	_zoomView	View to show in this view when view is zoomed
 @param[in]	_preferredSize	Size of _topView
 */
- (void) setTopView:(UIView *)_topView zoomView:(UIView *)_zoomView preferredSize:(CGSize)_preferredSize;

@end
