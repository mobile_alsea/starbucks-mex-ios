//
//  DYScrollViewDelegate.h
//  DYWidgets
//
//  Created by Dylvian on 28/09/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYScrollView;
/**
 @class NSObject
 @addtogroup	DYScrollView
 @{
 @protocol	DYScrollViewDelegate
 
 @brief	Protocol to receive notifications of changes in a scroll view
 */
@protocol DYScrollViewDelegate <UIScrollViewDelegate, NSObject>
@optional

/**
 @param[in]	_scrollView	Scroll view that caused the event
 @param[in]	_page	Current page in the scroll view
 
 Called after the current page has been changed
 */
- (void) DYScrollView:(DYScrollView *)_scrollView didChangeToPage:(NSInteger)_page;

@end

///@}