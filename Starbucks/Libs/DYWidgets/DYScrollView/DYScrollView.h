//
//  DYScrollView.h
//  DYWidgets
//
//  Created by Dylvian on 28/09/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYScrollView/DYScrollViewDataSource.h>
#import <DYWidgets/DYScrollView/DYScrollViewDelegate.h>

/**
 @class	UIScrollView
 @protocol UIScrollViewDelegate
 @addtogroup	DYScrollView
 @{
 @class DYScrollView
 
 @brief	Item gallery supporting rotation and zoom of each element
 */
/// @}
@interface DYScrollView : UIScrollView <UIScrollViewDelegate>
{
	BOOL mWrapAround;
	
	id<DYScrollViewDataSource>	mDataSource;
	id<DYScrollViewDelegate>	mDelegate;
	
	CGPoint	mPreviousPoint;
	
	NSInteger	mViewCount;
	NSInteger	mSlotCount;
	NSInteger	mLeftSlot;
	
	NSMutableDictionary *mVisibleDictionary;
	NSMutableArray *mReusableArray;
	
	BOOL mIsZooming;
	NSInteger mCurrentPage;
}

@property (nonatomic, readonly)             NSInteger   currentPage;

/**
 @property	BOOL wrapAround	Whether the scroll view should be endless. If wrapAround is YES, the next item of the last item will be the first item again
 */
@property (nonatomic)						BOOL        wrapAround;

/**
 @property	id<DYScrollViewDataSource> dataSource	data source containing the items for the gallery
 */
@property (nonatomic, retain)	IBOutlet	id<DYScrollViewDataSource>	dataSource;
/**
 @property	id<DYScrollViewDelegate> delegate	delegate that will receive events from gallery
 */
@property (nonatomic, assign)	IBOutlet	id<DYScrollViewDelegate>	delegate;

/**
 @brief	Asks the data source for items
 */
- (void) reloadData;

- (void) reloadPreferredSizes;

/**
 @param[in]	_isZooming	Whether the view is zooming
 */
- (void) setZooming:(BOOL)_isZooming;

/**
 @param[in]	_animated	Whether the transition should be animated or not
 
 Show the item at the left of the current item
 */
- (void) scrollLeftAnimated:(BOOL)_animated;
/**
 @param[in]	_animated	Whether the transition should be animated or not
 
 Show the item at the right of the current item
 */
- (void) scrollRightAnimated:(BOOL)_animated;

/**
 @param[in]	_scale	Zoom scale
 @param[in] _page	Item index to set zoom in
 @param[in]	_animated	Whether the transition should be animated or not
 
 Show the item at the left of the current item
 */
- (void) setZoomScale:(float)_scale inPage:(NSInteger)_page animated:(BOOL)_animated;

@end
