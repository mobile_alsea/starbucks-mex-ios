//
//  DYAnimatableView.h
//  pruebaDYAnimatableView
//
//  Created by Dylvian on 27/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class	UIView
 @addtogroup	DYAnimatableView
 @{
 @class DYAnimatableView
 
 @brief	Base view for sequential animations
 
 Each animation is seen as an iteration. In each iteration you configure the values and after that iteration is completed, you decide whether another animation iteration should be run or not.
 Derived classes should override prepareAnimation to set the target values of the animation and shouldContinueAnimatingAfterIterationCompleted to stop or continue animating.
 */
/// @}
@interface DYAnimatableView : UIView 
{
	NSInteger	mIteration;
	BOOL		mIsAnimating;
	BOOL		mIsAnimationEnqueued;
	
	// options
	NSTimeInterval mAnimationDuration;
	NSTimeInterval mAnimationDelay;
	UIViewAnimationOptions mAnimationOptions;
}

/**
 @property	NSInteger iteration	Animation iteration. First iteration is 0
 */
@property (nonatomic, readonly)	NSInteger iteration;
/**
 @property	BOOL isAnimating	YES while the animation is being run
 */
@property (nonatomic, readonly) BOOL isAnimating;

/**
 @property	NSTimeInterval animationDuration	Duration of the next animation iteration
 */
@property (nonatomic)	NSTimeInterval animationDuration;
/**
 @property	NSTimeInterval animationDelay	Time to wait before the next animation iteration begins
 */
@property (nonatomic)	NSTimeInterval animationDelay;
/**
 @property	UIViewAnimationOptions animationOptions	Animation options such as the interpolator used
 */
@property (nonatomic)	UIViewAnimationOptions animationOptions;

/**
 @brief	Start a new animation iteration. Duration, delay and options are read from the properties. Ask prepareAnimation and shouldContinueAnimatingAfterIterationCompleted to setup animation
 and stop animation or continue.
 */
- (void) startAnimations;

// Override
/**
 @brief	Initialization method. This method is called during view initialization. Derived classes should call [super initialize] if this method is overriden
 */
- (void) initialize;
// Override, should be abstract
/**
 @brief	Derived classes should override this method. This method is called before each animation iteration is run to setup animation.
 */
- (void) prepareAnimation;
/**
 @return	YES if another animation iteration should be run, NO to stop animating
 
 Derived classes should override this method. This method is called after each animation iteration has run to decide if an animation is finished or not.
 */
- (BOOL) shouldContinueAnimatingAfterIterationCompleted;

@end
