//
//  DYTransitionView.h
//  pruebaDYAnimatableView
//
//  Created by Dylvian on 28/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DYTransitionView : UIView 
{
	NSInteger	mIteration;
	BOOL		mIsAnimating;
	BOOL		mIsAnimationEnqueued;
	
	// options
	NSTimeInterval mAnimationDuration;
	UIViewAnimationOptions mAnimationOptions;
	
	// views
	UIView *mFrontView;
	UIView *mBackView;
}

@property (nonatomic, readonly)	NSInteger iteration;
@property (nonatomic, readonly) BOOL isAnimating;

@property (nonatomic)	NSTimeInterval animationDuration;
@property (nonatomic)	UIViewAnimationOptions animationOptions;

@property (nonatomic, retain)	IBOutlet UIView *frontView;
@property (nonatomic, retain)	IBOutlet UIView *backView;

- (void) startAnimations;

// Override
- (void) initialize;	// don't call directly, will be called during initialization
// Override, should be abstract
- (void) prepareAnimation;
- (BOOL) shouldContinueAnimatingAfterIterationCompleted;

@end
