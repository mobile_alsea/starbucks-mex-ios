//
//  DYEffectView.h
//  privalia
//
//  Created by Dylvian on 13/06/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYView/DYView.h>

@interface DYEffectView : DYView 
{
    UIImage *mImage;
	CGRect	mSourceRect;
	CGPoint mTargetPoint;
	
	CGFloat *mpStartVector;
	CGFloat *mpEndVector;
	CGImageRef *mpImageVector;
	
	NSInteger mCount;
	
	CGFloat mT;
}

@property (nonatomic)	CGFloat t;

- (void) setImage:(UIImage *)_image from:(CGRect)_sourceRect to:(CGPoint)_targetPoint;

@end
