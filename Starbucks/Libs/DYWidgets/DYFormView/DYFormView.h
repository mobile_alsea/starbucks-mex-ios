//
//  DYFormView.h
//  pruebaiBanestoProductos
//
//  Created by Dylvian on 12/07/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DYFormViewDataSource.h"

@interface DYFormView : UIScrollView
{
	id<DYFormViewDataSource> mDataSource;
	
	NSMutableArray *mFieldArray;
	NSMutableArray *mTextFieldArray;
	NSMutableArray *mDelegateArray;
	
	UITextField *mActiveField;
	CGSize mKeyboardSize;
	CGRect mStartKeyboardFrame;
	CGRect mEndKeyboardFrame;
	
	CGFloat mExtraHeight;
	
	BOOL mIgnoreKeyboardFrame;
}

@property (nonatomic, assign)	id<DYFormViewDataSource> dataSource;
@property (nonatomic)			CGFloat extraHeight;
@property (nonatomic)			BOOL ignoreKeyboardFrame;

- (void) reloadData;

@end

@interface DYFormView(UITextFieldDelegate) <UITextFieldDelegate>
@end 
