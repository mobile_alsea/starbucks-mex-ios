//
//  DYFormField.h
//  pruebaiBanestoProductos
//
//  Created by Dylvian on 12/07/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol DYFormField <NSObject>
@required

@property (nonatomic, readonly) UITextField *textField;
@property (nonatomic, retain) IBOutlet id<DYFormField> nextField;

@end
