//
//  DYFormViewDataSource.h
//  pruebaiBanestoProductos
//
//  Created by Dylvian on 12/07/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DYFormField.h"

@class DYFormView;

@protocol DYFormViewDataSource <NSObject>
@required

- (NSInteger) numberOfFieldsInForm:(DYFormView *)_formView;
- (id<DYFormField>) form:(DYFormView *)_formView fieldForIndex:(NSInteger)_field;

@end
