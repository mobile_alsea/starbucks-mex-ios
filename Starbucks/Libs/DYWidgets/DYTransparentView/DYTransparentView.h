//
//  DYTransparentView.h
//  DYWidgets
//
//  Created by Dylvian on 10/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class	UIView
 @addtogroup	DYTransparentView
 @{
 @class DYTransparentView
 
 @brief	A view that may be transparent to touches. That is, if it doesn't intercept touches, views located under this view will be able to receive touch events
 */
/// @}
@interface DYTransparentView : UIView 
{
	BOOL	mInterceptTouches;
}

/**
 @property BOOL	interceptTouches	Whether this view should receive touches or not
 */
@property	(nonatomic)	BOOL	interceptTouches;

@end
