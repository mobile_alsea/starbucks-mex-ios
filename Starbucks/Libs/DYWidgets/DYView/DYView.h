//
//  DYView.h
//  DYWidgets
//
//  Created by Dylvian on 28/04/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const UIViewAutoresizing UIViewAutoresizingAll;

/**
 @class	UIView
 @addtogroup	DYView
 @{
 @class DYView
 
 @brief	Improved base class for a UIView
 */
/// @}
@interface DYView : UIView 
{
	id mView;
}

/**
 @param[in]	_name	Name of the nib file to load
 @param[in]	_bundleOrNil	Bundle containing the nib file. If this parameter is nil, the main bundle will be used
 
 Load a DYView object from a nib. If there are more than one DYViews in the nib, the first one will be returned. The object is not retained.
 
 @return	A DYView loaded from the nib or nil if no such object exists in the nib
 */
+ (id) viewWithNibName:(NSString *)_name bundle:(NSBundle *)_bundleOrNil;
/**
 @param[in]	_name	Name of the nib file to load
 @param[in]	_bundleOrNil	Bundle containing the nib file. If this parameter is nil, the main bundle will be used
 @param[in] _class	Class of the object to load
 
 Load an object from a nib. If there are more than one _class objects in the nib, the first one will be returned. The object is not retained.
 
 @return	An object of class _class loaded from the nib or nil if no such object exists in the nib
 */
+ (id) viewWithNibName:(NSString *)_name bundle:(NSBundle *)_bundleOrNil class:(Class)_class;

/**
 @param[in]	_name	Name of the nib file to load
 @param[in]	_bundleOrNil	Bundle containing the nib file. If this parameter is nil, the main bundle will be used
 
 Load an object of the invoking class from a nib. If there are more than one objects in the nib, the first one will be returned. The object is retained.
 
 @return	An object loaded from the nib or nil if no such object exists in the nib
 */
- (id) initWithNibName:(NSString *)_name bundle:(NSBundle *)_bundleOrNil;
/**
 @param[in]	_name	Name of the nib file to load
 @param[in]	_bundleOrNil	Bundle containing the nib file. If this parameter is nil, the main bundle will be used
 @param[in] _class	Class of the object to load
 
 Load an object from a nib. If there are more than one _class objects in the nib, the first one will be returned. The object is retained.
 
 @return	An object of class _class loaded from the nib or nil if no such object exists in the nib
 */
- (id) initWithNibName:(NSString *)_name bundle:(NSBundle *)_bundleOrNil class:(Class)_class;

/**
 @brief	Unified initializer called from initWithFrame and initWithCoder
 
 This method is called from the class initializers to unify initialization. You must implement this method in your class.
 
 @return	The default implementation always return YES. If you return NO in your implementation, the object will not be created (eg: initWithFrame will return nil)
 */
- (BOOL) initialize;

@end
