//
//  DYPaintViewGraphicState.h
//  DYWidgets
//
//  Created by Dylvian on 16/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 enum PaintViewMode	Drawing mode: painting or erasing
 */
typedef enum PaintViewMode { PaintViewModePaint = 0, PaintViewModeErase } PaintViewMode;

/**
 @class	NSObject
 @addtogroup	DYPaintView
 @{
 @class DYPaintViewGraphicState
 
 @brief	Graphic state in a paint view
 */
/// @}
@interface DYPaintViewGraphicState : NSObject 
{
	UIColor *mColor;
	CGFloat mLineWidth;
	
	CGFloat mEraseWidth;
	
	PaintViewMode mPaintMode;
}

/**
 @property UIColor *color	drawing color for painting
 */
@property	(nonatomic, retain)		UIColor *color;
/**
 @property CGFloat lineWidth	width of lines when painting
 */
@property	(nonatomic)				CGFloat lineWidth;
/**
 @property CGFloat eraseWidth	width of lines when erasing
 */
@property	(nonatomic)				CGFloat eraseWidth;
/**
 @property PaintViewMode paintMode	Paint mode: painting or erasing
 */
@property	(nonatomic)				PaintViewMode paintMode;

/**
 @return An object initialized with the contents of the receiver
 */
- (DYPaintViewGraphicState *) clone;

@end
