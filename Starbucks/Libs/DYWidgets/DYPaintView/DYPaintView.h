//
//  DYPaintView.h
//  DYWidgets
//
//  Created by Dylvian on 16/02/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYTransparentView/DYTransparentView.h>

#import "DYPaintViewGraphicState.h"

/**
 @class	DYTransparentView
 @addtogroup	DYPaintView
 @{
 @class DYPaintView
 
 @brief	A view used for simple drawing
 */
/// @}
@interface DYPaintView : DYTransparentView
{
	//BOOL	mIsModified;
	
	DYPaintViewGraphicState	*mGraphicState;
	
	//NSMutableArray *mStepsArray;
	
	UIImageView *mImageView;
	CGPoint mLastPoint;
	CGPoint mCurrPoint;
	
	//UIImage *mCurrentImage;
	//BOOL mShouldClear;
}

//@property	(nonatomic, readonly)	BOOL isModified;

/**
 @property DYPaintViewGraphicState *graphicState	Graphic state: color, line width, paint mode
 */
@property	(nonatomic, readonly)	DYPaintViewGraphicState *graphicState;

/**
 @return Content as an image, or nil if view remains unmodified
 */
- (UIImage *) viewContents;

/**
 @brief	Sets contents of the view
 */
- (void) setViewContents:(UIImage *)_image;

/**
 @brief	Clear contents of the view
 */
- (void) clearContents;

@end
