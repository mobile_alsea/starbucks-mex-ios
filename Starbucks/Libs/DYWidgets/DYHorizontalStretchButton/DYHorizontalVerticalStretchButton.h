//
//  DYHorizontalVerticalStretchButton.h
//  DYWidgets
//
//  Created by Dylvian on 08/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DYHorizontalStretchButton.h"

/**
 @class	UIButton
 @addtogroup	DYHorizontalStretchButton
 @{
 @class DYHorizontalVerticalStretchButton
 
 @brief	Button whose images expand from a central pixel, maintaining aspect of the borders
 */
/// @}
@interface DYHorizontalVerticalStretchButton : DYHorizontalStretchButton 
{
}

@end
