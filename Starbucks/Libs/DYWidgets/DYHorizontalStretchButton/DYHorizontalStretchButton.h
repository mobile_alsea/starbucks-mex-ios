//
//  DYHorizontalStretchButton.h
//  DYWidgets
//
//  Created by Dylvian on 08/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class	UIButton
 @addtogroup	DYHorizontalStretchButton
 @{
 @class DYHorizontalStretchButton
 
 @brief	Button whose images expand from a central column of pixels, maintaining aspect of the borders
 */
/// @}
@interface DYHorizontalStretchButton : UIButton 
{
}

@end
