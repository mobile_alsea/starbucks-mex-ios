/*
 *  doxygen.h
 *  DYWidgets
 *
 *  Created by Dylvian on 10/02/11.
 *  Copyright 2011 Friz. All rights reserved.
 *
 */

/**
 @defgroup	DYWidgets	DYWidgets - Objects that are painted on screen
 
 @defgroup	DYMenuView	DYMenuView - Menu with customizable panels
 @ingroup	DYWidgets
 
 @defgroup	DYAnimatableView	DYAnimatableView - Base view for sequential animations
 @ingroup	DYWidgets
 
 @defgroup	DYPDFPageView	DYPDFPageView - Show PDF pages
 @ingroup	DYWidgets
  
 @defgroup	DYCheckButton	DYCheckButton - Customizable check button
 @ingroup	DYWidgets
  
 @defgroup	DYDoubleSlideView	DYDoubleSlideView - Slider for ranges
 @ingroup	DYWidgets
  
 @defgroup	DYPageControl	DYPageControl - Customizable page control
 @ingroup	DYWidgets
  
 @defgroup	DYHorizontalPicker	DYHorizontalPicker - Horizontal picker
 @ingroup	DYWidgets
  
 @defgroup	DYLabel	DYLabel - Improved labels
 @ingroup	DYWidgets
  
 @defgroup	DYImageView	DYImageView - Improved image view
 @ingroup	DYWidgets
  
 @defgroup	DYScrollView	DYScrollView - Gallery
 @ingroup	DYWidgets
  
 @defgroup	DYTextView	DYTextView - Textview with header and footer
 @ingroup	DYWidgets
 
 @defgroup	DYTransparentView	DYTransparentView - UIView that may not intercept touches
 @ingroup	DYWidgets
 
 @defgroup	DYTabBar	DYTabBar - Customizable tab bar
 @ingroup	DYWidgets
 
 @defgroup	DYPaintView	DYPaintView - Paintable UIView
 @ingroup	DYWidgets
 
 @defgroup	DYHorizontalStretchButton	DYHorizontalStretchButton - Button whose images expand maintaining borders
 @ingroup	DYWidgets
 */