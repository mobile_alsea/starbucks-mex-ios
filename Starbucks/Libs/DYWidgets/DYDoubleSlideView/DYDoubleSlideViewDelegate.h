//
//  DYDoubleSlideViewDelegate.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 19/11/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>


@class DYDoubleSlideView;

/**
 @class NSObject
 @addtogroup	DYDoubleSlideView
 @{
 @protocol	DYDoubleSlideViewDelegate
 
 @brief	Protocol that a class must implement to receive events from a double slider
 */
@protocol DYDoubleSlideViewDelegate <NSObject>
@optional

/**
 @param[in]	_slideView	Slide view that caused the event
 @param[in]	_minValue	Lower value of the slider
 
 Called in response to a change of the lower position
 */
- (void) DYDoubleSlideView:(DYDoubleSlideView *)_slideView didChangeMinValue:(double)_minValue;
/**
 @param[in]	_slideView	Slide view that caused the event
 @param[in]	_maxValue	Upper value of the slider
 
 Called in response to a change of the upper position
 */
- (void) DYDoubleSlideView:(DYDoubleSlideView *)_slideView didChangeMaxValue:(double)_maxValue;

@end

///@}