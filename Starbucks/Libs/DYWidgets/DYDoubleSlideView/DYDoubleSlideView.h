//
//  DYDoubleSlideView.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 18/11/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DYWidgets/DYDoubleSlideView/DYDoubleSlideViewDelegate.h>

/**
 @class	UIControl
 @addtogroup	DYDoubleSlideView
 @{
 @class DYDoubleSlideView
 
 @brief	Slider with two selectors, to define ranges
 */
/// @}
@interface DYDoubleSlideView : UIControl
{
	id<DYDoubleSlideViewDelegate> mDelegate;
	
	// VIEW's
	UIImageView		* mSlideImageView;
	UIImageView		* mBackgroundImageView;
	
	UIButton	* mLeftButton;
	UIButton	* mRightButton;
	
	// DATA's
	double mMaxLimit;
	double mMinLimit;
	
	double mMaxPosition;
	double mMinPosition;
}

/**
 @property id<DYDoubleSlideViewDelegate> delegate	Delegate that will receive notifications of events
 */
@property (nonatomic, assign) IBOutlet	id<DYDoubleSlideViewDelegate>	delegate;

/**
 @property double maxLimit	Upper limit of the slider
 */
@property (nonatomic) double 	maxLimit;
/**
 @property double minLimit	Lower limit of the slider
 */
@property (nonatomic) double 	minLimit;
/**
 @property double maxPosition	Position of the upper button slider
 */
@property (nonatomic) double	maxPosition;
/**
 @property double minPosition	Position of the lower button slider
 */
@property (nonatomic) double 	minPosition;

/**
 @property UIColor *slideColor	Color of the slider view
 */
@property (nonatomic, retain) UIColor	*slideColor;
/**
 @property UIImage *slideImage	Image of the slider
 */
@property (nonatomic, retain) UIImage	*slideImage;
/**
 @property UIImage *backgroundImage	Background image of the slider
 */
@property (nonatomic, retain) UIImage	*backgroundImage;
/**
 @property UIImage *leftButtonImage	Image of the lower slider button
 */
@property (nonatomic, retain) UIImage	*leftButtonImage;
/**
 @property UIImage *rightButtonImage	Image of the upper slider button
 */
@property (nonatomic, retain) UIImage	*rightButtonImage;

/**
 @param[in] _value	Lower position
 @param[in]	_animated	Whether the transition should be animated
 
 Sets the lower slider position
 */
- (void) setMinPosition: (double) _value animated: (BOOL) _animated;
/**
 @param[in] _value	Upper position
 @param[in]	_animated	Whether the transition should be animated
 
 Sets the upper slider position
 */
- (void) setMaxPosition: (double) _value animated: (BOOL) _animated;

@end
