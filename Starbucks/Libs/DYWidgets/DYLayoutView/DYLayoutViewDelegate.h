//
//  DYLayoutViewDelegate.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 17/06/11.
//  Copyright 2011 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYLayoutView;

@protocol DYLayoutViewDelegate <NSObject>

- (void) DYLayoutView:(DYLayoutView *) _layoutView changeContentSize:(CGSize) _contentSize;
//- (void) DYLayoutView:(DYLayoutView *) _layoutView changeFrame:(CGRect) _frame;

@end
