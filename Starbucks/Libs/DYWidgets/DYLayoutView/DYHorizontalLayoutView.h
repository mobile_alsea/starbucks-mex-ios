//
//  DYHorizontalLayoutView.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 13/04/11.
//  Copyright 2011 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DYWidgets/DYLayoutView/DYLayoutView.h>
#import <DYWidgets/DYLayoutView/DYLayoutViewDataSource.h>

typedef enum HorizontalLayoutAlignment { HorizontalLayoutAlignmentDefault = 0, HorizontalLayoutAlignmentTop, HorizontalLayoutAlignmentBottom } HorizontalLayoutAlignment;

@interface DYHorizontalLayoutView : DYLayoutView 
{
    BOOL mAdjustToHeight;
	
	HorizontalLayoutAlignment mAlignment;
}

@property (nonatomic) BOOL adjustToHeigth;

@property (nonatomic) HorizontalLayoutAlignment alignment;

@end
