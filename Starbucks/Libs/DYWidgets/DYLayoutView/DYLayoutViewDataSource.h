//
//  DYVerticalLayoutViewDataSource.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 13/04/11.
//  Copyright 2011 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYLayoutView;


@protocol DYLayoutViewDataSource <NSObject>

@required

- (NSUInteger) numberOfViewsInLayout:(DYLayoutView *) _LayoutView;

- (UIView *) DYLayoutView:(DYLayoutView *) _layoutView viewAtIndex:(NSUInteger) _index;

@end
