//
//  DYLayoutView.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 13/04/11.
//  Copyright 2011 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DYWidgets/DYLayoutView/DYLayoutViewDataSource.h>
#import <DYWidgets/DYLayoutView/DYLayoutViewDelegate.h>

@interface DYLayoutView : UIView 
{
    id <DYLayoutViewDelegate>   mDelegate;
    id <DYLayoutViewDataSource> mDataSource;
    NSMutableArray  *mViewsArray;
    
    CGFloat mSpacing;
	
	BOOL	mIsLayingSubviews;
	
	CGSize	mContentSize;
    
    CGRect	mFrame;
}
@property (nonatomic, assign)	IBOutlet id <DYLayoutViewDelegate>     delegate;
@property (nonatomic, assign)	IBOutlet id <DYLayoutViewDataSource>	dataSource;
@property (nonatomic)			CGFloat spacing;

@property (nonatomic, readonly)	CGSize	contentSize;

- (void) initialize;
- (void) reloadData;

@end
