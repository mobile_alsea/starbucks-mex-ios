//
//  DYVerticalLayoutView.h
//  DYWidgets
//
//  Created by Isabelo Pamies on 13/04/11.
//  Copyright 2011 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DYWidgets/DYLayoutView/DYLayoutView.h>
#import <DYWidgets/DYLayoutView/DYLayoutViewDataSource.h>

@interface DYVerticalLayoutView : DYLayoutView 
{
    BOOL    mAdjustToWidth;
	BOOL	mSnapToOrigin;
}

@property (nonatomic) BOOL adjustToWidth;
@property (nonatomic) BOOL snapToOrigin;

@end
