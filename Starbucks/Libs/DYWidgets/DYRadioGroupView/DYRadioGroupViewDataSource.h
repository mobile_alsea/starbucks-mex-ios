//
//  DYRadioGroupViewDataSource.h
//  privalia
//
//  Created by Dylvian on 16/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DYRadioGroupButtonPositionEnum.h"

@class DYRadioGroupView;

typedef enum DYRadioGroupViewMode { DYRadioGroupViewModePicker = 0, DYRadioGroupViewModeVerticalList, DYRadioGroupViewModeHorizontalList } DYRadioGroupViewMode;

@protocol DYRadioGroupViewDataSource <NSObject>
@required

- (DYRadioGroupViewMode) viewModeInRadioGroupView:(DYRadioGroupView *)_radioGroupView;
- (NSInteger) selectedIndexInRadioGroupView:(DYRadioGroupView *)_radioGroupView;
- (NSInteger) numberOfRowsInRadioGroupView:(DYRadioGroupView *)_radioGroupView;
- (NSString *) radioGroupView:(DYRadioGroupView *)_radioGroupView titleForRow:(NSInteger)_row;

- (UIImage *) checkedImageInRadioGroup:(DYRadioGroupView *)_radioGroupView;
- (UIImage *) uncheckedImageInRadioGroup:(DYRadioGroupView *)_radioGroupView;

@optional

- (NSString *) titleForRadioGroupView:(DYRadioGroupView *)_radioGroupView;	// only used if view mode is DYRadioGroupViewModePicker
- (UITextAlignment) radioGroupView:(DYRadioGroupView *)_radioGroupView alignmentForRow:(NSInteger)_row;	// only used if view mode is not DYRadioGroupViewModePicker
- (BOOL) radioGroupView:(DYRadioGroupView *)_radioGroupView selectRowOnClick:(NSInteger)_row;

- (UIFont *) fontInRadioGroupView:(DYRadioGroupView *)_radioGroupView;
- (UIColor *) colorInRadioGroupView:(DYRadioGroupView *)_radioGroupView;
- (DYRadioGroupButtonPosition) buttonPositionInRadioGroupView:(DYRadioGroupView *)_radioGroupView;

- (CGFloat) spacingInRadioGroupView:(DYRadioGroupView *)_radioGroupView;	// only used if view mode is DYRadioGroupViewModeHorizontalList
- (CGFloat) widthInRadioGroupView:(DYRadioGroupView *)_radioGroupView;	// used if view mode is not picker

@end
