//
//  DYRadioGroupButtonPositionEnum.h
//
//  Created by Dylvian on 16/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum DYRadioGroupButtonPosition
{
	DYRadioGroupButtonPositionLeft,
	DYRadioGroupButtonPositionRight
} DYRadioGroupButtonPosition;
