//
//  DYRadioGroupView.h
//  privalia
//
//  Created by Dylvian on 16/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DYRadioGroupViewDataSource.h"
#import "DYRadioGroupViewDelegate.h"
#import "DYRadioGroupButtonPositionEnum.h"

@interface DYRadioGroupView : UIView //<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
{
    UIScrollView *mScrollView;
	UIView *mContentView;
	UITextField *mPickerTextField;
	
	UIView *mPickerBaseView;
	UINavigationBar *mPickerNavBar;
	UINavigationItem *mPickerNavItem;
	UIPickerView *mPickerView;
	BOOL mIsShowingPicker;
	
	DYRadioGroupViewMode mRadioMode;
	
	NSMutableArray *mElementArray;
	NSInteger mSelectedIndex;
	
	id<DYRadioGroupViewDataSource> mDataSource;
	id<DYRadioGroupViewDelegate> mDelegate;
}

@property (nonatomic, readonly)	UITextField *pickerTextField;
@property (nonatomic, readonly) UINavigationBar *pickerNavBar;

@property (nonatomic, readonly) NSInteger selectedIndex;

@property (nonatomic, assign) IBOutlet id<DYRadioGroupViewDataSource> dataSource;
@property (nonatomic, assign) IBOutlet id<DYRadioGroupViewDelegate> delegate;

- (void) setScrollingEnabled:(BOOL)_enabled;
- (void) reloadData;

@end
