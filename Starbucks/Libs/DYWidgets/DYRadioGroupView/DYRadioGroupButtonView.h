//
//  DYRadioGroupButtonView.h
//  privalia
//
//  Created by Dylvian on 16/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <DYWidgets/DYCheckButton/DYCheckButton.h>
#import "DYRadioGroupButtonPositionEnum.h"

@interface DYRadioGroupButtonView : UIView 
{
    UILabel *mLabel;
	DYCheckButton *mButton;
	
	CGFloat mButtonWidth;
	DYRadioGroupButtonPosition mButtonPosition;
}

@property (nonatomic, readonly)	UILabel *label;
@property (nonatomic, readonly)	DYCheckButton *button;
@property (nonatomic, assign) DYRadioGroupButtonPosition buttonPosition;

- (id) initWithFrame:(CGRect)_frame checkedImage:(UIImage *)_checkedImage uncheckedImage:(UIImage *)_uncheckedImage;

- (void) setAlignment:(UITextAlignment)_textAlignment;

@end
