//
//  DYRadioGroupViewDelegate.h
//  privalia
//
//  Created by Dylvian on 16/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYRadioGroupView;

@protocol DYRadioGroupViewDelegate <NSObject>
@optional

- (void) radioGroupView:(DYRadioGroupView *)_radioGroupView didSelectRow:(NSInteger)_row;
- (void) radioGroupView:(DYRadioGroupView *)_radioGroupView didChangeSelectedRow:(NSInteger)_row;

@end
