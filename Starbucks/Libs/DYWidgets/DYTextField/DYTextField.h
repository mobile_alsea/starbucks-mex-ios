//
//  DYTextField.h
//  privalia
//
//  Created by Dylvian on 21/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <DYWidgets/DYFormView/DYFormField.h>

@interface DYTextField : UITextField <DYFormField>
{
    UIColor *mPlaceholderColor;
	
	id<DYFormField>	mNextField;
}

@property (nonatomic, retain)	UIColor *placeholderColor;

@property (nonatomic, retain) IBOutlet id<DYFormField> nextField;

@end
