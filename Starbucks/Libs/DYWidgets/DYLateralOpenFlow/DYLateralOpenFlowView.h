//
//  DYLateralOpenFlowView.h
//  DYWidgets
//
//  Created by Dylvian on 27/05/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#warning DYLateralOpenFlow hereda de OpenFlow. Si usas DYLateralOpenFlow debes incluir OpenFlow en tu proyecto, si no lo usas borralo del proyecto
#import <OpenFlow/AFOpenFlowView.h>

typedef enum DYLateralOpenFlowViewSide { DYLateralOpenFlowViewSideLeft = 0x01, DYLateralOpenFlowViewSideRight = 0x02, DYLateralOpenFlowViewSideCentered = 0x04, DYLateralOpenFlowViewSideFrontLeft = 0x05, DYLateralOpenFlowViewSideFrontRight = 0x06 } DYLateralOpenFlowViewSide;

@interface DYLateralOpenFlowView : AFOpenFlowView 
{
    DYLateralOpenFlowViewSide mViewSide;
	NSInteger mVisibleCovers;
}

- (void) setViewSide:(DYLateralOpenFlowViewSide)_side;
- (void) setVisibleCovers:(NSInteger)_visibleCovers;

@end
