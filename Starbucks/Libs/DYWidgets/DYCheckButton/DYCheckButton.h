//
//  DYCheckButton.h
//  DYWidgets
//
//  Created by Dylvian on 25/01/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @class	UIView
 @addtogroup	DYCheckButton
 @{
 @class DYCheckButton
 
 @brief	Customizable check button to be used instead of an UISwitch. Allows to configure images or text for the checked and non-checked states.
 
 Issues value changed control actions when the state changes
 */
/// @}
@interface DYCheckButton : UIButton 
{
	UIImage *mCheckedImage;
	UIImage *mUncheckedImage;
	
	NSString *mCheckedText;
	NSString *mUncheckedText;
	
	BOOL mIsChecked;
}

/**
 @property	BOOL isChecked	Whether this button is checked or not
 */
@property (nonatomic, readonly)	BOOL isChecked;

/**
 @param[in]	_checked	Checked state of the button
 @param[in]	_animated	Whether the transition should be animated or not
 
 Changes the checked state of the button and issue control actions if needed 
 */
- (void) setChecked:(BOOL)_checked animated:(BOOL)_animated;
/**
 @param[in]	_selectedImage	Image for the checked state
 @param[in]	_unselectedImage	Image for the non-checked state
 
 Customize the appearance of the check button
 */
- (void) setCheckedImage:(UIImage *)_selectedImage uncheckedImage:(UIImage *)_unselectedImage;
/**
 @param[in]	_selectedText	Text for the checked state
 @param[in]	_unselectedText	Text for the non-checked state
 
 Customize the text of the check button
 */
- (void) setCheckedText:(NSString *)_selectedText uncheckedText:(NSString *)_unselectedText;

@end
