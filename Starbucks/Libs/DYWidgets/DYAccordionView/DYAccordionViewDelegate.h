//
//  DYAccordionViewDelegate.h
//  privalia
//
//  Created by Dylvian on 15/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYAccordionView;

@protocol DYAccordionViewDelegate <NSObject>
@optional

- (void) accordionView:(DYAccordionView *)_accordionView foldedViewInRow:(NSInteger)_row;
- (void) accordionView:(DYAccordionView *)_accordionView unfoldedViewInRow:(NSInteger)_row;

- (void) accordionView:(DYAccordionView *)_accordionView changedSize:(CGSize)_newSize;

@end
