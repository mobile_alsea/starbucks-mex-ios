//
//  DYAccordionView.h
//  privalia
//
//  Created by Dylvian on 15/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DYAccordionViewDataSource.h"
#import "DYAccordionViewDelegate.h"

@interface DYAccordionView : UIView 
{
    CGFloat mSpacing;
	
	NSMutableArray *mViewArray;
	
	id<DYAccordionViewDataSource>	mDataSource;
	id<DYAccordionViewDelegate>		mDelegate;
	
	CGSize mLastSize;
}

@property (nonatomic, readonly)	CGFloat spacing;

@property (nonatomic, assign)	IBOutlet id<DYAccordionViewDataSource>	dataSource;
@property (nonatomic, assign)	IBOutlet id<DYAccordionViewDelegate>		delegate;

- (void) reloadData;

@end
