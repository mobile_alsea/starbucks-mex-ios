//
//  DYAccordionViewDataSource.h
//  privalia
//
//  Created by Dylvian on 15/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DYUnfoldView.h"

@class DYAccordionView;

@protocol DYAccordionViewDataSource <NSObject>
@required

- (NSInteger) numberOfRowsInAccordionView:(DYAccordionView *)_accordionView;
- (DYUnfoldView *) accordionView:(DYAccordionView *)_accordionView viewForRow:(NSInteger)_row;
- (UIImage *) foldedImageInAccordionView:(DYAccordionView *)_accordionView;
- (UIImage *) unfoldedImageInAccordionView:(DYAccordionView *)_accordionView;

@optional

- (CGFloat) spacingInAccordionView:(DYAccordionView *)_accordionView;

@end
