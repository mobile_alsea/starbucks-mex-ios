//
//  DYUnfoldView.h
//  privalia
//
//  Created by Dylvian on 15/03/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DYUnfoldView : UIView 
{
    BOOL mIsFolded;
	
	UIView *mFoldedView;
	UIView *mUnfoldedView;
	
	UIButton *mFoldButton;
	UIButton *mUnfoldButton;
}

@property (nonatomic, readonly)	BOOL isFolded;

@property (nonatomic, retain)	IBOutlet UIView *foldedView;
@property (nonatomic, retain)	IBOutlet UIView *unfoldedView;

@property (nonatomic, retain)	IBOutlet UIButton *foldButton;
@property (nonatomic, retain)	IBOutlet UIButton *unfoldButton;

- (void) initialize;

- (CGSize) foldedSize;
- (CGSize) unfoldedSize;

- (void) foldAnimated:(BOOL)_animated;
- (void) unfoldAnimated:(BOOL)_animated;

@end
