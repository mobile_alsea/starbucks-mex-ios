//
//  UILabel_font.h
//  ccm
//
//  Created by José Servet on 02/02/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UILabel(font)

- (void) setCurrentFont:(NSString *)_font;

@end
