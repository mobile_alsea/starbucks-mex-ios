//
//  UIButton_font.h
//  ccm
//
//  Created by José Servet on 02/02/12.
//  Copyright (c) 2012 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIButton(font)

- (void) setCurrentFont:(NSString *)_font;

@end
