//
//  UIAlertView_misc.h
//  DYWidgets
//
//  Created by Dylvian on 11/07/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIAlertView (UIAlertView_misc)

+ (void) alertViewWithTitle:(NSString *)_title message:(NSString *)_message delegate:(id<UIAlertViewDelegate>)_delegate cancelButtonTitle:(NSString *)_cancelTitle otherButtonTitles:(NSArray *)_otherButtonTitleArray;

+ (UIAlertView *) alertViewWithTitle:(NSString *)_title message:(NSString *)_message delegate:(id<UIAlertViewDelegate>)_delegate cancelButtonTitle:(NSString *)_cancelTitle otherButtonTitles:(NSArray *)_otherButtonTitleArray style:(NSInteger)_alertViewStyle;	// iOS5+

@end
