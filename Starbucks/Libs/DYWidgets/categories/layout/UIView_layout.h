//
//  UIView_layout.h
//  DYWidgets
//
//  Created by Dylvian on 28/04/11.
//  Copyright 2011 Friz. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (UIView_layout)

/**
 @brief Centers a view in its superview
 */
- (void) centerInSuperview;

- (UIView *) ancestorOfClass:(Class)_class;

/**
 @return An image with the view contents
 */
- (UIImage *) screenshot;

@end
