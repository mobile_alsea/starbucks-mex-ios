//
//  UIImage_image.h
//  privalia
//
//  Created by Isabelo Pamies on 09/06/11.
//  Copyright 2011 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImage (UIImage_image)

- (UIImage *) imageScaledToSize:(CGSize)_size;

- (UIColor *) color;

@end
