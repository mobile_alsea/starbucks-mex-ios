//
//  DYTabBar.h
//  PlanB
//
//  Created by Isabelo Pamies on 26/11/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYTabBarDataSource.h"
#import "DYTabBarDelegate.h"

/**
 @class	UITabBar
 @protocol UITabBarDelegate
 @addtogroup	DYTabBar
 @{
 @class DYTabBar
 
 @brief	Customizable tab bar for tab bar controllers
 */
/// @}
@interface DYTabBar : UITabBar  <UITabBarDelegate>
{
	
	UIImage *mBackgroundImage;
	UIImage *mBackgroundSelectedImage;
	
	id <DYTabBarDataSource> mDYDataSource;
	id <DYTabBarDelegate>	mDYDelegate;
	
	NSMutableDictionary *mImages;
	NSMutableDictionary *mImagesHighlighted;
}

/**
 @property id<DYTabBarDataSource>	DYdataSource	data source of the Tab Bar
 */
@property (nonatomic, assign) IBOutlet id <DYTabBarDataSource>	DYdataSource;
/**
 @property id<DYTabBarDelegate>	DYdelegate	object that will receive notifications of events in the Tab Bar
 */
@property (nonatomic, assign) IBOutlet id <DYTabBarDelegate>		DYdelegate;

- (void) reloadData;

@end
