//
//  DYTabBarDelegate.h
//  PlanB
//
//  Created by Isabelo Pamies on 26/11/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>


@class DYTabBar;


@protocol DYTabBarDelegate <NSObject>

@optional
- (void)tabBar:(DYTabBar *)tabBar willBeginCustomizingItems:(NSArray *)items;
- (void)tabBar:(DYTabBar *)tabBar didBeginCustomizingItems:(NSArray *)items;
- (void)tabBar:(DYTabBar *)tabBar willEndCustomizingItems:(NSArray *)items changed:(BOOL)changed;
- (void)tabBar:(DYTabBar *)tabBar didEndCustomizingItems:(NSArray *)items changed:(BOOL)changed;

@required
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item;

@end

