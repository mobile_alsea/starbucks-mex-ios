//
//  DYTabBarDataSource.h
//  PlanB
//
//  Created by Isabelo Pamies on 26/11/10.
//  Copyright 2010 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYTabBar;

/**
 @class NSObject
 @addtogroup	DYTabBar
 @{
 @protocol	DYTabBarDataSource
 
 @brief	Protocol to configure the items in a tab bar
 */
@protocol DYTabBarDataSource <NSObject>

@optional
/**
 @param[in]	_tabBar	tab bar asking for data
 
 @return	Implementor must return the background image for tab bar
 */
- (UIImage *) backgroundImageForTabBar: (DYTabBar *) _tabBar;

/**
 @param[in]	_tabBar	tab bar asking for data
 
 @return	Implementor must return the background image for the selected item in the tab bar
 */
- (UIImage *) backgroundSelectedImageForTabBar: (DYTabBar *) _tabBar;

- (UIFont *) fontForTabBarItemAtIndex:(NSInteger)_index;
- (UIColor *) colorForTabBarItems;
- (UIColor *) colorForSelectedTabBarItem;

@required
/**
 @param[in]	_tabBar	Tab bar asking for data
 @param[in] _index	item index.
 
 @return	Implementor must return the icon image to use for the non-selected item at _index
 */
- (UIImage *) DYTabBar: (DYTabBar *) _tabBar imageForTabBarItemAtIndex: (NSInteger) _index;

/**
 @param[in]	_tabBar	Tab bar asking for data
 @param[in] _index	item index.
 
 @return	Implementor must return the icon image to use for the selected item at _index
 */
- (UIImage *) DYTabBar: (DYTabBar *) _tabBar imageForTabBarItemSelectedAtIndex: (NSInteger) _index;

@end

///@}