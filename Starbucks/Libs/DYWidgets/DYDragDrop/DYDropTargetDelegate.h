//
//  DYDropTargetDelegate.h
//  hofmann
//
//  Created by Juan Antonio Romero on 03/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYDraggableView;
@class DYDropTarget;

@protocol DYDropTargetDelegate <NSObject>

- (void) dropTarget:(DYDropTarget*)_dropTarget enteredDraggable:(DYDraggableView*)_draggableView;

- (void) dropTarget:(DYDropTarget*)_dropTarget exitedDraggable:(DYDraggableView*)_draggableView;

- (void) dropTarget:(DYDropTarget*)_dropTarget droppedDraggable:(DYDraggableView*)_draggableView;

@end