//
//  DYDraggableView.h
//  hofmann
//
//  Created by Juan Antonio Romero on 02/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYDraggableViewDelegate.h"
#import "DYDragDropHandler.h"

@class DYDropTarget;
typedef enum DragDropGesture
{
	DragDropGestureLongPress, 
	DragDropGestureSwipe
} DragDropGesture;


@interface DYDraggableView : UIView
{
	NSString *mId;
	DYDragDropHandler *mDragDropHandler;
	
	id<DYDraggableViewDelegate> mDraggableViewDelegate;
}

@property (nonatomic, readonly) NSString *identifier;
@property (nonatomic, retain) id<DYDraggableViewDelegate> dragDropDelegate;


- (void) setDragDropHandler:(DYDragDropHandler*)_handler forGesture:(DragDropGesture)_gesture;

- (void) startedDragging;

- (void) stoppedDragging;

- (void) droppedOnTarget:(DYDropTarget*)_dropTarget;

@end
