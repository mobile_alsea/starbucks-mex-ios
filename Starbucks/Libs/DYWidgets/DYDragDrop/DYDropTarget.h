//
//  DYDropTarget.h
//  hofmann
//
//  Created by Juan Antonio Romero on 02/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYDropTargetDelegate.h"

@class DYDraggableView;

@interface DYDropTarget : UIView
{
	id<DYDropTargetDelegate> mDropTargetDelegate;
}

@property (nonatomic, retain) id<DYDropTargetDelegate> dropTargetDelegate;

- (void) enteredDraggable:(DYDraggableView*)_draggableView;

- (void) exitedDraggable:(DYDraggableView*)_draggableView;

- (void) droppedDraggable:(DYDraggableView*)_draggableView;

@end
