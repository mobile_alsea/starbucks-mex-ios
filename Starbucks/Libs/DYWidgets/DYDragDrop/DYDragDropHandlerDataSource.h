//
//  DYDragDropHandlerDataSource.h
//  hofmann
//
//  Created by Juan Antonio Romero on 20/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYDraggableView;
@class DYDropTarget;

@protocol DYDragDropHandlerDataSource <NSObject>

@optional

- (BOOL) dropTarget:(DYDropTarget*)_dropTarget acceptsDraggableView:(DYDraggableView*)_draggableView;

@end
