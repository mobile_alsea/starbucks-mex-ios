//
//  DYDraggableViewDelegate.h
//  hofmann
//
//  Created by Juan Antonio Romero on 03/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DYDraggableView;
@class DYDropTarget;

@protocol DYDraggableViewDelegate <NSObject>

@required

- (void) draggableViewStartedDragging:(DYDraggableView*)_draggableView;

- (void) draggableViewStoppedDragging:(DYDraggableView*)_draggableView;

- (void) draggableView:(DYDraggableView*)_draggableView droppedOnTarget:(DYDropTarget*)_dropTarget;


@optional

- (BOOL) shouldStartDragging:(DYDraggableView*)_draggableView;


@end
