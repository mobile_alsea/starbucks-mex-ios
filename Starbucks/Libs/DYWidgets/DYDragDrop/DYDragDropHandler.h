//
//  DYDragDropHandler.h
//  hofmann
//
//  Created by Juan Antonio Romero on 02/07/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DYDragDropHandlerDataSource.h"

@class DYDraggableView;
@class DYDropTarget;

@interface DYDragDropHandler : NSObject
{
	UIView *mDraggingSurface;
	
	NSMutableDictionary *mDropTargets;
	
	DYDraggableView *mDraggingView;
	DYDropTarget *mCurrentDropTarget;
	UIImageView *mDraggingImage;
	
	id<DYDragDropHandlerDataSource> mDataSource;
}

@property (nonatomic, retain) id<DYDragDropHandlerDataSource> dataSource;

- (id) initWithDraggingSurface:(UIView*)_surface;

- (void) addDropTarget:(DYDropTarget*)_dropTarget forDraggableView:(DYDraggableView*)_draggable;
- (void) addDropTargetArray:(NSArray*)_dropTargetArray forDraggableView:(DYDraggableView*)_draggable;
- (void) setDropTargets:(NSArray*)_dropTargets forDraggableView:(DYDraggableView*)_draggable;
- (void) removeDropTarget:(DYDropTarget*)_dropTarget fromDraggableView:(DYDraggableView*)_draggable;
- (void) removeAllDropTargetsForDraggableView:(DYDraggableView*)_draggable;

- (void) handleDragDropActionForView:(DYDraggableView*)_draggable withGestureRecognizer:(UIGestureRecognizer*)_gestureRecognizer;

@end
