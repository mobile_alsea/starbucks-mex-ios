//
//  ODrinkDetailViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinkDetailViewController.h"
#import "OAppDelegate.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OCustomizeDrinkViewController.h"
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OAbbreviatures.h"
#import "OAbbreviatures+Extras.h"
#import "ODrinkSubcategoryViewController.h"
@interface ODrinkDetailViewController ()

@end

@implementation ODrinkDetailViewController
@synthesize drinkSelected = mDrinkSelected;
@synthesize delegate = mDelegate;
@synthesize nameLabel = mNameLabel;
@synthesize descriptionLabel = mDescriptionLabel;
@synthesize plusButton = mPlusButton;
@synthesize availableButton = mAvailableButton;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize addFoodLabel = mAddFoodLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

/*-(void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
	
}*/

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[self.navigationController.navigationBar setHidden:NO];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	
	mThumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-bebidas-low.png"];
	mPlusDYImageView.failureImage = [UIImage imageNamed:@"noimage-bebidas-high.png"];
    // Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:NSLocalizedString(@"Detalle de bebida", nil)];
	
	mNameLabel.text = mDrinkSelected.name;
	mDescriptionLabel.text = mDrinkSelected.largedescription;
        NSString * s = mDrinkSelected.glassmark;
        s= mDrinkSelected.name;
        s=mDrinkSelected.largedescription;

  
    
    [mThumbnailImageView setAnimationStyle:DYImageViewAnimationStyleWhite];
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:mDrinkSelected.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[mThumbnailImageView setUrl:[NSURL URLWithString:mDrinkSelected.image]];
	}
	else
	{
		[mThumbnailImageView setImage:[UIImage imageWithData:image.image]];
	}
	
	
	
	mFavorite =[OMyDrinks myDrinkById:mDrinkSelected.ide inContext:mAddingContext];
	
	if(mFavorite!=nil)
	{ 
		mIsAdded = YES;
		[mAddingContext deleteObject:mFavorite];
		mAddFoodLabel.text = @"Eliminar de mis Bebidas";
	}
	else
	{
		mIsAdded = NO;
		mAddFoodLabel.text = @"Añadir a mis Bebidas";
	}
	[self updateSchedule];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	//if(!isOS6())
    /*
	{
		[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
		UIViewController * prueba = [[UIViewController alloc] init];
		[prueba.view setHidden:YES];
		[self presentModalViewController:prueba animated:NO];
		[self dismissModalViewControllerAnimated:NO];
	}
    */
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

-(IBAction)plusButtonSelected:(id)sender
{
	@try{
	[mPlusView setHidden:NO];
	//[mPlusDYImageView setAnimationStyle:DYImageViewAnimationStyleWhite];
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:mDrinkSelected.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[mPlusDYImageView setUrl:[NSURL URLWithString:mDrinkSelected.image]];
	}
	else
	{
		[mPlusDYImageView setImage:[UIImage imageWithData:image.image]];
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
}
-(IBAction)dismmisPlusButtonSelected:(id)sender
{
	[mPlusView setHidden:YES];
	[mPlusDYImageView setImage:nil];
	
}

-(IBAction)CustomizeDrinkButtonSelected:(id)sender
{
    @try{
	OCustomizeDrinkViewController *detailViewController = [[OCustomizeDrinkViewController alloc] initWithNibName:@"OCustomizeDrinkViewController" bundle:nil];
	[detailViewController setDrinkSelected:mDrinkSelected];
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

-(IBAction)addToMyDrink:(id)sender
{
    @try{
	if(mIsAdded==YES)
	{
		//Delete from bd
		
		[mAddingContext deleteObject:mFavorite];
		mAddFoodLabel.text = @"Añadir a mis Bebidas";
		NSError * error = nil;
		[mAddingContext save:&error];
		mIsAdded = NO;
		
	}
	else
	{
		NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyDrinks" inManagedObjectContext:mAddingContext];
		mFavorite = [[OMyDrinks alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:mAddingContext];
		
		mFavorite.ide = mDrinkSelected.ide;
		mFavorite.available = mDrinkSelected.available;
		mFavorite.category = mDrinkSelected.category;
        NSString * glassmark = ((OAbbreviatures*)[OAbbreviatures AbbreviaturesByName:mDrinkSelected.name inContext:mAddingContext]).abbreviature;
        mFavorite.glassmark =glassmark;
		mFavorite.image = mDrinkSelected.image;
		mFavorite.largedescription = mDrinkSelected.largedescription;
		mFavorite.name = mDrinkSelected.name;
		mFavorite.shortdescription = mDrinkSelected.shortdescription;
		mFavorite.subcategory = mDrinkSelected.subcategory;
		mFavorite.temperature = mDrinkSelected.temperature;
		mFavorite.user = @"Yo";
		mFavorite.userType = @"1";
		mFavorite.nickName=@"";
		mFavorite.notes=@"";
		NSError * error = nil;
		[mAddingContext save:&error];
		mAddFoodLabel.text = @"Eliminar de mis Bebidas";
		mIsAdded = YES;
		
	}	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)updateSchedule
{
    @try{
    UIImage *stateSummer = [UIImage imageNamed:@"DrinkSizeButtonUnselected.png"];
    [mAvailableButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [mAvailableButton setTitle:mDrinkSelected.available forState:UIControlStateNormal];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkDetailViewController");
        ODrinkSubcategoryViewController  *regresa =[[ODrinkSubcategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

@end
