//
//  OStoresAdressCell.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OStoresAdressCell : UITableViewCell
{
    UILabel *mNameLabel;
    UILabel *mAddressLabel;
    UILabel *mAreaLabel;
    UILabel *mCityLabel;
    UILabel *mPostalCodeLabel;
}

@property (nonatomic, strong) IBOutlet UILabel* nameLabel;
@property (nonatomic, strong) IBOutlet UILabel* addressLabel;
@property (nonatomic, strong) IBOutlet UILabel* areaLabel;
@property (nonatomic, strong) IBOutlet UILabel* cityLabel;
@property (nonatomic, strong) IBOutlet UILabel* postalcodeLabel;


@end
