//
//  OFiltersViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OFoodViewController.h"
#import "OBaseViewController.h"

@interface OFiltersViewController : OBaseViewController<UITableViewDelegate,UITableViewDataSource>
{
	IBOutlet UIButton * mEndFiltersButton;
	IBOutlet UITableView *mTableView;
	NSMutableArray * mFiltersArray;
	NSMutableArray * mFiltersSelected;
	OFoodViewController * mDelegate;
}

@property(nonatomic, strong) OFoodViewController * delegate;
@property(nonatomic, strong) NSMutableArray *filtersSelected;

-(IBAction) endFilter:(id) sender;
-(void) UpdateFiltersArrayForIndex:(int)mIndex;
@end
