//
//  OStoresCell.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OStoresCell : UITableViewCell
{
    UILabel *mNameLabel;
    UILabel *mAddressLabel;
    
    BOOL mIndexPathPair;
    
    UIButton *mScheduleButton;
	
	UIImageView		*mDriveView;
	UIImageView		*mMobileView;
	UIImageView		*mReserveView;
	UIImageView		*mWirelessView;
	UIImageView		*mWarmedView;
	UILabel			*mDistanceLabel;
}

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *addressLabel;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, strong) IBOutlet UIButton *scheduleButton;

@property (nonatomic, strong) IBOutlet UIImageView	*driveView;
@property (nonatomic, strong) IBOutlet UIImageView	*mobileView;
@property (nonatomic, strong) IBOutlet UIImageView	*reserveView;
@property (nonatomic, strong) IBOutlet UIImageView	*wirelessView;
@property (nonatomic, strong) IBOutlet UIImageView	*warmedView;
@property (nonatomic, strong) IBOutlet UILabel *distanceLabel;

- (void)updateSchedule:(BOOL)_state;

@end
