//
//  ONotImplementedViewController.h
//  Starbucks
//
//  Created by Adrián Caramés on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface ONotImplementedViewController : OBaseViewController

@end
