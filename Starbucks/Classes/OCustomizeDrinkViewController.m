//
//  OCustomizeDrinkViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCustomizeDrinkViewController.h"
#import "OCustomizeShotsViewController.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "ODrinkViewController.h"
@interface OCustomizeDrinkViewController ()

@end

@implementation OCustomizeDrinkViewController
@synthesize drinkSelected = mDrinkSelected;
@synthesize textLabel = mTextLabel;
@synthesize sizeLabel = mSizeLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
	
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeDrinkViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	mVentiButton.selected = NO;
	mGrandeButton.selected = YES;
	mAltoButton.selected = NO;
	mImageView.transform = CGAffineTransformMakeScale(0.9, 0.9);
	
	NSString * name = mDrinkSelected.name;
	name = [name stringByAppendingString:@".png"];
	UIImage * image = [UIImage imageNamed:name];
	if(image==nil)
	{
		mImageView.image = [UIImage imageNamed:@"Cafe standar.png"];
	}
	else
	{
		//mImageView.image = 
		mImageView.image = image;
	}
	
	
	
	
	[self.navigationItem setTitle:NSLocalizedString(@"Tamaño", nil)];
	mTextLabel.text = mDrinkSelected.name;
	mSizeLabel.text =@"Grande";
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Siguiente", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnNextSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeDrinkViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
	

    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
	UIViewController * prueba = [[UIViewController alloc] init];
	[prueba.view setHidden:YES];
	[self presentModalViewController:prueba animated:NO];
	[self dismissModalViewControllerAnimated:NO];
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)btnNextSelected:(id)_sender
{
    @try{
	OCustomizeShotsViewController *detailViewController = [[OCustomizeShotsViewController alloc] initWithNibName:@"OCustomizeShotsViewController" bundle:nil];
	[detailViewController setDrinkSelected:mDrinkSelected];
	detailViewController.sizeSelected = mSizeSelected;
	
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeDrinkViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
	
}

#pragma mark IBACtions

-(IBAction)ButtonSizeSelected:(id)sender
{
	@try{
	UIButton * button = (UIButton*) sender;
	switch (button.tag) {
		case 0:
		{
			if(mVentiButton.selected==NO)
			{
				mVentiButton.selected = !mVentiButton.selected;
				mSizeSelected = @"Venti";
			}
			[UIView animateWithDuration:0.5 animations:^(void) {
				mImageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
			}];
			mSizeLabel.text =@"Venti";
			mGrandeButton.selected = NO;
			mAltoButton.selected = NO;

		}
			
			break;
		
		case 1:
		{
			mVentiButton.selected = NO;
			[UIView animateWithDuration:0.5 animations:^(void) {
				mImageView.transform = CGAffineTransformMakeScale(0.85, 0.85);
			}];
			mSizeLabel.text =@"Grande";
			
			if(mGrandeButton.selected==NO)
			{
				mGrandeButton.selected = !mGrandeButton.selected;
				mSizeSelected = @"Grande";
			}
			mAltoButton.selected = NO;
		
		}
			break;
		
		case 2: 
		{
			mVentiButton.selected = NO;
			
			
			[UIView animateWithDuration:0.5 animations:^(void) {
				mImageView.transform = CGAffineTransformMakeScale(0.7, 0.7);
			}];
			mSizeLabel.text =@"Alto";
			mGrandeButton.selected = NO;
			if(mAltoButton.selected==NO)
			{
				mAltoButton.selected = !mAltoButton.selected;
				mSizeSelected=@"Alto";
			}
		
		}
			break;
		default:
			break;
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeDrinkViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }


}

@end
