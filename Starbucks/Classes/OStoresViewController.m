//
//  OStoresViewController.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <DYCore/files/DYFileNameManager.h>

#import "OStoresViewController.h"
#import "OStoresCell.h"
#import "OStoresDetailViewController.h"
#import "OStoresScheduleViewController.h"
#import "OStore.h"
#import "OStore+Extras.h"
#import "OSchedule.h"
#import "OService.h"
#import "OService+Extras.h"
#import "OMapAnnotation.h"

#import "ADClusterAnnotation.h"
#import "ARGeoCoordinate.h"
#import "ARCameraViewController.h"
#import "SBStoreARView.h"
#import "SBCoachingView.h"

static NSString *storeCellIdentifier = @"OStoresCell";

@interface OStoresViewController () <OStoresDetailViewControllerDelegate>

// Initialize Core Data on iOS
@property (nonatomic, readonly) NSManagedObjectContext *storesMOC;
@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, readonly) CMMotionManager *motionManager;
@property (nonatomic, readonly) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet ADClusterMapView *mapView;
@property (assign, nonatomic) NSInteger selectedSegmentedIndex;
@property (strong, nonatomic) UIButton *lightButton;
@property (assign, nonatomic) BOOL isARAllowed;
@property (strong, nonatomic) ARCameraViewController *cameraController;
@property (assign, readonly) BOOL isDismissAllowed;
@property (assign, readonly) BOOL isDismissLockedFromController;
@property (strong, nonatomic) SBCoachingView *coachingView;
 
@end

@implementation OStoresViewController {
    BOOL                    _isRAControllerUpside;
    BOOL                    _isControllerAnimating;
    NSPredicate             *_predicate;
    CLLocation              *_userLocation;
    UIView                  *_coachView;
    UIView                  *_backgroundBlack;
    UIButton                *_okButton;
    BOOL                    _firstLoad;
    BOOL                    _needUpdateMap;
    NSString                *_lastGeneralDistance;
    
    NSMutableArray          *_annotations;
    NSMutableArray          *_coordinates;
    NSMutableArray          *_arCoordinates;
    OSControllerMode        _mode;
}

@synthesize storesMOC = _storesMOC;
@synthesize fetchedResultsController = _fetchedResultsController;

@synthesize servicesArray = mServicesArray;
@synthesize searchBar = mSearchBar;
@synthesize isActualizeView = mIsActualizeView;

@synthesize motionManager = _motionManager;
@synthesize locationManager = _locationManager;
@synthesize isARAllowed = _isARAllowed;

#pragma mark - Custom Setters
- (void)setIsARAllowed:(BOOL)isARAllowed {
    _isARAllowed = isARAllowed;    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:isARAllowed?@YES:@NO forKey:kOSARAllowed];
    [defaults synchronize];
    self.lightButton.enabled = _isARAllowed;
}

#pragma mark - Custom Getters
- (BOOL)isARAllowed {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:kOSARAllowed] boolValue];    
}

- (NSManagedObjectContext *)storesMOC {
    if (_storesMOC)
        return _storesMOC;
    
    _storesMOC = [[NSManagedObjectContext alloc] init];
    _storesMOC.persistentStoreCoordinator = [OAppDelegate coordinator];
    return _storesMOC;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController)
        return _fetchedResultsController;
    
    NSFetchRequest *fetchRequest = [self getStoresFetchRequestForSelf:self];
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:self.storesMOC
                                                                      sectionNameKeyPath:nil
                                                                               cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error])
        DLog(@"Hubo un error al ejecutar el fetched result controller > %@", error);
    
    return _fetchedResultsController;
}

- (CMMotionManager *)motionManager {
    if (_motionManager)
        return _motionManager;
    
    _motionManager = [[CMMotionManager alloc] init];
    return _motionManager;
}

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.headingFilter = kCLHeadingFilterNone;
        _locationManager.distanceFilter = 20.0;
        _locationManager.delegate = self;
    }
    
    return _locationManager;
}

- (BOOL)isDismissAllowed {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [[defaults objectForKey:kOSLockARDismissKey] boolValue];
}

- (BOOL)isDismissLockedFromController {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [[defaults objectForKey:kOSDismissLockFromController] boolValue];
}

#pragma mark - AR Locations Methods
- (NSMutableArray *)locationsFromStoresInSelf:(OStoresViewController *)strongSelf {
    NSMutableArray *tempLocationArray = [NSMutableArray arrayWithCapacity:self.fetchedResultsController.fetchedObjects.count];
    
    for (OStore *store in strongSelf.fetchedResultsController.fetchedObjects) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[store.latitude doubleValue] longitude:[store.longitude doubleValue]];
        ARGeoCoordinate *tempCoordinate = [ARGeoCoordinate coordinateWithLocation:location];
        tempCoordinate.title = store.name;
        tempCoordinate.subtitle = store.address;
        tempCoordinate.storeID = store.objectID;
        [tempLocationArray addObject:tempCoordinate];
    }        
    return tempLocationArray;
}

#pragma mark - Private Selectors
- (void)didSaveContext:(NSNotification *)notification {
    [self.storesMOC mergeChangesFromContextDidSaveNotification:notification];
}

- (BOOL)isOpenInStore:(OStore *)_store {
    @try{
        BOOL ret = NO;
        
        NSDate *today = [NSDate date];
        NSDateFormatter *dformatter = [[NSDateFormatter alloc] init];
        
        [dformatter setDateFormat:@"e"];
        NSInteger weekDay = [[dformatter stringFromDate:today] integerValue];
        if(weekDay == 0) // Starts on Sunday
            weekDay = 6;
        else
            --weekDay;
        
        if([_store.schedulesArray count] > weekDay)
        {
            OSchedule *schedule = [_store.schedulesArray objectAtIndex:weekDay];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"HH:mm"];
            
            NSDate *timeToday = [timeFormatter dateFromString:[timeFormatter stringFromDate:today]];
            NSDate *timeStar = [timeFormatter dateFromString:schedule.startTime];
            NSDate *timeEnd = [timeFormatter dateFromString:schedule.endTime];
            
            if([timeToday laterDate:timeStar] && [timeToday earlierDate:timeEnd])
            {
                ret = YES;
            }
        }
        
        return ret;
    }
    
    
    @catch (NSException *ex) {
        
        DLog(@"No Disponible - Starbucks/OStoresViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (NSFetchRequest *)getStoresFetchRequestForSelf:(OStoresViewController *)strongSelf {
    NSString *readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"km"];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Stores"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES]];
    NSMutableArray *predicates = [@[] mutableCopy];
    
    if (strongSelf->_predicate)
        [predicates addObject:strongSelf->_predicate];
    
    if (readValue && ![readValue isEqualToString:@"-1"])
        [predicates addObject:[NSPredicate predicateWithFormat:@"distance <= %@", @([readValue doubleValue])]];
    
    NSArray *services = [OService servicesSelectedInContext:[OAppDelegate managedObjectContext]];
    for (OService *service in services)
        [predicates addObject:[NSPredicate predicateWithFormat:@"ANY services.name == %@", service.name]];
    
    // Create the predicate
    if (predicates.count == 1)
        fetchRequest.predicate = [predicates lastObject];
    else if (predicates.count > 1)
        fetchRequest.predicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:predicates];
    
    return fetchRequest;
}

- (NSPredicate *)getStoresPredicateForSelf:(OStoresViewController *)strongSelf {
    NSString *readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"km"];
    NSMutableArray *predicates = [@[] mutableCopy];
    
    if (strongSelf->_predicate)
        [predicates addObject:strongSelf->_predicate];
    
    if (readValue && ![readValue isEqualToString:@"-1"])
        [predicates addObject:[NSPredicate predicateWithFormat:@"distance <= %@", @([readValue doubleValue])]];
    
    NSArray *services = [OService servicesSelectedInContext:[OAppDelegate managedObjectContext]];
    for (OService *service in services)
        [predicates addObject:[NSPredicate predicateWithFormat:@"ANY services.name == %@", service.name]];
    
    if (predicates.count == 1)
        return [predicates lastObject];
    else if (predicates.count > 1)
        return [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType subpredicates:predicates];
    
    return nil;
}

- (void)reloadInformation {
    NSPredicate *predicate = [self getStoresPredicateForSelf:self];
    [self.fetchedResultsController.fetchRequest setPredicate:predicate];
    NSError *error;
    
    if ([self.fetchedResultsController performFetch:&error])
        [self.tableView reloadData];
}

#pragma mark - NotificationCenter Methods

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    if (_userLocation)
        [self startDeviceMotionUpdates];
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    [self stopDeviceMotion];
}

#pragma mark - Custom Alert Methods
- (void)showCoachMark {
    if([SBCoachingView shouldPresentView] && self.coachingView == nil) {
        SBCoachingView *coachingView = [SBCoachingView getView];
        CGRect viewFrame = self.view.bounds;
        viewFrame.size.height -= 48.0f;
        coachingView.frame = viewFrame;
        self.coachingView = coachingView;
        [self.view addSubview:self.coachingView];
        [self.coachingView show];
    }
}

- (void)calculateDistanceFromUserLocation:(CLLocation *)userLocation completion:(void(^)(void))completion {
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    __weak OStoresViewController *weakSelf = self;
    [queue addOperationWithBlock:^{
        OStoresViewController *strongSelf = weakSelf;
        
        if (strongSelf) {
            for (ARGeoCoordinate *coordinate in strongSelf->_coordinates)
                coordinate.radialDistance = [coordinate.geoLocation distanceFromLocation:userLocation];
            
            [strongSelf->_coordinates sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                ARGeoCoordinate *first = (ARGeoCoordinate *)obj1;
                ARGeoCoordinate *second = (ARGeoCoordinate *)obj2;
                
                if (first.radialDistance < second.radialDistance)
                    return NSOrderedAscending;
                else if (first.radialDistance > second.radialDistance)
                    return NSOrderedDescending;
                
                return NSOrderedSame;
            }];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:completion];
        }
    }];
}

#pragma mark - UIViewController Life Cycle Selectors
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        mAppDelegate = (OAppDelegate *) [[UIApplication sharedApplication] delegate];
        _annotations = [@[] mutableCopy];
        mIsActualizeView = NO;
        
        _firstLoad = YES;
        _needUpdateMap = NO;
        _isRAControllerUpside = NO;
        _isControllerAnimating = NO;
        _mode = OSControllerModeNone; // Al inicio no se tiene ningún controlador encima

        // Reser dismiss lock from controller (when watching a detail or the orientation)
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@NO forKey:kOSDismissLockFromController];
        [defaults synchronize];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationWillResignActive:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didSaveContext:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc {
	[self.locationManager stopUpdatingLocation];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)customizeAppearance {
    _navBar.tintColor = mSearchBar.tintColor;
    [self.navigationItem setTitle:NSLocalizedString(@"Tiendas", nil)];
    
    // Setting Segmented
    mSegmentedControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Mapa", nil), NSLocalizedString(@"Lista", nil), NSLocalizedString(@"Vista", nil)]];
    [mSegmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    [mSegmentedControl setWidth:80.0 forSegmentAtIndex:0];
    [mSegmentedControl setWidth:80.0 forSegmentAtIndex:1];
    [mSegmentedControl setWidth:80.0 forSegmentAtIndex:2];
    [mSegmentedControl setSelectedSegmentIndex:0];
    self.selectedSegmentedIndex = mSegmentedControl.selectedSegmentIndex;
    [mSegmentedControl addTarget:self action:@selector(segmentedControlSelected:) forControlEvents:UIControlEventValueChanged];
    
    if([CLLocationManager authorizationStatus]!=kCLAuthorizationStatusAuthorized || ![CLLocationManager locationServicesEnabled]) {
        self.isARAllowed = NO;
    }
    
    self.lightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.lightButton setImage:[UIImage imageNamed:@"luz_off"] forState:UIControlStateDisabled];
    [self.lightButton setImage:[UIImage imageNamed:@"luz_on"] forState:UIControlStateNormal];
    self.lightButton.enabled = self.isARAllowed;
    self.lightButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0f);
    
    UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ToolbarFilter.png"]
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(btnFilterSelected:)];
    [self.navigationItem setTitleView:mSegmentedControl];
    [self.navigationItem setRightBarButtonItem:btnFilter];
    [self.navigationItem.titleView addSubview:self.lightButton];
    
    CGFloat viewMidX = CGRectGetMidX(self.navigationItem.titleView.bounds);
    CGFloat segmentWidth = 80.0f;
    self.lightButton.center = CGPointMake(viewMidX+(segmentWidth*1.5f)-CGRectGetMidX(self.lightButton.bounds), CGRectGetMidY(mSegmentedControl.bounds));
    
    mSearchBar.placeholder = @"cp, colonia, ciudad, etc.";
    mLastUserLocation = [[CLLocation alloc] initWithLatitude:0.0 longitude:0.0];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OStoresCell" bundle:nil] forCellReuseIdentifier:storeCellIdentifier];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeAppearance];
    
    // Create the AR controller
    self.cameraController = [[ARCameraViewController alloc] init];
	self.cameraController.arGeoController.delegate = self;
	self.cameraController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    // Pass the location manager
    self.cameraController.arGeoController.locationManager = self.locationManager;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self showCoachMark];
    
    if (_firstLoad) {
        _lastGeneralDistance = [[NSUserDefaults standardUserDefaults] stringForKey:@"km"];

        if (!_lastGeneralDistance) {
            [[NSUserDefaults standardUserDefaults] setObject:@"-1" forKey:@"km"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            _lastGeneralDistance = @"-1";
        }
        
        _firstLoad = NO;
        [self.locationManager startUpdatingLocation];
        [self reloadMapAnnotations];
    }
    else {
        DLog(@"_mode > %d", _mode);
        
        NSString *distance = [[NSUserDefaults standardUserDefaults] stringForKey:@"km"];
        if (_lastGeneralDistance != distance) {
            _lastGeneralDistance = distance;
            [self reloadInformation];
            
            if (self.mapView.hidden)
                _needUpdateMap = YES;
            else
                [self reloadMapAnnotations];
        }
    }
    
    if (_mode == OSControllerModeDetail) // Returns _mode variable to his default state
        _mode = OSControllerModeNone;
    
    if (_arCoordinates && _mode != OSControllerModeAR) // If we have the coordenates array
        [self startDeviceMotionUpdates];
}

- (void)viewDidDisappear:(BOOL)animated {
    if (_mode != OSControllerModeAR)
        [self stopDeviceMotion];
    
    [super viewDidDisappear:animated];
}


#pragma mark - Private Methods
- (void)segmentedControlSelected:(id)sender {
    [mSearchBar resignFirstResponder];
    NSInteger selectedIndex = [mSegmentedControl selectedSegmentIndex];
    
    switch (selectedIndex) {
        case 0: {
            self.selectedSegmentedIndex = selectedIndex;
            [self.mapView setHidden:NO];
            [self.tableView setHidden:YES];
            
            if (_needUpdateMap) {
                _needUpdateMap = NO;
                [self reloadMapAnnotations];
            }
        }   break;
        case 1: {
            self.selectedSegmentedIndex = selectedIndex;
            [self.mapView setHidden:YES];
            [self.tableView setHidden:NO];
        }   break;
        default: {
            UISegmentedControl *segmentedControl = sender;
            segmentedControl.selectedSegmentIndex = self.selectedSegmentedIndex;
            BOOL newARAllowedValue = !self.isARAllowed;
            if(newARAllowedValue && ([CLLocationManager authorizationStatus]!=kCLAuthorizationStatusAuthorized || ![CLLocationManager locationServicesEnabled])) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Starbucks", nil)
                                                                    message:NSLocalizedString(@"Necesitas habilitar el servicio de localización para acceder a la vista de Realidad Aumentada.", nil)
                                                                   delegate:nil
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:NSLocalizedString(@"Continuar", nil), nil];
                [alertView show];
                self.isARAllowed= NO;
            } else {
                self.isARAllowed = newARAllowedValue;
            }
        }   break;
    }
}

- (void)executeFetch {
    _fetchedResultsController = nil;
    NSFetchedResultsController *fcontroller = [self fetchedResultsController];
    NSError *error = nil;
    
    if (![fcontroller performFetch:&error]) {
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    if (!_coordinates)
        _coordinates = [self locationsFromStoresInSelf:self];
    
    [self calculateDistanceFromUserLocation:_userLocation
                                 completion:^{
                                     DLog(@"");
                                 }];
    
    [self.tableView reloadData];
}

- (void)configureCell:(OStoresCell *)_cell atIndexPath:(NSIndexPath *)_indexPath {
    @try{
        OStore * store = [self.fetchedResultsController objectAtIndexPath:_indexPath];
        
        BOOL isOpen = [self isOpenInStore:store];
        
        [_cell updateSchedule:isOpen];
        _cell.scheduleButton.tag = _indexPath.row;
        
        if([[_cell.scheduleButton actionsForTarget:self forControlEvent:UIControlEventTouchUpInside] count] <= 0)
			[_cell.scheduleButton addTarget:self action:@selector(actionScheduleClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _cell.distanceLabel.text =[NSString stringWithFormat:@"%.1f km",[store.distance floatValue]];
        _cell.nameLabel.text = store.name;
        _cell.addressLabel.text = store.address;
        _cell.driveView.alpha = 0.3;
        _cell.mobileView.alpha = 0.3;
        _cell.reserveView.alpha = 0.3;
        _cell.wirelessView.alpha = 0.3;
        _cell.warmedView.alpha = 0.3;
        
        for(OService *s in store.services)
        {
            if([s.name hasPrefix:@"Drive"])
                _cell.driveView.alpha = 1.0;
            
            else if([s.name hasPrefix:@"Pago"])
                _cell.mobileView.alpha = 1.0;
			
            else if([s.name hasSuffix:@"Reserve"])
                _cell.reserveView.alpha = 1.0;
			
            else if([s.name hasPrefix:@"Wireless"])
                _cell.wirelessView.alpha = 1.0;
            
            else if([s.name hasSuffix:@"Horno"])
                _cell.warmedView.alpha = 1.0;
        }
    }
    
    
    @catch (NSException *ex) {
        
        DLog(@"No Disponible - Starbucks/OStoresViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void) actionScheduleClick:(UIButton *)_button {
    @try{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_button.tag inSection:0];
        OStore *store = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        OStoresScheduleViewController *storeScheduleController = [[OStoresScheduleViewController alloc] initWithNibName:@"OStoresScheduleViewController" bundle:nil];
        [self.navigationController pushViewController:storeScheduleController animated:YES];
        storeScheduleController.storeSelected = store;
    }
    
    
    @catch (NSException *ex) {
        
        DLog(@"No Disponible - Starbucks/OStoresViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)reloadMapAnnotations {
    if (_annotations.count) {
        [self.mapView removeAnnotations:_annotations];
        [_annotations removeAllObjects];
    }
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Cargando mapa", nil) maskType:SVProgressHUDMaskTypeGradient];
    __weak OStoresViewController *weakSelf = self;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        OStoresViewController *strongSelf = weakSelf;
        
        if (strongSelf) {
            NSArray *stores = strongSelf.fetchedResultsController.fetchedObjects;
            [stores enumerateObjectsUsingBlock:^(OStore *store, NSUInteger idx, BOOL *stop) {
                OMapAnnotation *annotation = [[OMapAnnotation alloc] init];
                annotation.storeInfo = @{@"objectID": store.objectID,
                                         @"name": store.name,
                                         @"address": store.address,
                                         @"latitude": store.latitude,
                                         @"longitude": store.longitude};
                [strongSelf->_annotations addObject:annotation];
            }];
            
            __weak OStoresViewController *weakSelfL1 = strongSelf;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                OStoresViewController *strongSelfL1 = weakSelfL1;
                
                if (strongSelfL1)
                    [strongSelfL1.mapView setAnnotations:strongSelfL1->_annotations];
                else
                    [SVProgressHUD dismiss];
            }];
        }
    }];
}

- (MKCoordinateRegion)getRegionForMapInUbications: (NSArray *) _ubications {
    @try {
        MKCoordinateRegion region;
        
        if(_ubications == nil || [_ubications count] == 0)
        {
            region = self.mapView.region;
        }
        else if([_ubications count] == 1)
        {
            id <MKAnnotation> pin = (id <MKAnnotation>) [_ubications objectAtIndex:0];
            region = MKCoordinateRegionMakeWithDistance(pin.coordinate, 1000, 1000);
        }
        else
        {
            double latCurrent = 0.0;
            double lonCurrent = 0.0;
            
            double latMax = -FLT_MAX;
            double lonMax = -FLT_MAX;
            double latMin =  FLT_MAX;
            double lonMin =  FLT_MAX;
            
            for(id <MKAnnotation> pin in _ubications)
            {
                latCurrent = pin.coordinate.latitude;
                lonCurrent = pin.coordinate.longitude;
                
                if(latMax < latCurrent)
                    latMax = latCurrent;
                
                if(lonMax < lonCurrent)
                    lonMax = lonCurrent;
                
                if(latMin > latCurrent)
                    latMin = latCurrent;
                
                if(lonMin > lonCurrent)
                    lonMin = lonCurrent;
            }
            
            CLLocationCoordinate2D center;
            center.latitude  = (latMax + latMin)/2;
            center.longitude = (lonMax + lonMin)/2;
            
            MKCoordinateSpan span;
            span.latitudeDelta  = ((latMax - latMin)/2) * 3;
            span.longitudeDelta = ((lonMax - lonMin)/2) * 3;
            
            region = MKCoordinateRegionMake(center, span);
        }
        
        return region;
    }
    
    
    @catch (NSException *ex) {
        
        DLog(@"No Disponible - Starbucks/OStoresViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Public Selectors

- (void)stopUpdatingUserLocation {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - IBActions

- (void)btnFilterSelected:(id)_sender {
    @try{
//      If we have OStoresFiltersViewController as a Controller, stop acelerometer updates
        [self stopDeviceMotion];
        _mode = OSControllerModeFilter;
        
        OStoresFiltersViewController *filtersViewController = [[OStoresFiltersViewController alloc] initWithNibName:@"OStoresFiltersViewController" bundle:nil];
        
        SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:filtersViewController];
        
        if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] )
        {
            UIImage *image = [UIImage imageNamed:@"NavBar.png"];
            [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
            navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
            
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
                [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
            }
        }
        
        
        [self presentModalViewController:navigationController animated:YES];
        
        //[self presentModalViewController:filtersViewController animated:YES];
        filtersViewController.delegate = self;
    }
    
    
    @catch (NSException *ex) {
        
        DLog(@"No Disponible - Starbucks/OStoresViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction) btnUserLocationSelected:(id)_sender {
    @try{
        if([CLLocationManager locationServicesEnabled])
        {
            MKCoordinateRegion userLocationRegion;
            MKCoordinateSpan span = {0.03, 0.03};
            
            userLocationRegion.span = span;
            userLocationRegion.center = self.mapView.userLocation.coordinate;
            
            [self.mapView setRegion:userLocationRegion animated:YES];
            [self.tableView reloadData];
            [self.mapView reloadInputViews];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"La localización en ajustes de su dispositivo está deshabilitada, no se podrán mostrar distancias ni rutas." delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil,nil];
            [alertView show];
        }
    }
    
    
    @catch (NSException *ex) {
        
        DLog(@"No Disponible - Starbucks/OStoresViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
	
}

#pragma mark - MKMapViewDelegate Delegate
- (NSInteger)numberOfClustersInMapView:(ADClusterMapView *)mapView {
    return 20;
}

- (NSString *)clusterTitleForMapView:(ADClusterMapView *)mapView {
    return @"%d tiendas";
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if (![annotation isKindOfClass:[MKUserLocation class]]) {
        static NSString *annIdentifier = @"StoreAnnotation";
        MKPinAnnotationView *aView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annIdentifier];
        
        if (!aView) {
            aView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annIdentifier];
            aView.canShowCallout = YES;
            aView.pinColor = MKPinAnnotationColorGreen;
            aView.animatesDrop = NO;
        }
        else
            aView.annotation = annotation;

        OMapAnnotation *mapAnnotation = (OMapAnnotation *)((ADClusterAnnotation *)annotation).cluster.annotation.annotation;
        if (mapAnnotation.storeInfo) {
            OStore *store = (OStore *)[self.storesMOC objectWithID:mapAnnotation.storeInfo[@"objectID"]];
            UIImageView *iview;
            
            if ([self isOpenInStore:store])
                iview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"StoreMap_OpenNow_bkgnd.png"]];
            else
                iview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"StoreMap_Closed_bkgnd.png"]];
            
            aView.leftCalloutAccessoryView = iview;
            aView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        }
        
        return aView;
    }
    else
        return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    _mode = OSControllerModeDetail;
    OMapAnnotation *mapAnnotation = (OMapAnnotation *)((ADClusterAnnotation *)view.annotation).cluster.annotation.annotation;
    OStore *store = (OStore *)[self.storesMOC objectWithID:mapAnnotation.storeInfo[@"objectID"]];
    OStoresDetailViewController *detailViewController = [[OStoresDetailViewController alloc] initWithNibName:@"OStoresDetailViewController" bundle:nil];
    [detailViewController setStoreSelected:store];
    detailViewController.delegate = self;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)mapViewDidFinishClustering:(ADClusterMapView *)mapView {
    NSArray *annotations = _annotations;
    
    if (annotations.count) {
        MKMapRect zoomRect = MKMapRectNull;
        
        for (OMapAnnotation *ann in annotations) {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(ann.coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
        
        [mapView setVisibleMapRect:zoomRect animated:YES];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Listo", nil)];
    }
    else
        [SVProgressHUD dismiss];
}

#pragma mark - UITableViewDataSource Selectors
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sections = [[self.fetchedResultsController sections] count];
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OStoresCell *cell = (OStoresCell *)[tableView dequeueReusableCellWithIdentifier:storeCellIdentifier];    
    [cell setIndexPathPair:(indexPath.row % 2 == 0) ? YES : NO];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate Selectors
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _mode = OSControllerModeDetail;
    OStore *store = [self.fetchedResultsController objectAtIndexPath:indexPath];
    OStoresDetailViewController *detailViewController = [[OStoresDetailViewController alloc] initWithNibName:@"OStoresDetailViewController" bundle:nil];
    [detailViewController setStoreSelected:store];
    detailViewController.delegate = self;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - NSFetchedResultsControllerDelegate Selectors

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(OStoresCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - UISearchBar Delegate Methods
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (_predicate)
        _predicate = nil;
    
    if (searchText.length)
        _predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ OR city CONTAINS[cd] %@ OR address CONTAINS[cd] %@ OR area CONTAINS[cd] %@ OR postalCode CONTAINS[cd] %@ OR province CONTAINS[cd] %@" , searchText, searchText, searchText, searchText, searchText, searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    _predicate = nil;
    
    [self reloadInformation];
    
    if (self.mapView.hidden)
        _needUpdateMap = YES;
    else
        [self reloadMapAnnotations];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	mIsActualizeView = NO;
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    [self reloadInformation];
    
    if (self.mapView.hidden)
        _needUpdateMap = YES;
    else
        [self reloadMapAnnotations];
}

#pragma mark - CLLocationManagerDelegate Selectors
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    DLog(@"new location > [%g,%g - %gm]", newLocation.coordinate.latitude, newLocation.coordinate.longitude, newLocation.altitude);
    CLLocation *theNewLocation = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    
    NSManagedObjectContext *updateMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    updateMOC.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
    updateMOC.persistentStoreCoordinator = [OAppDelegate coordinator];
    __weak OStoresViewController *weakSelf = self;
    [updateMOC performBlock:^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Stores"];
        NSError *error;
        NSArray *allStores = [updateMOC executeFetchRequest:fetchRequest error:&error];
        
        if (allStores) {
            [allStores enumerateObjectsUsingBlock:^(OStore *store, NSUInteger idx, BOOL *stop) {
                CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:[store.latitude doubleValue]
                                                                       longitude:[store.longitude doubleValue]];
                CLLocationDistance meters = [storeLocation distanceFromLocation:theNewLocation];
                double totalkm = meters/1000;
                store.distance = @(totalkm);
            }];
            
            NSError *saveError;
            if ([updateMOC save:&saveError]) {
                OStoresViewController *strongSelf = weakSelf;
                
                if (strongSelf) {
                    NSMutableArray *allStoresMutable = [allStores mutableCopy];
                    NSPredicate *predicate = [strongSelf getStoresPredicateForSelf:strongSelf];
                    
                    if (predicate)
                        [allStoresMutable filterUsingPredicate:predicate];
                    
                    NSArray *resultStores = [allStoresMutable sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES]]];
                    strongSelf->_arCoordinates = [@[] mutableCopy];
                    [resultStores enumerateObjectsUsingBlock:^(OStore *store, NSUInteger idx, BOOL *stop) {
                        CLLocation *location = [[CLLocation alloc] initWithLatitude:[store.latitude doubleValue] longitude:[store.longitude doubleValue]];
                        ARGeoCoordinate *tempCoordinate = [ARGeoCoordinate coordinateWithLocation:location];
                        tempCoordinate.title = store.name;
                        tempCoordinate.subtitle = store.address;
                        tempCoordinate.storeID = store.objectID;
                        [strongSelf->_arCoordinates addObject:tempCoordinate];
                        
                        if (idx > 8)
                            *stop = YES;
                    }];
                    
                    __weak OStoresViewController *weakSelfL1 = strongSelf;
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        OStoresViewController *strongSelfL1 = weakSelfL1;
                        
                        if (strongSelfL1) {
                            if (!strongSelfL1->_userLocation)
                                [strongSelfL1 startDeviceMotionUpdates];
                            
                            if(!strongSelfL1.cameraController.arGeoController.isShowingCompass && !strongSelfL1.cameraController.arGeoController.isShowingDetail)
                                [strongSelfL1.cameraController.arGeoController reloadCoordinatesWithCoordinates:strongSelfL1->_arCoordinates];
                            
                            if (strongSelfL1.cameraController.arGeoController && (strongSelfL1->_userLocation.coordinate.longitude != theNewLocation.coordinate.longitude || strongSelfL1->_userLocation.coordinate.latitude != theNewLocation.coordinate.latitude)) {
                                strongSelfL1.cameraController.arGeoController.centerLocation = theNewLocation;
                            }
                            
                            strongSelfL1->_userLocation = theNewLocation;
                        }
                    }];
                }
            }
            else
                DLog(@"Hubo un error al actualizar la distancia de las tiendas en segundo plano > %@", saveError);
        }
        else
            DLog(@"Hubo un error al obtener todas las tiendas en segundo plano > %@", error);
    }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    if([self.cameraController.arGeoController respondsToSelector:@selector(locationManager:didUpdateHeading:)])
        [self.cameraController.arGeoController locationManager:manager didUpdateHeading:newHeading];
}

- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager {
    return YES;
}

#pragma mark - ARViewDelegate Selectors

- (UIView *)viewForCoordinate:(ARCoordinate *)coordinate {
	OStore *store = (OStore *)[mAppDelegate.managedObjectContext objectWithID:coordinate.storeID];
	SBStoreARView *storeView = [SBStoreARView getView];
	storeView.storeNameLabel.text = coordinate.title;
	storeView.storeAddressLabel.text = coordinate.subtitle;
	storeView.isOpen = [self isOpenInStore:store];
	return storeView;
}

- (id)objectWithManagedObjectID:(NSManagedObjectID *)managedObjectID {
	OStore *store = (OStore *)[mAppDelegate.managedObjectContext objectWithID:managedObjectID];
	return store;	
}

- (id)inforForStoreWithManagedObjectID:(NSManagedObjectID *)managedObjectID {	
	OStore *store = (OStore *)[mAppDelegate.managedObjectContext objectWithID:managedObjectID];
	NSMutableDictionary *info = [NSMutableDictionary dictionary];
	info[@"isOpen"] = [self isOpenInStore:store]?@YES:@NO;
	info[@"name"] = store.name;
	info[@"address"] = store.address;
	info[@"distance"] = store.distance;
	return info;
}

#pragma mark - OStoresFiltersViewControllerDelegate Selector
- (void)closeFiltersViewController:(OStoresFiltersViewController *)controller {
    self.isActualizeView = NO;
    [self reloadInformation];
    
    __weak OStoresViewController *weakSelf = self;
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 OStoresViewController *strongSelf = weakSelf;
                                 
                                 if (strongSelf) {
                                     if (strongSelf->_userLocation)
                                         [strongSelf startDeviceMotionUpdates];
                                     
                                     strongSelf->_mode = OSControllerModeNone;
                                     
                                     if (strongSelf.mapView.hidden)
                                         strongSelf->_needUpdateMap = YES;
                                     else
                                         [strongSelf reloadMapAnnotations];
                                 }
                             }];
}

#pragma mark - OStoresDetailViewControllerDelegate Methods
- (CLLocation *)storesDetailControllerRequestLocation:(OStoresDetailViewController *)storesDetailController {
	return _userLocation;
}

#pragma mark - Motion Methods
- (void)startDeviceMotionUpdates {
    if ([self.motionManager isDeviceMotionAvailable] && ![self.motionManager isDeviceMotionActive]) {
        [self.motionManager setDeviceMotionUpdateInterval:0.5];
        __weak OStoresViewController *weakSelf = self;
        [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                                withHandler:^(CMDeviceMotion *motion, NSError *error) {
                                                    OStoresViewController *strongSelf = weakSelf;
                                                    
                                                    if (strongSelf) {
                                                        if (motion.gravity.y < -0.9) {
                                                            if (!strongSelf->_isRAControllerUpside && !strongSelf->_isControllerAnimating && strongSelf.isARAllowed) {
                                                                strongSelf->_mode = OSControllerModeAR; // Cuando se termina de colocar el controlar el _mode se pone en realidad aumentada
                                                                strongSelf->_isRAControllerUpside = YES;
                                                                strongSelf->_isControllerAnimating = YES;
																strongSelf.cameraController.arGeoController.centerLocation = strongSelf->_userLocation;
																			
																[[NSNotificationCenter defaultCenter] postNotificationName:kOSAREnabled object:nil];
																[SVProgressHUD showWithStatus:NSLocalizedString(@"Cargando Realidad Aumentada", nil) maskType:SVProgressHUDMaskTypeClear];
																__weak OStoresViewController *weakSelfL1 = strongSelf;
																[strongSelf presentViewController:strongSelf.cameraController
																						 animated:YES
																					   completion:^
																 {
																	 OStoresViewController *strongSelfL1 = weakSelfL1;
																	 if (strongSelfL1) {
																		 [strongSelfL1.cameraController.arGeoController addCoordinates:strongSelfL1->_arCoordinates];
																		 strongSelfL1.cameraController.arGeoController.centerLocation = strongSelfL1->_userLocation;
																		 strongSelfL1->_isControllerAnimating = NO;
																		 [strongSelfL1.cameraController.arGeoController startListening];
																		 [SVProgressHUD dismiss];
																	 }
																 }];                                                                
                                                            }
                                                        }
                                                        else {

                                                            if (strongSelf->_isRAControllerUpside  && !strongSelf->_isControllerAnimating && strongSelf.isDismissAllowed && !strongSelf.isDismissLockedFromController) {
                                                                strongSelf->_isRAControllerUpside = NO;
                                                                strongSelf->_isControllerAnimating = YES;
                                                                																
                                                                __weak OStoresViewController *weakSelfL1 = strongSelf;
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:kOSARDisabled object:nil];
                                                                [strongSelf dismissViewControllerAnimated:YES completion:^{
																	OStoresViewController *strongSelfL1 = weakSelfL1;
																	if (strongSelfL1) {
																		strongSelfL1->_isRAControllerUpside = NO;
																		strongSelfL1->_isControllerAnimating = NO;
																		strongSelfL1->_mode = OSControllerModeNone;
																		[SVProgressHUD dismiss];
																		[strongSelfL1.cameraController.arGeoController stopListening];
																		[strongSelfL1.cameraController.arGeoController removeAllCoordinates];
                                                                        [strongSelfL1.cameraController.captureManager.captureSession stopRunning];
																	}
                                                                }];
                                                            }
                                                        }
                                                    }
                                                }];
    }
}

- (void)stopDeviceMotion {
    if ([self.motionManager isDeviceMotionAvailable] && [self.motionManager isDeviceMotionActive]) {
        [self.motionManager stopDeviceMotionUpdates];
    }
}


@end
