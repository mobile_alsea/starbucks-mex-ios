//
//  OGenericCell.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OGenericCell.h"

#define RGBToFloat(f) (f/255.0)


@implementation OGenericCell


#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}


#pragma mark - Properties

@synthesize titleLabel = mTitleLabel;
@synthesize iconImageView = mIconImageView;
@synthesize disclosureImageView = mDisclosureImageView;
@synthesize delegate = mDelegate;
@synthesize tag = mTag;


- (BOOL) showDisclosure
{
    return mShowDisclosure;
}

- (void) setShowDisclosure:(BOOL)_showDisclosure
{
    mShowDisclosure = _showDisclosure;
    
    if (mShowDisclosure)
        [mDisclosureImageView setHidden:NO];
    else
        [mDisclosureImageView setHidden:YES];
}

- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}


#pragma mark IBACtions

-(IBAction)buttonPressed:(id)sender
{

	if(mTag==10)
	{
		[mDelegate addToMyStores:nil];
	
	}

}


#pragma mark - ViewController Methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
