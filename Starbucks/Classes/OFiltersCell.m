//
//  OFiltersCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFiltersCell.h"

@implementation OFiltersCell

@synthesize filterNameLabel = mFilterNameLabel;
@synthesize checkButton = mCheckButton;
@synthesize isActive = mIsActive;
@synthesize delegate = mDelegate;
@synthesize index = mIndex;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    @try{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFiltersCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction) RadioButtonClick:(id)sender
{
	
	if(mCheckButton.selected==YES)
	{
		mCheckButton.selected=NO;
		mIsActive=NO;
		[mDelegate UpdateFiltersArrayForIndex:mIndex];
	}
	else
	{
		mCheckButton.selected=YES;
		mIsActive=YES;
		[mDelegate UpdateFiltersArrayForIndex:mIndex];
		
	}
	
	
	[mCheckButton setSelected:(!mCheckButton.selected)];
	mIsActive = !mIsActive;
	
}

@end
