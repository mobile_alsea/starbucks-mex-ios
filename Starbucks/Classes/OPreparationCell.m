//
//  OPreparationCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 18/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OPreparationCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation OPreparationCell

@synthesize preparationNameLabel = mPreparationNameLabel;
@synthesize checkButton = mCheckButton;
@synthesize isActive = mIsActive;
@synthesize delegate = mDelegate;
@synthesize index = mIndex;
@synthesize indexPathPair = mIndexPathPair;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
	{
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
		
		
	}
    else
	{
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
		
	}

	
}


- (IBAction) RadioButtonClick:(id)sender
{
	@try{
	if(mCheckButton.selected==YES)
	{
		mCheckButton.selected=NO;
		mIsActive=NO;
		[mDelegate UpdatePreparationArrayForIndex:mIndex];
	}
	else
	{
		mCheckButton.selected=YES;
		mIsActive=YES;
		[mDelegate UpdatePreparationArrayForIndex:mIndex];
		
	}
	
	
	[mCheckButton setSelected:(!mCheckButton.selected)];
	mIsActive = !mIsActive;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/OPreparationCell.m");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

@end
