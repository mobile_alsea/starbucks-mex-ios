//
//  OMyFavoritesViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyFavoritesViewController.h"
#import "OMyFavoritesCell.h"
#import "OMyFavoritesDetailViewController.h"
#import "OMyFavorites.h"
#import "OMyfavorites+Extras.h"
#import "OCoffee.h"
#import "OCoffee+Extras.h"
#import "OFood.h"
#import "OFood+Extras.h"
#import "OStore.h"
#import "OStore+Extras.h"
#import "OAppDelegate.h"
#import "OHomeViewController.h"

@interface OMyFavoritesViewController ()

@end

@implementation OMyFavoritesViewController
@synthesize  tableView = mTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/initWithNibName");
    
    
    OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    @try{
	[super viewWillAppear:animated];
	
	if(mStoresArray!=nil)
	{ 
		mStoresArray = nil;
	}
	if(mCoffeesArray!=nil)
	{
		mCoffeesArray = nil;
	}
	if(mFoodArray!=nil)
	{
		mFoodArray = nil;
	}
	
	mFavoritesArray = [OMyFavorites favoritesWithType:@"1" context:mAddingContext];
	if(mFavoritesArray!=nil && [mFavoritesArray count]>0)
	{
		mStoresArray = [OStore storesByNames:mFavoritesArray inContext:mAddingContext];
	}
	
	
	mFavoritesArray = [OMyFavorites favoritesWithType:@"2" context:mAddingContext];
	if(mFavoritesArray!=nil && [mFavoritesArray count]>0)
	{
		mCoffeesArray = [OCoffee coffeesByNames:mFavoritesArray inContext:mAddingContext];
	}
	
	mFavoritesArray = [OMyFavorites favoritesWithType:@"3" context:mAddingContext];
	if(mFavoritesArray!=nil && [mFavoritesArray count]>0)
	{
		mFoodArray = [OFood foodsByNames:mFavoritesArray inContext:mAddingContext];
	}
	[mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/viewwllappear");
    
    
    OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }

}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:NSLocalizedString(@"Favoritos", nil)];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OMyfavoritesCell";
    
    OMyFavoritesCell *cell = (OMyFavoritesCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
       
    }

	switch (_indexPath.row) {
		case 0:
		{
			cell.titleLabel.text = NSLocalizedString(@"Tiendas", nil);
			NSString * how =@"(";
			how = [how stringByAppendingString:[NSString stringWithFormat:@"%d",[mStoresArray count]]];
			how = [how stringByAppendingString:@")"];
			cell.howManyLabel.text =how;
			cell.iconImageView.image = [UIImage imageNamed:@"icon-Stores-black"]; 
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
		}
			break;
		case 1:
		{
			cell.titleLabel.text = NSLocalizedString(@"Café", nil);
			NSString * how =@"(";
			how = [how stringByAppendingString:[NSString stringWithFormat:@"%d",[mCoffeesArray count]]];
			how = [how stringByAppendingString:@")"];
			cell.howManyLabel.text = how;
			cell.iconImageView.image = [UIImage imageNamed:@"icon-Coffees-black"];
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
		}
			break;
		case 2: 
		{
			cell.titleLabel.text = NSLocalizedString(@"Alimentos", nil);
			NSString * how =@"(";
			how = [how stringByAppendingString:[NSString stringWithFormat:@"%d",[mFoodArray count]]];
			how = [how stringByAppendingString:@")"];
			cell.howManyLabel.text = how;
			cell.iconImageView.image = [UIImage imageNamed:@"icon-Food-black"];
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
		}
			break;
		default:
			break;
	}

	
	
    
    
    return cell;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/tableview");
    
    
    OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}



- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
	OMyFavoritesDetailViewController *detailViewController = [[OMyFavoritesDetailViewController alloc] initWithNibName:@"OMyFavoritesDetailViewController" bundle:nil];
	detailViewController.type = _indexPath.row;
	switch (_indexPath.row) {
		case 0:
		{
			if(mStoresArray!=nil)
			{
			
				detailViewController.favoritesArray=mStoresArray;
				[self.navigationController pushViewController:detailViewController animated:YES];
			}
		}
			break;
		case 1:
		{
			if(mCoffeesArray!=nil)
			{
		
				detailViewController.favoritesArray=mCoffeesArray;
				[self.navigationController pushViewController:detailViewController animated:YES];
			}
		}
			break;
			
		case 2:
		{
			if(mFoodArray!=nil)
			{
				detailViewController.favoritesArray=mFoodArray;
				[self.navigationController pushViewController:detailViewController animated:YES];
			}
		}
			break;
			
		default:
			break;
	}
	
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/tableview");
    
    
    OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}




@end
