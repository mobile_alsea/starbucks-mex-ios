//
//  OPersonalCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 17/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OPersonalCell.h"

@implementation OPersonalCell
@synthesize serviceButton = mServiceButton;
@synthesize textLabel =mTextLabel;
@synthesize type = mType;
@synthesize delegate = mDelegate;
@synthesize index = mIndex;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)sendActionButtonPressed:(id)sender
{
	[mDelegate sendActionButtonPressed:sender withType:mType andIndex:mIndex];

}


@end
