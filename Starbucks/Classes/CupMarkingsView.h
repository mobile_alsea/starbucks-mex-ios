//
//  CupMarkingsView.h
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class CustomDrink;
@class DNLinearLayoutView;

@interface CupMarkingsView : UIView {
//	CustomDrink *drink;
	
	DNLinearLayoutView *decafContainer;
	DNLinearLayoutView *shotsContainer;
	DNLinearLayoutView *syrupContainer;
	DNLinearLayoutView *milkContainer;
	DNLinearLayoutView *customContainer;
	DNLinearLayoutView *drinkContainer;
	NSArray * mCupMarkings;
	IBOutlet UIScrollView *scrollView;
	IBOutlet UIView *contentView;
}

@property (nonatomic, strong) IBOutlet DNLinearLayoutView *decafContainer;
@property (nonatomic, strong) IBOutlet DNLinearLayoutView *shotsContainer;
@property (nonatomic, strong) IBOutlet DNLinearLayoutView *syrupContainer;
@property (nonatomic, strong) IBOutlet DNLinearLayoutView *milkContainer;
@property (nonatomic, strong) IBOutlet DNLinearLayoutView *customContainer;
@property (nonatomic, strong) IBOutlet DNLinearLayoutView *drinkContainer;
//@property (nonatomic, retain) CustomDrink *drink;
@property (nonatomic, strong) NSArray *CupMarkings;;

+ (CupMarkingsView *)viewFromNib;

- (void)reload;

- (void)close;

@end
