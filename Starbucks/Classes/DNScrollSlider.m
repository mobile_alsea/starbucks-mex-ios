//
//  DNScrollSlider.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "DNScrollSlider.h"

#import "DNScrollSlider.h"

#import "DNScrollSliderValueIndicator.h"

@interface DNScrollSlider ()
@property (nonatomic, strong) NSMutableArray *valueIndicators;
@property (nonatomic, strong) NSMutableArray *intermediateValueIndicators;
@end


@implementation DNScrollSlider

@synthesize valuesBetweenNamedValues;
@synthesize intermediateValueIndicators;
@synthesize valueIndicators;
@synthesize valueSeparation;
@synthesize valueCount;
@synthesize valueIndex;
@synthesize broadcastChanges;


- (id)initWithCoder:(NSCoder *)aDecoder {
    @try{
	self = [super initWithCoder:aDecoder];
	if (!self) return nil;
	
	backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScrollSliderBackground.png"]];
	[self addSubview:backgroundView];
	
	valueSeparation = 60.f;
	scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
	scrollView.delegate = self;
	scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
	scrollView.showsHorizontalScrollIndicator = NO;
	[self addSubview:scrollView];
	
	valueIndicators = [NSMutableArray new];
	
	
	if (self.frame.size.width > 230)
	{
		selectionIndicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScrollSliderValueIndicator.png"]];
	}
	else
	{
		selectionIndicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ScrollSliderValueIndicatorSmall.png"]];
	}
	
	CGRect r = selectionIndicator.frame;
	r.size.width = self.frame.size.width;
	selectionIndicator.frame = r;
	[self addSubview:selectionIndicator];
	
	self.broadcastChanges = YES;
	
	return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/DNScrollSlider");
    }
}

#pragma mark -

- (CGPoint)contentOffsetForValueIndex:(NSInteger)inValueIndex {
	return CGPointMake(inValueIndex * valueSeparation / (1 + valuesBetweenNamedValues) - self.bounds.size.width /2, 0);
}

- (NSInteger)valueIndexForContentOffset:(CGPoint)inContentOffset {
	CGFloat position = (inContentOffset.x + self.bounds.size.width/2) / (valueSeparation / (valuesBetweenNamedValues + 1));
	NSInteger rawValue = (NSInteger)floorf(position + 0.5);
	return MAX( MIN(rawValue, (NSInteger)valueCount - 1), 0);
}

- (void)scrollViewDidScroll:(UIScrollView *)inScrollView {
	NSInteger newValueIndex = [self valueIndexForContentOffset:inScrollView.contentOffset];
	if (newValueIndex == valueIndex) return;
	
	valueIndex = newValueIndex;
	if (self.broadcastChanges) {
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	} else {
		self.broadcastChanges = YES;
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)inScrollView willDecelerate:(BOOL)decelerate {
	if (!decelerate) [scrollView setContentOffset:[self contentOffsetForValueIndex:valueIndex] animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)inScrollView {
	[scrollView setContentOffset:[self contentOffsetForValueIndex:valueIndex] animated:YES];
}


#pragma mark -

- (void)updateContentSize {
	scrollView.contentSize = CGSizeMake((valueCount - 1) * valueSeparation, self.bounds.size.height);
}

- (void)reload {
    @try{
	for (UIView *subview in valueIndicators) {
		[subview removeFromSuperview];
	}
	self.valueIndicators = [NSMutableArray array];
	
	for (UIView *subview in intermediateValueIndicators) {
		[subview removeFromSuperview];
	}
	self.intermediateValueIndicators = [NSMutableArray array];
	
	valueCount = self.valueNames.count;
	NSInteger intermediateValueCount = valuesBetweenNamedValues * (valueCount - 1);
	valueCount += intermediateValueCount;
	
	for (NSString *valueName in self.valueNames) {
		DNScrollSliderValueIndicator *valueIndicator = [DNScrollSliderValueIndicator valueIndicator];
		valueIndicator.backgroundColor = [UIColor clearColor];
		valueIndicator.valueLabel.text = valueName;
		
		[valueIndicators addObject:valueIndicator];
		[scrollView addSubview:valueIndicator];
	}
	for (NSInteger i=0; i<intermediateValueCount; i++) {
		UIView *intermediateValueIndicator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, 10)];
		intermediateValueIndicator.backgroundColor = [UIColor grayColor];
		
		[intermediateValueIndicators addObject:intermediateValueIndicator];
		[scrollView addSubview:intermediateValueIndicator];
	}
	
	[self updateContentSize];
	[self setNeedsLayout];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/DNScrollSlider");
    }
}

- (NSArray *)valueNames {
    if (!valueNames) {
        valueNames = [NSArray new];
    }
    return valueNames;
}

- (void)setValueNames:(NSArray *)inValueNames {
	if (valueNames == inValueNames) return;
	valueNames = [inValueNames copy];
	
	[self reload];
}

- (void)setValuesBetweenNamedValues:(NSInteger)value {
	if (valuesBetweenNamedValues == value) return;
	valuesBetweenNamedValues = value;
	[self reload];
}


- (void)setValueSeparation:(CGFloat)newValueSeparation {
	valueSeparation = newValueSeparation;
	[self updateContentSize];
	[self setNeedsLayout];
}

- (void)setValueIndex:(NSInteger)inValueIndex {
	[self setValueIndex:inValueIndex animated:NO];
}

- (void)setValueIndex:(NSInteger)inValueIndex animated:(BOOL)animated {
	valueIndex = inValueIndex;
	[scrollView setContentOffset:[self contentOffsetForValueIndex:valueIndex] animated:animated];
}

#pragma mark -

- (void)layoutSubviews {
    @try{
	CGRect bounds = self.bounds;
	scrollView.contentInset = UIEdgeInsetsMake(0, bounds.size.width/2, 0, bounds.size.width/2);
	
	NSInteger i = 0;
	NSInteger intermediateValueIndicatorIndex = 0;
	for (DNScrollSliderValueIndicator *valueIndicator in valueIndicators) {
		CGFloat base = valueSeparation * i;
		valueIndicator.frame = CGRectMake(base - valueSeparation/2, 0, valueSeparation, self.bounds.size.height);
		
		for (NSInteger j = 0; j < valuesBetweenNamedValues; j++) {
			if (intermediateValueIndicatorIndex >= intermediateValueIndicators.count) break;
			UIView *intermediateIndicator = [intermediateValueIndicators objectAtIndex:intermediateValueIndicatorIndex];
			intermediateIndicator.center = CGPointMake(base + (j + 1) * valueSeparation/(CGFloat)(valuesBetweenNamedValues + 1),
													   intermediateIndicator.bounds.size.height);
			intermediateValueIndicatorIndex++;
		}
		i++;
	}
	
	selectionIndicator.center = CGPointMake(floorf(bounds.size.width/2), selectionIndicator.bounds.size.height/2);
	backgroundView.frame = bounds;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/DNScrollSlider");
    }
}

- (void)dealloc {
    scrollView.delegate = nil;
	scrollView = nil;
}


@end


