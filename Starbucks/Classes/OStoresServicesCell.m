//
//  OStoresServicesCell.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresServicesCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation OStoresServicesCell


#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


#pragma mark - Properties

@synthesize titleLabel = mTitleLabel;
@synthesize iconImageView = mIconImageView;
@synthesize checkImageView = mCheckImageView;

- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}


#pragma mark - UITableViewCell Methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected)
        [mCheckImageView setHidden:NO];
    else
        [mCheckImageView setHidden:YES];

}


#pragma mark - Private Methods

- (void)configureCellWithIndex:(int)_index
{
    @try{
    switch (_index)
    {
        case 0:
            [mTitleLabel setText:NSLocalizedString(@"Acceso WIFI", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 1:
            [mTitleLabel setText:NSLocalizedString(@"Pedido en Automóvil", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 2:
            [mTitleLabel setText:NSLocalizedString(@"Abierta Ahora", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 3:
            [mTitleLabel setText:NSLocalizedString(@"Comida al Horno", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 4:
            [mTitleLabel setText:NSLocalizedString(@"Starbucks Reserve® - Clover Brewed", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 5:
            [mTitleLabel setText:NSLocalizedString(@"iTunes® WiFi Music Store", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 6:
            [mTitleLabel setText:NSLocalizedString(@"Pago con Móvil", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
        case 7:
            [mTitleLabel setText:NSLocalizedString(@"Starbucks Reserve®", nil)];
            [mIconImageView setImage:[UIImage imageNamed:@""]];
            break;
            
        default:
            break;
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresServicesCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

@end
