//
//  OOptionsCodeViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface OOptionsCodeViewController : OBaseViewController<UITableViewDelegate,UITableViewDataSource>
{

	IBOutlet UITableView * mTableView;

}
@end
