//
//  OCustomizeDrinkViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODrink.h"
#import "OBaseViewController.h"

@interface OCustomizeDrinkViewController : OBaseViewController
{

	ODrink * mDrinkSelected;
	IBOutlet UIButton * mVentiButton;
	IBOutlet UIButton * mGrandeButton;
	IBOutlet UIButton * mAltoButton;
	UILabel * mTextLabel;
	IBOutlet UIImageView * mImageView;
	UILabel * mSizeLabel;
	NSString * mSizeSelected;

	

}

@property(nonatomic, strong) ODrink * drinkSelected;
@property(nonatomic, strong) IBOutlet UILabel * sizeLabel;
@property(nonatomic, strong) IBOutlet UILabel * textLabel;




-(IBAction)ButtonSizeSelected:(id)sender;

@end
