//
//  OCoffeeDetailViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCoffee.h"
#import <DYWidgets/DYLayoutView/DYVerticalLayoutView.h>
#import <DYWidgets/DYHorizontalPicker/DYHorizontalPicker.h>
#import "DYImageView.h"
#import "OMyFavorites.h"
#import "OAppDelegate.h"
#import "OBaseViewController.h"

@interface OCoffeeDetailViewController : OBaseViewController<DYHorizontalPickerDelegate,DYHorizontalPickerDataSource>
{

	id __weak mDelegate;
	OCoffee *mcoffeeSelected;
	UIView * mHeaderView;
	UIView * mTextView;
	UIView * mDownView;
	DYVerticalLayoutView * mVerticalLayout;
	
	UILabel * mNameLabel;
	UILabel * mSubTitleLabel;
	UILabel * mHeaderLabel;
	DYImageView * mHeaderImageView;
	UILabel * mDescriptionLabel;
	UIButton * mMoreButton;
	UIButton * mAddCoffeeButton;
	UILabel * mAddCoffeeLabel;
	UIScrollView * mScrollView;
	IBOutlet UILabel * mMoreLabel;
	IBOutlet DYImageView * mArrowImageView;
	IBOutlet DYHorizontalPicker * mHorizontalPicker;
	IBOutlet UIPageControl * mPageControl;
	BOOL mIsAdded;
	OMyFavorites * mFavorite;
	OAppDelegate * mAppDelegate;
	NSManagedObjectContext *mAddingContext;
    IBOutlet UILabel *mAvailability;

}

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) OCoffee *coffeeSelected;
@property (nonatomic, strong) IBOutlet UIView * headerView;
@property (nonatomic, strong) IBOutlet UIView * textView;
@property (nonatomic, strong) IBOutlet UIView * downView;
@property(nonatomic,strong) IBOutlet DYVerticalLayoutView * verticalLayout;
@property(nonatomic, strong) IBOutlet UILabel * nameLabel;
@property(nonatomic, strong) IBOutlet UILabel * subtitleLabel;
@property(nonatomic, strong) IBOutlet UILabel * headerLabel;
@property(nonatomic, strong) IBOutlet DYImageView * headerImageView;
@property(nonatomic, strong) IBOutlet UILabel * descriptionLabel;
@property(nonatomic, strong) IBOutlet UIButton * moreButton;
@property(nonatomic, strong) IBOutlet UIButton * addCoffeeButton;
@property(nonatomic, strong) IBOutlet UILabel * addCoffeeLabel;
@property(nonatomic, strong) IBOutlet UIScrollView * scrollView;
@property (strong, nonatomic) IBOutlet UILabel *Availability;

-(void) setValues;
-(IBAction)mMoreDetailButtonPressed:(id)sender;
-(IBAction)addToMyCoffees:(id)sender;
-(IBAction)pageChangeValue;

@end
