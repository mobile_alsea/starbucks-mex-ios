//
//  OBaseImporter.h
//  Starbucks
//
//  Created by Adrián Caramés Ramos on 22/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OAppDelegate.h"


@protocol BaseImporterDelegate

- (void) importerDidFinishAllItems:(id)_importer withData:(id)_data withIdentifier:(NSString *)_identifier;
- (void) importerDidFinishItem:(id)_importer withData:(id)_data withIdentifier:(NSString *)_identifier;
- (void) importerDidFail:(id)_importer withError:(NSError *)_error withIdentifier:(NSString *)_identifier;

@end


@interface OBaseImporter : NSOperation <NSXMLParserDelegate>
{
    id __weak mDelegate;
    OAppDelegate *mAppDelegate;
    
    NSManagedObjectContext *mAddingContext;
    
    NSMutableString *mCurrentString;
    NSDateFormatter *mDateFormatter;
	
	NSString		*mServiceName;
}

@property (nonatomic, weak) id <BaseImporterDelegate> delegate;
@property (weak, readonly) NSString *requestURL;
@property (nonatomic, strong) NSManagedObjectContext *addingContext;
@property (nonatomic, strong) NSString				 *serviceName;

- (NSData *)getData;

// Overwrite Methods
- (void) didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDict;
- (void) didEndElement:(NSString *)_elementName withElementValue:(NSString *)_elementValue;
- (void) importerDidFail:(NSError *)_parseError;

@end
