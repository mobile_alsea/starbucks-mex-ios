//
//  DNScrollSliderValueIndicator.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DNScrollSliderValueIndicator : UIView {
	UILabel *valueLabel;
}

+ (DNScrollSliderValueIndicator *)valueIndicator;

@property (nonatomic, strong) IBOutlet UILabel *valueLabel;

@end
