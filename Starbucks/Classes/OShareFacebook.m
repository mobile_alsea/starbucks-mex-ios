//
//  OShareFacebook.m
//  Starbucks
//
//  Created by Santi Belloso López on 24/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OShareFacebook.h"

#import "JSON.h"

//#import <ShareKit/Sharers/Services/Facebook/SHKFBStreamDialog.h>
#import <ShareKit/Sharers/Services/Facebook/SHKFacebook.h>

@implementation OShareFacebook

+ (BOOL) useSessionProxy
{
	return NO;
}

+ (NSString *) facebookKey
{
    
    return @"526763180679965";
	//return @"348041965287294";
}

+ (NSString *) facebookSecret
{
	return @"a30dea20da872f3d67d99ba37fd267d7";
}

+ (NSString *) facebookSessionProxyURL 
{
	return @"";
}

+ (NSString *) appName
{
	return @"Starbucks";
}

+ (NSString *) appURL
{
	return @"http://www.starbucks.com";
}


- (BOOL)shouldAutoShare
{
	return YES;
}

- (BOOL)canAutoShare
{
	return YES;

}
#pragma mark -
#pragma mark Authentication

//- (BOOL)isAuthorized
//{
//	if (session == nil)
//	{
//		
//		if(![OShareFacebook useSessionProxy])
//		{
//			self.session = [FBSession sessionForApplication:[OShareFacebook facebookKey]
//													 secret:[OShareFacebook facebookSecret]
//												   delegate:self];
//			
//		}
//		else 
//		{
//			self.session = [FBSession sessionForApplication:[OShareFacebook facebookKey]
//											getSessionProxy:[OShareFacebook facebookSessionProxyURL]
//												   delegate:self];
//		}
//		
//		
//		return [session resume];
//	}
//	
//	return [session isConnected];
//}
//
//+ (void)logout
//{
//	FBSession *fbSession; 
//	
//	if(![OShareFacebook useSessionProxy])
//	{
//		fbSession = [FBSession sessionForApplication:[OShareFacebook facebookKey]
//											  secret:[OShareFacebook facebookSecret]
//											delegate:self];
//		
//	}
//	else
//	{
//		fbSession = [FBSession sessionForApplication:[OShareFacebook facebookKey]
//									 getSessionProxy:[OShareFacebook facebookSessionProxyURL]
//											delegate:self];
//	}
//	
//	[fbSession logout];
//}

#pragma mark -
#pragma mark Share API Methods

//- (BOOL)send
//{		
//	self.quiet = YES;
//	
//	if (item.shareType == SHKShareTypeURL)
//	{
//		self.pendingFacebookAction = SHKFacebookPendingStatus;
//		
//		SHKFBStreamDialog* dialog = [[[SHKFBStreamDialog alloc] init] autorelease];
//		dialog.delegate = self;
//		dialog.userMessagePrompt = SHKLocalizedString(@"Enter your message:");
//		
////		dialog.attachment = [NSString stringWithFormat:
////							 @"{\
////							 \"name\":\"%@\",\
////							 \"href\":\"%@\"\
////							 }",
////							 item.title == nil ? SHKEncodeURL(item.URL) : SHKEncode(item.title),
////							 SHKEncodeURL(item.URL)
////							 ];
//		
//		NSMutableDictionary *shareDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//										  item.title == nil ? SHKEncodeURL(item.URL) : SHKEncode(item.title), @"name", 
//										  SHKEncodeURL(item.URL), @"href",
//										  nil];
//		if(item.imgURL!=nil && item.imgHref!=nil)
//		{
//			NSDictionary *imgDict = [NSDictionary dictionaryWithObjectsAndKeys:
//									 @"image", @"type", 
//									 item.imgURL, @"src",
//									 item.imgHref, @"href",
//									 nil];
//			[shareDict setObject:[NSArray arrayWithObject:imgDict] forKey:@"media"];
//		}
//		NSString *shareJSON = [shareDict JSONRepresentation];
//		dialog.attachment = shareJSON;
//		
//		dialog.defaultStatus = item.text;
//		dialog.actionLinks = [NSString stringWithFormat:@"[{\"text\":\"Get %@\",\"href\":\"%@\"}]",
//							  SHKEncode([OShareFacebook appName]),
//							  SHKEncode([OShareFacebook appURL])];
//		[dialog show];
//		
//	}
//	
//	else if (item.shareType == SHKShareTypeText)
//	{
//		self.pendingFacebookAction = SHKFacebookPendingStatus;
//		
//		SHKFBStreamDialog* dialog = [[[SHKFBStreamDialog alloc] init] autorelease];
//		dialog.delegate = self;
//		dialog.userMessagePrompt = @"Enter your message:";
//		dialog.defaultStatus = item.text;
//		dialog.actionLinks = [NSString stringWithFormat:@"[{\"text\":\"Get %@\",\"href\":\"%@\"}]",
//							  SHKEncode([OShareFacebook appName]),
//							  SHKEncode([OShareFacebook appURL])];
//		[dialog show];
//		
//	}
//	
//	else if (item.shareType == SHKShareTypeImage)
//	{		
//		self.pendingFacebookAction = SHKFacebookPendingImage;
//		
//		FBPermissionDialog* dialog = [[[FBPermissionDialog alloc] init] autorelease];
//		dialog.delegate = self;
//		dialog.permission = @"photo_upload";
//		[dialog show];		
//	}
//	
//	return YES;
//}

- (BOOL) send
{
	if (![self validateItem])
		return NO;
	/*NSMutableDictionary *params = [NSMutableDictionary dictionary];
	NSString *actions = [NSString stringWithFormat:@"{\"name\":\"%@ %@\",\"link\":\"%@\"}",
						 SHKLocalizedString(@"Get"), [OShareFacebook appName], [OShareFacebook appURL]];
	[params setObject:actions forKey:@"actions"];
	
	if (item.shareType == SHKShareTypeURL && item.URL)
	{
		NSString *url = [item.URL absoluteString];
		[params setObject:url forKey:@"link"];
		[params setObject:item.title == nil ? url : item.title
				   forKey:@"name"];
		
		if(item.imgURL!=nil && item.imgHref!=nil)
		{
			NSString *pictureURI = item.imgURL;
			if (pictureURI)
				[params setObject:pictureURI forKey:@"picture"];
			
			NSString *description = item.title;
			if (description)
				[params setObject:description forKey:@"description"];
			
			[params setObject:item.imgHref forKey:@"link"];
		}
		
        //message parameter is invalid since 2011. Next two lines are useless.
        if (item.text)
			[params setObject:item.text forKey:@"message"];
        
		
	}
	else if (item.shareType == SHKShareTypeText && item.text)
	{
		[params setObject:item.text forKey:@"message"];
        [[SHKFacebook facebook] requestWithGraphPath:@"me/feed"
                                           andParams:params
                                       andHttpMethod:@"POST"
                                         andDelegate:self];
        [self retain]; //must retain, because FBConnect does not retain its delegates. Released in callback.
        return YES;
	}
	else if (item.shareType == SHKShareTypeImage && item.image)
	{
		if (item.title)
			[params setObject:item.title forKey:@"caption"];
		if (item.text)
			[params setObject:item.text forKey:@"message"];
		[params setObject:item.image forKey:@"picture"];
		// There does not appear to be a way to add the photo
		// via the dialog option:
		[[SHKFacebook facebook] requestWithGraphPath:@"me/photos"
										   andParams:params
									   andHttpMethod:@"POST"
										 andDelegate:self];
        [self retain]; //must retain, because FBConnect does not retain its delegates. Released in callback.
		return YES;
	}
    else if (item.shareType == SHKShareTypeUserInfo)
    {
        [self setQuiet:YES];
        [[SHKFacebook facebook] requestWithGraphPath:@"me" andDelegate:self];
        [self retain]; //must retain, because FBConnect does not retain its delegates. Released in callback.
        return YES;
    }
	else
		// There is nothing to send
		return NO;*/
	
	NSMutableDictionary *parametros = [NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:item.text,item.title,item.image, nil] 
														   forKeys:[NSArray arrayWithObjects:@"message", @"title",@"image", nil]];
	
	[[SHKFacebook facebook] requestWithGraphPath:@"/me/feed" andParams:parametros andHttpMethod:@"POST" andDelegate:self];
	//[[SHKFacebook facebook] dialog:@"feed" andParams:params andDelegate:self];

     //must retain, because FBConnect does not retain its delegates. Released in callback.
    
	return YES;
}

- (void)sendDidFinish
{	
	[super sendDidFinish];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SOCIAL-SEND" object:nil];
}

- (void)sendDidFailWithError:(NSError *)error
{
	[super sendDidFailWithError:error];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SOCIAL-SEND" object:nil];
}

@end