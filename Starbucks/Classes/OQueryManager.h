//
//  OQueryManager.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "DYSingletonObject.h"
#import <Foundation/Foundation.h>
#import <DYCore/singleton/DYSingletonObject.h>
#import <DYXML/soap/DYSOAPManager.h>

#define kSOAPParam_Header @"xsd:req"
#define kMethod_Login @"ConsultaDatos"
#define kParamUserId @"impl:usuario"
#define kParamPassword @"impl:password"
#define kParamUid @"impl:Uid"


@protocol DataSourceAtachement <NSObject>

- (NSString*) nameForAttachment;
- (NSString*) typeForAttachment;

@end


@interface OQueryManager : DYSingletonObject
{
	id<DataSourceAtachement> mDataSource;
	
}
+ (void) queryService:(NSString *)_service params:(NSDictionary *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate;

+ (void) queryServiceWithData:(NSString *)_service params:(NSData *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate;

@property(nonatomic,strong) id<DataSourceAtachement> datasource;
@end

@interface OQueryManager (DYSOAPManagerDataSource) <DYSOAPManagerDataSource>
@end

@interface OQueryManager (DYSOAPManagerPreprocessDelegate) <DYSOAPManagerPreprocessDelegate>
@end



