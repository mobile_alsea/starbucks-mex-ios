//
//  ODrinkSubcategoryViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"

@interface ODrinkSubcategoryViewController : OBaseViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>
{
	BOOL mIsHot;
	NSString * mSubCategory;
	IBOutlet UITableView * mTableView;
	NSFetchedResultsController *mFetchedResultsController;
	OAppDelegate * mAppDelegate;
	NSManagedObjectContext *mAddingContext;
}

@property(nonatomic, strong) NSString * subCategory;
@property(nonatomic, assign) BOOL isHot;
@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;

-(void) makeDrinkButtonPressed:(int)_index;

@end
