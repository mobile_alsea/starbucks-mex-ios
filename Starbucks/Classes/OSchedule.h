//
//  OSchedule.h
//  Starbucks
//
//  Created by Isabelo Pamies López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OStore;

@interface OSchedule : NSManagedObject

@property (nonatomic, strong) NSString * endTime;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * startTime;
@property (nonatomic, strong) NSNumber * order;
@property (nonatomic, strong) OStore *store;

@end
