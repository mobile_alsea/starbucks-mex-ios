//
//  OPreparationCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 18/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCustomizeShotsViewController.h"
@interface OPreparationCell : UITableViewCell
{
	UILabel * mPreparationNameLabel;
    UIButton * mCheckButton;
    bool mIsActive;
    int mIndex;
	OCustomizeShotsViewController * mDelegate;
	BOOL mIndexPathPair;

}


- (IBAction) RadioButtonClick:(id)sender;

@property(nonatomic,assign) bool isActive;
@property(nonatomic,assign) int index;
@property(nonatomic,strong) IBOutlet UIButton * checkButton;
@property(nonatomic,strong) IBOutlet UILabel * preparationNameLabel;
@property(nonatomic,strong) OCustomizeShotsViewController * delegate;
@property (nonatomic, assign) BOOL indexPathPair;



@end
