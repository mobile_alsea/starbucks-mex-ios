//
//  ONotesCustomizedViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 21/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OAppDelegate.h"
#import "OCustomizeShotsViewController.h"
#import "OBaseViewController.h"

@interface ONotesCustomizedViewController : OBaseViewController<UITextFieldDelegate,UITextViewDelegate>
{
	
	IBOutlet UITextView * mTextView;
	IBOutlet UITextField * mTextField;
	UILabel * mUserLabel;
	NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	OCustomizeShotsViewController * mDelegate;

	
}

@property(nonatomic, strong) IBOutlet UILabel * userLabel;
@property (nonatomic, strong) OCustomizeShotsViewController * delegate;



-(IBAction)addUserButtonPressed:(id)sender;

@end

@interface ONotesCustomizedViewController(PeoplePickerNavigationControllerDelegateMethods) <ABPeoplePickerNavigationControllerDelegate>
@end