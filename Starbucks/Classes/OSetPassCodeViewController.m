//
//  OSetPassCodeViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 03/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OSetPassCodeViewController.h"
#import <DYCore/files/DYFileNameManager.h>
#import "OOptionsCodeViewController.h"
@interface OSetPassCodeViewController ()

@end

@implementation OSetPassCodeViewController
@synthesize isNew = mIsNew;
@synthesize passwordSavedInFile = mPasswordSavedInFile;
@synthesize modal = mModal;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mNumberOfNumbersInserted = 0;
		mNewPassCode = @"";
		mPasswordFirstTime = @"";
		mPasswordSecondTime= @"";
		
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSetPassCodeViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	if(mIsNew)
	{
		[mForgotButton setHidden:YES];
		[mInformationLabel setHidden:NO];
		[mImformationImageView setHidden:NO];
		[self.navigationItem setTitle:NSLocalizedString(@"Configurar Código", nil)];
	}
	else 
	{
		[mForgotButton setHidden:NO];
		[mInformationLabel setHidden:YES];
		[mImformationImageView setHidden:YES];
		[self.navigationItem setTitle:NSLocalizedString(@"Validar Código", nil)];
	}
	
	
    // Do any additional setup after loading the view from its nib.
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSetPassCodeViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	

}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[digitEntry setText:@""];
	[digitEntry becomeFirstResponder];
	
}

- (void)viewDidDisappear:(BOOL)animated 
{
	[super viewDidDisappear:animated];
	//[digitEntry resignFirstResponder];
	
	
}

- (IBAction)forgotButtonPresed:(id)sender
{

	UIAlertView * mAlertView = [[UIAlertView alloc] initWithTitle:@"Reinicio Aplicaión" message:@"Esta acción desactivará el código de acceso y borrará las Starbucks Cards almacenadas de este dispositivo.¿Estás seguro de continuar?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",@"Cancelar", nil];
	[mAlertView show];

}



#pragma mark TextFieldDelegate Methods


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    @try{
	NSString *passcode = digitEntry.text;
	if([string isEqualToString:@""]) passcode = [passcode substringWithRange:NSMakeRange(0, [passcode length]-1)];
	else passcode = [passcode stringByAppendingString:string];
	
	
	
	[digit1 setTextColor:([passcode length] >= 1) ? [UIColor blackColor] : [UIColor clearColor]];
	[digit2 setTextColor:([passcode length] >= 2) ? [UIColor blackColor] : [UIColor clearColor]];
	[digit3 setTextColor:([passcode length] >= 3) ? [UIColor blackColor] : [UIColor clearColor]];
	[digit4 setTextColor:([passcode length] >= 4) ? [UIColor blackColor] : [UIColor clearColor]];
	
	
	if ([passcode length] >= 4) 
	{
		
			passcode = [passcode substringToIndex:4];
			
			if(![mPasswordSavedInFile isEqualToString:@""])
			{
				//Comprobara si passcode == mPasswordSavedInFile
				[self confirmPassword:passcode];
			
			
			}
			else
			{
				if([mPasswordFirstTime isEqualToString:@""])
				{
					mPasswordFirstTime = passcode;
					//Esta función hara animacion como que pasa a nuevos botones
					//limpiará cosas y empezara a rellenar el confirmationcode
					[digitEntry setText:@""];
					[digit1 setTextColor:[UIColor whiteColor]];
					[digit2 setTextColor:[UIColor whiteColor]];
					[digit3 setTextColor:[UIColor whiteColor]];
					[digit4 setTextColor:[UIColor whiteColor]];
					[mInformationLabel setHidden:YES];
					[mIsertPasswordLabel setHidden:YES];
					[mReenterPasswordLabel setHidden:NO];
					[mImformationImageView setHidden:YES];
				
				}
				else
				{
					mPasswordSecondTime = passcode;
					//Este método compara mPasswordFirstTime y SecondTime
					[self goToComparePasswords];
				
				}
			
			
			}
			
			
			
			
			
		
		/*switch (pageType) {
		 case kValidateStartupPasscode:
		 case kValidateChangePasscode:
		 [self performSelector:@selector(validatePasscode:) withObject:passcode afterDelay:0.01];	// Give the OS a chance to show the last "digit" entry
		 break;
		 
		 case kResetPasscode:
		 case kSetPasscode:
		 [self performSelector:@selector(doConfirmPasscode:) withObject:passcode afterDelay:0.01];	// Give the OS a chance to show the last "digit" entry
		 break;
		 
		 case kConfirmPasscode:
		 case kConfirmResetPasscode:
		 [self performSelector:@selector(confirmPasscode:) withObject:passcode afterDelay:0.01];		// Give the OS a chance to show the last "digit" entry
		 break;
		 }*/
		
		
		//hacer cositas con passcode
		
		return NO;
		
	}
	else {
		return YES;
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSetPassCodeViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}



-(void) confirmPassword: (NSString*) _passcode
{
	@try{
	if([mPasswordSavedInFile isEqualToString:_passcode])
	{
		if(mModal==YES)
		{
			[self dismissModalViewControllerAnimated:YES];
		
		}
		else
		{
			//Acceder a nuevo controlador con tablitas o en este mismo, apagar codigo o cambiar
			OOptionsCodeViewController *detailViewController = [[OOptionsCodeViewController alloc] initWithNibName:@"OOptionsCodeViewController" bundle:nil];
			[self.navigationController pushViewController:detailViewController animated:YES];
		}
	
	
	}
	else
	{
		UIAlertView * mAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Código incorrecto. Inténtelo de nuevo por favor" delegate:nil cancelButtonTitle:@"Cerrar" otherButtonTitles: nil];
		[mAlertView show];
		[digitEntry setText:@""];
		[digit1 setTextColor:[UIColor whiteColor]];
		[digit2 setTextColor:[UIColor whiteColor]];
		[digit3 setTextColor:[UIColor whiteColor]];
		[digit4 setTextColor:[UIColor whiteColor]];
	
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSetPassCodeViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

	
}

-(void) goToComparePasswords
{
	@try{
	//Este método compara mPasswordFirstTime y SecondTime
	if([mPasswordFirstTime isEqualToString:mPasswordSecondTime])
	{
		//Salvar en fichero y salir.
		[[NSUserDefaults standardUserDefaults] setObject:mPasswordFirstTime forKey:@"Code"];
		[[NSUserDefaults standardUserDefaults] synchronize];
		[self.navigationController popToRootViewControllerAnimated:YES];
	
	}
	else 
	{
		UIAlertView * mAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Ambos códigos deben de ser iguales. Inténtelo de nuevo por favor" delegate:nil cancelButtonTitle:@"Cerrar" otherButtonTitles: nil];
		[mAlertView show];
		[digitEntry setText:@""];
		[digit1 setTextColor:[UIColor whiteColor]];
		[digit2 setTextColor:[UIColor whiteColor]];
		[digit3 setTextColor:[UIColor whiteColor]];
		[digit4 setTextColor:[UIColor whiteColor]];
		mPasswordFirstTime = @"";
		mPasswordSecondTime = @"";
		[mInformationLabel setHidden:NO];
		[mIsertPasswordLabel setHidden:NO];
		[mReenterPasswordLabel setHidden:YES];
		[mImformationImageView setHidden:NO];
		
		
		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSetPassCodeViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

	
}


#pragma mark AlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

	switch (buttonIndex) {
		case 0:
		{
			[[NSUserDefaults standardUserDefaults] setObject:@"-1" forKey:@"Code"];
			[[NSUserDefaults standardUserDefaults] synchronize];
			if(mModal)
			{
				[self dismissModalViewControllerAnimated:YES];
			}
			else 
			{
				[self.navigationController popToRootViewControllerAnimated:YES];
			}
			

		}
			break;
		case 1:
			
			break;
		default:
			break;
	}

}

@end
