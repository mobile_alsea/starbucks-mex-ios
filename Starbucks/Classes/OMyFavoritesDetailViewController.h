//
//  OMyFavoritesDetailViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import <MapKit/MapKit.h>
#import "OBaseViewController.h"

@interface OMyFavoritesDetailViewController : OBaseViewController
{
	IBOutlet UITableView * mTableView;
	NSMutableArray *mFavoritesArray;
	int mType;
	NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	NSArray * mFavoritesActualized;
     MKMapView *mMapView;

}

@property(nonatomic, strong) NSMutableArray * favoritesArray;
@property(nonatomic, assign) int type;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@end
