//
//  OTwitterViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OMyDrinks.h"
#import "OBaseViewController.h"

@interface OTwitterViewController : OBaseViewController<UITextViewDelegate>
{
	IBOutlet UITextView * mTextView;
	IBOutlet UILabel * mCounterLabel;
	OMyDrinks * mDrinkSelected;
	NSString * mPreString;
	
}

@property(nonatomic, strong) OMyDrinks * drinkSelected;


@end
