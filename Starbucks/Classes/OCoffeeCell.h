//
//  OCoffeeCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCoffeeCell : UITableViewCell
{
	UILabel * mTitleLabel;
	UILabel * mDescriptionLabel;
	BOOL mIndexPathPair;

}

@property(nonatomic, strong) IBOutlet UILabel * titleLabel;
@property(nonatomic, strong) IBOutlet UILabel * descriptionLabel;
@property (nonatomic, assign) BOOL indexPathPair;
@end
