//
//  OCustomizeShotsViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODrink.h"
#import <DYWidgets/DYHorizontalPicker/DYHorizontalPicker.h>
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OCustomization.h"
#import "OAppDelegate.h"
#import "CupMarkingsView.h"
#import "OBaseViewController.h"

@class DNScrollSlider;


@interface OCustomizeShotsViewController : OBaseViewController
{

	DNScrollSlider *shotCountSlider;
   DNScrollSlider *TemperatureSlider;
   DNScrollSlider *FoamSlider;
    ODrink * mDrinkSelected;
	OMyDrinks * mDrinkCustomize;
	IBOutlet UILabel * mSectionLabel;
	IBOutlet UIScrollView * mHorizontalScrollView;
	IBOutlet UIScrollView * mHorizontalTemperatureScrollView;
	IBOutlet UIScrollView * mHorizontalEspumaScrollView;
	IBOutlet UIView * mScrollVieView;
	IBOutlet UIView * mTemperatureScrollVieView;
	IBOutlet UIView * mEspumaScrollVieView;
	IBOutlet UIButton * mMilkButton;
	IBOutlet UIButton * mSiropeButton;
	IBOutlet UIButton * mToppingButton;
	IBOutlet UIButton * mPreparationButton;
	IBOutlet UIButton * mAddinsButton;
	IBOutlet UIButton * mCoffeeButton;
	IBOutlet UIButton * mTeaButton;
	IBOutlet UIButton * mShotsButton;
	
	IBOutlet UIView * mCustomizeView;
	IBOutlet UITableView * mTableView;
	IBOutlet UIButton * mAceptarButton;
	IBOutlet UIButton * mCancelButton;
	IBOutlet UILabel * mFootterLabel;
	IBOutlet UIView * mInsideView;
	IBOutlet UIView * mMilkView;
	IBOutlet UIView * mCoffeeView;
	IBOutlet UIView * mWithTableViewView;
	IBOutlet UIView * mShotsView;
	int mTypeOfHeaderSelected;
    
	NSMutableArray * mShotsCafeinaArray;
    NSMutableArray * mTemperatureArray;
    NSMutableArray * mFoamArray;
	NSMutableArray * mPreparationArray;
	NSMutableArray * mToppingsArray;
	NSMutableArray * mSyrupsArray;
	NSMutableArray * mAddinsArray;
	NSMutableArray * mTeasArray;
	NSMutableArray * mPreparationSelectedArray;
	BOOL mIsDown;
	IBOutlet UILabel * mNumberOfShotsLabel;
	int mId;
	NSString * mMilkTypeSelected;
	OAppDelegate * mAppDelegate;
	NSManagedObjectContext *mAddingContext;
	
	IBOutlet UIButton * mMilkButton1;
	IBOutlet UIButton * mMilkButton2;
	IBOutlet UIButton * mMilkButton3;
	IBOutlet UIButton * mMilkButton4;
	IBOutlet UIButton * mMilkButton5;
	IBOutlet UIButton * mMilkButton6;
    
    IBOutlet UIButton *mShotsButton1;
  
    IBOutlet UIButton *mShotsButton2;
    	NSString * mShotsCafeinaSelected;
    
	IBOutlet UIButton * mDoCupRotationButton;
    UILabel * mTextLabel;
    
	NSString * mSizeSelected;
    
    NSString * mTemperatureSelected;
	NSString * mFoamSelected;
	

	NSMutableArray * mSavedSyrupsArray;
	NSMutableArray * mSavedAddinsArray;
	
	
	/**Animation**/
	
	NSArray *animationImages;
	NSArray *reverseAnimationImages;
	UIImageView * imageView; 
	UIImageView * imageView2; 
	BOOL mIsReverse;
	CupMarkingsView *cupMarkingsView;
	
	
}

@property(nonatomic, strong) ODrink * drinkSelected;
@property(nonatomic, strong) OMyDrinks * drinkCustomize;
@property (nonatomic, strong) IBOutlet DNScrollSlider *shotCountSlider;
@property (strong, nonatomic) IBOutlet DNScrollSlider *FoamSlider;

@property(nonatomic,strong) NSString * sizeSelected;
@property (nonatomic, strong) NSArray *reverseAnimationImages;
@property (nonatomic, strong) NSArray *animationImages;
@property (nonatomic, strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) IBOutlet UIImageView * imageView2;
@property(nonatomic, strong) IBOutlet UILabel * textLabel;


- (IBAction)mShotsButtonPressed:(id)sender;

-(IBAction)CategoryButtonPressed:(id)sender;
-(IBAction)CancelButtonPressed:(id)sender;
-(IBAction)DoneButtonPressed:(id)sender;
-(void) UpdatePreparationArrayForIndex:(int)mIndex;
-(IBAction)shotCountChanged:(id)sender;
- (IBAction)temperatureValuechanged:(id)sender;
- (IBAction)foamValueChanged:(id)sender;
-(void) updateArraywithIndex:(int) _index;
-(IBAction)milkButtonPressed:(id)sender;
-(void) saveAllDrink:(NSString*)_user :(NSString*)_userType :(NSString*)_notes :(NSString*)_nickName;
@property (strong, nonatomic) IBOutlet DNScrollSlider *TemperatureSlider;
-(IBAction)showHelp:(id)sender;

@end
