//
//  OCoffeeDetailViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCoffeeDetailViewController.h"
#import "DYImageView.h"
#import "OCoffee.h"
#import "OCoffee+Extras.h"
#import "OAppDelegate.h"
#import "OMyFavorites.h"
#import "OMYFavorites+Extras.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OCoffeeViewController.h"


@interface OCoffeeDetailViewController ()

@end

@implementation OCoffeeDetailViewController
@synthesize delegate = mDelegate;
@synthesize coffeeSelected = mcoffeeSelected;
@synthesize headerView = mHeaderView;
@synthesize textView = mTextView;
@synthesize downView = mDownView;
@synthesize verticalLayout = mVerticalLayout;
@synthesize nameLabel = mNameLabel;
@synthesize Availability=mAvailability;
@synthesize subtitleLabel = mSubTitleLabel;
@synthesize headerLabel = mHeaderLabel;
@synthesize headerImageView = mHeaderImageView;
@synthesize moreButton = mMoreButton;
@synthesize descriptionLabel = mDescriptionLabel;
@synthesize addCoffeeLabel = mAddCoffeeLabel;
@synthesize addCoffeeButton = mAddCoffeeButton;
@synthesize scrollView = mScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
	
	
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/initWithNibName");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}
/*-(void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];

}*/

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	mHeaderImageView.failureImage = [UIImage imageNamed:@"noimage-cafe-512x512.png"];
	
	
	[self.navigationItem setTitle:NSLocalizedString(@"Detalle del Café", nil)];
	[self setValues];
	
	
	//mFavorite = [[OMyFavorites alloc] init];
	mFavorite =[OMyFavorites favoriteWithName:mcoffeeSelected.name context:mAddingContext];
	
	if(mFavorite!=nil)
	{ 
		mIsAdded = YES;
		[mAddingContext deleteObject:mFavorite];
		mAddCoffeeLabel.text = @"Eliminar de mis Cafés";
	}
	else
	{
		mIsAdded = NO;
		mAddCoffeeLabel.text = @"Añadir a mis Cafés";
	}
    mPageControl.numberOfPages = [mcoffeeSelected.images count];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/viewdidload");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void) viewDidAppear:(BOOL)animated
{
	@try{
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:mcoffeeSelected.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[mHeaderImageView setUrl:[NSURL URLWithString:mcoffeeSelected.image]];
	}
	else
	{
		[mHeaderImageView setImage:[UIImage imageWithData:image.image]];
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/viewDidAppear");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)viewDidUnload
{
    mAvailability = nil;
    [self setAvailability:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];

    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/didFinishLoadingImg");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
	
}



-(void) setValues
{
    @try{
	mVerticalLayout.spacing = 0.0;
	mNameLabel.text = mcoffeeSelected.name;
	
	if([[mcoffeeSelected.flavors allObjects] count]>0)
	{
		
		NSString * flavors = [[[mcoffeeSelected.flavors allObjects] objectAtIndex:0] name];
		for(int i=1;i<[[mcoffeeSelected.flavors allObjects] count];i++)
		{
			if(i==[[mcoffeeSelected.flavors allObjects] count]-1) flavors = [flavors stringByAppendingString:@" y "];
			else flavors = [flavors stringByAppendingString:@", "];
			flavors = [flavors stringByAppendingString:[[[mcoffeeSelected.flavors allObjects] objectAtIndex:i] name]];
			
		}
		mSubTitleLabel.text = flavors;
	}

	mDescriptionLabel.text = mcoffeeSelected.descrip;
	mHeaderLabel.text = mcoffeeSelected.profile;
      
        mAvailability.text=     mcoffeeSelected.form;
     
     //   NSString * se= mcoffeeSelected.availability;
        
   

	if([mcoffeeSelected.profile isEqualToString:@"Blonde"])
		mHeaderView.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:165.0/255.0 blue:19.0/255.0 alpha:1.0];
	else if([mcoffeeSelected.profile isEqualToString:@"Dark"])
		mHeaderView.backgroundColor = [UIColor colorWithRed:122.0/255.0 green:0.0/255.0 blue:60.0/255.0 alpha:1.0];
	else if([mcoffeeSelected.profile isEqualToString:@"Medium"])
		mHeaderView.backgroundColor = [UIColor colorWithRed:177.0/255.0 green:91.0/255.0 blue:17.0/255.0 alpha:1.0];
	else mHeaderView.backgroundColor = [UIColor blackColor];

	float hei = [mDescriptionLabel.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap].height;
	if(hei<66)
	{
		[mMoreButton setHidden:YES];
		[mMoreLabel setHidden:YES];
		[mArrowImageView setHidden:YES];

	}
	[mVerticalLayout reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/setValues");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark IBActions

-(IBAction)mMoreDetailButtonPressed:(id)sender
{
    @try{
	[mMoreButton setHidden:YES];
	[mMoreLabel setHidden:YES];
	[mArrowImageView setHidden:YES];
	float hei = [mDescriptionLabel.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap].height;
    
    [mDescriptionLabel setFrame:CGRectMake(20, 5, 280, hei+10)];
	[mTextView setFrame:CGRectMake(0, 0, 320, mTextView.frame.size.height-66+hei)];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/mMoreDetailButton");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}
-(IBAction)addToMyCoffees:(id)sender
{

    @try{
	
	if(mIsAdded==YES)
	{
		//Delete from bd

		[mAddingContext deleteObject:mFavorite];
		
		NSError *error = nil;
		if(![mAddingContext save:&error])
			NSLog(@"Save error: %@", error.description);
		mAddCoffeeLabel.text = @"Añadir a mis Cafés";
		mIsAdded = NO;
	
	}
	else
	{
		NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyFavorites" inManagedObjectContext:mAddingContext];
		
		mFavorite = [[OMyFavorites alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:mAddingContext];
		mFavorite.type = @"2";
		mFavorite.name = mNameLabel.text;
		
		NSError * error = nil;
		[mAddingContext save:&error];
		mAddCoffeeLabel.text = @"Eliminar de mis Cafés";
		mIsAdded = YES;
		
	
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/addTocoffes");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

-(IBAction)pageChangeValue
{
	[mHorizontalPicker setCurrentPage:mPageControl.currentPage animated:YES];
	
}

#pragma mark Vertical Layout methods

- (NSUInteger) numberOfViewsInLayout:(DYLayoutView *) _LayoutView
{
	
    return 3;
	
}

- (UIView *) DYLayoutView:(DYLayoutView *) _layoutView viewAtIndex:(NSUInteger) _index
{
	
	switch (_index) {
		case 0:
			return mHeaderView;
			break;
		case 1:
			return mTextView;
			break;
		case 2:
			return mDownView;
			break;
			
		default:
			break;
	}
	return nil;
}
- (void) DYLayoutView:(DYLayoutView *) _layoutView changeContentSize:(CGSize) _contentSize
{
    @try{
	[mScrollView setContentSize:_contentSize];
	[mVerticalLayout setFrame:CGRectMake(CGRectGetMinX(mVerticalLayout.frame), CGRectGetMinY(mVerticalLayout.frame), _contentSize.width, _contentSize.height)];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/DYLayoutView");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

#pragma mark DYHorizontalPicker

- (NSUInteger) numberOfSectionsInPicker:(DYHorizontalPicker *)_picker
{
	return [mcoffeeSelected.images count];
}

- (CGFloat) widthOfSectionsInPicker:(DYHorizontalPicker *)_picker
{
	return 240;
}



- (UIView *) DYHorizontalPicker:(DYHorizontalPicker *)_picker viewForSectionAtIndex:(NSUInteger)_index
{
	
    @try{

    NSURL *imageURL = [NSURL URLWithString:[[[mcoffeeSelected.images allObjects] objectAtIndex:_index] name]];
    DYImageView *imageView = [[DYImageView alloc] init];
	imageView.failureImage = [UIImage imageNamed:@"noimage-cafe-150x180.png"];
    [imageView setAnimationStyle:DYImageViewAnimationStyleWhite];
	
	
	imageView.frame = CGRectMake(0, 0, 220, 342);
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:[imageURL absoluteString] context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[imageView setUrl:imageURL];
	}
	else
	{
		[imageView setImage:[UIImage imageWithData:image.image]];
	}

	
	UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 342)];
	[view addSubview:imageView];
    
   
    return view;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeDetailCntroller/DYHorinotalicker");
        
        
        OCoffeeViewController  *regresa =[[OCoffeeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}


- (void) DYHorizontalPicker:(DYHorizontalPicker *)_picker didChangeToSection:(NSInteger)_page
{
	
	[mPageControl setCurrentPage:_page];
}


@end
