//
//  OSocialNetworkCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSocialNetworkCell : UITableViewCell <UITextFieldDelegate>

{
	UILabel *mTitleLabel;
	UIImageView *mIconImageView;
	UIImageView *mDisclosureImageView;
	
	
	UIImageView * mImageView;
	UISwitch * mMySwitch;
    
 
   

    IBOutlet UILabel *mStatusOnLabel;
	
	BOOL mShowDisclosure;
	BOOL mIndexPathPair;
	int mTag;
	id __weak mDelegate;
}

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) IBOutlet UIImageView *disclosureImageView;

@property (nonatomic, strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) IBOutlet UISwitch * myswitch;


@property (nonatomic, strong) IBOutlet UILabel * StateOnLabel;
@property (nonatomic, assign) BOOL showDisclosure;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int tag;
@property (nonatomic, weak) id delegate;

-(IBAction)buttonPressed:(id)sender;
@end
