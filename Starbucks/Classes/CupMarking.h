//
//  CupMarking.h
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CupMarking : NSObject {
	NSString *text;
	BOOL strikethrough;
}

- (id)initWithText:(NSString *)inText strikethrough:(BOOL)inStrikethrough;

@property BOOL strikethrough;
@property (nonatomic, strong) NSString *text;

@end