//
//  OConfigurationViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OConfigurationViewController.h"
#import "OConfigurationCell.h"
#import "OSocialNetworkCell.h"
#import "OConfigurationRadarViewController.h"
#import "OSetPassCodeViewController.h"
#import <DYCore/files/DYFileNameManager.h>
#import "OWebViewController.h"
#import "OShareFacebook.h"
#import "OShareTwitter.h"
#import "ORewardsCardViewController.h"

@interface OConfigurationViewController ()

@end

@implementation OConfigurationViewController


#pragma mark - Constructors

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - ViewController Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	[self.navigationItem setTitle:NSLocalizedString(@"Configuración", nil)];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    switch (_tableView.tag) {
		case 0:
			return 3;
			break;
		case 1:
			return 2;
			break;
		case 2:
			return 2;
			break;
		default:
			break;
	}
return 0;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	switch (_tableView.tag) {
		case 0:
		{
		
			static NSString *CellIdentifier = @"OConfigurationCell";
			
			OConfigurationCell *cell = (OConfigurationCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                cell.nickNameTextField.delegate = self;
                
				
			}
			
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			
			switch (_indexPath.row) 
			{
				case 0: {
					[cell.nickNameTextField setHidden:NO];
					cell.titleLabel.text = @"Mi Nickname";
					
					NSString * readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"NickName"];
					
				
					
					cell.nickNameTextField.text = readValue;
                }   break;
				case 1: {
					[cell.nickNameTextField setHidden:YES];
					cell.titleLabel.text = @"Localizador de tiendas";
					cell.titleLabel.frame = CGRectMake(14, 9, 200, 21);
                }   break;
				case 2:
				{
					
					

					cell.nickNameTextField.enabled = NO;
					cell.nickNameTextField.userInteractionEnabled = NO;
					NSString * readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"Code"];
					if(readValue != nil && ![readValue isEqualToString:@"-1"])
					{
						[cell.nickNameTextField setHidden:NO];
						cell.nickNameTextField.text = @"On";
						cell.nickNameTextField.textAlignment = UITextAlignmentRight;
						
					}
					else
					{
						
						
						[cell.nickNameTextField setHidden:NO];
						cell.nickNameTextField.text = @"Off";
                        
						cell.nickNameTextField.textAlignment = UITextAlignmentRight;
					
					}
					
					
					cell.titleLabel.text = @"Código de acceso";
				}
					break;
					
				default:
					break;
			}
			
			
			
			return cell;
			return nil;
		
		}
			break;
		case 1:
		{
			static NSString *CellIdentifier = @"OConfigurationCell";
			
			OConfigurationCell *cell = (OConfigurationCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
				
			}
			
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			
			switch (_indexPath.row) 
			{
				case 0:
					[cell.nickNameTextField setHidden:YES];
					cell.titleLabel.text = @"Crear cuenta";
					break;
				case 1:
					[cell.nickNameTextField setHidden:YES];
					cell.titleLabel.text = @"Iniciar sesión";
					break;
				default:
					break;
			}
			
			
			
			return cell;
			return nil;
			

		
		
		}
			break;
		case 2:
		{
			static NSString *CellIdentifier = @"OSocialNetworkCell";
			
			OSocialNetworkCell *cell = (OSocialNetworkCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
				
			}
			
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			
			switch (_indexPath.row) 
			{
				case 0:
					
					cell.imageView.image = [UIImage imageNamed:@"icon_facebook.png"];
					cell.titleLabel.text = @"Configura Facebook";
					
					if([OShareFacebook isServiceAuthorized])
					{
						
						[cell.myswitch setOn:YES];
                     
                                [cell.StateOnLabel setText:@"On"];
					}
					else
					{
						 
                                [cell.StateOnLabel setText:@"Off"];
						[cell.myswitch setOn:NO];
					}
					
					
					
					break;
				case 1:
									cell.imageView.image = [UIImage imageNamed:@"icon_twitter.png"];
					cell.titleLabel.text = @"Configura Twitter";
					if([OShareTwitter isServiceAuthorized])
					{
						
						[cell.myswitch setOn:YES];
                      
                                [cell.StateOnLabel setText:@"On"];
					}
					else
					{
						
                                [cell.StateOnLabel setText:@"Off"];
						[cell.myswitch setOn:NO];
					}
					break;
				default:
					break;
			}
			
			
			
			return cell;
			return nil;

		
		}
			break;
		default:
			break;
	}
    
    return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:NO];
	
	OConfigurationCell * cell = (OConfigurationCell*)[mAplicacionTableView cellForRowAtIndexPath:_indexPath];
	
	OConfigurationCell * cell2 = (OConfigurationCell*)[mAplicacionTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	
	switch (_tableView.tag) {
		case 0:
		{
			
			
			switch (_indexPath.row) 
			{
				case 0:
					
					break;
				case 1:
				{
					OConfigurationRadarViewController *detailViewController = [[OConfigurationRadarViewController alloc] initWithNibName:@"OConfigurationRadarViewController" bundle:nil];
					[self.navigationController pushViewController:detailViewController animated:YES];
					[cell2.nickNameTextField resignFirstResponder];
				}
					break;
				case 2:
				{
					[cell2.nickNameTextField resignFirstResponder];
					OSetPassCodeViewController *detailViewController = [[OSetPassCodeViewController alloc] initWithNibName:@"OSetPassCodeViewController" bundle:nil];
					
					
					
					
					NSString * readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"Code"];
					if([cell.nickNameTextField.text isEqualToString:@"Off"])
					{
						detailViewController.isNew = YES;
						detailViewController.passwordSavedInFile = @"";
					}
					else
					{
						detailViewController.isNew = NO;
						detailViewController.passwordSavedInFile = readValue;
					
					}
					
					
					
					[self.navigationController pushViewController:detailViewController animated:YES];

				}
					break;
					
				default:
					break;
			}

			
		}
			break;
		case 1:
		{
			[cell2.nickNameTextField resignFirstResponder];
			switch (_indexPath.row) 
			{
				case 0:
				{
                    OSignUpRewardsViewController *detailViewController = [[OSignUpRewardsViewController alloc] initWithNibName:@"OSignUpRewardsViewController" bundle:nil];
                    SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:detailViewController];
                    
                    if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] )
                    {
                        UIImage *image = [UIImage imageNamed:@"NavBar.png"];
                        [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
                        navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
                        
                        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                            [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
                            [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                            [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
                        }
                    }
                    [self presentViewController:navigationController animated:YES completion:nil];
				
				}
					break;
				case 1:
				{
                    OSignInRewardsViewController *detailViewController = [[OSignInRewardsViewController alloc] initWithNibName:@"OSignInRewardsViewController" bundle:nil];
					[self.navigationController pushViewController:detailViewController animated:YES];
				
				}
					break;
				default:
					break;
			}
			
		}
			break;
		case 2:
		{
             @try{
			[cell2.nickNameTextField resignFirstResponder];
			switch (_indexPath.row) 
			{
				case 0:
				{
					[self changeStatusFacebook];
				
				}
					break;
				case 1:
				{
					[self changeStatusTwitter];
				
				}
					break;
				default:
					break;
			}
             }
            
            
            @catch (NSException *ex) {
                
                                
            }


		}
			break;
		default:
			break;
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	return 40;
	
}


#pragma mark - Private Methods

-(void) changeStatusFacebook
{
    @try{
	OSocialNetworkCell * cell = (OSocialNetworkCell*)[mAplicacionTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
	if([OShareFacebook isServiceAuthorized])
	{
		[OShareFacebook logout];
        
        
       
     
		[cell.myswitch setOn:NO];
        [cell.StateOnLabel setText:@"Off"];
        
	}
	else
	{
		[OShareFacebook shareItem:nil];
		[cell.myswitch setOn:YES];
        
                [cell.StateOnLabel setText:@"On"];
	}
		[mSocialNetwork reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(void) changeStatusTwitter
{
    @try{
	OSocialNetworkCell * cell = (OSocialNetworkCell*)[mAplicacionTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
	if([OShareTwitter isServiceAuthorized])
	{
		[OShareTwitter logout];
		[cell.myswitch setOn:NO];
                [cell.StateOnLabel setText:@"Off"];
      
	}
	else
	{
		[OShareTwitter shareItem:nil];
		[cell.myswitch setOn:YES];
                [cell.StateOnLabel setText:@"On"];
  
	}
	[mSocialNetwork reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)_textField
{
    [_textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)_textField
{
	[[NSUserDefaults standardUserDefaults] setObject:_textField.text forKey:@"NickName"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark OSignUpRewardsViewController.h

- (void)OsignUpRewardsDidClose:(OSignUpRewardsViewController *)controller
{
    [self dismissModalViewControllerAnimated:NO];
    [self.navigationController popToRootViewControllerAnimated:YES];
    ORewardsCardViewController *detailViewController = [[ORewardsCardViewController alloc] initWithNibName:@"ORewardsCardViewController" bundle:nil];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark OSignInRewardsViewController.h

- (void)OsignInRewardsDone:(OSignInRewardsViewController *)controller
{
    self.navigationController.navigationBar.hidden= NO;
    ORewardsCardViewController *detailViewController = [[ORewardsCardViewController alloc] initWithNibName:@"ORewardsCardViewController" bundle:nil];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)OsignInRewardsDidCancel:(OSignInRewardsViewController *)controller
{
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
