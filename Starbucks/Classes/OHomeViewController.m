//
//  OHomeViewController.m
//  Starbucks
//
//  Created by Adrián Caramés on 08/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OHomeViewController.h"
#import "OStoresViewController.h"
#import "OMydrinksViewController.h"
#import "OCoffeeViewController.h"
#import "OFoodViewController.h"
#import "OMyFavoritesViewController.h"
#import <DYCore/operation/DYOperationQueue.h>
#import <DYWidgets/categories/misc/UIAlertView_misc.h>
#import "ODrinkViewController.h"
#import "OMyFavorites.h"
#import "OMyFavorites+Extras.h"
#import "ODrink.h"
#import "ODrink+Extras.h"
#import "OConfigurationViewController.h"
#import "OAbbreviatures.h"
#import "OAbbreviatures+Extras.h"
#import "OInitRewardsViewController.h"
#import <DYCore/files/DYFileNameManager.h>
#import "OServiceManager.h"
#import "OUpdate+Extras.h"

#import "Reachability.h"
#include <arpa/inet.h>
#import "SEDraggable.h"
#import "SEDraggableLocation.h"

#import "CustomScrollView.h"

#import "ORewardsCardViewController.h"


@interface OHomeViewController ()

- (void) reachabilityConfig;
- (void)reachabilityChanged:(NSNotification*)note;

- (void) didFinishImporter;

@property (nonatomic, strong) SEDraggableLocation *draggableLocationTop;
@property (nonatomic, strong) SEDraggableLocation *draggableLocationBottom;
//@property (nonatomic, unsafe_unretained, readwrite) UIView *bottomWrapper;
@property (nonatomic, strong) CustomScrollView  *scrollview;
//@property (nonatomic, unsafe_unretained, readwrite) UIView * GreenLine;
@property (nonatomic, strong) OAppDelegate *StarbucksAppDelegate;

@property (strong, nonatomic) UINavigationController    *currentController;
@property (strong, nonatomic) NSDictionary              *sectionImages;

@end

#define RGBToFloat(f)           (f/255.0)
#define kSectionsMarginTop      40
#define kSectionsMarginLeft     10
#define kSectionsMarginMiddle   6
#define kSectionsWidth          96
#define kSectionsHeight         80
#define OBJECT_WIDTH 50.0f
#define OBJECT_HEIGHT 50.0f
#define MARGIN_VERTICAL 4.0f
#define MARGIN_HORIZONTAL 12.0f
#define DRAGGABLE_LOCATION_WIDTH  ((OBJECT_WIDTH  * 3) + (MARGIN_HORIZONTAL * 5))
#define DRAGGABLE_LOCATION_HEIGHT ((OBJECT_HEIGHT * 5) + (MARGIN_VERTICAL   * 5))


@implementation OHomeViewController
@synthesize StarbucksAppDelegate=_StarbucksAppDelegate;
@synthesize draggableLocationTop = _draggableLocationTop;
@synthesize draggableLocationBottom = _draggableLocationBottom;
@synthesize scrollview=_scrollview;
@synthesize SectionnavController;
@synthesize LeftBar=_LeftBar;
@synthesize RightBar=_RightBar;
@synthesize  mBackGroundTabBar=_mBackGroundTabBar;
@synthesize mGreenLineTabBar=_mGreenLineTabBar;
@synthesize btnFilter;

#pragma mark - Constructors

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
if (self)
{
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishImporter) name:kFinishImporterNotification object:nil];
    
}
return self;
}


#pragma mark - Management Memory



#pragma mark - Properties

@synthesize loadingView = mLoadingView;
@synthesize unreachableView = mUnreachableView;
@synthesize refreshBtn = mRefreshBtn;


#pragma mark - ViewController Methods

- (void)loadView
{
    [super loadView];
    self.sectionImages = @{@0:@{@"on":[UIImage imageNamed:@"Home_selected"], @"off":[UIImage imageNamed:@"Home"]},
                           @1:@{@"on":[UIImage imageNamed:@"Favorites_selected"], @"off":[UIImage imageNamed:@"Favorites"]},
                           @2:@{@"on":[UIImage imageNamed:@"Beans_selected"], @"off":[UIImage imageNamed:@"Beans"]},
                           @3:@{@"on":[UIImage imageNamed:@"Foods_selected"], @"off":[UIImage imageNamed:@"Foods"]},
                           @4:@{@"on":[UIImage imageNamed:@"Drinks_selected"], @"off":[UIImage imageNamed:@"Drinks"]},
                           @5:@{@"on":[UIImage imageNamed:@"Rewards_selected"], @"off":[UIImage imageNamed:@"Rewards"]},
                           @6:@{@"on":[UIImage imageNamed:@"MyDrinks_selected"], @"off":[UIImage imageNamed:@"MyDrinks"]},
                           @7:@{@"on":[UIImage imageNamed:@"Stores_selected"], @"off":[UIImage imageNamed:@"Store"]},
                           @8:@{@"on":[UIImage imageNamed:@"Settings_selected"], @"off":[UIImage imageNamed:@"Settings"]},
                           @9:@{@"on":[UIImage imageNamed:@"Tarjeta_selected"], @"off":[UIImage imageNamed:@"Tarjeta"]}};
}


- (void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidLoad
{
@try{
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"card"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RELOAD_AMOUNT"];
    
    self.view.userInteractionEnabled = NO;
    
    mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
    
    // Setting Controller
    self.title = @"Starbucks";
    //self.navigationItem.rightBarButtonItem = mRefreshBtn;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Menú", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    
    btnFilter = [[UIBarButtonItem alloc] initWithTitle:@"Editar"  style:UIBarButtonItemStyleBordered target:self action:@selector(goToEditMenu:)];
    [self.navigationItem setRightBarButtonItem:btnFilter];
    self.navigationItem.backBarButtonItem = backButton;
    
    // Load Menu's sections
    mRefreshBtn.enabled = NO;
    btnFilter.enabled=NO;
 	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMenu) name:kOSARDisabled object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideMenu) name:kOSAREnabled object:nil];
 
    if(mAppDelegate.window.frame.size.height==568)
    {
        yTabBar= 432+88;
        WidthTabBar=320;
        HeightTabBar=480+88;
        ScrollContentSize=580;
        yEditTabBar=370+88;
    }
    
   else if(mAppDelegate.window.frame.size.height==1024)
    {
      
        
        
    }
    //Others Iphne 4, 4s, 3gs, - 3.5'
    else{
       
        yTabBar= 432;
        WidthTabBar=320;
        HeightTabBar=480;
        ScrollContentSize=580;
        yEditTabBar=370;
        

    }
  
   
    [self setupDraggableLocations];
    [self setupDraggableObjects];
    
    // Reachability
    [self reachabilityConfig];
    
}

@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/viewDidLoad");
}
}

- (void)viewDidUnload
{
    [self setRightBar:nil];
    [self setLeftBar:nil];
[super viewDidUnload];
// Release any retained subviews of the main view.
// e.g. self.myOutlet = nil;
self.unreachableView = nil;
self.loadingView = nil;
}


- (void)dealloc
{	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kOSAREnabled object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kOSARDisabled object:nil];
}

#pragma mark - Menu Animations

- (void)showMenu
{
	__weak OHomeViewController *weakSelf = self;
	[UIView animateWithDuration:0.4
						  delay:0.0
						options:UIViewAnimationOptionCurveEaseInOut
					 animations:^
	{
		if(weakSelf) {
			OHomeViewController *strongSelf = weakSelf;
			strongSelf.mBackGroundTabBar.alpha = 1.0f;
			strongSelf.scrollview.alpha = 1.0f;
			strongSelf.mGreenLineTabBar.alpha = 1.0f;
			strongSelf.LeftBar.alpha = 1.0f;
			strongSelf.RightBar.alpha = 1.0f;
		}
	}
					 completion:nil];
	
}

- (void)hideMenu
{
	__weak OHomeViewController *weakSelf = self;
	[UIView animateWithDuration:0.4
						  delay:0.0
						options:UIViewAnimationOptionCurveEaseInOut
					 animations:^
	 {
		 if(weakSelf) {
			 OHomeViewController *strongSelf = weakSelf;
			 strongSelf.mBackGroundTabBar.alpha = 0.0f;
			 strongSelf.scrollview.alpha = 0.0f;
			 strongSelf.mGreenLineTabBar.alpha = 0.0f;
			 strongSelf.LeftBar.alpha = 0.0f;
			 strongSelf.RightBar.alpha = 0.0f;
		 }
	 }
					 completion:nil];
}

#pragma mark - Private Methods

-(void) goToEditMenu:(id) sender
{
@try{
    if(mIsEditing)
    {
        mIsEditing = NO;
        [mEditTimer invalidate];
        [self DeleteRecognizer]; //0821
        mEditTimer = nil;
        [self performEditTick:nil];	// reset view angle
        self.navigationItem.rightBarButtonItem.title = @"Editar";
        self.draggableLocationTop.shouldAcceptDroppedObjects=NO;
        self.draggableLocationBottom.shouldAcceptDroppedObjects=NO;
        [self ResetAngle];
        
        
        // Agregar TabBar al Appdelegate cuando no esta en edicion
        
        
        self.draggableLocationBottom.frame=CGRectMake(-8,2, DRAGGABLE_LOCATION_WIDTH*2.8,OBJECT_HEIGHT );
        self.mBackGroundTabBar.frame= CGRectMake(0,yTabBar, WidthTabBar,OBJECT_HEIGHT);
        self.scrollview.frame= CGRectMake(0, yTabBar,   WidthTabBar, OBJECT_HEIGHT );
        [self.scrollview setContentSize:CGSizeMake(ScrollContentSize, OBJECT_HEIGHT )];
        self.scrollview.showsHorizontalScrollIndicator=NO;
    //     self.GreenLine.frame=CGRectMake(0,434+HeightScreen, 320,1 );
        self.mGreenLineTabBar.frame = CGRectMake(0,yTabBar,   WidthTabBar,1 );
        if(mAppDelegate.window.frame.size.width<600)
        [self scrollViewDidScroll:self.scrollview];
        
        [mAppDelegate.window insertSubview:self.mBackGroundTabBar atIndex:9];
        [mAppDelegate.window  insertSubview:self.scrollview atIndex:10];
        [mAppDelegate.window insertSubview:self.mGreenLineTabBar atIndex:11];
        
        [mAppDelegate.window bringSubviewToFront:self.LeftBar];
        [mAppDelegate.window bringSubviewToFront:self.RightBar];
        [self SaveOrderMenu];
        
        
          }
    else
    {
        mIsEditing = YES;
        [self AddRecognizer];
        self.draggableLocationTop.shouldAcceptDroppedObjects=YES;
        self.draggableLocationBottom.shouldAcceptDroppedObjects=YES;
        mEditTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/15.0 target:self selector:@selector(performEditTick:) userInfo:nil repeats:YES] ;
        self.navigationItem.rightBarButtonItem.title = @"Hecho";
        
        
        // Agregar TabBar HomeViewController en modo edicion
        
        int cont=0;
        for(UIView *i in mAppDelegate.window.subviews)
        {
            cont=i.tag;
            if(cont==15)
            {
                [i removeFromSuperview];
                CustomScrollView  *scroll = (CustomScrollView *)i;
                scroll.frame= CGRectMake(0, 0, WidthTabBar, yTabBar);
                [scroll setContentSize:CGSizeMake(ScrollContentSize, OBJECT_HEIGHT )];
                
                
                for(UIView *ScrollSubview in scroll.subviews)
                {
                ScrollSubview.frame =CGRectMake(-8,yEditTabBar, WidthTabBar*2.1,OBJECT_HEIGHT  );
                   }
                [self.view insertSubview:self.scrollview atIndex:1];
                
            }
            if(cont==17)
            {
                [i removeFromSuperview];
                i.frame=CGRectMake(0,yEditTabBar-2, WidthTabBar,OBJECT_HEIGHT );
                [self.view insertSubview:i atIndex:0];
                
            }
            
        }
        
          }
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/goEditMenu");
}
}


- (void) reachabilityConfig
{
@try{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    OReachability * reach = [OReachability reachabilityWithHostname:@"www.google.es"];
    
    [reach startNotifier];
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/reachabilityConfig");
}
}

-(void)reachabilityChanged:(NSNotification*)note
{
@try{
    OReachability * reach = [note object];
    
    if([reach isReachable])
    {
        [self refreshActiveNetWork];
    }
    else
    {
        [self refreshDesactiveNetWork];
    }
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/reachabilityEdit");
}
}

-(void) refreshActiveNetWork
{
mRefreshBtn.enabled = YES;
mUnreachableView.hidden = YES;
self.draggableLocationTop.hidden=NO;
self.scrollview.hidden=NO;

}

-(void) refreshDesactiveNetWork
{
@try{
    mRefreshBtn.enabled = NO;
    if([[OUpdate allUpdatesWithContext:[mAppDelegate managedObjectContext]] count] > 0)
    {
        mUnreachableView.hidden = YES;
        self.draggableLocationTop.hidden=NO;
        self.scrollview.hidden=NO;
        
        [UIAlertView alertViewWithTitle:@"Sin conexión" message:@"No ha sido posible actualizar los datos"
                               delegate:nil cancelButtonTitle:@"Cerrar" otherButtonTitles:nil];
    }
    else
    {
        mUnreachableView.hidden = NO;
        
        self.draggableLocationTop.hidden=YES;
        
        self.scrollview.hidden=YES;
        
    }
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/refreshDeactivateNetWork");
}
}



#pragma mark - Private Methods

/// new

- (void) setupDraggableLocations {

 //Frames
    
 
    
SEDraggableLocation *draggableLocationTop = [[SEDraggableLocation alloc] initWithFrame:CGRectMake(-5, 0, WidthTabBar, yEditTabBar)];
SEDraggableLocation *draggableLocationBottom = [[SEDraggableLocation alloc] initWithFrame:CGRectMake(-5,372, DRAGGABLE_LOCATION_WIDTH*2.8,OBJECT_HEIGHT)];
self.scrollview = [[CustomScrollView  alloc] init];
    
    

//Tags 
draggableLocationTop.tag=9;
draggableLocationBottom.tag=10;
self.scrollview.tag=15;
self.draggableLocationBottom.tag=16;
self.mBackGroundTabBar.tag=17;
self.mGreenLineTabBar.tag=18;
self.RightBar.tag=19;
self.LeftBar.tag=20;
    
    

draggableLocationTop.backgroundColor = [UIColor clearColor];
draggableLocationBottom.backgroundColor = [UIColor clearColor];

[self.view addSubview: draggableLocationTop];
[self.scrollview  addSubview:draggableLocationBottom];
[self.scrollview setContentSize:CGSizeMake(ScrollContentSize, 20)];
self.scrollview.showsHorizontalScrollIndicator=NO;


    // Config  Drag & Drop Locations
    
[self configureDraggableLocation: draggableLocationTop:YES:[UIColor clearColor]:OBJECT_WIDTH*1.93:OBJECT_HEIGHT*1.93:0.0f];
[self configureDraggableLocation: draggableLocationBottom:NO:[UIColor blackColor]:OBJECT_WIDTH*1.05:OBJECT_HEIGHT:MARGIN_HORIZONTAL];

 
draggableLocationBottom.shouldAcceptDroppedObjects=NO;
draggableLocationTop.shouldAcceptDroppedObjects=NO;
self.draggableLocationTop = draggableLocationTop;
self.draggableLocationBottom = draggableLocationBottom;

}



- (void) configureDraggableLocation:(SEDraggableLocation *)draggableLocation :(BOOL)HorizontallyFirst :(UIColor*)Color :(float)widthO :(float)heightO :(float)MarginH{
// set the width and height of the objects to be contained in this SEDraggableLocation (for spacing/arrangement purposes)
draggableLocation.objectWidth =widthO;
draggableLocation.objectHeight = heightO;

// set the bounding margins for this location
draggableLocation.marginLeft = MarginH;
draggableLocation.marginRight =MarginH;
draggableLocation.marginTop = MARGIN_VERTICAL;
draggableLocation.marginBottom = MARGIN_VERTICAL;

// set the margins that should be preserved between auto-arranged objects in this location
draggableLocation.marginBetweenX = MARGIN_HORIZONTAL;
draggableLocation.marginBetweenY = MARGIN_VERTICAL;

// set up highlight-on-drag-over behavior
draggableLocation.highlightColor = Color.CGColor;
draggableLocation.highlightOpacity = 0.4f;
draggableLocation.shouldHighlightOnDragOver = YES;

// you may want to toggle this on/off when certain events occur in your app
draggableLocation.shouldAcceptDroppedObjects = YES;

// set up auto-arranging behavior
draggableLocation.shouldKeepObjectsArranged = YES;
draggableLocation.fillHorizontallyFirst = HorizontallyFirst; // NO makes it fill rows first
draggableLocation.allowRows = YES;
draggableLocation.allowColumns = YES;
draggableLocation.shouldAnimateObjectAdjustments = YES; // if this is set to NO, objects will simply appear instantaneously at their new positions
draggableLocation.animationDuration = 0.5f;
draggableLocation.animationDelay = 0.0f;
draggableLocation.animationOptions = UIViewAnimationOptionLayoutSubviews ; // UIViewAnimationOptionBeginFromCurrentState;

draggableLocation.shouldAcceptObjectsSnappingBack = YES;


}



- (void) setupDraggableObjects {


NSMutableArray *pngs=[NSMutableArray array];
BOOL First=NO;
int tagcount=0;
BOOL TabBar=NO;

@try{
    // Get icon order from file
    NSString *pListPath = [DYFileNameManager pathForFile:@"MenuIcons.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
    
    NSString *firstLaunch = @"firstLaunch";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *firstLaunchValue = [defaults objectForKey:firstLaunch];
    NSInteger isFirstLaunch = [firstLaunchValue integerValue];
    
    if(dictionary == nil || isFirstLaunch==0)
    {
        First=YES;
        
        [defaults setObject:@1 forKey:firstLaunch];
        [defaults synchronize];

       pngs = [NSArray arrayWithObjects:@"Home",@"Favorites",@"Beans", @"Foods", @"Drinks", @"Rewards", @"MyDrinks", @"Store",@"Settings",@"Tarjeta",  nil];
        
        
    }
    else
    {
                for (int i =0;i<dictionary.allKeys.count;i++) {
             NSString* value = [dictionary objectForKey: [NSString stringWithFormat:@"%d", i]];
             [pngs addObject:value];
          }
       }
    
    
    
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/loadSections");
}

    @try{

for (NSString *png in pngs) {
    UIImage *draggableImage = [UIImage imageNamed:png];
    UIImageView *draggableImageView = [[UIImageView alloc] initWithImage: draggableImage];
    SEDraggable *draggable = [[SEDraggable alloc] initWithImageView: draggableImageView:[self SectionName:[self SectionTag:png]]];
    
    draggable.tag=[self SectionTag:png];
    
    if(draggable.tag==0||TabBar)
    {
        if(!First)
        TabBar=YES;
        
        if(draggable.tag==0)
        {
        
        UIImageView *Ui = (UIImageView *) [[draggable subviews] objectAtIndex:0];
        Ui.image=  [UIImage imageNamed:@"Home_selected"];
        draggable.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TabBarButtonHighlight.png"]];
        }
        [self configureDraggableObjectBottom:draggable];
       
     }
    
    else
    {
        draggable.frame=CGRectMake(tagcount+100,tagcount+100,draggable.frame.size.width,draggable.frame.size.height);
        [self configureDraggableObjectTop:draggable ];
     }tagcount++;
}
        
    
    
    } @catch (NSException *ex) {
            
          
        }
        


 
    
self.mBackGroundTabBar.frame = CGRectMake(0,yTabBar,  WidthTabBar,OBJECT_HEIGHT );
self.mGreenLineTabBar.frame = CGRectMake(0,yTabBar,  WidthTabBar,1 );
self.draggableLocationBottom.frame=CGRectMake(-8,2, WidthTabBar*2,OBJECT_HEIGHT);
self.scrollview.frame= CGRectMake(0,yTabBar, WidthTabBar, OBJECT_HEIGHT);
[self.scrollview setContentSize:CGSizeMake(ScrollContentSize, OBJECT_HEIGHT)];
self.RightBar.frame=CGRectMake(0,yTabBar, self.RightBar.frame.size.width,OBJECT_HEIGHT );
self.LeftBar.frame=CGRectMake(0,yTabBar,  self.RightBar.frame.size.width,OBJECT_HEIGHT);
    
[mAppDelegate.window insertSubview:self.mBackGroundTabBar atIndex:9];
[mAppDelegate.window insertSubview:self.scrollview atIndex:10];
[mAppDelegate.window insertSubview:self.mGreenLineTabBar atIndex:11];
[mAppDelegate.window insertSubview:self.RightBar atIndex:13];
[mAppDelegate.window insertSubview:self.LeftBar atIndex:14];

    
    //config
    self.RightBar.hidden=YES;
    self.LeftBar.hidden=YES;
    
    if(WidthTabBar<600)
        self.scrollview.delegate=self;
    
    if(self.draggableLocationBottom.containedObjects.count>5&&WidthTabBar<600)
    {
        self.RightBar.hidden=NO;
    }
    
    
    // save OrderMenu First time
    
    if(First)
    {
        [self SaveOrderMenu];

    }
    
    

}


-(void)SaveOrderMenu
{
    @try{
    NSString *pListPath = [DYFileNameManager pathForFile:@"MenuIcons.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
    
    for(int i = 0;i<[[self.draggableLocationTop containedObjects] count];i++ )
    {
        
        SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationTop containedObjects]objectAtIndex:i];
        
        NSString * newValue= [self SectionNameTag:draggable.tag];
        
        [dictionary setObject:newValue forKey:[NSString stringWithFormat:@"%d",i]];
        
    }
    
    for(int i = 0;i<[[self.draggableLocationBottom containedObjects] count];i++ )
    {
        
        SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationBottom containedObjects]objectAtIndex:i];
        
        int Cont = i+self.draggableLocationTop.containedObjects.count;
        
        [dictionary setObject:[self SectionNameTag:draggable.tag] forKey:[NSString stringWithFormat:@"%d",Cont]];
    }
    
    [dictionary writeToFile:pListPath atomically:YES];
    
    }
    @catch (NSException *ex) {
            
            NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/performEditTick");
        }
    
    
}

-(NSString*)SectionNameTag:(int)tag {



switch (tag)
{
    case 0:
        return @"Home";
        break;
    case 1:
        return @"Favorites";
        break;
    case 2:
        return @"Beans";
        break;
    case 3:
        return @"Foods";
        break;
    case 4:
        return @"Drinks";
        break;
    case 5:
        return @"Rewards";
        break;
    case 6:
        return @"MyDrinks";
        break;
    case 7:
        return @"Store";
        break;
        
    case 8:
        return @"Settings";
        break;
        
    case 13:
        return @"Tarjeta";
        break;
        
    default:
        return @"";
        break;
}

   

}

-(NSString*)SectionName:(int)tag {



switch (tag)
{
    case 0:
        return @"Home";
        break;
    case 1:
        return @"Favoritos";
        break;
    case 2:
        return @"Café";
        break;
    case 3:
        return @"Alimentos";
        break;
    case 4:
        return @"Bebidas";
        break;
    case 5:
        return @"Rewards";
        break;
    case 6:
        return @"Mis Bebidas";
        break;
    case 7:
        return @"Tiendas";
        break;
        
    case 8:
        return @"Configuración";
        break;
    
    case 13:
        return @"Tarjeta";
        break;
        
    default:
        return @"";
        break;
}


}

-(int)SectionTag:(NSString*)SectionName
{
 if([SectionName isEqualToString:@"Home"])
{
    return 0;
    
}

else if([SectionName isEqualToString:@"Favorites"])
{
    return 1;
    
}

else if([SectionName isEqualToString:@"Beans"])
{
    return 2;
    
}

else if([SectionName isEqualToString:@"Foods"])
{
    return 3;
    
}

else if([SectionName isEqualToString:@"Drinks"])
{
    return 4;
    
}

else if([SectionName isEqualToString:@"Rewards"])
{
    return 5;
    
}

else if([SectionName isEqualToString:@"MyDrinks"])
{
    return 6;
    
}

else if([SectionName isEqualToString: @"Store"])
{
    return 7;
    
}

else if([SectionName isEqualToString:@"Settings"])
{
    return 8;
}

else if([SectionName isEqualToString:@"Tarjeta"])
{
    return 13;
    
}

return 0;

}

-(void)SectionSelected:(int)tag :(UIImageView*)ImageSelected :(BOOL)Selected :(int)ParentTag
{
    if(ParentTag!=10)
    {
        
        SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationBottom containedObjects]objectAtIndex:0];
        if(draggable.tag==0)
        {
           draggable.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
            
          UIImageView *view=  (UIImageView*)[draggable.subviews objectAtIndex:0];
            view.image=[UIImage imageNamed:@"Home"];
        }
    }

    switch (tag) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 13:
        {
            NSNumber *sectionKey = @(tag);
            if(Selected) {
                if(ParentTag==10)
                    ImageSelected.image = ImageSelected.image = self.sectionImages[sectionKey][@"on"];
                
                [self setNewCurrentControllerAtIndex:tag];
            } else
                ImageSelected.image = self.sectionImages[sectionKey][@"off"];
        }   break;
        default:
            break;
    }
}


-(void)AddTouchRecognizer {

for(int i = 0;i<[[self.draggableLocationBottom containedObjects] count];i++ )
{
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    
    
    SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationBottom containedObjects]objectAtIndex:i];
    
    
    [draggable addGestureRecognizer:singleFingerTap];
    
}


for(int i = 0;i<[[self.draggableLocationTop containedObjects] count];i++ )
{
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    
    
    SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationTop containedObjects]objectAtIndex:i];
    
    [draggable addGestureRecognizer:singleFingerTap];
    
}


}
-(void) AddRecognizer {



for(int i = 0;i<[[self.draggableLocationBottom containedObjects] count];i++ )
{
    
    SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationBottom containedObjects]objectAtIndex:i];
    
    
    [draggable addGestureRecognizer:draggable.panGestureRecognizer];
    
}


for(int i = 0;i<[[self.draggableLocationTop containedObjects] count];i++ )
{
    
    SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationTop containedObjects]objectAtIndex:i];
    
    [draggable addGestureRecognizer:draggable.panGestureRecognizer];
    
    
}

}

-(void) DeleteRecognizer {



for(int i = 0;i<[[self.draggableLocationBottom containedObjects] count];i++ )
{
    
    SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationBottom containedObjects]objectAtIndex:i];
    [draggable removeGestureRecognizer:draggable.panGestureRecognizer];
    
}


for(int i = 0;i<[[self.draggableLocationTop containedObjects] count];i++ )
{
    
    SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationTop containedObjects]objectAtIndex:i];
    [draggable removeGestureRecognizer:draggable.panGestureRecognizer];
    
}

}

//Reconocer touch
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
//   CGPoint location = [recognizer locationInView:[recognizer.view superview]];

int tagSuperView = [[recognizer.view superview]tag];

UIImageView *aView =(UIImageView*)[[recognizer.view subviews] objectAtIndex:0];


if(!mIsEditing)
{
    if(tagSuperView==10)
    { for(int i = 0;i<[[self.draggableLocationBottom containedObjects] count];i++ )
    {
        
        SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationBottom containedObjects]objectAtIndex:i];
        draggable.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        if(draggable.tag!=[recognizer.view tag])
            [self SectionSelected:[draggable tag]:[[draggable subviews]objectAtIndex:0] :NO:tagSuperView];
        
    }
        recognizer.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TabBarButtonHighlight.png"]];
        [self SectionSelected:[recognizer.view tag]:aView:YES:tagSuperView];
        
       
        
    }
    else if(tagSuperView==9)
    {
        
        [self SectionSelected:[recognizer.view tag]:[recognizer.view.subviews objectAtIndex:0] :YES:tagSuperView];
    }
    
}

}


- (void) configureDraggableObjectTop:(SEDraggable *)draggable {
draggable.homeLocation = self.draggableLocationTop;
   
[draggable addAllowedDropLocation: self.draggableLocationTop];
[draggable addAllowedDropLocation: self.draggableLocationBottom];


 
    
[self.draggableLocationTop addDraggableObject:draggable animated:NO];
    
}


- (void) configureDraggableObjectBottom:(SEDraggable *)draggable {
    draggable.homeLocation = self.draggableLocationBottom;
    [draggable addAllowedDropLocation: self.draggableLocationTop];
    [draggable addAllowedDropLocation: self.draggableLocationBottom];
    
    
    [self.draggableLocationBottom addDraggableObject:draggable animated:NO];
}

- (void) performEditTick:(NSTimer *)_timer
{
@try{
    for(int i = 0;i<[[self.draggableLocationTop containedObjects] count];i++ )
    {
        
        SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationTop containedObjects] objectAtIndex:i];
        
        
        CGAffineTransform transform = CGAffineTransformIdentity;
        
        NSInteger rval = rand() & 0x07;
        CGFloat ang = (CGFloat)(rval-0x04);
        CGFloat rad = ang * M_PI / 180.0;
        
        transform = CGAffineTransformMakeRotation(rad);
        
        
        [UIView animateWithDuration:1.0/15.0 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^
         {
             draggable.transform = transform;
         } completion:^(BOOL finished){}];
        
    }
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/performEditTick");
}
}

-(void) ResetAngle{


@try{
    for(int i = 0;i<[[self.draggableLocationTop containedObjects] count];i++ )
    {
        
        SEDraggable *draggable = (SEDraggable*)[[self.draggableLocationTop containedObjects] objectAtIndex:i];
        
        
        float angle = 0;
        draggable.transform = CGAffineTransformMakeRotation(angle);
        
    }
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/performEditTick");
}

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    

    if(self.scrollview.contentOffset.x>20)
    {
        
        
        self.LeftBar.hidden=NO;
    }
    
    else
    {
        
        self.LeftBar.hidden=YES;
    }
    
    
    float Offset = self.scrollview.contentOffset.x;
    int total = self.draggableLocationBottom.containedObjects.count;
    
    if(total>5)
    {
        if(Offset<(total-5)*55)
    {
            self.RightBar.hidden=NO;
        
    }
        
        else
        
            self.RightBar.hidden=YES;
    }
    else{
        
        self.RightBar.hidden=YES;
    }
}



- (IBAction) actionRefreshClick
{

}


// terminando de importar y activar los botones
- (void) didFinishImporter
{
@try{
    NSArray *updatesArray = [OUpdate allUpdatesWithContext:[mAppDelegate managedObjectContext]];
    for(OUpdate *up in updatesArray)
    {
        if([up.name isEqualToString:@"cafes"])
            mCoffeeButton.enabled = YES;
        else if([up.name isEqualToString:@"comidas"])
            mFoodButton.enabled = YES;
        else if([up.name isEqualToString:@"tiendas"])
            mStoresButton.enabled = YES;
        else if([up.name isEqualToString:@"bebidas"])
            mDrinksButton.enabled = YES;
    }
    mUnreachableView.hidden = YES;
    mFavoritesButton.enabled = YES;
    mMyDrinksButton.enabled = YES;
    mConfigurationButton.enabled = YES;
    mRewardsButton.enabled = YES;
    btnFilter.enabled=YES;
    
    mCardButton.enabled = YES;
    
    
    [self AddTouchRecognizer];
    
    
    NSArray *mFavoritesArray = [OAbbreviatures AbbreviaturesInContext:[OAppDelegate managedObjectContext]];
    
    NSArray * arrayNames = [[NSArray alloc] initWithObjects:@"Vainilla",@"Caramelo",@"Cinnamon Dolce",@"Clásico",@"Avellana",@"Menta",@"Coco",@"Almendra",@"Frambuesa",@"Vainilla Sugar Free",@"Mocha Blanco",@"Salsa Fresa",@"Cajeta",@"Crema Batida",@"Espiral de Mocha",@"Espiral de Caramelo",@"Espiral de Cajeta",@"Más espuma",@"Más leche",@"Extra hot",@"Tibio",@"Splenda",@"Canderel",@"Azúcar",@"Azúcar Mascabado",@"Agua",@"Hielo",@"Canela en Polvo",@"Vainilla en Polvo",@"Mocha en Polvo",@"Chip",@"Entera",@"Light",@"Deslactosada",@"Deslactosada Light",@"Soya",@"Half-half",@"Café del Día",@"Café Misto",@"Espresso Clásico",@"Espresso con Panna",@"Espresso Macchiato",@"Caffè Expresso Americano",@"Caffè Latte",@"Caramel Macchiato",@"Cappuccino",@"Vainilla Cappuccino",@"Caffè Mocha",@"Caffè Mocha Blanco",@"Tè Passion Hibiscus",@"Té de Menta",@"Té de Manzanilla",@"Té China Green Tips",@"Té English Breakfast",@"Té Chai",@"Té Vainilla Rooibos",@"Té Chai Latte",@"Green Tea Latte",@"English Breakfast Latte",@"Vanilla Rooibos Latte",@"Chocolate",@"Chocolate Blanco",@"Leche al vapor",@"Café helado",@"Helado Latte",@"Helado Cajeta Latte",@"Helado Caramel Macchiato",@"Helado Mocha",@"Helado Mocha Blanco",@"Caramel Frappuccino",@"Café Frappuccino",@"Mocha Frappuccino",@"Mocha Blanco Frappuccino",@"Frappuccino Chip",@"Té helado",@"Shaken Lemon Passion Tea",@"Shaken Lemon Black Tea",@"Shaken Green Tea",@"Helado Té Chai Latte",@"Chai Cream Frappuccino",@"Helado Green Tea Latte",@"Green Tea Frappuccino",@"Chocolate Cream Frappuccino",@"Vainilla Cream Frappuccino",@"Fresa Cream Frappuccino",@"Mango Maracuyá Frappuccino",@"Frambuesa Grosella Frappuccino", @"Cafè del Día",nil];
    
    NSArray * arrayAbvs = [[NSArray alloc] initWithObjects:@"V",@"CR",@"CD",@"CL",@"H",@"MENTA",@"CO",@"A",@"F",@"VSF",@"MB",@"FRESA",@"K",@"CB",@"M",@"RC",@"K",@"Dry",@"Wet",@"XH",@"130º",@"SPL",@"CAN",@"AZ",@"AM",@"AGUA",@"ICE",@"PCIN",@"PV",@"PM",@"CHIP",@"E",@"L",@"D",@"DL",@"SOY",@"B",@"COD",@"MIS",@"E",@"EP",@"EM",@"EA",@"L",@"CM",@"C",@"C",@"M",@"MB",@"T",@"T",@"T",@"T",@"T",@"T",@"T",@"CHAI",@"GTL",@"EBTL",@"VRTL",@"CH",@"CHB",@"LV",@"IC",@"L",@"L",@"CM",@"M",@"MB",@"CRF",@"CF",@"MF",@"MBF",@"FC",@"T",@"LPT",@"LBT",@"LGT",@"CHAI",@"CRM",@"GTL",@"CRM",@"CRM",@"CRM",@"CRM",@"MM",@"FG",@"COD", nil];
    
    if(mFavoritesArray==nil || [mFavoritesArray count]==0)
    {
        
        for(int i=0;i<[arrayAbvs count];i++)
        {
            NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"Abbreviatures" inManagedObjectContext:[mAppDelegate managedObjectContext]];
            OAbbreviatures * abv = [[OAbbreviatures alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:[mAppDelegate managedObjectContext]];
            abv.name=[arrayNames objectAtIndex:i];
            abv.abbreviature=[arrayAbvs objectAtIndex:i];
        }
        NSError * error = nil;
        [[mAppDelegate managedObjectContext] save:&error];
        
        
        
    }
    
    [mAppDelegate saveContext];
    
    
    mRefreshBtn.enabled = YES;
    mLoadingView.hidden = YES;
    
    self.view.userInteractionEnabled = YES;
}
@catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/HomeViewController/disFinishImporter");
}
}

#pragma mark - Container Controller Methods

- (void)setNewCurrentControllerAtIndex:(NSInteger)index
{
    __weak OHomeViewController *weakSelf = self;
    void (^showNewController)(void) = ^{
        if(weakSelf) {
            OHomeViewController *strongSelf = weakSelf;
            [strongSelf.currentController dismissViewControllerAnimated:NO completion:nil];
            [strongSelf.currentController popToRootViewControllerAnimated:NO];
            
            if(index==0) {
                strongSelf.currentController = nil;
            } else {
                strongSelf.currentController = strongSelf.controllers[@(index)];
                [strongSelf presentViewController:strongSelf.currentController animated:NO completion:nil];
            }
        }
    };    
    if(self.currentController!=nil) {
        [self dismissViewControllerAnimated:NO completion:^{
            showNewController();
        }];            
    } else {
        showNewController();
    }
}


@end




