//
//  ODrinkCategoryViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFOpenFlowView.h"
#import "OAppDelegate.h"
#import "OMyDrinks.h"

@interface ODrinkCategoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,AFOpenFlowViewDelegate,AFOpenFlowViewDataSource>
{
	
	UITableView * mTableView;
	NSFetchedResultsController *mFetchedResultsController;
	BOOL mIsHot;
	NSMutableArray * mSectionsArray;
	IBOutlet AFOpenFlowView * mOpenFlowView;
	
	OAppDelegate *mAppDelegate;
	IBOutlet UIView * mCoverFlowDetailView;
	IBOutlet UILabel * mProductTitleLabel;
	IBOutlet UILabel * mProductNameDetail;
	IBOutlet UILabel * mProductDetailDetail;
	IBOutlet UIButton * mAvailableButton;
	IBOutlet UIView * mDetailView;
	IBOutlet UILabel * mAddToFavsLabel;
	OMyDrinks * mFavorite;
	int mOpenFlowSelected;
	NSManagedObjectContext *mAddingContext;
	BOOL mIsAdded;
	NSArray * mDrinksArray;

}

@property(nonatomic, strong) IBOutlet UITableView * tableView;
@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, assign) BOOL isHot;

-(IBAction)viewDetailButtonPressed:(id)sender;
-(IBAction)hideDetailViewButtonPressed:(id)sender;
-(IBAction)addToMyDrink:(id)sender;
-(IBAction) makeDrinkButtonPressed:(int)_index;

@end
