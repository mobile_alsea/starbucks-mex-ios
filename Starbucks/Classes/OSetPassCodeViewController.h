//
//  OSetPassCodeViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 03/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"


typedef enum {
	kValidateStartupPasscode, kValidateChangePasscode, kSetPasscode,	// These are the public types
	kResetPasscode, kConfirmPasscode, kConfirmResetPasscode				// These are internal-only types -- DO NOT USE FROM EXTERNAL CALLS!!!
} PasscodePageType;


@interface OSetPassCodeViewController : OBaseViewController<UIAlertViewDelegate,UITextFieldDelegate>
{

	IBOutlet UILabel *digit1;
    IBOutlet UILabel *digit2;
    IBOutlet UILabel *digit3;
    IBOutlet UILabel *digit4;
	IBOutlet UITextField *digitEntry;
	BOOL mIsNew;
	int mNumberOfNumbersInserted;
	NSString * mNewPassCode;
	IBOutlet UIButton * mForgotButton;
	IBOutlet UILabel * mInformationLabel;
	IBOutlet UIImageView * mImformationImageView;
	IBOutlet UILabel *mIsertPasswordLabel;
	IBOutlet UILabel *mReenterPasswordLabel;
	
	NSString * mPasswordFirstTime;
	NSString * mPasswordSecondTime;
	NSString * mPasswordSavedInFile;
	BOOL mModal;
	

}

@property (readwrite) BOOL passcodeIsModal;
@property (nonatomic, assign) BOOL isNew;
@property (nonatomic, assign) BOOL modal;
@property (nonatomic,strong) NSString * passwordSavedInFile;





- (IBAction)forgotButtonPresed:(id)sender;


@end
