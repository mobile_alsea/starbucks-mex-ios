//
//  ODrinkDetailViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODrink.h"
#import "DYImageView.h"
#import "OMyDrinks.h"
#import "OAppDelegate.h"
#import "OBaseViewController.h"

@interface ODrinkDetailViewController : OBaseViewController
{
	id __weak mDelegate;
	ODrink *mDrinkSelected;
	DYImageView * mThumbnailImageView;
	UILabel * mNameLabel;
	UILabel * mDescriptionLabel;
	IBOutlet UIButton * mPlusButton;
	IBOutlet UIButton * mAvailableButton;
	IBOutlet DYImageView * mPlusDYImageView;
	IBOutlet UIView * mPlusView;
	BOOL mIsAdded;
	OMyDrinks * mFavorite;
	OAppDelegate * mAppDelegate;
	UILabel * mAddFoodLabel;
	NSManagedObjectContext *mAddingContext;
	

}

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) ODrink *drinkSelected;
@property (nonatomic, strong) IBOutlet UILabel * nameLabel;
@property (nonatomic, strong) IBOutlet UILabel * descriptionLabel;
@property (nonatomic, strong) IBOutlet UIButton * plusButton;
@property (nonatomic, strong) IBOutlet UIButton * availableButton;
@property (nonatomic, strong) IBOutlet DYImageView * thumbnailImageView;
@property (nonatomic, strong) IBOutlet UILabel * addFoodLabel;


-(IBAction)plusButtonSelected:(id)sender;
-(IBAction)dismmisPlusButtonSelected:(id)sender;
-(IBAction)addToMyDrink:(id)sender;
-(IBAction)CustomizeDrinkButtonSelected:(id)sender;

@end
