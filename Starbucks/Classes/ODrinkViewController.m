//
//  ODrinkViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinkViewController.h"
#import "ODrinkCategoryViewController.h"
#import "OHomeViewController.h"

@interface ODrinkViewController ()

@end

@implementation ODrinkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkViewController");
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.navigationItem setTitle:NSLocalizedString(@"Tipo", nil)];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark IBActionMethods

-(IBAction)hotButtonPressed:(id)sender
{
    @try{
	ODrinkCategoryViewController *detailViewController = [[ODrinkCategoryViewController alloc] initWithNibName:@"ODrinkCategoryViewController" bundle:nil];
	;
	detailViewController.isHot = YES;
    
    
    //detailViewController.delegate = self;
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkViewController");
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

-(IBAction)coldButtonPressed:(id)sender
{
    @try{
	ODrinkCategoryViewController *detailViewController = [[ODrinkCategoryViewController alloc] initWithNibName:@"ODrinkCategoryViewController" bundle:nil];
	;
	detailViewController.isHot = NO;
    
    
    //detailViewController.delegate = self;
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkViewController");
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

@end
