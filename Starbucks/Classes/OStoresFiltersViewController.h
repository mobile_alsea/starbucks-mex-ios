//
//  OStoresFiltersViewController.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@class OStoresFiltersViewController;
@protocol OStoresFiltersViewControllerDelegate <NSObject>
- (void)closeFiltersViewController:(OStoresFiltersViewController *)controller;
@end

@interface OStoresFiltersViewController : OBaseViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, MKMapViewDelegate>
{
    id __weak mDelegate;
    
    //UINavigationItem *mNavigationItem;
    UITableView *mTableView;
    
    NSFetchedResultsController *mFetchedResultsController;
}

@property (nonatomic, weak) id <OStoresFiltersViewControllerDelegate> delegate;
//@property (nonatomic, strong) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

- (void) executeFetch;

@end
