//
//  OCoffeeViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"

@interface OCoffeeViewController : OBaseViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
{
	OAppDelegate *mAppDelegate;
	UITableView * mTableView;
	UIButton * mProfileButton;
	UIButton * mListButton;
	UIImageView * mSegmentedBackgroundImageView;
	NSFetchedResultsController *mFetchedResultsController;
	NSArray * mHeadersArray;
	IBOutlet UIView * mHeaderView;
	BOOL misProfileButtonSelected;

}

@property(nonatomic, strong) IBOutlet UITableView * tableView;
@property(nonatomic, strong) IBOutlet UIButton * profileButton;
@property(nonatomic, strong) IBOutlet UIButton * listButton;
@property(nonatomic, strong) IBOutlet UIImageView * segmentedBackgroundImageView;
@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;

-(IBAction)btnProfileSelected:(id)sender;
-(IBAction)btnListSelected:(id)sender;

@end
