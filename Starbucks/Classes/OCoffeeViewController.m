//
//  OCoffeeViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCoffeeViewController.h"
#import "OCoffeeCell.h"
#import "OCoffee.h"
#import "OCoffee+Extras.h"
#import "OAppDelegate.h"
#import "OCoffeeDetailViewController.h"
#import "OHomeViewController.h"
@interface OCoffeeViewController ()

@end

@implementation OCoffeeViewController
@synthesize tableView = mTableView;
@synthesize profileButton = mProfileButton;
@synthesize listButton = mListButton;
@synthesize segmentedBackgroundImageView = mSegmentedBackgroundImageView;
@synthesize fetchedResultsController = mFetchedResultsController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mHeadersArray = nil;
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/initWithNibName");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[self.navigationItem setTitle:NSLocalizedString(@"Café", nil)];

	if(mHeadersArray!=nil)
	{
		mHeadersArray = nil;
	}
	mHeadersArray = [[NSArray alloc] initWithArray:[OCoffee categoriesOfCoffeesInContext:[OAppDelegate managedObjectContext]]];
	// Load Data
    [self executeFetch:0];
	misProfileButtonSelected = YES;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/viewdidload");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) configureCell:(OCoffeeCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    OCoffee *coffee = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    
    _cell.titleLabel.text = coffee.name;
	
	
	if([[coffee.flavors allObjects] count]>0)
	{
	
		NSString * flavors = [[[coffee.flavors allObjects] objectAtIndex:0] name];
		for(int i=1;i<[[coffee.flavors allObjects] count];i++)
		{
			if(i==[[coffee.flavors allObjects] count]-1) flavors = [flavors stringByAppendingString:@" y "];
			else flavors = [flavors stringByAppendingString:@", "];
			flavors = [flavors stringByAppendingString:[[[coffee.flavors allObjects] objectAtIndex:i] name]];
	
		}
		_cell.descriptionLabel.text = flavors;
	}
	
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/configurecell");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
}


- (void) executeFetch: (int) _fetchtype
{
    @try{
    mFetchedResultsController = nil;
    
	NSError *error;
	
	if(_fetchtype ==0)
	{
	
		if (![[self fetchedResultsController] performFetch:&error])
		{
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
	else 
	{
		if (![[self fetchedResultsControllerList] performFetch:&error])
		{
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
    
    [mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/executeFetch");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
}


- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	
    NSFetchRequest *fetchRequest;
	fetchRequest = [OCoffee coffeesInContextFetchRequest:[OAppDelegate managedObjectContext]];
   
	// Create the sort descriptors array.
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"profile" ascending:YES];
	NSSortDescriptor *sellerDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor,sellerDescriptor2, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:@"profile" cacheName:nil];
    mFetchedResultsController.delegate = self;
	return mFetchedResultsController;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/fetchedResultController");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (NSFetchedResultsController *)fetchedResultsControllerList
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	
    NSFetchRequest *fetchRequest;
	fetchRequest = [OCoffee coffeesInContextFetchRequest:[OAppDelegate managedObjectContext]];
	
	// Create the sort descriptors array.
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"form" ascending:YES];
	NSSortDescriptor *sellerDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor,sellerDescriptor2, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:@"form" cacheName:nil];
    mFetchedResultsController.delegate = self;
	return mFetchedResultsController;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/fetchedResultsController");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}




#pragma mark IBActions

-(IBAction)btnProfileSelected:(id)sender
{
    @try{
	mProfileButton.selected = YES;
	mListButton .selected = NO;
	[mSegmentedBackgroundImageView setImage:[UIImage imageNamed:@"SectionHeaderLeftSelected.png"]];
	misProfileButtonSelected = YES;
	// Load Data
    [self executeFetch:0];
	[mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/btnPRofileSelect");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
	
}
-(IBAction)btnListSelected:(id)sender
{
    @try{
	mProfileButton.selected = NO;
	mListButton .selected = YES;
	[mSegmentedBackgroundImageView setImage:[UIImage imageNamed:@"SectionHeaderRightSelected.png"]];
	misProfileButtonSelected = NO;
	// Load Data
    [self executeFetch:1];
	[mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/btnListSelected");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark - UITableView Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
	return [[self.fetchedResultsController sections] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 15;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    @try{
	 id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	OCoffee *  coffee = (OCoffee*)[[sectionInfo objects] objectAtIndex:0];
	
	if(misProfileButtonSelected)return coffee.profile;
	else return  coffee.form;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/tableview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    @try{
	 NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
	UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 15);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:12];
    label.text = sectionTitle;
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 15)];
	
	if(misProfileButtonSelected)
	{
	
		switch (section) 
		{
			case 0:
				view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:165.0/255.0 blue:19.0/255.0 alpha:1.0];
			
			
				break;
			case 1:
				view.backgroundColor = [UIColor colorWithRed:122.0/255.0 green:0.0/255.0 blue:60.0/255.0 alpha:1.0];
				break;
			
			case 2:
				view.backgroundColor = [UIColor colorWithRed:177.0/255.0 green:91.0/255.0 blue:17.0/255.0 alpha:1.0];
				break;
	
			default:
				view.backgroundColor = [UIColor blackColor];
				break;
		}
	}
	else 
	{
		view.backgroundColor = [UIColor blackColor];
	}
	
	
	[view addSubview:label];
	return view;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/tableview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}


- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    @try{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_section];
    return [sectionInfo numberOfObjects];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/tableview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OCoffeeCell";
    
    OCoffeeCell *cell = (OCoffeeCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
		
    }
    
    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];

    
    [self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
return nil;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/tableview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
    
    OCoffee *coffee = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    OCoffeeDetailViewController *detailViewController = [[OCoffeeDetailViewController alloc] initWithNibName:@"OCoffeeDetailViewController" bundle:nil];
	;
    
    
	[detailViewController setCoffeeSelected:coffee];
    detailViewController.delegate = self;
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/tabelview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

	

}

#pragma mark - NSFetchedResultsController Protocol

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [mTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    @try{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [mTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
					  withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [mTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
					  withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/controller");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    @try{
    UITableView *tableView = mTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(OCoffeeCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Coffe/oCoffeeViewCntroller/controller");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [mTableView endUpdates];
}





@end
