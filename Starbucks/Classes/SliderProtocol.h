//
//  SliderProtocol.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

@protocol UIView;


@protocol SliderProtocol <NSObject>

@property (nonatomic, assign) NSInteger valueIndex;
@property (nonatomic, assign) NSInteger valueCount;
@property (nonatomic, assign) NSInteger valuesBetweenNamedValues;
@property (nonatomic, copy) NSArray *valueNames;
@property (nonatomic, assign) CGFloat valueSeparation;

- (void)setValueIndex:(NSInteger)valueIndex animated:(BOOL)animated;
- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)events;


@end
