//
//  DNLinearLayoutView.m
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "DNLinearLayoutView.h"

@implementation DNLinearLayoutView

@synthesize padding;

- (void)setPadding:(CGFloat)value {
	padding = value;
	[self setNeedsLayout];
}

- (CGSize)sizeThatFits:(CGSize)size {
	size.width = 0;
	for (UIView *subview in self.subviews) {
		size.width += subview.bounds.size.width + padding;
	}
	return size;
}

- (void)removeAllSubviews {
    @try{
	for (UIView *subview in self.subviews) {
		[subview removeFromSuperview];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Networks/DNLinearLayoutView.m/removeAllSubviews");
    }
}

- (void)layoutSubviews {
	CGFloat x = 0;
	for (UIView *subview in self.subviews) {
		CGRect bounds = subview.bounds;
		subview.frame = CGRectMake(x, 0, bounds.size.width, bounds.size.height);
		x += bounds.size.width + padding;
	}
}


@end
