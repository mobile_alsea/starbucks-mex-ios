//
//  ONotesCustomizedViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 21/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ONotesCustomizedViewController.h"
#import <CoreLocation/CoreLocation.h>
#define RGBToFloat(f) (f/255.0)
@interface ONotesCustomizedViewController ()

@end

@implementation ONotesCustomizedViewController
@synthesize  userLabel = mUserLabel;
@synthesize delegate = mDelegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ONotesCustomizedViewContrller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:NSLocalizedString(@"Notas", nil)];
	UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Guardar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(SaveButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:btnSave];
	
	
	UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"  style:UIBarButtonItemStyleBordered target:self action:@selector(CancelButtonPressed:)];
	
    
    
    [self.navigationItem setLeftBarButtonItem:btnCancel];
	
	
	
	mTextView.text = @"¿Algo en especial?";
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ONotesCustomizedViewContrller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)addUserButtonPressed:(id)sender
{
    @try{
	[mTextView resignFirstResponder];
	[mTextField resignFirstResponder];
	ABPeoplePickerNavigationController *peoplePickeNavController = [[ABPeoplePickerNavigationController alloc] init];
	[peoplePickeNavController.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
	peoplePickeNavController.searchDisplayController.searchBar.tintColor = [UIColor blackColor];
	
	peoplePickeNavController.peoplePickerDelegate = self;
	[self presentModalViewController:peoplePickeNavController animated:YES];
	peoplePickeNavController.navigationBar.topItem.title = @"Todos los contactos";
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ONotesCustomizedViewContrller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
}
-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
	/*UIViewController * prueba = [[[UIViewController alloc] init] autorelease];
	[prueba.view setHidden:YES];
	[self presentModalViewController:prueba animated:NO];
	[self dismissModalViewControllerAnimated:NO];*/
	
}
-(IBAction)CancelButtonPressed:(id)sender
{
	[self dismissModalViewControllerAnimated:YES];
}

-(IBAction)SaveButtonPressed:(id)sender
{
	
	
	
	@try{
	
	NSString * nickName = @"";
	if([mTextField.text isEqualToString:@"Ingresa nombre"]) nickName = @"Mi favorito";
	else nickName = mTextField.text;
	NSString * user =@"";
	if(![mUserLabel.text isEqualToString:@"busca contacto"]) user = mUserLabel.text;
	NSString * notes =@""; 
	if([mTextView.text isEqualToString:@"¿Algo en especial?"])
	{
		notes = @"";
	
	}
	else notes = mTextView.text;
	
	NSString * userType=@"1";
	
	
	if([mUserLabel.text isEqualToString:@"busca contacto"])
	{
		userType = @"1";
		
	}
	else userType = @"2";
	
	[mDelegate saveAllDrink:user :userType :notes :nickName];
	
	
	[self dismissModalViewControllerAnimated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ONotesCustomizedViewContrller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

#pragma mark TextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	if([textView.text isEqualToString:@"¿Algo en especial?"])
	{	
		textView.text = @"";
		textView.textColor = [UIColor blackColor];
	}

}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
	textField.placeholder=@"";

}

@end



@implementation ONotesCustomizedViewController (PeoplePickerNavigationControllerDelegateMethods)

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    @try{
	NSString *firstName = (NSString *) CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
	if(firstName==nil) firstName=@"";
	NSString * lastName = (NSString *) CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
	//NSLog(lastName);
	if(lastName!=nil && lastName.length>0)
		mUserLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
	else mUserLabel.text = firstName;
	mUserLabel.textColor = [UIColor blackColor];
	
	[self dismissModalViewControllerAnimated:YES];
	
	return NO;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ONotesCustomizedViewContrller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
	[self dismissModalViewControllerAnimated:YES];
}

@end