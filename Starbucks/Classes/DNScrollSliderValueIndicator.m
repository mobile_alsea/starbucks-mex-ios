//
//  DNScrollSliderValueIndicator.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "DNScrollSliderValueIndicator.h"


@implementation DNScrollSliderValueIndicator

@synthesize valueLabel;

+ (DNScrollSliderValueIndicator *)valueIndicator {
    @try{
	//Generated with help from nib2objc
	
	UIView *indicatorLine = [[UIView alloc] initWithFrame:CGRectMake(18.5, 0.0, 2.0, 16.0)];
	indicatorLine.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
	indicatorLine.backgroundColor = [UIColor colorWithWhite:1.000 alpha:1.000];
	indicatorLine.opaque = YES;
	
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(-1.0, 18.0, 40.0, 18.0)];
	textLabel.adjustsFontSizeToFitWidth = YES;
	textLabel.alpha = 1.000;
	textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
	textLabel.backgroundColor = [UIColor clearColor];
	textLabel.font = [UIFont boldSystemFontOfSize:14.f];
	textLabel.hidden = NO;
	textLabel.lineBreakMode = UILineBreakModeTailTruncation;
	textLabel.minimumFontSize = 10.000;
	textLabel.numberOfLines = 1;
	textLabel.shadowColor = [UIColor colorWithWhite:0.000 alpha:1.000];
	textLabel.shadowOffset = CGSizeMake(0.0, -1.0);
	textLabel.textAlignment = UITextAlignmentCenter;
	textLabel.textColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.000];
	textLabel.userInteractionEnabled = NO;
	
	DNScrollSliderValueIndicator *indicatorView = [[DNScrollSliderValueIndicator alloc] initWithFrame:CGRectMake(0.0, 0.0, 40.0, 50.0)];
	indicatorView.frame = CGRectMake(0.0, 0.0, 40.0, 50.0);
	indicatorView.alpha = 1.000;
	indicatorView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
	indicatorView.backgroundColor = [UIColor colorWithRed:0.062 green:0.062 blue:0.062 alpha:1.000];
	indicatorView.clearsContextBeforeDrawing = YES;
	indicatorView.clipsToBounds = NO;
	indicatorView.contentMode = UIViewContentModeScaleToFill;
	indicatorView.hidden = NO;
	indicatorView.multipleTouchEnabled = NO;
	indicatorView.opaque = YES;
	indicatorView.tag = 0;
	indicatorView.userInteractionEnabled = YES;
	
	[indicatorView addSubview:indicatorLine];
	[indicatorView addSubview:textLabel];
	indicatorView.valueLabel = textLabel;
	
	
	return indicatorView;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/DNScrollSliderValueIndicator");
    }
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
}




@end


