//
//  OMapAnnotation.h
//  Starbucks
//
//  Created by Adrián Caramés Ramos on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

@interface OMapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, strong) NSDictionary *storeInfo;

@end
