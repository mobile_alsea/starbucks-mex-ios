//
//  OShareConfigurator.m
//  Starbucks
//
//  Created by Santi Belloso López on 25/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OShareConfigurator.h"

@implementation OShareConfigurator

- (NSString*)appName
{
	return @"Starbucks";
}

- (NSString*)appURL
{
	return @"http://www.starbucks.com";
}
- (NSString*)sharersPlistName
{
	return @"SHKMySharers.plist";

}

/*
 API Keys
 --------
 This is the longest step to getting set up, it involves filling in API keys for the supported services.
 It should be pretty painless though and should hopefully take no more than a few minutes.
 
 Each key below as a link to a page where you can generate an api key.  Fill in the key for each service below.
 
 A note on services you don't need:
 If, for example, your app only shares URLs then you probably won't need image services like Flickr.
 In these cases it is safe to leave an API key blank.
 
 However, it is STRONGLY recommended that you do your best to support all services for the types of sharing you support.
 The core principle behind ShareKit is to leave the service choices up to the user.  Thus, you should not remove any services,
 leaving that decision up to the user.
 */

// Vkontakte

// Facebook - https://developers.facebook.com/apps
// SHKFacebookAppID is the Application ID provided by Facebook
// SHKFacebookLocalAppID is used if you need to differentiate between several iOS apps running against a single Facebook app. Useful, if you have full and lite versions of the same app,
// and wish sharing from both will appear on facebook as sharing from one main app. You have to add different suffix to each version. Do not forget to fill both suffixes on facebook developer ("URL Scheme Suffix"). Leave it blank unless you are sure of what you are doing.
// The CFBundleURLSchemes in your App-Info.plist should be "fb" + the concatenation of these two IDs.
// Example:
//    SHKFacebookAppID = 555
//    SHKFacebookLocalAppID = lite
//
//    Your CFBundleURLSchemes entry: fb555lite
- (NSString*)facebookAppId
{
	return @"526763180679965";
}

//Change if your app needs some special Facebook permissions only. In most cases you can leave it as it is.
- (NSArray*)facebookListOfPermissions
{
    return [NSArray arrayWithObjects:@"publish_stream", /*@"offline_access", */nil];
}

// Read It Later - http://readitlaterlist.com/api/signup/

// Diigo - http://www.diigo.com/api_keys/new/

// Twitter - http://dev.twitter.com/apps/new
/*
 Important Twitter settings to get right:
 
 Differences between OAuth and xAuth
 --
 There are two types of authentication provided for Twitter, OAuth and xAuth.  OAuth is the default and will
 present a web view to log the user in.  xAuth presents a native entry form but requires Twitter to add xAuth to your app (you have to request it from them).
 If your app has been approved for xAuth, set SHKTwitterUseXAuth to 1.
 
 Callback URL (important to get right for OAuth users)
 --
 1. Open your application settings at http://dev.twitter.com/apps/
 2. 'Application Type' should be set to BROWSER (not client)
 3. 'Callback URL' should match whatever you enter in SHKTwitterCallbackUrl.  The callback url doesn't have to be an actual existing url.  The user will never get to it because ShareKit intercepts it before the user is redirected.  It just needs to match.
 */


 //If you want to force use of old-style, pre-IOS5 twitter framework, for example to ensure
 //twitter accounts don't end up in the devices account store, set this to true.
 
- (NSNumber*)forcePreIOS5TwitterAccess
{
	return [NSNumber numberWithBool:YES];
}

- (NSString*)twitterConsumerKey
{
	return @"1EL3pxsfFvz2G4vz1yxQ";
}

- (NSString*)twitterSecret
{
	return @"xdNweHCtkiRrrJTPwIYkfmrOhqmKJuUJIIR16y4";
}
// You need to set this if using OAuth, see note above (xAuth users can skip it)
- (NSString*)twitterCallbackUrl
{
	return @"http://starbucsPrueba.com";
}
// To use xAuth, set to 1
- (NSNumber*)twitterUseXAuth
{
	return [NSNumber numberWithInt:0];
}
// Enter your app's twitter account if you'd like to ask the user to follow it when logging in. (Only for xAuth)
- (NSString*)twitterUsername
{
	return @"";
}

// Evernote - http://www.evernote.com/about/developer/api/

// Flickr - http://www.flickr.com/services/apps/create/


// Bit.ly for shortening URLs in case you use original SHKTwitter sharer (pre iOS5). If you use iOS 5 builtin framework, the URL will be shortened anyway, these settings are not used in this case. http://bit.ly/account/register - after signup: http://bit.ly/a/your_api_key If you do not enter bit.ly credentials, URL will be shared unshortened.
- (NSString*)bitLyLogin
{
	return @"dylvianpruebasproduccion";
}

- (NSString*)bitLyKey
{
	return @"R_31fb65f484dbd1871a754c6418a78bc0";
}

// LinkedIn - https://www.linkedin.com/secure/developer

// Readability - http://www.readability.com/publishers/api/

// Foursquare V2 - https://developer.foursquare.com

/* SHKFacebook */

//when you share URL on Facebook, FBDialog scans the page and fills picture and description automagically by default. Use these item properties to set your own.

@end

