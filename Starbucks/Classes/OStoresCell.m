//
//  OStoresCell.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation OStoresCell


#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


#pragma mark - Properties

@synthesize nameLabel = mNameLabel;
@synthesize addressLabel = mAddressLabel;
@synthesize scheduleButton = mScheduleButton;
@synthesize driveView = mDriveView;
@synthesize mobileView = mMobileView;
@synthesize reserveView = mReserveView;
@synthesize wirelessView = mWirelessView;
@synthesize warmedView = mWarmedView;
@synthesize distanceLabel = mDistanceLabel;



- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}

#pragma mark - UITableViewCell Methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Private Methods

- (void)updateSchedule:(BOOL)_state
{
	@try{
	if(_state){
        UIImage *stateOpen = [[UIImage imageNamed:@"StoreList_OpenNow_bkgnd.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        [mScheduleButton setBackgroundImage:stateOpen forState:UIControlStateNormal];
		[mScheduleButton setTitle:NSLocalizedString(@"ABIERTO", nil) forState:UIControlStateNormal];
	}else{
        
        UIImage *stateOpen = [[UIImage imageNamed:@"StoreList_CloseNow_bkgnd.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        [mScheduleButton setBackgroundImage:stateOpen forState:UIControlStateNormal];
        
		[mScheduleButton setTitle:NSLocalizedString(@"CERRADO", nil) forState:UIControlStateNormal];
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


@end
