//
//  ODrinksImporter.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OBaseImporter.h"

@interface ODrinksImporter : OBaseImporter
{

	NSMutableDictionary *mCurrentDrinkDict;
    NSMutableDictionary *mCurrentShotDict;
    NSMutableDictionary *mCurrentPersonalizationDict;
    
    NSMutableArray *mDrinkShotArray;
    NSMutableArray *mDrinkPersonalizationArray;
	
    BOOL mIsShot;
    BOOL mIsPersonalization;
	int mId;


}

- (void) insertIntoContext:(NSDictionary *)_item;
@end
