//
//  ODrinkSubcategoryViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinkSubcategoryViewController.h"
#import "ODrinksCell.h"
#import "ODrink.h"
#import "ODrink+Extras.h"
#import "OAppDelegate.h"
#import "ODrinkDetailViewController.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OCustomizeDrinkViewController.h"
#import "ODrinkCategoryViewController.h"
@interface ODrinkSubcategoryViewController ()

@end

@implementation ODrinkSubcategoryViewController
@synthesize isHot = mIsHot;
@synthesize subCategory = mSubCategory;
@synthesize fetchedResultsController = mFetchedResultsController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

/*-(void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
	
}*/

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	

    // Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:mSubCategory];
	[self executeFetch];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

	
}

- (void) configureCell:(ODrinksCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    ODrink *drink = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    
    _cell.titleLabel.text = drink.name;
	
	UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [_cell.avalaibleButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [_cell.avalaibleButton setTitle:drink.available forState:UIControlStateNormal];
	
	/*if(drink.image!=nil)
	{
		[_cell.thumbnailImageView setImage:[UIImage imageWithData:drink.image]];
	}*/
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:drink.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[_cell.thumbnailImageView setUrl:[NSURL URLWithString:drink.image]];
	}
	else
	{
		[_cell.thumbnailImageView setImage:[UIImage imageWithData:image.image]];
	}
	

    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

	
	
    
}


-(void) makeDrinkButtonPressed:(int)_index
{
    @try{
	ODrink *drink = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:_index inSection:0]];
	OCustomizeDrinkViewController *detailViewController = [[OCustomizeDrinkViewController alloc] initWithNibName:@"OCustomizeDrinkViewController" bundle:nil];
	[detailViewController setDrinkSelected:drink];
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void) executeFetch
{
    @try{
    mFetchedResultsController = nil;
    
	NSError *error;
	
	if (![[self fetchedResultsController] performFetch:&error])
	{
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
	
    [mTableView reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}


- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	NSString * temperature=@"";
	if(mIsHot==YES) temperature = @"Caliente";
	else temperature = @"Frío";
	
	
    NSFetchRequest *fetchRequest;
	fetchRequest = [ODrink drinksInContext:[OAppDelegate managedObjectContext] withTemperature:temperature andSubcategory:mSubCategory];

	// Create the sort descriptors array.
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    mFetchedResultsController.delegate = self;
	return mFetchedResultsController;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}



#pragma mark UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    @try{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_section];
        
        int ObjectsNumber=6;
    if(mAppDelegate.window.frame.size.height==480)
         ObjectsNumber=6;
     else  if(mAppDelegate.window.frame.size.height==568)
            ObjectsNumber=7;
     else  if(mAppDelegate.window.frame.size.height==1024)
         ObjectsNumber=12;
        
        
	if([sectionInfo numberOfObjects]<=ObjectsNumber)
	{
		mTableView.frame = CGRectMake(10, 10, 300, [sectionInfo numberOfObjects]*65);
        // MBT 12-02-13
  [mTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];
	
    }
    return [sectionInfo numberOfObjects];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	
    static NSString *CellIdentifier = @"ODrinksCell";
    
    ODrinksCell *cell = (ODrinksCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
    }
    
    // Setting Cell
	cell.index = _indexPath.row;
	cell.delegate = self;
	cell.thumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-bebidas-low.png"];
	[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
    [cell updateSchedule:0];

	// Configure cell
	[self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
	ODrink *drink = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    
     
	
	ODrinkDetailViewController *detailViewController = [[ODrinkDetailViewController alloc] initWithNibName:@"ODrinkDetailViewController" bundle:nil];
	[detailViewController setDrinkSelected:drink];
    detailViewController.delegate = self;	
	
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinksubcategoryController");
        ODrinkCategoryViewController  *regresa =[[ODrinkCategoryViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }


}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 65;
}



@end
