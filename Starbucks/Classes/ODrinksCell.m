//
//  ODrinksCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinksCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation ODrinksCell

@synthesize titleLabel = mTitleLabel;
@synthesize avalaibleButton = mAvailableButton;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize index = mIndex;
@synthesize delegate = mDelegate;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}


#pragma mark - Private methods


- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
	{
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
		
		
	}
    else
	{
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
		
	}
	
	
	
	
}

- (void)updateSchedule:(int)_state
{
    UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [mAvailableButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [mAvailableButton setTitle:NSLocalizedString(@"Verano", nil) forState:UIControlStateNormal];
}

-(IBAction)makeDrinkButtonPressed:(id)sender
{

	[mDelegate makeDrinkButtonPressed:mIndex];

}

@end
