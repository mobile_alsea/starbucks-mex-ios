//
//  CupMarkingView.m
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "CupMarkingView.h"

@implementation CupMarkingView

@synthesize strikethrough;
@synthesize label;


- (id)initWithFrame:(CGRect)frame {
    @try{
    self = [super initWithFrame:frame];
	if (!self) return nil;
	
    label = [[UILabel alloc] initWithFrame:frame];
	label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	label.font = [UIFont fontWithName:@"Marker Felt" size:30];
	label.minimumFontSize = 16;
	label.adjustsFontSizeToFitWidth = YES;
	label.backgroundColor = [UIColor clearColor];
	[self addSubview:label];
	
	strikethroughView = [[UIImageView alloc] initWithFrame:frame];
	strikethroughView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	//TODO
	strikethroughView.image = [UIImage imageNamed:@"images/Drinks/CupMarkStrikethrough.png"];
	[self addSubview:strikethroughView];
	
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Networks/CupMarkingView.m/initWithFrame");
    }
}



- (void)setStrikethrough:(BOOL)value {
    strikethrough = value;
	strikethroughView.hidden = !value;
}



- (void)layoutSubviews {
	CGRect bounds = self.bounds;
	label.frame = CGRectMake(5, 5, bounds.size.width - 10, bounds.size.height - 10);
	strikethroughView.frame = bounds;
}

- (CGSize)sizeThatFits:(CGSize)size {
	size = [label sizeThatFits:size];
	//Cap the width of any one cup marking
	if (size.width > 50) size.width = 50;
	size.width += 10;
	size.height += 10;
	return size;
}



@end

