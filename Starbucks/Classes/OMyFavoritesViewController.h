//
//  OMyFavoritesViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"


@interface OMyFavoritesViewController : OBaseViewController
{

	UITableView * mTableView;
	NSMutableArray * mCoffeesArray;
	NSMutableArray * mFoodArray;
	NSMutableArray * mStoresArray;
	NSArray * mFavoritesArray;
	NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	

}

@property(nonatomic,strong) IBOutlet UITableView * tableView;


@end
