//
//  ODrinkViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface ODrinkViewController : OBaseViewController
{
}


-(IBAction)hotButtonPressed:(id)sender;
-(IBAction)coldButtonPressed:(id)sender;
@end
