//
//  OStoresServicesCell.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OStoresServicesCell : UITableViewCell
{
    BOOL mIndexPathPair;
    
    UILabel *mTitleLabel;
    UIImageView *mIconImageView;
    UIImageView *mCheckImageView;
}

@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) IBOutlet UIImageView *checkImageView;

- (void)configureCellWithIndex:(int)_index;

@end
