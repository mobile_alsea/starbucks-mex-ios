//
//  OFiltersViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFiltersViewController.h"
#import "OFiltersCell.h"
@interface OFiltersViewController ()

@end

@implementation OFiltersViewController
@synthesize delegate = mDelegate;
@synthesize filtersSelected = mFiltersSelected;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[self.navigationController.navigationBar setHidden:NO];

	[self.navigationItem setTitle:NSLocalizedString(@"Filtrar Alimentos", nil)];
	
   /* UIBarButtonItem * endButtonItem = [[UIBarButtonItem alloc] initWithCustomView:mEndFiltersButton];

    self.navigationItem.rightBarButtonItem =endButtonItem;
	*/
	
	UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithTitle:@"Finalizar"  style:UIBarButtonItemStyleBordered target:self action:@selector(endFilter:)];
	
    
    
    [self.navigationItem setRightBarButtonItem:btnFilter];
	
	
	if(mFiltersArray==nil) mFiltersArray = [[NSMutableArray alloc] initWithObjects:@"Sin Cacahuate",@"Sin Almendras",@"Sin Lactosa",@"Sin Soya",@"Sin Huevo",@"Sin Trigo",@"Sin Pescado",@"Sin Marisco", nil];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction) endFilter:(id) sender
{
	[self dismissModalViewControllerAnimated:YES];

}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
	return 1;
}


- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{   
	if(mFiltersArray==nil) return 0;
	else return [mFiltersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OFiltersCell";
    
    OFiltersCell *cell = (OFiltersCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
		
    }
	cell.filterNameLabel.text = [mFiltersArray objectAtIndex:_indexPath.row];
	cell.delegate = self;
	cell.index = _indexPath.row;
	NSString * row = [NSString stringWithFormat:@"%d",_indexPath.row];
	if(mFiltersSelected!=nil && [mFiltersSelected count]>0 && [mFiltersSelected indexOfObject:row]!=NSNotFound)
	{
		cell.checkButton.selected=YES;
		cell.isActive=YES;
	}
	else
	{
		cell.checkButton.selected=NO;
		cell.isActive=NO;
		
	}

	
    return cell;
	return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    
	
	
}

-(void) UpdateFiltersArrayForIndex:(int)mIndex
{
	@try{
	NSString * ind = [NSString stringWithFormat:@"%d",mIndex];
	if(mFiltersSelected==nil ||[mFiltersSelected count]==0)
	{
		mFiltersSelected = [[NSMutableArray alloc] initWithObjects:ind,nil];
		
		
	}
	else
	{
		if([mFiltersSelected indexOfObject:ind]==NSNotFound)
		{
			
			[mFiltersSelected addObject:ind];
		}
		else {
			[mFiltersSelected removeObject:ind];
		}
		
	}
	[mDelegate setFiltersSelectedArray:mFiltersSelected];
	[mTableView reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


@end
