//
//  OStoresImporter.h
//  Starbucks
//
//  Created by Adrián Caramés Ramos on 22/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBaseImporter.h"


@interface OStoresImporter : OBaseImporter
{
    NSMutableDictionary *mCurrentStoreDict;
    NSMutableDictionary *mCurrentServiceDict;
    NSMutableDictionary *mCurrentScheduleDict;
    
    NSMutableArray *mStoreServicesArray;
    NSMutableArray *mStoreSchedulesArray;

    BOOL mIsService;
    BOOL mIsSchedule;
}

- (void) insertIntoContext:(NSDictionary *)_item;

@end
