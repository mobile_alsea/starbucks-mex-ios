//
//  OFiltersCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OFiltersViewController.h"
@interface OFiltersCell : UITableViewCell
{

	UILabel * mFilterNameLabel;
    UIButton * mCheckButton;
    bool mIsActive;
    int mIndex;
	OFiltersViewController * mDelegate;

}

- (IBAction) RadioButtonClick:(id)sender;

@property(nonatomic,assign) bool isActive;
@property(nonatomic,assign) int index;
@property(nonatomic,strong) IBOutlet UIButton * checkButton;
@property(nonatomic,strong) IBOutlet UILabel * filterNameLabel;
@property(nonatomic,strong) OFiltersViewController * delegate;




@end
