//
//  OFoodCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OFood.h"
#import "DYImageView.h"
@interface OFoodCell : UITableViewCell
{
	DYImageView * mThumbnailImageView;
	UIButton * mAvailableButton;
	UILabel * mTitleLabel;
	UILabel * mCategoryLabel;
	BOOL mIndexPathPair;
	int mIndex;

}

@property(nonatomic, strong) IBOutlet DYImageView * thumbnailImageView;
@property(nonatomic, strong) IBOutlet UIButton * avalaibleButton;
@property(nonatomic, strong) IBOutlet UILabel * titleLabel;
@property(nonatomic, strong) IBOutlet UILabel * categoryLabel;
@property(nonatomic, strong) IBOutlet UILabel * nickNameLabel;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;


- (void)updateSchedule:(int)_state;
@end
