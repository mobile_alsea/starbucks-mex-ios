//
//  OMyFavoritesDetailViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyFavoritesDetailViewController.h"
#import "OCoffee.h"
#import "OCoffeeCell.h"
#import "OFood.h"
#import "OFoodCell.h"
#import "OStoresCell.h"
#import "OService.h"
#import "OSchedule.h"
#import "OStore.h"
#import "OStore+Extras.h"
#import "OCoffeeDetailViewController.h"
#import "OFoodDetailViewController.h"
#import "OStoresDetailViewController.h"
#import "OMyFavorites.h"
#import "OMyFavorites+Extras.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OMyFavorites.h"
#import "OMyfavorites+Extras.h"
#import "OCoffee.h"
#import "OCoffee+Extras.h"
#import "OFood.h"
#import "OFood+Extras.h"
#import "OStore.h"
#import "OStore+Extras.h"
#import "OMyFavoritesViewController.h"

@interface OMyFavoritesDetailViewController ()

<OStoresDetailViewControllerDelegate>

@end

@implementation OMyFavoritesDetailViewController

@synthesize type = mType;
@synthesize favoritesArray = mFavoritesArray;
@synthesize mapView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/initWithNibName");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	
	
	switch (mType) 
	{
		case 0:
			[self.navigationItem setTitle:NSLocalizedString(@"Mis Tiendas", nil)];
			break;
		case 1:
			[self.navigationItem setTitle:NSLocalizedString(@"Mis Cafés", nil)];
			break;
		case 2:
			[self.navigationItem setTitle:NSLocalizedString(@"Mis Alimentos", nil)];
			break;
		default:
			break;
	}
	
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Editar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnEditSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
	
	}
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/viewDidLoad");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }

}



-(void) viewWillAppear:(BOOL)animated
{
    @try{
	[super viewWillAppear:animated];
	
	if(mFavoritesArray!=nil)
	{
		mFavoritesArray = nil;
	
	}
	
	
	switch (mType) {
		case 0:
		{
			mFavoritesActualized = [OMyFavorites favoritesWithType:@"1" context:mAddingContext];
			if(mFavoritesActualized!=nil && [mFavoritesActualized count]>0)
			{
				mFavoritesArray = [OStore storesByNames:mFavoritesActualized inContext:mAddingContext];
			}
			
		}
			break;
		case 1:
		{
			mFavoritesActualized = [OMyFavorites favoritesWithType:@"2" context:mAddingContext];
			if(mFavoritesActualized!=nil && [mFavoritesActualized count]>0)
			{
				mFavoritesArray = [OCoffee coffeesByNames:mFavoritesActualized inContext:mAddingContext];
			}
			
		}
			break;
			
		case 2:
		{
			mFavoritesActualized = [OMyFavorites favoritesWithType:@"3" context:mAddingContext];
			if(mFavoritesActualized!=nil && [mFavoritesActualized count]>0)
			{
				mFavoritesArray = [OFood foodsByNames:mFavoritesActualized inContext:mAddingContext];
			}
			
		}
			break;
		default:
			break;
	}
	
	[mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/viewWillAppear");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
	
}
- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/didFinishLoadImage");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}







- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL) isOpenInStore:(OStore *)_store
{
    @try{
	BOOL ret = NO;
	
	NSDate *today = [NSDate date];
	NSDateFormatter *dformatter = [[NSDateFormatter alloc] init];
	
	[dformatter setDateFormat:@"e"];
	NSInteger weekDay = [[dformatter stringFromDate:today] integerValue];
	if(weekDay == 0) // DOMINGO
		weekDay = 6;
	else
		--weekDay;
	
	
	if([_store.schedulesArray count] > weekDay)
	{
		OSchedule *schedule = [_store.schedulesArray objectAtIndex:weekDay];
		
		NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
		[timeFormatter setDateFormat:@"HH:mm"];
		
		NSDate *timeToday = [timeFormatter dateFromString:[timeFormatter stringFromDate:today]];
		NSDate *timeStar = [timeFormatter dateFromString:schedule.startTime];
		NSDate *timeEnd = [timeFormatter dateFromString:schedule.endTime];
		
		if([timeToday laterDate:timeStar] && [timeToday earlierDate:timeEnd])
		{
			ret = YES;
		}
	}
	
	return ret;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/isOpeninStore");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}


- (void) configureCell:(id)_cell atIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	switch (mType) 
	{
		case 0:
		{
			OStoresCell * cell = (OStoresCell*) _cell;
			OStore *store = (OStore*)[mFavoritesArray objectAtIndex:_indexPath.row];
			
			BOOL isOpen = [self isOpenInStore:store];
			[cell updateSchedule:isOpen];
			
			cell.scheduleButton.tag = _indexPath.row;
			if([[cell.scheduleButton actionsForTarget:self forControlEvent:UIControlEventTouchUpInside] count] <= 0)
				[cell.scheduleButton addTarget:self action:@selector(actionScheduleClick:) forControlEvents:UIControlEventTouchUpInside];
			
			cell.nameLabel.text = store.name;
			cell.addressLabel.text = store.address;
			
			cell.driveView.alpha = 0.3;
			cell.mobileView.alpha = 0.3;
			cell.reserveView.alpha = 0.3;
			cell.wirelessView.alpha = 0.3;
			cell.warmedView.alpha = 0.3;
			for(OService *s in store.services)
			{
				if([s.name hasPrefix:@"Drive"])
					cell.driveView.alpha = 1.0;
				
				else if([s.name hasPrefix:@"Pago"])
					cell.mobileView.alpha = 1.0;
				
				else if([s.name hasSuffix:@"Reserve"])
					cell.reserveView.alpha = 1.0;
				
				else if([s.name hasPrefix:@"Wireless"])
					cell.wirelessView.alpha = 1.0;
				
				else if([s.name hasSuffix:@"Horno"])
					cell.warmedView.alpha = 1.0;
			}

		}
			
			break;
		case 2:
		{
			OFoodCell * cell = (OFoodCell*) _cell;
			OFood *food = (OFood*)[mFavoritesArray objectAtIndex:_indexPath.row];
			
			cell.titleLabel.text = food.name;
			cell.thumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-alimentos-low.png"];
			UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
			[cell.avalaibleButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
			[cell.avalaibleButton setTitle:food.availability forState:UIControlStateNormal];
			
			OImagesDownloaded * image = [OImagesDownloaded imageWithName:food.image context:mAddingContext];
			if(image==nil || image.image==nil)
			{
				[cell.thumbnailImageView setUrl:[NSURL URLWithString:food.image]];
			}
			else
			{
				[cell.thumbnailImageView setImage:[UIImage imageWithData:image.image]];
			}
			
			
		
		}
			break;
		case 1:
		{
			OCoffeeCell * cell = (OCoffeeCell*)_cell;
			OCoffee *coffee = (OCoffee*)[mFavoritesArray objectAtIndex:_indexPath.row];
			
			cell.titleLabel.text = coffee.name;
			
			
			if([[coffee.flavors allObjects] count]>0)
			{
				
				NSString * flavors = [[[coffee.flavors allObjects] objectAtIndex:0] name];
				for(int i=1;i<[[coffee.flavors allObjects] count];i++)
				{
					if(i==[[coffee.flavors allObjects] count]-1) flavors = [flavors stringByAppendingString:@" y "];
					else flavors = [flavors stringByAppendingString:@", "];
					flavors = [flavors stringByAppendingString:[[[coffee.flavors allObjects] objectAtIndex:i] name]];
					
				}
				cell.descriptionLabel.text = flavors;
			}

		
		
		}
			break;
		default:
			break;
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/configureCell");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }

}

- (void)btnEditSelected:(id)_sender
{
    @try{
    [mTableView setEditing:YES animated:YES];
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Hecho", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnDoneSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/btnEditselect");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
	
}

- (void)btnDoneSelected:(id)_sender
{
    @try{
	[mTableView setEditing:NO animated:YES];
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Editar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnEditSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/btnDoneSelect");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
	
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	return @"Eliminar";
	
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    @try{
         int TotalRows;
        if(mAppDelegate.window.frame.size.height== 480)
        {
            TotalRows=5;
        }
        else if(mAppDelegate.window.frame.size.height== 568)
        {
            TotalRows=6;
        }
        else if(mAppDelegate.window.frame.size.height== 1024)
        {
            TotalRows=13;
        }
        
	switch (mType) {
		case 0:
			if([mFavoritesArray count]<=TotalRows)
			{
				_tableView.frame = CGRectMake(10, 10, 300, [mFavoritesArray count]*70);
			}
			break;
		case 1:
			if([mFavoritesArray count]<=TotalRows+4)
			{
			_tableView.frame = CGRectMake(10, 10, 300, [mFavoritesArray count]*44);
			}
			break;
		case 2:
           
           
			if([mFavoritesArray count]<=TotalRows)
			{
                
              
				_tableView.frame = CGRectMake(10, 10, 300, [mFavoritesArray count]*65);
			}
			break;
			
		default:
			break;
	}
	
	
	
    if(mFavoritesArray==nil) return 0;
	else return [mFavoritesArray count];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/tableview");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}

-(CGFloat) tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)_indexPath
{
	switch (mType) {
	case 0:
			return 70;
			
		break;
		case 1:
			return 44;
			
		break;
		case 2:
			return 65;
			
		break;
		
	default:
		break;
	}
	
    return 65;
	
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	
	switch (mType) {
		case 0:
		{
			static NSString *CellIdentifier = @"OStoresCell";
			
			OStoresCell *cell = (OStoresCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
			
			}
			
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];    
			[self configureCell:cell atIndexPath:_indexPath];
			
			return cell;

		
		
		}
			
			break;
		case 1:
		{
			static NSString *CellIdentifier = @"OCoffeeCell";
			
			OCoffeeCell *cell = (OCoffeeCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
				
			}
			
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			[self configureCell:cell atIndexPath:_indexPath];
			
			return cell;

		
		}
			break;
		case 2:
		{
		
		
			static NSString *CellIdentifier = @"OFoodCell";
			
			OFoodCell *cell = (OFoodCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
			}
			
			// Setting Cell
			cell.index = _indexPath.row;
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			[cell updateSchedule:0];
			[self configureCell:cell atIndexPath:_indexPath];
			
			return cell;

		
		
		}
			
			break;
		default:
			break;
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/tableView");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
	
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
	switch (mType) {
		case 0:
		{
			OStore *store =(OStore*) [mFavoritesArray objectAtIndex:_indexPath.row];
			
			OStoresDetailViewController *detailViewController = [[OStoresDetailViewController alloc] initWithNibName:@"OStoresDetailViewController" bundle:nil];
			[detailViewController setStoreSelected:store];
			detailViewController.delegate = self;
			
			[self.navigationController pushViewController:detailViewController animated:YES];
		
		}
			break;
		case 1:
		{
			OCoffee *coffee =(OCoffee*) [mFavoritesArray objectAtIndex:_indexPath.row];
			OCoffeeDetailViewController *detailViewController = [[OCoffeeDetailViewController alloc] initWithNibName:@"OCoffeeDetailViewController" bundle:nil];
			;
			
			
			[detailViewController setCoffeeSelected:coffee];
			detailViewController.delegate = self;
			[self.navigationController pushViewController:detailViewController animated:YES];
		}

			break;
		case 2:
		{
			OFood *food = (OFood*) [mFavoritesArray objectAtIndex:_indexPath.row];
			
			OFoodDetailViewController *detailViewController = [[OFoodDetailViewController alloc] initWithNibName:@"OFoodDetailViewController" bundle:nil];
			[detailViewController setFoodSelected:food];
			detailViewController.delegate = self;	
			
			[self.navigationController pushViewController:detailViewController animated:YES];
		
		}
			break;
			
		default:
			break;
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/tableview");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	//TODO: cambiar de orden en el arrayList de objetos el from y ponerlo en el to.
	[mFavoritesArray exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
	
	
	
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	@try{
	if (editingStyle == UITableViewCellEditingStyleDelete) 
	{
//      Delete array when does not exist!
		[tableView beginUpdates];
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
		 withRowAnimation:UITableViewRowAnimationFade];
		OMyFavorites * mFavorite =[OMyFavorites favoriteWithName:((OMyFavorites*)[mFavoritesArray objectAtIndex:indexPath.row]).name context:mAddingContext];
		[mAddingContext deleteObject:mFavorite];
		[mAddingContext save:nil];
		[mFavoritesArray removeObjectAtIndex:indexPath.row];
		
		[tableView endUpdates];
		[tableView reloadData];
		
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Favorites/MyfavoritesViewController/tableView");
    
    
    OMyFavoritesViewController  *regresa =[[OMyFavoritesViewController  alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:regresa animated:YES];
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
	
}

#pragma mark - OStoresDetailViewControllerDelegate Methods

- (CLLocation *)storesDetailControllerRequestLocation:(OStoresDetailViewController *)storesDetailController
{
	return mMapView.userLocation.location;
}



@end
