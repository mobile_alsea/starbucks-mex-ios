//
//  OStoresPersonDetailViewController.m
//  Starbucks
//
//  Created by Isabelo Pamies López on 30/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresPersonDetailViewController.h"
#import <DYWidgets/DYLabel/DYVerticalAligmentLabel.h>
#import "OStore+Extras.h"
#import "OPersonalCell.h"

@interface OStoresPersonDetailViewController ()

@end

#define RGBToFloat(f) (f/255.0)

@implementation OStoresPersonDetailViewController

@synthesize nameLabel = mNameLabel;
@synthesize addressLabel = mAddressLabel;
@synthesize photoView = mPhotoView;
@synthesize emailLabel = mEmailLabel;
@synthesize phoneLabel = mPhoneLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mPerson = nil;
		mStore = nil;
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresPersonDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) dealloc
{
	if(mPerson != nil)
		CFRelease(mPerson);
	
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	mInternalCount=0;
	mAddressLabel.text = mStore.address;
	NSString *firstName = (NSString *) CFBridgingRelease(ABRecordCopyValue(mPerson, kABPersonFirstNameProperty));
	if(firstName==nil) firstName=@"";
	NSString *lastName = (NSString *) CFBridgingRelease(ABRecordCopyValue(mPerson, kABPersonLastNameProperty));
	if(lastName==nil)lastName=@"";
	self.title = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
	mNameLabel.text = [NSString stringWithFormat:@"Invita a %@ %@ a", firstName, lastName];
	
	ABMultiValueRef phonesValues = (ABMultiValueRef) ABRecordCopyValue(mPerson, kABPersonPhoneProperty);
	mNumberOfPhones = ABMultiValueGetCount(phonesValues);
	if(ABMultiValueGetCount(phonesValues) > 0)
		mNumberPhone = (NSString *) CFBridgingRelease(ABMultiValueCopyValueAtIndex(phonesValues, 0));
	
	ABMultiValueRef emailsValues = (ABMultiValueRef) ABRecordCopyValue(mPerson, kABPersonEmailProperty);
	if(ABMultiValueGetCount(emailsValues) > 0)
		mEmailLabel.text = (NSString *) CFBridgingRelease(ABMultiValueCopyValueAtIndex(emailsValues, 0));
	mNumberOfMails = ABMultiValueGetCount(emailsValues);
	NSData *photoData = (NSData *) CFBridgingRelease(ABPersonCopyImageData(mPerson));
	if(photoData != nil)
		mPhotoView.image = [UIImage imageWithData:photoData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
	self.nameLabel = nil;
	self.addressLabel = nil;
	self.photoView = nil;
	self.emailLabel = nil;
	self.phoneLabel = nil;
}

- (void) setStore:(OStore *)_store
{
	mStore = _store;
	
}

- (void) setPerson:(ABRecordRef) person
{
	mPerson = CFRetain(person);
}

- (void)sendActionButtonPressed:(id)sender withType:(int) type andIndex:(int)index;
{

	switch (type) 
	{
		case 0:
			[self actionCallPhone:index];
			break;
		case 1:
			[self actionSendSMS:index];
			break;
		case 2:
			[self actionSendMail:index];
			break;
		default:
			break;
	}

}


#pragma mark IBAction Methods


- (IBAction)actionSendMail:(int) index
{
    @try{
	OPersonalCell * cell  = (OPersonalCell*)[mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
	
	
	MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
	mailController.mailComposeDelegate = self;
	[mailController.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
	[mailController setSubject: @"Starbucks"];
	[mailController setToRecipients:[NSArray arrayWithObject:cell.textLabel.text]];
	
	[mailController setMessageBody:[NSString stringWithFormat:@"Hola %@, te invito a venir al Starbucks de %@ ¡Acompáñame!", self.title, mAddressLabel.text] isHTML:NO];
	
	[self presentModalViewController:mailController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (IBAction)actionSendSMS: (int) index
{
    @try{
	OPersonalCell * cell  = (OPersonalCell*)[mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
	
	NSURL *urlPhone = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", [cell.textLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""]]];
	if([[UIApplication sharedApplication] canOpenURL:urlPhone])
	{
		MFMessageComposeViewController *sms_controller = [[MFMessageComposeViewController alloc] init];
		sms_controller.messageComposeDelegate = self;
		[sms_controller.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
		sms_controller.recipients = [NSArray arrayWithObject:[cell.textLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
		sms_controller.body = [NSString stringWithFormat:@"Hola %@, te invito al Starbucks de %@", self.title, mAddressLabel.text];
		
		[self presentModalViewController:sms_controller animated:YES];
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (IBAction)actionCallPhone: (int) index
{
    @try{
	OPersonalCell * cell  = (OPersonalCell*)[mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
	
	NSURL *urlPhone = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", [cell.textLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""]]];
	if([[UIApplication sharedApplication] canOpenURL:urlPhone])
	{
		[[UIApplication sharedApplication] openURL:urlPhone];
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

@end

#pragma mark - Message Compose Delegate Methods
@implementation OStoresPersonDetailViewController (MessageComposeDelegateMethods)

- (void) closeModalView
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissModalViewControllerAnimated:YES];
}

@end

#pragma mark - Mail Compose Delegate Methods
@implementation OStoresPersonDetailViewController (MailComposeDelegateMethods)

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - UITableView Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
	return 1;
	
	
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
	@try{
	int mrows = mNumberOfMails + mNumberOfPhones +mNumberOfPhones;
	
	if(mrows<5)
	{
		mTableView.frame = CGRectMake(0, 44, 300, mrows*44);
	
	}
	else 
	{
		mTableView.frame = CGRectMake(0, 44, 300, 200);
	}
   
	return mrows;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	static NSString *CellIdentifier = @"OPersonalCell";
    
    OPersonalCell *cell = (OPersonalCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
		
    }
    cell.delegate = self;
	cell.index = _indexPath.row;
	
	if(_indexPath.row<mNumberOfPhones)
	{
		ABMultiValueRef phonesValues = (ABMultiValueRef) ABRecordCopyValue(mPerson, kABPersonPhoneProperty);
		cell.textLabel.text=(NSString *) CFBridgingRelease(ABMultiValueCopyValueAtIndex(phonesValues, _indexPath.row));
		[cell.serviceButton setImage:[UIImage imageNamed:@"Call.png"] forState:UIControlStateNormal];
		[cell.serviceButton setTitle:@"Movil" forState:UIControlStateNormal];
		cell.type=0;
		
	}
	else if (_indexPath.row<mNumberOfPhones+mNumberOfPhones) 
	{
		ABMultiValueRef phonesValues = (ABMultiValueRef) ABRecordCopyValue(mPerson, kABPersonPhoneProperty);
		cell.textLabel.text=(NSString *) CFBridgingRelease(ABMultiValueCopyValueAtIndex(phonesValues, _indexPath.row-mNumberOfPhones));
		[cell.serviceButton setTitle:@"SMS" forState:UIControlStateNormal];
		[cell.serviceButton setImage:[UIImage imageNamed:@"SMS.png"] forState:UIControlStateNormal];
		cell.type=1;
		
	}
	else if (_indexPath.row<mNumberOfPhones+mNumberOfPhones+mNumberOfMails) 
	{
		ABMultiValueRef emailsValues = (ABMultiValueRef) ABRecordCopyValue(mPerson, kABPersonEmailProperty);
		cell.textLabel.text=(NSString *) CFBridgingRelease(ABMultiValueCopyValueAtIndex(emailsValues, _indexPath.row-mNumberOfPhones-mNumberOfPhones));
		[cell.serviceButton setImage:[UIImage imageNamed:@"Email.png"] forState:UIControlStateNormal];
		[cell.serviceButton setTitle:@"Email" forState:UIControlStateNormal];
		cell.type=2;
		
	}
    
    return cell;
	return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	
	
}





@end
