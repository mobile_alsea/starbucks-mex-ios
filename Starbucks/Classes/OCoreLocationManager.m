//
//  OCoreLocationManager.m
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCoreLocationManager.h"

@interface OCoreLocationManager (PrivateMethods)
@end

@interface OCoreLocationManager (LocationManagerDelegateMethods) <CLLocationManagerDelegate>
@end

@implementation OCoreLocationManager

- (void) dealloc
{
	[mLocationManager stopUpdatingLocation];
	mLocationManager.delegate = nil;
}

- (id) init
{
    @try{
	self = [super init];
	if(self != nil)
	{
		mLocationManager = [[CLLocationManager alloc] init];
		mLocationManager.delegate = self;
		mLocationManager.distanceFilter = kCLDistanceFilterNone;
		mLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
	}
	
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OCoreLocationManager.m/init");
    }
}

- (void) startLocation
{
	[mLocationManager startUpdatingLocation];
}

- (void) stopLocation
{
	[mLocationManager stopUpdatingLocation];
}

+ (void) startLocation
{
    @try{
	OCoreLocationManager *locationManager = [OCoreLocationManager sharedInstance];
	[locationManager startLocation];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OCoreLocationManager.m/starLocation");
    }
}

+ (void) stopLocation
{
    @try{
	OCoreLocationManager *locationManager = [OCoreLocationManager sharedInstance];
	[locationManager stopLocation];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OCoreLocationManager.m/stopLocation");
    }
}

@end


#pragma mark - Location Manager Delegate Methods
@implementation OCoreLocationManager (LocationManagerDelegateMethods)

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    @try{
	[[NSNotificationCenter defaultCenter] postNotificationName:kLocationUpdatedNotification object:newLocation];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OCoreLocationManager.m/locationManager");
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"Error Location Updating Manager %@", error.description);
}

@end
