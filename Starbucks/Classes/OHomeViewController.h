//
//  OHomeViewController.h
//  Starbucks
//
//  Created by Adrián Caramés on 08/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"


@class OAppDelegate;

@interface OHomeViewController : OBaseViewController  <UIScrollViewDelegate>
{
    OAppDelegate *mAppDelegate;
    
    NSMutableArray *mSectionsArray;
    NSMutableArray *mSectionsButtonsArray;
    
    UIView *mLoadingView;
    
    int mImportersCounter;
	
	UIView		*mUnreachableView;
	
	UIBarButtonItem *mRefreshBtn;
    UIBarButtonItem *btnFilter;
	
	UIButton * mFavoritesButton;
	UIButton * mMyDrinksButton;
	UIButton * mCoffeeButton;
	UIButton * mFoodButton;
	UIButton * mStoresButton;
	UIButton * mConfigurationButton;
	UIButton * mRewardsButton;
	UIButton * mDrinksButton;
    
    UIButton * mCardButton;
	
	UIPanGestureRecognizer * mFavoritesGestureRecognizer;
	UIPanGestureRecognizer * mMyDrinksGestureRecognizer;
	UIPanGestureRecognizer * mCoffeeGestureRecognizer;
	UIPanGestureRecognizer * mFoodGestureRecognizer;
	UIPanGestureRecognizer * mStoresGestureRecognizer;
	UIPanGestureRecognizer * mConfigurationGestureRecognizer;
	UIPanGestureRecognizer * mRewardsGestureRecognizer;
	UIPanGestureRecognizer * mDrinksGestureRecognizer;
    
    UIPanGestureRecognizer * mCardGestureRecognizer;
	
	NSMutableArray * mButtonsArray;
	NSMutableArray * mGestureRecognizersArray;
    UINavigationController *SectionnavController;
	
	BOOL mIsEditing;
  
	
	int mNewPosition;
    int xTabBar;
    int yTabBar;
    int HeightTabBar;
    int WidthTabBar;
    int ScrollSize;
    int ScrollContentSize;
    int yEditTabBar;
    
	NSTimer *mEditTimer;
}

@property (strong, nonatomic) IBOutlet UIImageView *mGreenLineTabBar;
@property (strong, nonatomic) IBOutlet UIImageView *mBackGroundTabBar;

@property (strong, nonatomic) IBOutlet UIImageView *RightBar;
@property (strong, nonatomic) IBOutlet UIImageView *LeftBar;

@property (nonatomic, strong) IBOutlet UIView *loadingView;
@property (nonatomic, strong) IBOutlet UIView *unreachableView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem	*refreshBtn;
@property (nonatomic,strong) IBOutlet UIBarButtonItem *btnFilter;
@property (nonatomic,strong) IBOutlet UINavigationController *SectionnavController;

@property (strong, nonatomic) NSDictionary *controllers;

- (IBAction) actionRefreshClick;

@end
