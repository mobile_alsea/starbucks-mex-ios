//
//  OFacebookViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFacebookViewController.h"
#import "OShareFacebook.h"
@interface OFacebookViewController ()

@end

@implementation OFacebookViewController
@synthesize drinkSelected = mDrinkSelected;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideModal:) name:@"Starbucks-SOCIAL-SEND" object:nil];
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
               UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:NSLocalizedString(@"facebook", nil)];
	UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Post", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(postToFacebook:)];
    [self.navigationItem setRightBarButtonItem:btnSave];
	
	mFavoriteNameLabel.text = mDrinkSelected.nickName;
	mDrinkNameLabel.text = mDrinkSelected.name;
	
	UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cerrar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(hideModal:)];
    [self.navigationItem setLeftBarButtonItem:btnClose];

	UIImageView * mImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navbar_facebook.png"]];
	[self.navigationItem setTitleView:mImageView];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)hideModal:(id)sender
{

	[self dismissModalViewControllerAnimated:YES];

}

-(IBAction)postToFacebook:(id)sender
{
	
	[self sendByFacebook];
	
}


- (void) sendByFacebook
{
	SHKItem *item = [self itemToShare];	
	if(item!=nil)
		[OShareFacebook shareItem:item];

}



- (SHKItem *) itemToShare
{
    @try{
	SHKItem *item = nil;
	
    //MBT 10Dic2012
	//NSString * sended = [NSString stringWithFormat:@"%@. Acabo de guardar mi bebida %@ de Starbucks Coffee mediante su aplicación móvil http://goo.gl/r2WGv",mTextView.text,mDrinkSelected.name];
    NSString * sended = [NSString stringWithFormat:@"%@. Acabo de guardar mi bebida %@ de Starbucks Coffee mediante su aplicación móvil.",mTextView.text,mDrinkSelected.name];
	
	item = [SHKItem URL:[NSURL URLWithString:@"http://www.google.es"] title:mDrinkSelected.name imgURL:mDrinkSelected.image imgHref:mDrinkSelected.image ];
	item.text = sended;
	item.title = mDrinkSelected.name;
	item.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mDrinkSelected.image]]];
	item.imgHref = @"http://www.starbucks.com.mx/starbucks/online-community/mobileapp";
	
	[item setCustomValue:item.text forKey:@"status"];
	[item setCustomValue:item.title forKey:@"title"];
	
	
	return item;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}




- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	
	return YES;
	
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end

#pragma mark - FBSession Delegate
@implementation OFacebookViewController (FBSessionDelegate)

- (void)fbDidLogin
{
    @try{
    NSMutableDictionary *facebook = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"Starbucks-Facebook"]];
    if(facebook != nil)
    {
        facebook = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    [facebook setObject:[NSNumber numberWithBool:YES] forKey:@"is_config"];
	
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:facebook forKey:@"Starbucks-Facebook"];
    [defaults setObject:mFacebook.accessToken forKey:@"FBAccessTokenKey"];
    [defaults setObject:mFacebook.expirationDate forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
}

- (void)fbDidExtendToken:(NSString*)accessToken expiresAt:(NSDate*)expiresAt
{
    @try{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessToken forKey:@"FBAccessTokenKey"];
    [defaults setObject:expiresAt forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)fbDidLogout
{
    @try{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)fbSessionInvalidated
{
}

@end

@implementation OFacebookViewController (FBRequestDelegate)

- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response
{
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
	NSLog(@"Request failed! Error: %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SOCIAL-SEND" object:nil];
}

- (void)request:(FBRequest *)request didLoad:(id)result
{
	NSLog(@"Request success!");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SOCIAL-SEND" object:nil];
    
}

@end


