//
//  OMydrinksViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"

@interface OMydrinksViewController : OBaseViewController<UITableViewDataSource, UITableViewDelegate,NSFetchedResultsControllerDelegate>
{
	UITableView * mTableView;
	UIButton * mFavoritesButton;
	UIButton * mFriendsButton;
	UIImageView * mSegmentedBackgroundImageView;
	NSFetchedResultsController *mFetchedResultsController;
	NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	int mTypeOrderSelected;

}
@property(nonatomic, strong) IBOutlet UITableView * tableView;
@property(nonatomic, strong) IBOutlet UIButton * favoritesButton;
@property(nonatomic, strong) IBOutlet UIButton * friendsButton;
@property(nonatomic, strong) IBOutlet UIImageView * segmentedBackgroundImageView;

-(IBAction)btnFavoritesSelected:(id)sender;
-(IBAction)btnFriendsSelected:(id)sender;



@end
