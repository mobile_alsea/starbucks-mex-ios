//
//  OStoresFiltersViewController.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresFiltersViewController.h"
#import "OStoresServicesCell.h"
#import "OService.h"
#import "OService+Extras.h"
#import "OAppDelegate.h"


@interface OStoresFiltersViewController ()

@end


#define RGBToFloat(f) (f/255.0)

@implementation OStoresFiltersViewController


#pragma mark - Constructors

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - Properties

@synthesize delegate = mDelegate;
//@synthesize navigationItem = mNavigationItem;
@synthesize tableView = mTableView;

- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	
    NSFetchRequest *fetchRequest = [OService servicesInContextFetchRequest:[OAppDelegate managedObjectContext]];
    
	// Create the sort descriptors array.
//	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
//	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor, nil];
//	[fetchRequest setSortDescriptors:sortDescriptors];
	
	// Create the predicate
    //    if (mPredicate != nil)
    //        [fetchRequest setPredicate:mPredicate];
	
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
//    mFetchedResultsController.delegate = self;
	
//	[fetchRequest release];
//	[sellerDescriptor release];
//	[sortDescriptors release];
	NSLog(@"SERVICIOS: %d",[[mFetchedResultsController fetchedObjects]count]);
	
	return mFetchedResultsController;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - ViewController Methods

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    
    /*
    // Setting NavigationBar
    [mNavigationItem setTitle:NSLocalizedString(@"Filtrar por", nil)];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Finalizar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnDoneSelected:)];
    [mNavigationItem setRightBarButtonItem:btnDone];
    */
        [self.navigationItem setTitle:NSLocalizedString(@"Filtrar por", nil)];
        UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Finalizar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnDoneSelected:)];
        [self.navigationItem setRightBarButtonItem:btnSave];
        
        /*
        UIImageView * mImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navbar_facebook.png"]];
        [self.navigationItem setTitleView:mImageView];
        */

    
    [self executeFetch];
        
        
        
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Private Methods

- (void)btnDoneSelected:(id)_sender
{
    if ([self.delegate respondsToSelector:@selector(closeFiltersViewController:)])
        [self.delegate closeFiltersViewController:self];
    
//    [self dismissModalViewControllerAnimated:YES];
//	mDelegate.isActualizeView=NO;
//    [mDelegate executeFetch];
}

- (void) configureCell:(OStoresServicesCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    OService *service = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    
    _cell.titleLabel.text = service.name;
    [_cell.iconImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Amenity%@.png",[service.name stringByReplacingOccurrencesOfString:@" " withString:@""]]]];
    
    if ([service.isSelected boolValue])
        [mTableView selectRowAtIndexPath:_indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    else
        [mTableView deselectRowAtIndexPath:_indexPath animated:NO];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) executeFetch
{
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error])
	{
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    @try{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_section];
	NSLog(@"FILAS:%d", [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OStoresServicesCell";
    
    OStoresServicesCell *cell = (OStoresServicesCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];

    }
    
    // Setting Cell
    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
    [self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    OService *service = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    service.isSelected = [NSNumber numberWithBool:YES];
}

- (void)tableView:(UITableView *)_tableView didDeselectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    OService *service = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    service.isSelected = [NSNumber numberWithBool:NO];
}

-(CGFloat)tableView:(UITableView *)_tableView heightForHeaderInSection:(NSInteger)_section
{
    return 10;
}

-(UIView *)tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)_section
{
    @try{
    UIView *headerView = [[UIView alloc] init];
    [headerView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(30.0) green:RGBToFloat(30.0) blue:RGBToFloat(30.0) alpha:1.0]];
    [headerView setFrame:CGRectMake(0, 0, 300, 10)];
    return headerView;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - NSFetchedResultsController Protocol

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [mTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    @try{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [mTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [mTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    @try{
    UITableView *tableView = mTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //            [self configureCell:(OStoresCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresFiltersViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [mTableView endUpdates];
}


@end
