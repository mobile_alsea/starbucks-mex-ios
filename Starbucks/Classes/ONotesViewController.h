//
//  ONotesViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OAppDelegate.h"
#import "OSavedDrinkViewController.h"
#import "OBaseViewController.h"

@interface ONotesViewController : OBaseViewController<UITextFieldDelegate,UITextViewDelegate>
{

	IBOutlet UITextView * mTextView;
	IBOutlet UITextField * mTextField;
	UILabel * mUserLabel;
	OMyDrinks * mDrinkSelected;
		NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	OSavedDrinkViewController * mDelegate;
	
}

@property(nonatomic, strong) IBOutlet UILabel * userLabel;
@property (nonatomic, strong) OMyDrinks * drinkSelected;
@property (nonatomic, strong) OSavedDrinkViewController * delegate;
-(IBAction)addUserButtonPressed:(id)sender;

@end

@interface ONotesViewController (PeoplePickerNavigationControllerDelegateMethods) <ABPeoplePickerNavigationControllerDelegate>
@end