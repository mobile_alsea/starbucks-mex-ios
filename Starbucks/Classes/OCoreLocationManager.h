//
//  OCoreLocationManager.h
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "DYSingletonObject.h"
#import <MapKit/MapKit.h>

#define kLocationUpdatedNotification @"STARBUCKS-LOCATION-UPDATED"

@interface OCoreLocationManager : DYSingletonObject
{
	CLLocationManager * mLocationManager;
}

+ (void) startLocation;
+ (void) stopLocation;

- (void) startLocation;
- (void) stopLocation;

@end
