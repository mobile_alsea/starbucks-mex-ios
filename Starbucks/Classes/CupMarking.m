//
//  CupMarking.m
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "CupMarking.h"

@implementation CupMarking

@synthesize strikethrough;
@synthesize text;

- (id)initWithText:(NSString *)inText strikethrough:(BOOL)inStrikethrough {
    @try{
	self = [super init];
	if (!self) return nil;
	
	self.text = inText;
	self.strikethrough = inStrikethrough;
	
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Markings/CupMarking/initWithText");
    }
}


- (BOOL)isEqual:(id)object {
    @try{
	if (!object) return NO;
	if (![object isKindOfClass:[self class]]) return NO;
	
	CupMarking *cupMarking = (CupMarking *)object;
	return (self.strikethrough == cupMarking.strikethrough && [self.text isEqualToString:cupMarking.text]);
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Markings/CupMarking.m/isEqual");
    }
}

@end

