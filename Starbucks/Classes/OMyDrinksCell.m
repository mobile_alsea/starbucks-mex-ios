//
//  OMyDrinksCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyDrinksCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation OMyDrinksCell
@synthesize titleLabel = mTitleLabel;
@synthesize avalaibleButton = mAvailableButton;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize nickNameLabel = mNickNameLabel;
@synthesize index = mIndex;




- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Private methods


- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
	{
        [self.contentView setBackgroundColor:[UIColor whiteColor]];

	}
    else
	{
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];

	}
	
}

- (void)updateSchedule:(int)_state
{
    UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [mAvailableButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [mAvailableButton setTitle:NSLocalizedString(@"Verano", nil) forState:UIControlStateNormal];
}

@end
