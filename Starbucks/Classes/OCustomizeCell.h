//
//  OCustomizeCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 18/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCustomizeShotsViewController.h"
@interface OCustomizeCell : UITableViewCell
{

	UILabel * mNameLabel;
	UILabel * mIngredienteLabel;
    UIButton * mPlusButton;
	UIButton * mMinusButton;
    bool mIsActive;
    int mIndex;
	OCustomizeShotsViewController * mDelegate;
	BOOL mIndexPathPair;
	NSArray * mValuesArray;
	int mCount;
}



@property(nonatomic,assign) bool isActive;
@property(nonatomic,assign) int index;
@property(nonatomic,strong) IBOutlet UIButton * plusButton;
@property(nonatomic,strong) IBOutlet UIButton * minusButton;
@property(nonatomic,strong) IBOutlet UILabel * nameLabel;
@property(nonatomic,strong) IBOutlet UILabel * ingredientLabel;
@property(nonatomic,strong) NSArray * valuesArray;
@property(nonatomic,strong) OCustomizeShotsViewController * delegate;
@property(nonatomic, assign) BOOL indexPathPair;
@property(nonatomic, assign) int count;


- (IBAction) minusButtonClick:(id)sender;
- (IBAction) plusButtonClick:(id)sender;

@end
