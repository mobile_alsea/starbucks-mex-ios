//
//  OFoodImporter.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFoodImporter.h"
#import "OAppDelegate.h"
#import "OFood+Extras.h"
#import "OFood.h"
#import "OCombinations.h"
#import "OAlergenos.h"
@implementation OFoodImporter

- (NSData *) getData
{
    @try{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kFoodURL]];
	return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/getdata");
    }
    
}

- (void) didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDict
{
    @try{
    if ([_elementName isEqualToString:@"comidas"])
    {
        // Remove old stores
        NSArray *foods = [OFood foodInContext:mAddingContext];
        [mAppDelegate removeObjects:foods fromContext:mAddingContext];
        [mAddingContext processPendingChanges];
    }
	else if ([_elementName isEqualToString:@"comida"])
	{
		mCurrentFoodDict = [[NSMutableDictionary alloc] init];
        
        mIsAlergeno = NO;
        mIsCombination = NO;
	}
    else if ([_elementName isEqualToString:@"alergenos"])
    {
        mFoodAlergenoArray = [[NSMutableArray alloc] init];
        
        mIsAlergeno = YES;
    }
	else if ([_elementName isEqualToString:@"alergeno"]) {
		
		mCurrentAlergenoDict = [[NSMutableDictionary alloc] init];
	}
    else if ([_elementName isEqualToString:@"combinaciones"])
    {
        mFoodCombinationArray = [[NSMutableArray alloc] init];
        
        mIsCombination = YES;
    }
	else if ([_elementName isEqualToString:@"combinacion"])
    {
        mCurrentCombinationDict = [[NSMutableDictionary alloc] init];
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/didStartElement");
    }
}

- (void) didEndElement:(NSString *)_elementName withElementValue:(NSString *)_elementValue
{
    @try{
	if ([_elementName isEqualToString:@"comida"])
	{
        [self insertIntoContext:mCurrentFoodDict];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"nombre"])
	{
		if(mIsCombination)
			[mCurrentCombinationDict setObject:_elementValue forKey:@"name"];
		else
			[mCurrentFoodDict setObject:_elementValue forKey:@"name"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"descripcion"])
	{
		[mCurrentFoodDict setObject:_elementValue forKey:@"largedescrip"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"disponibilidad"])
	{
		if(mIsCombination)
			[mCurrentCombinationDict setObject:_elementValue forKey:@"availability"];
		else
			[mCurrentFoodDict setObject:_elementValue forKey:@"availability"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"descripcioncorta"])
	{
		[mCurrentFoodDict setObject:_elementValue forKey:@"shortdescrip"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"categoria"])
	{
		[mCurrentFoodDict setObject:_elementValue forKey:@"category"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"imagen"])
	{
		if(mIsCombination)
			[mCurrentCombinationDict setObject:_elementValue forKey:@"image"];
		else
			[mCurrentFoodDict setObject:_elementValue forKey:@"image"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"alergeno"])
	{
		[mCurrentAlergenoDict setObject:_elementValue forKey:@"name"];
        [mFoodAlergenoArray addObject:mCurrentAlergenoDict];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"combinacion"])
	{
		
        [mFoodCombinationArray addObject:mCurrentCombinationDict];
		
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/didendelement");
    }
}

#pragma mark - CoreData Methods

- (void) insertIntoContext:(NSDictionary *)_item
{
    @try{
    NSEntityDescription *foodEntity = [NSEntityDescription entityForName:@"Food" inManagedObjectContext:mAddingContext];
	
	OFood *food = [[OFood alloc] initWithEntity:foodEntity insertIntoManagedObjectContext:mAddingContext];
    
	food.name = [_item objectForKey:@"name"];
    food.shortdescrip = [_item objectForKey:@"shortdescrip"];
	food.availability = [_item objectForKey:@"availability"];
	food.largedescrip = [_item objectForKey:@"largedescrip"];
	food.image = [_item objectForKey:@"image"];
	food.category = [_item objectForKey:@"category"];
	
    
    for (NSDictionary *alergenoDict in mFoodAlergenoArray)
    {   
        NSEntityDescription *alergenosEntity = [NSEntityDescription entityForName:@"Alergenos" inManagedObjectContext:mAddingContext];
        OAlergenos * alergeno = [[OAlergenos alloc] initWithEntity:alergenosEntity insertIntoManagedObjectContext:mAddingContext];
        alergeno.name = [alergenoDict objectForKey:@"name"];
		
		[food addAlergenosObject:alergeno];
    }
	
	for (NSDictionary *combinationDict in mFoodCombinationArray)
    {   
        NSEntityDescription *combinationEntity = [NSEntityDescription entityForName:@"Combinations" inManagedObjectContext:mAddingContext];
        OCombinations * combination = [[OCombinations alloc] initWithEntity:combinationEntity insertIntoManagedObjectContext:mAddingContext];
        combination.name = [combinationDict objectForKey:@"name"];
		combination.image = [combinationDict objectForKey:@"image"];
		combination.available = [combinationDict objectForKey:@"availability"];
		
        
        [food addCombinationsObject:combination];
    }
    
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/insertintocontext");
    }
}


#pragma mark - Delegate Methods

- (void) importerDidFinishAllItems:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishAllItems:withData:withIdentifier:)])
		[mDelegate importerDidFinishAllItems:self withData:mAddingContext withIdentifier:@"OFoodImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/importerdidFinish");
    }
}

- (void) importerDidFinishItem:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishItem:withData:withIdentifier:)])
		[mDelegate importerDidFinishItem:self withData:mAddingContext withIdentifier:@"OFoodImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/importerDidFinish");
    }
}

- (void) importerDidFail:(NSError *)_error
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFail:withError:withIdentifier:)])
		[mDelegate importerDidFail:self withError:_error withIdentifier:@"OFoodImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/foodimporter/importerDidFail");
    }
}

@end

