//
//  OPersonalCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 17/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OStoresPersonDetailViewController.h"
@interface OPersonalCell : UITableViewCell
{
	UILabel * mTextLabel;
	UIButton * mServiceButton;
	int mType;
	OStoresPersonDetailViewController * mDelegate;
	int mIndex;

}

@property(nonatomic,strong) IBOutlet UILabel * textLabel;
@property(nonatomic, strong)IBOutlet UIButton * serviceButton;
@property(nonatomic, assign) int type;
@property(nonatomic, assign) int index;
@property(nonatomic, strong) OStoresPersonDetailViewController * delegate;


-(IBAction)sendActionButtonPressed:(id)sender;

@end
