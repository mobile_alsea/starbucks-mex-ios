//
//  OCustomizeShotsViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCustomizeShotsViewController.h"
#import "OPreparationCell.h"
#import "OCustomizeCell.h"
#import "DNScrollSlider.h"
#import "OAbbreviatures.h"
#import "OAbbreviatures+Extras.h"
#import "ONotesCustomizedViewController.h"
#import <DYCore/files/DYFileNameManager.h>
#import "CupMarking.h"
#import "OCustomizeDrinkViewController.h"

@interface OCustomizeShotsViewController ()

@end

@implementation OCustomizeShotsViewController
@synthesize drinkSelected = mDrinkSelected;
@synthesize drinkCustomize = mDrinkCustomize;
@synthesize sizeSelected = mSizeSelected;
@synthesize imageView;
@synthesize reverseAnimationImages;
@synthesize animationImages;
@synthesize imageView2;
@synthesize shotCountSlider;
@synthesize TemperatureSlider;
@synthesize  FoamSlider;

@synthesize textLabel = mTextLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mTypeOfHeaderSelected=-1;
		mIsDown = NO;
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
	UIViewController * prueba = [[UIViewController alloc] init];
	[prueba.view setHidden:YES];
	[self presentModalViewController:prueba animated:NO];
	[self dismissModalViewControllerAnimated:NO];
	
}

-(NSString *)UpdateCafeina :(NSString *)mShotscafeinaselected;
{
    
    int option=-1 ;
    
    if([mShotscafeinaselected isEqualToString:@"Regular"])
    {
        
        option =0;
    }
    else if ([mShotscafeinaselected isEqualToString:@"Descafeinado"])
    {
        option =1;
    }
    
    
    @try{
        switch (option)
        {
            case 0:
                return @"R";
                break;
            case 1:
                return @"D";
                break;
                
            default:
                return @"";

                break;
        }
        
        
        
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
    return @"";
    
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];

	if(mSavedAddinsArray!=nil)
	{
		mSavedAddinsArray=nil;
	}
	if(mSavedSyrupsArray!=nil)
	{
		mSavedSyrupsArray=nil;
	}
	mSavedSyrupsArray = [[NSMutableArray alloc] initWithCapacity:0];
	mSavedAddinsArray = [[NSMutableArray alloc] initWithCapacity:0];
	[imageView setImage:[UIImage imageNamed:@"1.png"]];
	
	
	
	
	
	NSString *pListPath = [DYFileNameManager pathForFile:@"Identifier.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
	NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
	
	NSString * ReadValue = [dictionary valueForKey:@"id"];

	mId = [ReadValue intValue];
	
	mId++;
	
	
	mCustomizeView.frame = CGRectMake(5, -415, 310, 415);
	
	
	if(mPreparationArray==nil) mPreparationArray = [[NSMutableArray alloc] initWithObjects:@"Preparado 1",@"Preparado 2",@"Preparado 3",@"Preparado 4",@"Preparado 5",@"Preparado 6",@"Preparado 7", nil];
	
	if(mToppingsArray==nil)mToppingsArray = [[NSMutableArray alloc] initWithObjects:@"Crema Batida",@"Mocha polvo",@"Canela",@"Caramelo",@"Cinnamon Dolce",@"Mocha jarabe", nil];
	
	if(mSyrupsArray==nil)mSyrupsArray = [[NSMutableArray alloc] initWithObjects:@"Vainilla",@"Caramelo",@"Cinnamon Dolce",@"Clásico",@"Avellana",@"Menta",@"Coco",@"Almendra",@"Frambuesa",@"Vainilla Sugar Free",@"Mocha",@"Mocha Blanco", nil];
	
	if(mAddinsArray==nil)mAddinsArray = [[NSMutableArray alloc] initWithObjects:@"Crema Batida",@"Espiral de Mocha",@"Espiral de Caramelo",@"Espiral de Cajeta",@"Más leche",@"Splenda",@"Canderel",@"Azúcar",@"Azúcar Mascabado",@"Agua",@"Hielo",@"Canela en polvo",@"Vainilla en polvo",@"Mocha en polvo",@"Chip", nil];
	
	if(mTeasArray==nil)mTeasArray = [[NSMutableArray alloc] initWithObjects:@"Te 1",@"Te 2",@"Te 3",@"Te 4",@"Te 5",@"Te 6", nil];
	
    if(mTemperatureArray==nil)mTemperatureArray= [[NSMutableArray alloc] initWithObjects:@"Tibia",@"Regular",@"Extra hot", nil];
	
    if(mFoamArray==nil)mFoamArray= [[NSMutableArray alloc] initWithObjects:@"Sin espuma",@"Poca espuma",@"Mucha espuma", nil];
        
	
	NSMutableArray *numbers = [NSMutableArray array];
	for (NSInteger i=0; i<=12; i++) 
	{
		[numbers addObject:[NSString stringWithFormat:@"%d", i]];
	}
    
        
        
        
        
	shotCountSlider.valueNames = numbers;
	shotCountSlider.valueIndex= 0;
	mNumberOfShotsLabel.text = @"0 Ninguno";
        
        
        //MBT 21-12-12
        
    TemperatureSlider.valueNames=mTemperatureArray;
         [TemperatureSlider setValueSeparation:80.f];
    TemperatureSlider.valueIndex=0;
       
        
    FoamSlider.valueNames=mFoamArray;

	 [FoamSlider setValueSeparation:110.f];
            FoamSlider.valueIndex=0;
        
	[self.navigationItem setTitle:NSLocalizedString(@"Personalizar", nil)];
        
    mTextLabel.text = mDrinkSelected.name;
	
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Guardar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnSaveSelected:)];
	[mHorizontalScrollView setContentSize:CGSizeMake(360, 0)];
	[mHorizontalScrollView addSubview:mScrollVieView];
	
	[mHorizontalTemperatureScrollView setContentSize:CGSizeMake(650, 0)];
	[mHorizontalTemperatureScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ScrollSliderBackground.png"]]];
	[mHorizontalTemperatureScrollView addSubview:mTemperatureScrollVieView];
	
	[mHorizontalEspumaScrollView setContentSize:CGSizeMake(650, 0)];
	[mHorizontalEspumaScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ScrollSliderBackground.png"]]];
	[mHorizontalEspumaScrollView addSubview:mEspumaScrollVieView];
	
    [self.navigationItem setRightBarButtonItem:btnEdit];
   
       //MBT 14.12.12
	 [mMilkButton4.titleLabel setTextAlignment:UITextAlignmentCenter];
	
	NSString * name = mDrinkSelected.name;
	name = [name stringByAppendingString:@".png"];
	UIImage * image = [UIImage imageNamed:name];
	if(image==nil)
	{
		//imageView.image = [UIImage imageNamed:@"Cafe standar.png"];
		imageView2.image = [UIImage imageNamed:@"Cafe standar.png"];
		
	}
	else
	{
		//mImageView.image = 
		//imageView.image = image;
		imageView2.image = image;
	}
	
	if([mDrinkSelected.name isEqualToString:@"Espresso Clásico"] || [mDrinkSelected.name isEqualToString:@"Espresso con Panna"] || [mDrinkSelected.name isEqualToString:@"Espresso Macchiato"])
	{
	
		//[mDoCupRotationButton setHidden:YES];
		[imageView setHidden:YES];
	
	}
	else
	{ 
		//[mDoCupRotationButton setHidden:NO];
		[imageView setHidden:NO];
	}
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
	
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setTemperatureSlider:nil];
    [self setTemperatureSlider:nil];
    mShotsButton1 = nil;
    mShotsButton1 = nil;
    mShotsButton2 = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)btnSaveSelected:(id)_sender
{
	@try{
	/*myDrink.user = @"Yo";
	 myDrink.userType =@"1";
	 myDrink.notes = @"";
	 myDrink.nickName=@"";*/

	
	//Crear notas y pasar adelante:
	//[self performSelector:@selector(popRootController) withObject:nil afterDelay:0.3];
	
	ONotesCustomizedViewController *controller = [[ONotesCustomizedViewController alloc] initWithNibName:@"ONotesCustomizedViewController" bundle:nil];

	controller.delegate = self;
	SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:controller];
	
	if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"NavBar.png"];
        [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
		navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
    }

	
	[self presentModalViewController:navigationController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}


-(void) saveAllDrink:(NSString*)_user :(NSString*)_userType :(NSString*)_notes :(NSString*)_nickName
{
    @try{
	NSEntityDescription *myDrinksEntity = [NSEntityDescription entityForName:@"MyDrinks" inManagedObjectContext:mAddingContext];
	
	mDrinkCustomize= [[OMyDrinks alloc] initWithEntity:myDrinksEntity insertIntoManagedObjectContext:mAddingContext];
	
	mDrinkCustomize.ide = [NSString stringWithFormat:@"%d",mId];
	mDrinkCustomize.available = mDrinkSelected.available;
	mDrinkCustomize.category = mDrinkSelected.category;
	mDrinkCustomize.glassmark = mDrinkSelected.glassmark;
	mDrinkCustomize.image = mDrinkSelected.image;
	mDrinkCustomize.largedescription = mDrinkSelected.largedescription;
	mDrinkCustomize.name = mDrinkSelected.name;
	mDrinkCustomize.shortdescription = mDrinkSelected.shortdescription;
	mDrinkCustomize.subcategory = mDrinkSelected.subcategory;
	mDrinkCustomize.temperature = mDrinkSelected.temperature;
	mDrinkCustomize.user = _user;
	mDrinkCustomize.userType =_userType;
	mDrinkCustomize.notes = _notes;
	mDrinkCustomize.nickName=_nickName;
	mDrinkCustomize.size = mSizeSelected;
	
	
        //MBT 21-12-12
        if([mShotsCafeinaSelected isEqualToString:@""] || [mShotsCafeinaSelected  length]==0)
        {}
        else{
            NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
            OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:mAddingContext];
            customization.ide =[NSString stringWithFormat:@"%d",mId];
            NSString *mcafeina= [self UpdateCafeina:mShotsCafeinaSelected];
          
            customization.abbreviature =  mcafeina;
            customization.amount = mShotsCafeinaSelected;
            customization.category = @"Cafe";
            customization.subcategory = @"tipoCafe";
            
            [mDrinkCustomize addCustomizationsObject:customization];
        }
        

        
	
	for(int i=0;i<[mSavedAddinsArray count];i++)
	 {
	 NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
	 OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:mAddingContext];
	 customization.ide =[NSString stringWithFormat:@"%d",mId];
	 customization.abbreviature = [[mSavedAddinsArray objectAtIndex:i] valueForKey:@"abbreviature"];
	 customization.amount = [[mSavedAddinsArray objectAtIndex:i] valueForKey:@"amount"];
	 customization.category = [[mSavedAddinsArray objectAtIndex:i] valueForKey:@"category"];
	 customization.subcategory = [[mSavedAddinsArray objectAtIndex:i] valueForKey:@"subcategory"];
	 
	 [mDrinkCustomize addCustomizationsObject:customization];
	 
	 }
	 
	 for(int i=0;i<[mSavedSyrupsArray count];i++)
	 {
	 NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
	 OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:mAddingContext];
	 customization.ide =[NSString stringWithFormat:@"%d",mId];
	 customization.abbreviature = [[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"abbreviature"];
	 customization.amount = [[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"amount"];
	 customization.category = [[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"category"];
	 customization.subcategory = [[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"subcategory"];
	 [mDrinkCustomize addCustomizationsObject:customization];
	 
	 }
        
       //MBT 21-12-12
        if(![mTemperatureSelected isEqualToString:@""] || ![mTemperatureSelected isEqualToString:nil])
        {
            NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
            OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:mAddingContext];
            customization.ide =[NSString stringWithFormat:@"%d",mId];
            NSString *Tvalue= [self temparatureAbbreviature];
            
            customization.abbreviature =  Tvalue;
            customization.amount = @"";
            customization.category = @"Añadidos";
            customization.subcategory = @"tipoTemperatura";
            
            [mDrinkCustomize addCustomizationsObject:customization];
        }
        
        
        
        //MBT 21-12-12
        if(![mFoamSelected isEqualToString:@""] || ![mFoamSelected isEqualToString:nil])
        {
            NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
            OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:mAddingContext];
            customization.ide =[NSString stringWithFormat:@"%d",mId];
            NSString *Fvalue= [self foamAbbreviature];
            
            customization.abbreviature =  Fvalue;
            customization.amount = @"";
            customization.category = @"Añadidos";
            customization.subcategory = @"tipoEspuma";
            
            [mDrinkCustomize addCustomizationsObject:customization];
        }
        
	 
	 if([mMilkTypeSelected isEqualToString:@""] || [mMilkTypeSelected length]==0)
	 {}
	 else{
	 NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
	 OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:mAddingContext];
	 customization.ide =[NSString stringWithFormat:@"%d",mId];
	 customization.abbreviature = ((OAbbreviatures*)[OAbbreviatures AbbreviaturesByName:mMilkTypeSelected inContext:mAddingContext]).abbreviature;
	 customization.amount = mMilkTypeSelected;
	 customization.category = @"Leche";
	 customization.subcategory = @"tipoLeche";
	 
	 [mDrinkCustomize addCustomizationsObject:customization];
	 }
	 if(![[shotCountSlider.valueNames objectAtIndex:shotCountSlider.valueIndex] isEqualToString:@"0"])
	 {
	 NSEntityDescription *myCustomizationEntity2 = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
	 OCustomization * customization2 = [[OCustomization alloc] initWithEntity:myCustomizationEntity2 insertIntoManagedObjectContext:mAddingContext];
	 customization2.ide =[NSString stringWithFormat:@"%d",mId];
	 customization2.abbreviature = [shotCountSlider.valueNames objectAtIndex:shotCountSlider.valueIndex];
	 customization2.amount = [shotCountSlider.valueNames objectAtIndex:shotCountSlider.valueIndex];
	 customization2.category = @"Shots";
	 customization2.subcategory = @"numero";
	 
	 [mDrinkCustomize addCustomizationsObject:customization2];
	 }
    
    if([mDrinkSelected.name isEqualToString:@""] || [mDrinkSelected.name length]==0)
    {}
    else{
        NSEntityDescription *myCustomizationEntity3 = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:mAddingContext];
        OCustomization * customization3 = [[OCustomization alloc] initWithEntity:myCustomizationEntity3 insertIntoManagedObjectContext:mAddingContext];
        customization3.ide =[NSString stringWithFormat:@"%d",mId];
        customization3.abbreviature = ((OAbbreviatures*)[OAbbreviatures AbbreviaturesByName:mDrinkSelected.name inContext:mAddingContext]).abbreviature;
        customization3.amount = mDrinkSelected.name;
        customization3.category = @"Bebida";
        customization3.subcategory = @"tipoBebida";
        
        [mDrinkCustomize addCustomizationsObject:customization3];
    }
	 
	 
	 NSError * error = nil;
	 [mAddingContext save:&error];
	
	
	NSString *pListPath = [DYFileNameManager pathForFile:@"Identifier.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
	NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];

	
	NSString * value = [NSString stringWithFormat:@"%d",mId];
	[dictionary setObject:value forKey:@"id"];
	[dictionary writeToFile:pListPath atomically:YES];
	
	[self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }


}

-(IBAction)milkButtonPressed:(id)sender
{

	UIButton * button = (UIButton*)sender;
	
	mMilkButton1.selected = NO;
	mMilkButton2.selected = NO;
	mMilkButton3.selected = NO;
	mMilkButton4.selected = NO;
	mMilkButton5.selected = NO;
	mMilkButton6.selected = NO;
 
	
	mMilkTypeSelected = button.titleLabel.text;
	button.selected = YES;

}




-(IBAction)CancelButtonPressed:(id)sender
{
	//Resetear todo
	if(mTypeOfHeaderSelected==1 || mTypeOfHeaderSelected==2 || mTypeOfHeaderSelected==3 || mTypeOfHeaderSelected==6 || mTypeOfHeaderSelected==4)
	{
	
		for(int i=0;i<[mTableView numberOfRowsInSection:0];i++)
		{
		
			OCustomizeCell * cell = (OCustomizeCell*)[mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
			cell.ingredientLabel.text = @"";
			[cell.plusButton setHidden:NO];
			[cell.plusButton setEnabled:YES];
			[cell.minusButton setHidden:YES];
			cell.count =-1;
		}
	
	
	}
	
	
	switch (mTypeOfHeaderSelected) {
		case 0:
		{
			mMilkButton1.selected = NO;
			mMilkButton2.selected = NO;
			mMilkButton3.selected = NO;
			mMilkButton4.selected = NO;
			mMilkButton5.selected = NO;
			mMilkButton6.selected = NO;
			
			mMilkTypeSelected = @"";
            mTemperatureSelected=nil;
            mFoamSelected=nil;
            TemperatureSlider.valueIndex=0;
            FoamSlider.valueIndex=0;
		
		}
			break;
		case 1:
		{
			[mSavedSyrupsArray removeAllObjects];
		
		}
			break;
		case 4:
		{
		
			[mSavedAddinsArray removeAllObjects];
		
		}
			
			break;
		case 7:
		{
			shotCountSlider.valueIndex= 0;
			mNumberOfShotsLabel.text = @"0 Ninguno";
            mShotsButton1.selected=NO;
            mShotsButton2.selected=NO;
            mShotsCafeinaSelected=@"";
            
		}
			break;
			
		default:
			break;
	}
	
	

}

-(void) updateArraywithIndex:(int) _index
{
    @try{
	switch (mTypeOfHeaderSelected) {
		case 0:
		{
			//Lo haran los botones.
			//Insertar Leche, tipo, botonseleccionado.title.,abreviatura.
			
		}
			break;
		case 1:
		{
			
				OCustomizeCell * cell = (OCustomizeCell*)[mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_index inSection:0]];

					NSString * category = @"Sirope";
					NSString * name = cell.nameLabel.text;
					NSString * value = cell.ingredientLabel.text;
					NSString * abbreviation =((OAbbreviatures*) [OAbbreviatures AbbreviaturesByName:name inContext:mAddingContext]).abbreviature;
					
					NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:category,@"category",name,@"subcategory",value,@"amount",abbreviation,@"abbreviature", nil];
					BOOL salir = NO;
					for(int i=0;(i<[mSavedSyrupsArray count]) && salir==NO;i++)
					{
						if([[[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"subcategory"] isEqualToString:cell.nameLabel.text])
						{
							
							if([cell.ingredientLabel.text length]>0)
							{
							
								[[mSavedSyrupsArray objectAtIndex:i] setValue:cell.ingredientLabel.text forKey:@"amount"];
							}
							else 
							{
								[mSavedSyrupsArray removeObjectAtIndex:i];
							}
							salir = YES;
						}
					}
					if(salir == NO)
					{
						if([cell.ingredientLabel.text length]>0)
						{
							[mSavedSyrupsArray addObject:dictionary];
						}
					}

		}
			break;
		case 4:
		{
			OCustomizeCell * cell = (OCustomizeCell*)[mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_index inSection:0]];
			
			NSString * category = @"Añadidos";
			NSString * name = cell.nameLabel.text;
			NSString * value = cell.ingredientLabel.text;
			NSString * abbreviation = ((OAbbreviatures*) [OAbbreviatures AbbreviaturesByName:name inContext:mAddingContext]).abbreviature;
			
			NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:category,@"category",name,@"subcategory",value,@"amount",abbreviation,@"abbreviature", nil];
			BOOL salir = NO;
			for(int i=0;(i<[mSavedAddinsArray count]) && salir==NO;i++)
			{
				if([[[mSavedAddinsArray objectAtIndex:i] valueForKey:@"subcategory"] isEqualToString:cell.nameLabel.text])
				{
					
					if([cell.ingredientLabel.text length]>0)
					{
						
						[[mSavedAddinsArray objectAtIndex:i] setValue:cell.ingredientLabel.text forKey:@"amount"];
					}
					else 
					{
						[mSavedAddinsArray removeObjectAtIndex:i];
					}
					salir = YES;
				}
			}
			if(salir == NO)
			{
				if([cell.ingredientLabel.text length]>0)
				{
					[mSavedAddinsArray addObject:dictionary];
				}
			}			
		}
			break;
		case 7:
		{
			//Insertar al guardar Shots, numero,[shotCountSlider.valueNames objectAtIndex:shotCountSlider.valueIndex],abreviatura;
			
		}
			break;
			
		default:
			break;
	}

    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}



-(IBAction)DoneButtonPressed:(id)sender
{

	
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
	mCustomizeView.frame = CGRectMake(5, -415, 310, 415);
    [UIView commitAnimations];
	
	[mMilkButton setSelected:NO];
	[mSiropeButton setSelected:NO];
	[mToppingButton setSelected:NO];
	[mPreparationButton setSelected:NO];
	[mAddinsButton setSelected:NO];
	[mTeaButton setSelected:NO];
	[mCoffeeButton setSelected:NO];
	[mShotsButton setSelected:NO];

}

- (IBAction)mShotsButtonPressed:(id)sender {
    
    
    UIButton * button = (UIButton*)sender;
	
	mShotsButton1.selected = NO;
	mShotsButton2.selected = NO;

    
	mShotsCafeinaSelected = button.titleLabel.text;
	button.selected = YES;
    
    
}

-(IBAction)CategoryButtonPressed:(id)sender
{

	@try{
	if(mIsReverse)[self doCupRotation:mIsReverse];
	
	
	UIButton * button = (UIButton *) sender;
	
	void (^completion)(void) = ^{
		switch (button.tag) {
			case 0:
			{
				
				[mInsideView addSubview:mMilkView];
				[mCoffeeView removeFromSuperview];
				[mWithTableViewView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"", nil);
				mTypeOfHeaderSelected = 0;
				
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mMilkButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
			}
				break;
			case 1:
			{
				
				[mInsideView addSubview:mWithTableViewView];
				[mCoffeeView removeFromSuperview];
				[mMilkView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"Elige tu Jarabe", nil);
				mTypeOfHeaderSelected = 1;
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mSiropeButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
				
			}
				
				break;
			case 2:
			{
				
				[mInsideView addSubview:mWithTableViewView];
				[mCoffeeView removeFromSuperview];
				[mMilkView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"Elige tus toppings", nil);
				mTypeOfHeaderSelected = 2;
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mToppingButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
				
			}
				
				break;
			case 3:
			{
				
				[mInsideView addSubview:mWithTableViewView];
				[mCoffeeView removeFromSuperview];
				[mMilkView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"Elige tu preparación", nil);
				mTypeOfHeaderSelected = 3;
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mPreparationButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
			}
				
				break;
			case 4:
			{
				
				[mInsideView addSubview:mWithTableViewView];
				[mCoffeeView removeFromSuperview];
				[mMilkView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"Elige tus añadidos", nil);
				mTypeOfHeaderSelected = 4;
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mAddinsButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
				
			}
				break;
			case 5:
			{
				
				[mInsideView addSubview:mCoffeeView];
				[mWithTableViewView removeFromSuperview];
				[mMilkView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"", nil);
				mTypeOfHeaderSelected = 5;
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mCoffeeButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
			}
				break;
			case 6:
			{
				[mInsideView addSubview:mWithTableViewView];
				[mCoffeeView removeFromSuperview];
				[mMilkView removeFromSuperview];
				[mShotsView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"", nil);
				mTypeOfHeaderSelected = 6;
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mTeaButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
			}
				
				break;
			case 7:
			{
				
				[mInsideView addSubview:mShotsView];
				[mCoffeeView removeFromSuperview];
				[mWithTableViewView removeFromSuperview];
				[mMilkView removeFromSuperview];
				mSectionLabel.text = NSLocalizedString(@"", nil);
				mTypeOfHeaderSelected = 7;
				
				CGPoint bottomOffset = CGPointMake(CGRectGetMidX(mShotsButton.frame)-CGRectGetMidX(mHorizontalScrollView.frame), 0);
				[mHorizontalScrollView setContentOffset:bottomOffset animated:YES];
				
			}
			default:
				break;
		}

		[mTableView reloadData];
		
        [UIView animateWithDuration:0.5f
                              delay:0.2f
                            options:0
                         animations:^{
                             mCustomizeView.frame = CGRectMake(5, 32, 310, 415);
							 mIsDown = YES;
                         }
                         completion:nil];
	};

	
	
	

	
	if(mTypeOfHeaderSelected == button.tag)
	{
		[UIView beginAnimations:nil context:nil];
		mTypeOfHeaderSelected=-1;
		[UIView setAnimationDuration:0.5];
		mCustomizeView.frame = CGRectMake(5, -415, 310, 415);
		[UIView commitAnimations];
		[self.navigationController setNavigationBarHidden:NO animated:YES];
		mIsDown = NO;
		
		[mMilkButton setSelected:NO];
		[mSiropeButton setSelected:NO];
		[mToppingButton setSelected:NO];
		[mPreparationButton setSelected:NO];
		[mAddinsButton setSelected:NO];
		[mTeaButton setSelected:NO];
		[mCoffeeButton setSelected:NO];
		[mShotsButton setSelected:NO];
	
		
	}
	else 
	{
		[self.navigationController setNavigationBarHidden:YES animated:YES];
		switch (button.tag) {
			case 0:
			{
				
				
				
				[mMilkButton setSelected:YES];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:NO];
				
				

			}
				break;
			case 1:
			{
				
				
				
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:YES];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:NO];
			

			}
				
				break;
			case 2:
			{
				
				
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:YES];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:NO];
				
				
			}
				
				break;
			case 3:
			{
				
				
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:YES];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:NO];
				
				
				
			}
				
				break;
			case 4:
			{
			
				
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:YES];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:NO];
				
				

			}
				break;
			case 5:
			{
				
				
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:YES];
				[mShotsButton setSelected:NO];

				
			}
				break;
			case 6:
			{
				
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:YES];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:NO];

				
			}
				break;
			case 7:
			{
			
				[mMilkButton setSelected:NO];
				[mSiropeButton setSelected:NO];
				[mToppingButton setSelected:NO];
				[mPreparationButton setSelected:NO];
				[mAddinsButton setSelected:NO];
				[mTeaButton setSelected:NO];
				[mCoffeeButton setSelected:NO];
				[mShotsButton setSelected:YES];
			
			
			}
				
				break;
			default:
				break;
		}
		
		
		

		if(mIsDown==NO) completion();
		else 
		{
			
			[UIView animateWithDuration:0.5f
							  delay:0.3f
							options:UIViewAnimationOptionCurveLinear
						 animations:^{
							 mCustomizeView.frame = CGRectMake(5, -415, 310, 415);
							 mIsDown=NO;
						 }
						 completion:^(BOOL finished) {
							 completion();
						 }];
		}
		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }

}


- (IBAction)shotCountChanged:(DNScrollSlider *)sender {
	// this event fires multiple times as the slider navigates 
	
    NSString *Shots =@"";
 
    
	int countshoots =    [sender valueIndex];
	//mNumberOfShotsLabel.text = [sender.valueNames objectAtIndex:sender.valueIndex];
    
    
    switch(countshoots)
    {
      	case 0 :
			Shots= @" Ninguno";
			break;
		case 1:
			Shots= @" Sencillo";
			break;
		case 2 :
			Shots= @" Doble";
			break;
		case 3 :
			Shots= @" Triple";
			break;
		case 4 :
			Shots= @" Cuádruple";
			break;
            
		default :
            
			break;
    }
    
    
    Shots = [NSString stringWithFormat:@"%d%@", countshoots, Shots];
    mNumberOfShotsLabel.text =Shots;
    
	
	/*if (internalShotsUpdate) {
		[shotsCountUpdatingTimer invalidate];
		[shotsCountUpdatingTimer autorelease];
		shotsCountUpdatingTimer = [[NSTimer scheduledTimerWithTimeInterval:.2 target:self selector:@selector(shotsCountTimerFired:) userInfo:nil repeats:NO] retain];
		return;
	}
	
	shotsQuantityIndicatorLabel.text = [drink localizedShotCount:sender.valueIndex];
	
	[drink addDrinkOption:[drink optionWithId:ShotQuantityOptionId] withQuantity:[NSNumber numberWithInteger:sender.valueIndex]];
	
    if (sender.valueIndex == 0 && !internalShotsUpdate) {
        [self regular];
	}*/
    
}

-(NSString *)temparatureAbbreviature{
    
    
    NSString *Tvalue ;
      
    if(mTemperatureSelected==nil)
    {
        Tvalue =@"";
    }
    else
    {    Tvalue= mTemperatureSelected;
       
    }
    
    return [NSString stringWithFormat:@"%@", Tvalue];
    
}

-(NSString *)foamAbbreviature{
    
    NSString *Fvalue;

    if(mFoamSelected ==nil)
    {
        Fvalue=@"";
    }
    else{
        Fvalue=mFoamSelected;
        
    }

    return [NSString stringWithFormat:@"%@", Fvalue];
    
}

- (IBAction)temperatureValuechanged:(DNScrollSlider *)sender {
    
    int index= [sender valueIndex];
    
    switch (index) {
        case 0:
            
            mTemperatureSelected =@"130º";
            break;
            
        case 1:
            mTemperatureSelected =@"";
            break;
            
            
        case 2 :
            mTemperatureSelected=@"XH";
            
      
    }
    
    
    
}

- (IBAction)foamValueChanged:(DNScrollSlider *)sender {
    int index= [sender valueIndex];
    switch (index) {
        case 0:
            
         mFoamSelected=@"/";
            break;
            
        case 1:
            mFoamSelected =@"WET";
            break;
            
            
        case 2 :
            mFoamSelected=@"DRY";
            
            break ;
    }
    
    
    
}


#pragma mark IBAction methods

-(IBAction)setCupRotation:(id)sender
{
	[self doCupRotation:mIsReverse];
	mIsReverse = !mIsReverse;
	
}

-(IBAction)showHelp:(id)sender
{
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ayuda" message:@"ELIGE TU BEBIDA\n1. Decide si quieres una bebida fría o caliente.\n2. Selecciona una bebida de nuestro menú de favoritos y ofertas de la temporada.\n3. Amplía o selecciona un tamaño de tu taza.\n\nHAZLA TUYA\n\n1. Toca el menú superior para personalizar tu bebida - Puedes elegir toppings, jarabes, leche.. y mucho más.\n2. Gira tu bebida para ver el marcado de sus ingredientes.\n\n GUARDA Y COMPARTE\n\n1. Ponle un nombre y añade tus notas para hacerla perfecta.\n2. Compártela con tus amigos por email, Facebook o Twitter." delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];

    [alertView show];


}

#pragma mark cup animation methods




- (NSArray *)animationImages {
	if (!animationImages) 
	{
		NSMutableArray *loadedImages = [NSMutableArray array];
		for (int i=0; i < 8; i++) 
		{
			UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png", i+1]];
			[loadedImages addObject:image];
		}
		animationImages = loadedImages;
	}
	
	
	return animationImages;
}

- (NSArray *)reverseAnimationImages {
    if (!reverseAnimationImages) 
	{
		NSEnumerator *reverseEnumerator = [self.animationImages reverseObjectEnumerator];
		NSMutableArray *reverseImagesMutable = [NSMutableArray array];
		for (UIImage *image in reverseEnumerator) 
		{
			[reverseImagesMutable addObject:image];
		}
		self.reverseAnimationImages = reverseImagesMutable;
	}
	return reverseAnimationImages;
}

- (void)setReverseAnimationImages:(NSArray *)value {
	if (reverseAnimationImages == value) return;
	reverseAnimationImages = value;
}


- (NSTimeInterval)delayToShowCupMarkings 
{
	
	return 0.4;
	
}

- (NSTimeInterval)cupRotationAnimationDuration 
{
	return 0.35;
}

- (void)doCupRotation:(BOOL)show {
	if (!show) {
		imageView.animationImages = self.animationImages;
		imageView.animationDuration = [self cupRotationAnimationDuration];
		imageView.animationRepeatCount = 1;
		imageView.image = [animationImages objectAtIndex:animationImages.count - 1];
		[imageView startAnimating];
		[self performSelector:@selector(viewCupMarkings) withObject:nil afterDelay:[self delayToShowCupMarkings]];
	} else {
		imageView.animationImages = self.reverseAnimationImages;
		imageView.animationDuration = [self cupRotationAnimationDuration];
		imageView.animationRepeatCount = 1;
		imageView.image = [reverseAnimationImages objectAtIndex:reverseAnimationImages.count - 1];
		[imageView startAnimating];
		[self hideCupMarkings];
	}
}


-(NSArray*) cupMarkingsArray
{
    @try{
	NSMutableArray *cupBoxMarkingArrayArray = [NSMutableArray array];
	
	for(int i=0;i<6;i++)
	{
		
		NSMutableArray *boxArray = [NSMutableArray array];
		
		switch (i) {
			case 0:
			{
				CupMarking *cupMarking  = [CupMarking new];
				
              
                
           		if(mShotsCafeinaSelected ==nil || [mShotsCafeinaSelected isEqualToString:@""])
				{
					cupMarking.text = @"";
					[boxArray addObject:cupMarking];
				}
				else
				{
                     NSString *mcafeina= [self UpdateCafeina:mShotsCafeinaSelected];
                     cupMarking.text =mcafeina;
                    [boxArray addObject:cupMarking];
                   
                    


				}
            
                
                               
               
						}
				break;
				
			case 1:
			{
			
				CupMarking *cupMarking  = [CupMarking new];
				if([mNumberOfShotsLabel.text isEqualToString:@"0 Ninguno"])
				{
					cupMarking.text = @"";
					[boxArray addObject:cupMarking];
				}
				else
				{//MBT 17-12-12

                    
                  
                    if(mNumberOfShotsLabel.text.length>=2)
                    {
                        if([[mNumberOfShotsLabel.text substringWithRange:NSMakeRange(1, 1)] isEqualToString:@" "])
                            
                                cupMarking.text = [mNumberOfShotsLabel.text substringToIndex:1];
                            else
                        cupMarking.text = [mNumberOfShotsLabel.text substringToIndex:2];
					}
                    else
                    {
                        cupMarking.text = [mNumberOfShotsLabel.text substringToIndex:1];
                                        
                    }
                    [boxArray addObject:cupMarking];
				}
				
			
			}

				break;
			case 2:
			{
				
				
				if(mSavedSyrupsArray==nil || [mSavedSyrupsArray count]==0)
				{
					CupMarking *cupMarking  = [CupMarking new];
					cupMarking.text = @"";
					[boxArray addObject:cupMarking];
                    
                    
                    
                    
				}
				else 
				{
					for(int i=0;i<[mSavedSyrupsArray count];i++)
					{
						
						
                        NSString  *count=	[[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"amount"];
						CupMarking *cupMarking  = [CupMarking new];
						NSString *value =  (NSString *)[[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"abbreviature"];
						cupMarking.text=       [NSString stringWithFormat:@"%@%@", count, value];
                        
                        [boxArray addObject:cupMarking];
				
					}
                    
             			}
                
      
				
			}
				break;
			case 3:
			{
				
				CupMarking *cupMarking  = [CupMarking new];
				if(mMilkTypeSelected==nil || [mMilkTypeSelected isEqualToString:@""])
				{
					cupMarking.text = @"";
					[boxArray addObject:cupMarking];
				}
				else
				{
					cupMarking.text = ((OAbbreviatures*) [OAbbreviatures AbbreviaturesByName:mMilkTypeSelected inContext:mAddingContext]).abbreviature;
					[boxArray addObject:cupMarking];
				}
				
				
			}
				break;
			case 4:
			{
				if(mSavedAddinsArray==nil || [mSavedAddinsArray count]==0)
				{
					CupMarking *cupMarking  = [CupMarking new];
					cupMarking.text = @"";
					[boxArray addObject:cupMarking];
				}
				else 
				{
					for(int i=0;i<[mSavedAddinsArray count];i++)
					{
                        
                             
						CupMarking *cupMarking  = [CupMarking new];
                        
                        NSString  *count=	[[mSavedAddinsArray objectAtIndex:i] valueForKey:@"amount"];
                        
                        NSString *value =  (NSString *)[[mSavedAddinsArray objectAtIndex:i] valueForKey:@"abbreviature"];
                    		cupMarking.text =[NSString stringWithFormat:@"%@%@", count, value];
						[boxArray addObject:cupMarking];
						
					}
				}
                
                //MBT 21-12-12
                CupMarking *cupMarking  = [CupMarking new];
                
                cupMarking.text=       [self temparatureAbbreviature];
                
                [boxArray addObject:cupMarking];
                
                CupMarking *cupMarking2  = [CupMarking new];
                
                cupMarking2.text=       [self foamAbbreviature];
                
                [boxArray addObject:cupMarking2];
			}
				break;
			case 5:
			{
				CupMarking *cupMarking  = [CupMarking new];
			//	NSString * texto = ((OAbbreviatures*) [OAbbreviatures AbbreviaturesByName:mDrinkSelected.name inContext:mAddingContext]).abbreviature;
			//	NSString * texto2 = mDrinkSelected.name;
				cupMarking.text = ((OAbbreviatures*) [OAbbreviatures AbbreviaturesByName:mDrinkSelected.name inContext:mAddingContext]).abbreviature;
				[boxArray addObject:cupMarking];
			}
				break;
			default:
				break;
		}
	
		[cupBoxMarkingArrayArray addObject:boxArray];
	}
	
	return cupBoxMarkingArrayArray;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
	
}





- (void)viewCupMarkings {
    @try{
	if (!mIsReverse) return [self hideCupMarkings];
	if (!cupMarkingsView) {
		cupMarkingsView = [CupMarkingsView viewFromNib];
		//cupMarkingsView.frame = CGRectMake(150, 0, 156, 359);
		//cupMarkingsView.drink = drink;
		cupMarkingsView.CupMarkings = [self cupMarkingsArray];
		[self.view addSubview:cupMarkingsView];
		[cupMarkingsView reload];
		
		//Animate
		
		//cupMarkingsView.layer.anchorPoint = CGPointMake(0, 0.5);
		cupMarkingsView.transform = CGAffineTransformMakeScale(0.1, 0.1);
		cupMarkingsView.alpha = 0.0;
		cupMarkingsView.center = CGPointMake(260, 160);
		
		[UIView beginAnimations:@"popInCupMarkings" context:nil];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
		[UIView setAnimationDuration:0.15];
		cupMarkingsView.transform = CGAffineTransformIdentity;
		cupMarkingsView.alpha = 1.0;
		[UIView commitAnimations];
	}
	else
	{
		cupMarkingsView.CupMarkings = [self cupMarkingsArray];
	}
	mIsReverse = YES;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}

- (void)hideCupMarkings {
	
	[UIView beginAnimations:@"popInCupMarkings" context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
	[UIView setAnimationDuration:0.15];
	[UIView setAnimationDidStopSelector:@selector(hideCupMarkingsAnimation:didFinish:context:)];
	[UIView setAnimationDelegate:self];
	cupMarkingsView.transform = CGAffineTransformMakeScale(0.1, 0.1);
	cupMarkingsView.alpha = 0.0;
	[UIView commitAnimations];
	
	
}

- (void)hideCupMarkingsAnimation:(NSString *)name didFinish:(BOOL)finished context:(void *)context {
	[cupMarkingsView removeFromSuperview];
	cupMarkingsView = nil;
	mIsReverse = NO;
}

#pragma mark - UITableView Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}


- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    @try{
    switch (mTypeOfHeaderSelected) 
	{
		case 0:
			return 0;
			break;
		case 1:
			if(mSyrupsArray!=nil)return [mSyrupsArray count];
			else return 0;
			break;
		case 2:
			if(mToppingsArray!=nil)return [mToppingsArray count];
			else return 0;
			break;
		case 3:
			if(mPreparationArray!=nil)return [mPreparationArray count];
			else return 0;
			break;
		case 4:
			if(mAddinsArray!=nil)return [mAddinsArray count];
			else return 0;
			break;
		case 5:
			return 0;
			break;
		case 6:
			if(mTeasArray!=nil)return [mTeasArray count];
			else return 0;
			break;
		case 7:
			return 0;
			break;
		default:
			break;
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	switch (mTypeOfHeaderSelected) 
	{
		case 0:
			return nil;
			break;
	
		case 3:
		{
			static NSString *CellIdentifier = @"OPreparationCell";
			
			OPreparationCell *cell = (OPreparationCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
				
			}
			cell.preparationNameLabel.text = [mPreparationArray objectAtIndex:_indexPath.row];
			cell.delegate = self;
			cell.index = _indexPath.row;
			NSString * row = [NSString stringWithFormat:@"%d",_indexPath.row];
			if(mPreparationSelectedArray!=nil && [mPreparationSelectedArray count]>0 && [mPreparationSelectedArray indexOfObject:row]!=NSNotFound)
			{
				cell.checkButton.selected=YES;
				cell.isActive=YES;
			}
			else
			{
				cell.checkButton.selected=NO;
				cell.isActive=NO;
				
			}
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			
			return cell;
		
		}
			break;
	
		case 5:
			return nil;
			break;
		
		default:
		{
			static NSString *CellIdentifier = @"OCustomizeCell";
			
			OCustomizeCell *cell = (OCustomizeCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil)
			{
				cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
				
			}
			
			cell.delegate = self;
			cell.index = _indexPath.row;
			cell.count =-1;
			[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
			
			switch (mTypeOfHeaderSelected) 
			{
				case 1:
				{
					cell.nameLabel.text = [mSyrupsArray objectAtIndex:_indexPath.row];
					NSArray * valuesArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", nil];
					cell.valuesArray = valuesArray;
					
					for(int i=0;i<[mSavedSyrupsArray count];i++)
					{
						if([[[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"subcategory"] isEqualToString:cell.nameLabel.text])
						{
						
							cell.count = [valuesArray indexOfObject:[[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"amount"]];
							cell.ingredientLabel.text = [[mSavedSyrupsArray objectAtIndex:i] valueForKey:@"amount"];
							[cell.minusButton setHidden:NO];
															
							if(cell.count<[valuesArray count]-1)[cell.plusButton setEnabled:YES];
				
							else [cell.plusButton setEnabled:NO];
						}
					}
					
					
				}
					break;
				case 2:
				{
					cell.nameLabel.text = [mToppingsArray objectAtIndex:_indexPath.row];
					NSArray * valuesArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", nil];
					cell.valuesArray = valuesArray;
				}
					break;
				case 4:
				{
					cell.nameLabel.text = [mAddinsArray objectAtIndex:_indexPath.row];
					//NSArray * valuesArray = [[NSArray alloc] initWithObjects:@"Light",@"Reg.",@"Extra", nil];
					NSArray * valuesArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", nil];

					cell.valuesArray = valuesArray;
					
					for(int i=0;i<[mSavedAddinsArray count];i++)
					{
						if([[[mSavedAddinsArray objectAtIndex:i] valueForKey:@"subcategory"] isEqualToString:cell.nameLabel.text])
						{
							
							cell.count = [valuesArray indexOfObject:[[mSavedAddinsArray objectAtIndex:i] valueForKey:@"amount"]];
							cell.ingredientLabel.text = [[mSavedAddinsArray objectAtIndex:i] valueForKey:@"amount"];
							[cell.minusButton setHidden:NO];
							
							if(cell.count<[valuesArray count]-1)[cell.plusButton setEnabled:YES];
							
							else [cell.plusButton setEnabled:NO];
						}
					}

				}
					break;
				case 6:
				{
					cell.nameLabel.text = [mTeasArray objectAtIndex:_indexPath.row];
					NSArray * valuesArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", nil];
					cell.valuesArray = valuesArray;
				}
					break;
				default:
					break;
			}
			return cell;
		
		}
			break;
	}
	
	
   
	return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
 
	
	
}


#pragma mark delegate cell methods
-(void) UpdatePreparationArrayForIndex:(int)mIndex;
{
	@try{
	NSString * ind = [NSString stringWithFormat:@"%d",mIndex];
	if(mPreparationSelectedArray==nil ||[mPreparationSelectedArray count]==0)
	{
		mPreparationSelectedArray = [[NSMutableArray alloc] initWithObjects:ind,nil];
		
		
	}
	else
	{
		if([mPreparationSelectedArray indexOfObject:ind]==NSNotFound)
		{
			
			[mPreparationSelectedArray addObject:ind];
		}
		else {
			[mPreparationSelectedArray removeObject:ind];
		}
		
	}
	[mTableView reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeShotsViewcontroller");
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        OCustomizeDrinkViewController  *regresa =[[OCustomizeDrinkViewController    alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
    }
}







@end
