//
//  AFOpenFlowConstants.h
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#ifndef Starbucks_AFOpenFlowConstants_h
#define Starbucks_AFOpenFlowConstants_h
// For OpenFlow
#define COVER_SPACING			80
#define CENTER_COVER_OFFSET		70
#define SIDE_COVER_ANGLE		0.90
#define SIDE_COVER_ZPOSITION	-80
#define SCALE_COVER				0.90
#define TRANSFORM_M34			-0.001

#define REFLECTION_FRACTION		0.0
#define REFLECTION_ALPHA		0.90


#endif
