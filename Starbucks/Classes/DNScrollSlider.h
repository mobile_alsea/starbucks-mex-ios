//
//  DNScrollSlider.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "SliderProtocol.h"

@interface DNScrollSlider : UIControl <SliderProtocol, UIScrollViewDelegate> {
	UIScrollView *scrollView;
	NSInteger valueIndex;
	NSInteger valueCount;
	CGFloat valueSeparation;
	
	NSMutableArray *valueIndicators;
	NSMutableArray *intermediateValueIndicators;
	NSArray *valueNames;
	
	UIImageView *selectionIndicator;
	UIImageView *backgroundView;
	
	NSInteger valuesBetweenNamedValues;
	BOOL broadcastChanges;
}

@property (readwrite, assign) BOOL broadcastChanges;
@property (nonatomic, assign) NSInteger valueIndex;


- (void)setValueIndex:(NSInteger)valueIndex animated:(BOOL)animated;

@end
