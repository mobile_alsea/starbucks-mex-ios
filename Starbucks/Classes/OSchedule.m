//
//  OSchedule.m
//  Starbucks
//
//  Created by Isabelo Pamies López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OSchedule.h"
#import "OStore.h"


@implementation OSchedule

@dynamic endTime;
@dynamic name;
@dynamic startTime;
@dynamic order;
@dynamic store;

@end
