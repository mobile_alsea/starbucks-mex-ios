//
//  DNLinearLayoutView.h
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DNLinearLayoutView : UIView {
	CGFloat padding;
}

@property (nonatomic) CGFloat padding;

- (void)removeAllSubviews;

@end
