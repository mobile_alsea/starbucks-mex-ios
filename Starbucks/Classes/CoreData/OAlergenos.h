//
//  OAlergenos.h
//  Starbucks
//
//  Created by Santi Belloso López on 14/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OFood;

@interface OAlergenos : NSManagedObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) OFood *food;

@end
