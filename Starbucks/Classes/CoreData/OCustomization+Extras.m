//
//  OCustomization+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 21/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCustomization+Extras.h"
#import "OAppDelegate.h"
@implementation OCustomization (Extras)
+(NSArray*) customizationWithCategory:(NSString*)_category identi:(NSString*)_ide context:(NSManagedObjectContext *)_context;
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"CustomizationWithCategory" substitutionVariables:[NSDictionary dictionaryWithObjectsAndKeys:_category,@"CATEGORYNAME",_ide,@"IDENAME", nil]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	if(fetchedArray==nil) return nil;
	else return fetchedArray;

		
	}
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OCustomization+Extras.m/customizationWithCategory");
    }

	
}

@end
