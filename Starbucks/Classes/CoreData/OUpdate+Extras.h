//
//  OUpdate+Extras.h
//  Starbucks
//
//  Created by Isabelo Pamies López on 31/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OUpdate.h"

@interface OUpdate (Extras)

+ (OUpdate *) updateWithName:(NSString *)_name context:(NSManagedObjectContext *)_context;
+ (BOOL) isUnsynchronizedToService:(NSString *)_service version:(NSInteger)_version context:(NSManagedObjectContext *)_context;

+ (NSArray *) allUpdatesWithContext:(NSManagedObjectContext *)_context;

@end
