//
//  OMyFavorites.h
//  Starbucks
//
//  Created by Santi Belloso López on 13/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OMyFavorites : NSManagedObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * order;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * typeOrder;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * note;

@end
