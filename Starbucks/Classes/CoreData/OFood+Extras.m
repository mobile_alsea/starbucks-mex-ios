//
//  OFood+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFood+Extras.h"
#import "OAppDelegate.h"
#import "OMyFavorites.h"
@implementation OFood (Extras)
+ (NSFetchRequest *)foodInContextFetchRequest:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllFood" substitutionVariables:[NSDictionary dictionary]];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/foodInContextFectrequest");
    }
}


+ (NSArray *)foodInContext:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllFood" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/foodInContext");
    }
}

+ (NSFetchRequest *)foodWithCategory:(NSString *)_categoryName andAlergenos:(NSMutableArray*) _alergenos context:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FoodWithoutAlergenos" substitutionVariables:[NSDictionary dictionaryWithObjectsAndKeys:_categoryName,@"CATEGORYNAME",_alergenos,@"ALERGENOS", nil]];

	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
	{
		NSFetchRequest *fetchRequest2 = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FoodWithoutFood" substitutionVariables:[NSDictionary dictionaryWithObjectsAndKeys:_categoryName,@"CATEGORYNAME",fetchedArray,@"ARRAY", nil]];
		return fetchRequest2;
	
	}
	else
	{
		NSFetchRequest *fetchRequest3 = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FoodWithCategory" substitutionVariables:[NSDictionary dictionaryWithObject:_categoryName forKey:@"CATEGORYNAME"]];
		return fetchRequest3;
	
	}
	
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/foodIwithcategory");
    }

}


+ (NSFetchRequest *)foodWithCategory:(NSString *)_categoryName context:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FoodWithCategory" substitutionVariables:[NSDictionary dictionaryWithObject:_categoryName forKey:@"CATEGORYNAME"]];

    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/foodWithCategory");
    }

}
+ (NSArray *)categoriesOfFoodInContext:(NSManagedObjectContext *)_context
{
	@try{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Food" inManagedObjectContext:_context];
	request.entity = entity;
	request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"category"]];
	request.returnsDistinctResults = YES;
	request.resultType = NSDictionaryResultType;
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	NSError *error = nil;
	NSArray *distincResults = [_context executeFetchRequest:request error:&error];
	// Use the results
	return  distincResults;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/categoriesofFoodIncontext");
    }
}



+(NSMutableArray *) foodsByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSMutableArray * retorno = [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int i=0;i<[_favorites count];i++)
	{
		
		OMyFavorites * fav = (OMyFavorites*) [_favorites objectAtIndex:i];
		OFood * food = [OFood foodByName:fav.name inContext:_context];
		[retorno addObject:food];
		
	}
	
	return retorno;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/foodsbynames");
    }

}
+(OFood*) foodByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FoodWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_name forKey:@"FOODNAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/foodbyname");
    }

}


@end
