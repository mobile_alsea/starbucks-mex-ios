//
//  OStore+Extras.h
//  Starbucks
//
//  Created by Adrián Caramés on 23/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStore.h"

@interface OStore (Extras)

+ (NSFetchRequest *)storesInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSFetchRequest *) storesWithServicesFetchRequest:(NSArray *)_services context:(NSManagedObjectContext *)_context;
+ (NSArray *)storesInContext:(NSManagedObjectContext *)_context;

+(NSMutableArray *) storesByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context;
+(OStore*) storeByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context;
- (NSArray *) schedulesArray;

+ (NSFetchRequest *)storesInContextFetchRequest:(NSManagedObjectContext *)_context withDistance:(NSString*)_distance;

@end
