//
//  OImage.m
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OImage.h"
#import "OCoffee.h"


@implementation OImage

@dynamic name;
@dynamic image;
@dynamic coffee;

@end
