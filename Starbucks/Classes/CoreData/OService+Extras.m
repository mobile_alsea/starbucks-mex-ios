//
//  OService+Extras.m
//  Starbucks
//
//  Created by Adrián Caramés on 23/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OService+Extras.h"
#import "OAppDelegate.h"

@implementation OService (Extras)


+ (OService *)serviceWithName:(NSString *)_serviceName context:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"ServiceWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_serviceName forKey:@"SERVICENAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Oservice/serviceName");
    }
}


+ (NSArray *)servicesInContext:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllServices" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [[OAppDelegate managedObjectContext] executeFetchRequest:fetchRequest error:&error];
   
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Oservice/servicesinContext");
    }
}

+ (NSFetchRequest *)servicesInContextFetchRequest:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllServices" substitutionVariables:[NSDictionary dictionary]];
    
    // Create the sort descriptors array.
	NSSortDescriptor *serviceDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:serviceDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Oservice/servicesInConetxtFetch");
    }
}

+ (NSArray *)servicesSelectedInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllServicesSelected"
                                                                                 substitutionVariables:@{}];
    NSError *error;
    NSArray *fetchedArray = [context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray.count)
        return fetchedArray;
    else
        return nil;
}

+ (NSArray *)servicesNoSelectedInContext:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllServicesNoSelected" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [[OAppDelegate managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Oservice/servicesNoSelected");
    }
}


@end
