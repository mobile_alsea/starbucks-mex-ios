//
//  OMyFavorites+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyFavorites.h"

@interface OMyFavorites (Extras)

+ (OMyFavorites *)favoriteWithName:(NSString *)_favoriteName context:(NSManagedObjectContext *)_context;
+ (void) insertIntoContext:(NSDictionary *)_item context:(NSManagedObjectContext *)_context;
+(NSArray*) favoritesWithType:(NSString*)_type context:(NSManagedObjectContext *)_context;
+(NSArray*) favoritesWithType:(NSString*)_type context:(NSManagedObjectContext *)_context andTypeOrder:(NSString*)_typeOrder;

@end
