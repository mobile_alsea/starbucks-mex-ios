//
//  OCustomization.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCustomization.h"
#import "OMyDrinks.h"


@implementation OCustomization

@dynamic abbreviature;
@dynamic amount;
@dynamic category;
@dynamic subcategory;
@dynamic ide;
@dynamic drink;

@end
