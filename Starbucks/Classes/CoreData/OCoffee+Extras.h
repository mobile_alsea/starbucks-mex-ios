//
//  OCoffee+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCoffee.h"

@interface OCoffee (Extras)

+ (NSFetchRequest *)coffeesInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSArray *)coffeesInContext:(NSManagedObjectContext *)_context;
+ (NSArray *)categoriesOfCoffeesInContext:(NSManagedObjectContext *)_context;
+(NSMutableArray *) coffeesByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context;
+(NSMutableArray *) coffeesByCombinationNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context;
+(OCoffee*) coffeeByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context;


@end
