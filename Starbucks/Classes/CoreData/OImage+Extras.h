//
//  OImage+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OImage.h"

@interface OImage (Extras)
+ (OImage *)imageWithName:(NSString *)_imageName context:(NSManagedObjectContext *)_context;
@end
