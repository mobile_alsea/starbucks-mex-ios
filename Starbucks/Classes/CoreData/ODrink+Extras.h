//
//  ODrink+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrink.h"

@interface ODrink (Extras)
+ (NSFetchRequest *)drinksInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSArray *)drinksInContext:(NSManagedObjectContext *)_context;
+ (NSFetchRequest *)drinksInContext:(NSManagedObjectContext *)_context withTemperature:(NSString*)_temperature;
+ (NSArray *)categoriesOfDrinksInContext:(NSManagedObjectContext *)_context withTemperature:(NSString*) _temperature;
+(NSMutableArray *) drinksByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context;
+(NSArray *) drinksByTemperature:(NSString*)_temperature inContext:(NSManagedObjectContext*)_context;
+(ODrink*) drinkByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context;
+ (NSFetchRequest *)drinksInContext:(NSManagedObjectContext *)_context withTemperature:(NSString*)_temperature andSubcategory:(NSString*)_subCategory;
@end
