//
//  OService.h
//  Starbucks
//
//  Created by Isabelo Pamies López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OStore;

@interface OService : NSManagedObject

@property (nonatomic, strong) NSNumber * isSelected;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSSet *store;
@end

@interface OService (CoreDataGeneratedAccessors)

- (void)addStoreObject:(OStore *)value;
- (void)removeStoreObject:(OStore *)value;
- (void)addStore:(NSSet *)values;
- (void)removeStore:(NSSet *)values;
@end
