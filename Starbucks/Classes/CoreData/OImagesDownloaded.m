//
//  OImagesDownloaded.m
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OImagesDownloaded.h"


@implementation OImagesDownloaded

@dynamic name;
@dynamic image;

@end
