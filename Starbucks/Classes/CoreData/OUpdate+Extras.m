//
//  OUpdate+Extras.m
//  Starbucks
//
//  Created by Isabelo Pamies López on 31/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OUpdate+Extras.h"
#import "OAppDelegate.h"

@implementation OUpdate (Extras)

+ (OUpdate *) updateWithName:(NSString *)_name context:(NSManagedObjectContext *)_context
{
    @try{
	if([_name length] <= 0)
		return nil;
	
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"UpdateWithName" 
																				 substitutionVariables:[NSDictionary dictionaryWithObject:_name forKey:@"NAME"]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
   return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OUpdate/udateWithName");
    }
}

+ (BOOL) isUnsynchronizedToService:(NSString *)_service version:(NSInteger)_version context:(NSManagedObjectContext *)_context
{
    @try{
	BOOL retorno = YES;
    
    OUpdate *update = [OUpdate updateWithName:_service context:_context];
	if(update != nil)
	{
		if([update.version integerValue] >= _version)
			retorno = NO;
	}
	
	return retorno;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OUpdate/isUnsynchronizedToService");
    }
}

+ (NSArray *) allUpdatesWithContext:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllUpdates" substitutionVariables:[NSDictionary dictionary]];
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
	return fetchedArray;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OUpdate/allUpdateWithContext");
    }
}

@end
