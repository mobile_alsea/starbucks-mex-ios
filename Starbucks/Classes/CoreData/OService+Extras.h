//
//  OService+Extras.h
//  Starbucks
//
//  Created by Adrián Caramés on 23/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OService.h"

@interface OService (Extras)

+ (OService *)serviceWithName:(NSString *)_serviceName context:(NSManagedObjectContext *)_context;
+ (NSArray *)servicesInContext:(NSManagedObjectContext *)_context;
+ (NSArray *)servicesSelectedInContext:(NSManagedObjectContext *)context;
+ (NSFetchRequest *)servicesInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSArray *)servicesNoSelectedInContext:(NSManagedObjectContext *)_context;


@end
