//
//  OMyFavorites+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyFavorites+Extras.h"
#import "OAppDelegate.h"
#import "OMyFavorites.h"

@implementation OMyFavorites (Extras)


+ (OMyFavorites *)favoriteWithName:(NSString *)_favoriteName context:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FavoriteWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_favoriteName forKey:@"FAVORITENAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyFavorites/favoriteWithName");
    }
}


+(NSArray*) favoritesWithType:(NSString*)_type context:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FavoriteWithType" substitutionVariables:[NSDictionary dictionaryWithObject:_type forKey:@"FAVORITETYPE"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	if(fetchedArray==nil) return nil;
	else return fetchedArray;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyFavorites/favoritesWithType");
    }

}

+(NSArray*) favoritesWithType:(NSString*)_type context:(NSManagedObjectContext *)_context andTypeOrder:(NSString*)_typeOrder
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"FavoriteWithTypeAndOrderType" substitutionVariables:[NSDictionary dictionaryWithObjectsAndKeys:_type,@"FAVORITETYPE",_typeOrder,@"FAVORITEORDERTYPE", nil]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	if(fetchedArray==nil) return nil;
	else return fetchedArray;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyFavorites/favoriteWithType");
    }
	
}


+ (void) insertIntoContext:(NSDictionary *)_item context:(NSManagedObjectContext *)_context
{
    @try{
    NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyFavorites" inManagedObjectContext:_context];
	
	OMyFavorites *favorite = [[OMyFavorites alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:_context];
    
	favorite.name = [_item objectForKey:@"name"];
    favorite.type = [_item objectForKey:@"type"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyFavorites/insertIntoContext");
    }
}






@end
