//
//  OMyDrinks+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyDrinks+Extras.h"
#import "OAppDelegate.h"
@implementation OMyDrinks (Extras)


+ (NSFetchRequest *)myDrinksInContextFetchRequest:(NSManagedObjectContext *)_context;
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllMyDrinks" substitutionVariables:[NSDictionary dictionary]];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyDrinks/myDrinksInContextFetch");
    }
}


+ (NSArray *)myDrinksInContext:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllMyDrinks" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyDrinks/myDrinksInContext");
    }
}

+ (NSFetchRequest *)myDrinksUserTypeRequest:(NSString *)_userName context:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"myDrinksByUser" substitutionVariables:[NSDictionary dictionaryWithObject:_userName forKey:@"USERTYPE"]];
	
	//NSError *error = nil;
   // NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
	return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyDrinks/myrinksUserTypeRequest");
    }

}

+ (NSArray *)myDrinksUserType:(NSString *)_userName context:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"myDrinksByUser" substitutionVariables:[NSDictionary dictionaryWithObject:_userName forKey:@"USERTYPE"]];
	
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    if(fetchedArray != nil && [fetchedArray count] > 0)
	{

		return fetchedArray;
	}
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyDrinks/myDrinkUserType");
    }
	
}
+(OMyDrinks*) myDrinkById:(NSString*)_ide inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"myDrinksById" substitutionVariables:[NSDictionary dictionaryWithObject:_ide forKey:@"IDNAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OMyDrinks/myDrinkById");
    }
}

@end
