//
//  OAbbreviatures+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OAbbreviatures+Extras.h"
#import "OAppDelegate.h"
@implementation OAbbreviatures (Extras)
+ (NSFetchRequest *)AbbreviaturesInContextFetchRequest:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllAbbreviatures" substitutionVariables:[NSDictionary dictionary]];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OAbbbreviatures/AbbreviatiresInContextFetch");
    }

}

+ (NSArray *)AbbreviaturesInContext:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllAbbreviatures" substitutionVariables:[NSDictionary dictionary]];
	
	NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OAbbbreviatures/AbbreviatiresInContext");
    }

}


+(OAbbreviatures*) AbbreviaturesByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AbbreviatureWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_name forKey:@"ABBREVIATURENAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OAbbbreviatures/AbbreviatiresbyName");
    }

}

@end
