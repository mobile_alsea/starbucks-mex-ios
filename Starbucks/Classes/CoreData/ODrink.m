//
//  ODrink.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrink.h"


@implementation ODrink

@dynamic available;
@dynamic category;
@dynamic glassmark;
@dynamic image;
@dynamic largedescription;
@dynamic name;
@dynamic shortdescription;
@dynamic subcategory;
@dynamic temperature;
@dynamic ide;

@end
