//
//  OUpdate.m
//  Starbucks
//
//  Created by Isabelo Pamies López on 31/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OUpdate.h"


@implementation OUpdate

@dynamic name;
@dynamic version;

@end
