//
//  OCombinations.h
//  Starbucks
//
//  Created by Santi Belloso López on 17/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OFood;

@interface OCombinations : NSManagedObject

@property (nonatomic, strong) NSString * available;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) OFood *food;

@end
