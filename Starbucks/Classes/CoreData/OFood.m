//
//  OFood.m
//  Starbucks
//
//  Created by Santi Belloso López on 14/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFood.h"
#import "OAlergenos.h"
#import "OCombinations.h"


@implementation OFood

@dynamic availability;
@dynamic category;
@dynamic image;
@dynamic imageIcon;
@dynamic largedescrip;
@dynamic name;
@dynamic shortdescrip;
@dynamic alergenos;
@dynamic combinations;

@end
