//
//  OMyDrinks+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyDrinks.h"

@interface OMyDrinks (Extras)

+ (NSFetchRequest *)myDrinksInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSArray *)myDrinksInContext:(NSManagedObjectContext *)_context;
+ (NSMutableArray *)myDrinksUserType:(NSString *)_userName context:(NSManagedObjectContext *)_context;
+ (NSFetchRequest *)myDrinksUserTypeRequest:(NSString *)_userName context:(NSManagedObjectContext *)_context;
+(OMyDrinks*) myDrinkById:(NSString*)_ide inContext:(NSManagedObjectContext*)_context;

@end
