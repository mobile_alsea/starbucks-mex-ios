//
//  OCustomization+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 21/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCustomization.h"

@interface OCustomization (Extras)

+(NSArray*) customizationWithCategory:(NSString*)_category identi:(NSString*)_ide context:(NSManagedObjectContext *)_context;
@end
