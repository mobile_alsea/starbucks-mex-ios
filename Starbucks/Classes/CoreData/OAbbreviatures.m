//
//  OAbbreviatures.m
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OAbbreviatures.h"


@implementation OAbbreviatures

@dynamic name;
@dynamic abbreviature;

@end
