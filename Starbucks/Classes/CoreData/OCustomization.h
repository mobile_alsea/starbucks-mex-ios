//
//  OCustomization.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OMyDrinks;

@interface OCustomization : NSManagedObject

@property (nonatomic, strong) NSString * abbreviature;
@property (nonatomic, strong) NSString * amount;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * subcategory;
@property (nonatomic, strong) NSString * ide;
@property (nonatomic, strong) OMyDrinks *drink;

@end
