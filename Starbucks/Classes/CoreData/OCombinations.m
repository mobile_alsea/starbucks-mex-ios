//
//  OCombinations.m
//  Starbucks
//
//  Created by Santi Belloso López on 17/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCombinations.h"
#import "OFood.h"


@implementation OCombinations

@dynamic available;
@dynamic image;
@dynamic name;
@dynamic food;

@end
