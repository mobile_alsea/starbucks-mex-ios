//
//  OStore.h
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OSchedule, OService;

@interface OStore : NSManagedObject

@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * area;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSNumber * distance;
@property (nonatomic, strong) NSNumber * latitude;
@property (nonatomic, strong) NSNumber * longitude;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * postalCode;
@property (nonatomic, strong) NSString * province;
@property (nonatomic, strong) NSSet *schedules;
@property (nonatomic, strong) NSSet *services;
@end

@interface OStore (CoreDataGeneratedAccessors)

- (void)addSchedulesObject:(OSchedule *)value;
- (void)removeSchedulesObject:(OSchedule *)value;
- (void)addSchedules:(NSSet *)values;
- (void)removeSchedules:(NSSet *)values;
- (void)addServicesObject:(OService *)value;
- (void)removeServicesObject:(OService *)value;
- (void)addServices:(NSSet *)values;
- (void)removeServices:(NSSet *)values;
@end
