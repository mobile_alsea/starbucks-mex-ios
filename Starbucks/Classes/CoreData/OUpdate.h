//
//  OUpdate.h
//  Starbucks
//
//  Created by Isabelo Pamies López on 31/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OUpdate : NSManagedObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSNumber * version;

@end
