//
//  ODrink+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrink+Extras.h"
#import "OAppDelegate.h"
#import "OMyFavorites.h"
@implementation ODrink (Extras)

+ (NSFetchRequest *)drinksInContextFetchRequest:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllDrinks" substitutionVariables:[NSDictionary dictionary]];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinksInContextFetch");
    }
}


+ (NSArray *)drinksInContext:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllDrinks" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinksInContext");
    }
}

+ (NSFetchRequest *)drinksInContext:(NSManagedObjectContext *)_context withTemperature:(NSString*)_temperature
{
	@try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"DrinkWithTemperature" substitutionVariables:[NSDictionary dictionaryWithObject:_temperature forKey:@"TEMPERATURENAME"]];

	return  fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinksInContext");
    }

}


+ (NSFetchRequest *)drinksInContext:(NSManagedObjectContext *)_context withTemperature:(NSString*)_temperature andSubcategory:(NSString*)_subCategory
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"DrinkWithTemperatureAndsubcategory" substitutionVariables:[NSDictionary dictionaryWithObjectsAndKeys:_temperature,@"TEMPERATURENAME",_subCategory,@"SUBCATEGORYNAME",nil]];
	
	return  fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinksInContext");
    }

}


+ (NSArray *)categoriesOfDrinksInContext:(NSManagedObjectContext *)_context withTemperature:(NSString*) _temperature
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"DrinkWithTemperature" substitutionVariables:[NSDictionary dictionaryWithObject:_temperature forKey:@"TEMPERATURENAME"]];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Drinks" inManagedObjectContext:_context];
	
	fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"category"]];
	fetchRequest.returnsDistinctResults = YES;
	fetchRequest.resultType = NSDictionaryResultType;
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	NSError *error = nil;
	NSArray *distincResults = [_context executeFetchRequest:fetchRequest error:&error];
	// Use the results
	
	return  distincResults;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/categoriesofdrinksInContext");
    }
	
}

+(NSArray *) drinksByTemperature:(NSString*)_temperature inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"DrinkWithTemperature" substitutionVariables:[NSDictionary dictionaryWithObject:_temperature forKey:@"TEMPERATURENAME"]];
	
	NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinksbytemperature");
    }

}


+(NSMutableArray *) drinksByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSMutableArray * retorno = [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int i=0;i<[_favorites count];i++)
	{
		
		OMyFavorites * fav = (OMyFavorites*) [_favorites objectAtIndex:i];
		ODrink * coffee = [ODrink drinkByName:fav.name inContext:_context];
		[retorno addObject:coffee];
		
	}
	
	return retorno;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinksbynames");
    }
	
}

+(ODrink*) drinkByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"DrinkWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_name forKey:@"DRINKNAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
	
	}
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/ODrink/drinkbyname");
    }
}



@end
