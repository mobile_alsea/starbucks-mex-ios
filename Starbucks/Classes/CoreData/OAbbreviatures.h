//
//  OAbbreviatures.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OAbbreviatures : NSManagedObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * abbreviature;

@end
