//
//  OImage+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OImage+Extras.h"
#import "OAppDelegate.h"
@implementation OImage (Extras)

+ (OImage *)imageWithName:(NSString *)_imageName context:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"ImageWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_imageName forKey:@"IMAGENAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OImage/imagewithname");
    }


}
@end
