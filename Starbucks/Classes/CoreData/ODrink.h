//
//  ODrink.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ODrink : NSManagedObject

@property (nonatomic, strong) NSString * available;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * glassmark;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSString * largedescription;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * shortdescription;
@property (nonatomic, strong) NSString * subcategory;
@property (nonatomic, strong) NSString * temperature;
@property (nonatomic, strong) NSString * ide;

@end
