//
//  OMyDrinks.m
//  Starbucks
//
//  Created by Santi Belloso López on 21/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyDrinks.h"
#import "OCustomization.h"


@implementation OMyDrinks

@dynamic available;
@dynamic category;
@dynamic glassmark;
@dynamic ide;
@dynamic image;
@dynamic largedescription;
@dynamic name;
@dynamic nickName;
@dynamic notes;
@dynamic shortdescription;
@dynamic subcategory;
@dynamic temperature;
@dynamic user;
@dynamic userType;
@dynamic size;
@dynamic customizations;

@end
