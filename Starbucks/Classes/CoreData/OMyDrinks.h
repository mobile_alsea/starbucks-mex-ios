//
//  OMyDrinks.h
//  Starbucks
//
//  Created by Santi Belloso López on 21/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OCustomization;

@interface OMyDrinks : NSManagedObject

@property (nonatomic, strong) NSString * available;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * glassmark;
@property (nonatomic, strong) NSString * ide;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSString * largedescription;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * nickName;
@property (nonatomic, strong) NSString * notes;
@property (nonatomic, strong) NSString * shortdescription;
@property (nonatomic, strong) NSString * subcategory;
@property (nonatomic, strong) NSString * temperature;
@property (nonatomic, strong) NSString * user;
@property (nonatomic, strong) NSString * userType;
@property (nonatomic, strong) NSString * size;
@property (nonatomic, strong) NSSet *customizations;
@end

@interface OMyDrinks (CoreDataGeneratedAccessors)

- (void)addCustomizationsObject:(OCustomization *)value;
- (void)removeCustomizationsObject:(OCustomization *)value;
- (void)addCustomizations:(NSSet *)values;
- (void)removeCustomizations:(NSSet *)values;
@end
