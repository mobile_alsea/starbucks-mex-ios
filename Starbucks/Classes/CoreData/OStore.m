//
//  OStore.m
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStore.h"
#import "OSchedule.h"
#import "OService.h"


@implementation OStore

@dynamic address;
@dynamic area;
@dynamic city;
@dynamic distance;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic postalCode;
@dynamic province;
@dynamic schedules;
@dynamic services;

@end
