//
//  OStore+Extras.m
//  Starbucks
//
//  Created by Adrián Caramés on 23/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStore+Extras.h"

#import "OAppDelegate.h"
#import "OMyFavorites.h"

@implementation OStore (Extras)


+ (NSFetchRequest *)storesInContextFetchRequest:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllStores" substitutionVariables:[NSDictionary dictionary]];
	
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/storesInContextFetch");
    }
}


+ (NSFetchRequest *)storesInContextFetchRequest:(NSManagedObjectContext *)_context withDistance:(NSString*)_distance
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllStoresInDistance" substitutionVariables:[NSDictionary dictionaryWithObject:_distance forKey:@"DISTANCE"]];
    
	return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/storesInContextFetch");
    }

}



+ (NSFetchRequest *) storesWithServicesFetchRequest:(NSArray *)_services context:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"StoresWithServices" substitutionVariables:[NSDictionary dictionaryWithObject:_services forKey:@"SERVICES"]];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/storesWithServices");
    }
}

+ (NSArray *)storesInContext:(NSManagedObjectContext *)_context
{
    @try {
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllStores" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/storesInContext");
    }
}

+(NSMutableArray *) storesByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSMutableArray * retorno = [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int i=0;i<[_favorites count];i++)
	{
	
		OMyFavorites * fav = (OMyFavorites*) [_favorites objectAtIndex:i];
		OStore * store = [OStore storeByName:fav.name inContext:_context];
		[retorno addObject:store];
	
	}

	return retorno;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/storesByNames");
    }
}

+(OStore*) storeByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"StoreWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_name forKey:@"STORENAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/storeByname");
    }
}


- (NSArray *) schedulesArray
{
    @try{
	return [self.schedules sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OStore/schedulesArray");
    }
}

@end
