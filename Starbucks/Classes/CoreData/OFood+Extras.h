//
//  OFood+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFood.h"

@interface OFood (Extras)

+ (NSFetchRequest *)foodInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSArray *)foodInContext:(NSManagedObjectContext *)_context;
+ (NSFetchRequest *)foodWithCategory:(NSString *)_categoryName context:(NSManagedObjectContext *)_context;
+ (NSFetchRequest *)foodWithCategory:(NSString *)_categoryName andAlergenos:(NSMutableArray*) _alergenos context:(NSManagedObjectContext *)_context;
+ (NSArray *)categoriesOfFoodInContext:(NSManagedObjectContext *)_context;
+(NSMutableArray *) foodsByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context;
+(OFood*) foodByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context;
@end
