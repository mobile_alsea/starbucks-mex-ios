//
//  OImagesDownloaded+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OImagesDownloaded+Extras.h"
#import "OAppDelegate.h"
@implementation OImagesDownloaded (Extras)


+ (OImagesDownloaded *)imageWithName:(NSString *)_imageName context:(NSManagedObjectContext *)_context
{
    
    NSFetchRequest *fetchRequest=nil;
    NSArray *fetchedArray=nil;
    
   @try{ 
	fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"ImageWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_imageName forKey:@"IMAGENAME"]];
	NSError *error = nil;
	fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
   
    
	
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/OImages/Oimagesdownloaded");
    }
    return [fetchedArray lastObject];
}

@end
