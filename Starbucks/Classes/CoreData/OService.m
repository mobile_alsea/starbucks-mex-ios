//
//  OService.m
//  Starbucks
//
//  Created by Isabelo Pamies López on 29/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OService.h"
#import "OStore.h"


@implementation OService

@dynamic isSelected;
@dynamic name;
@dynamic store;

@end
