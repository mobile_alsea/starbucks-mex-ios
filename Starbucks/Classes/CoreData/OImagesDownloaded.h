//
//  OImagesDownloaded.h
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OImagesDownloaded : NSManagedObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSData * image;

@end
