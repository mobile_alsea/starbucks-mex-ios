//
//  OFood.h
//  Starbucks
//
//  Created by Santi Belloso López on 14/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OAlergenos, OCombinations;

@interface OFood : NSManagedObject

@property (nonatomic, strong) NSString * availability;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSData * imageIcon;
@property (nonatomic, strong) NSString * largedescrip;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * shortdescrip;
@property (nonatomic, strong) NSSet *alergenos;
@property (nonatomic, strong) NSSet *combinations;
@end

@interface OFood (CoreDataGeneratedAccessors)

- (void)addAlergenosObject:(OAlergenos *)value;
- (void)removeAlergenosObject:(OAlergenos *)value;
- (void)addAlergenos:(NSSet *)values;
- (void)removeAlergenos:(NSSet *)values;
- (void)addCombinationsObject:(OCombinations *)value;
- (void)removeCombinationsObject:(OCombinations *)value;
- (void)addCombinations:(NSSet *)values;
- (void)removeCombinations:(NSSet *)values;
@end
