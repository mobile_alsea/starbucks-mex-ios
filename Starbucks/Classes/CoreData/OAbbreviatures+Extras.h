//
//  OAbbreviatures+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 20/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OAbbreviatures.h"

@interface OAbbreviatures (Extras)
+ (NSFetchRequest *)AbbreviaturesInContextFetchRequest:(NSManagedObjectContext *)_context;
+ (NSArray *)AbbreviaturesInContext:(NSManagedObjectContext *)_context;
+(OAbbreviatures*) AbbreviaturesByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context;
@end
