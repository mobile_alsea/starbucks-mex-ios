//
//  OMyFavorites.m
//  Starbucks
//
//  Created by Santi Belloso López on 13/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyFavorites.h"


@implementation OMyFavorites

@dynamic name;
@dynamic order;
@dynamic type;
@dynamic typeOrder;
@dynamic nickname;
@dynamic username;
@dynamic note;

@end
