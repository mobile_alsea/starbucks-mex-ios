//
//  OCoffee+Extras.m
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCoffee+Extras.h"
#import "OAppDelegate.h"
#import "OMyFavorites.h"

@implementation OCoffee (Extras)

+ (NSFetchRequest *)coffeesInContextFetchRequest:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllCoffees" substitutionVariables:[NSDictionary dictionary]];
    
    return fetchRequest;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/coffesInContextFetch");
    }
}


+ (NSArray *)coffeesInContext:(NSManagedObjectContext *)_context
{
    @try{
    NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"AllCoffees" substitutionVariables:[NSDictionary dictionary]];
    
    NSError *error = nil;
    NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedArray != nil && [fetchedArray count] > 0)
        return fetchedArray;
    else
    {
        NSLog(@"No results");
        return nil;
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/coffesInContext");
    }
}

+ (NSArray *)categoriesOfCoffeesInContext:(NSManagedObjectContext *)_context
{
    @try{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Coffees" inManagedObjectContext:_context];
	request.entity = entity;
	request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"profile"]];
	request.returnsDistinctResults = YES;
	request.resultType = NSDictionaryResultType;
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"profile" ascending:YES];
	[request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	NSError *error = nil;
	NSArray *distincResults = [_context executeFetchRequest:request error:&error];
	// Use the results
	
	return  distincResults;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/categoriesOfCoffesinContext");
    }

}


+(NSMutableArray *) coffeesByNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSMutableArray * retorno = [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int i=0;i<[_favorites count];i++)
	{
		
		OMyFavorites * fav = (OMyFavorites*) [_favorites objectAtIndex:i];
		OCoffee * coffee = [OCoffee coffeeByName:fav.name inContext:_context];
		[retorno addObject:coffee];
		
	}
	
	return retorno;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/coffessbynames");
    }

}

+(NSMutableArray *) coffeesByCombinationNames:(NSArray*)_favorites inContext:(NSManagedObjectContext*)_context
{
    NSMutableArray * retorno=nil;
    @try{
	 retorno= [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int i=0;i<[_favorites count];i++)
	{
		
		
		OCoffee * coffee = [OCoffee coffeeByName:[_favorites objectAtIndex:i] inContext:_context];
		[retorno addObject:coffee];
		
	}
	
	
    }
    
    @catch (NSException *ex) {
        retorno=nil;
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/coffesbycombinationNames");
    }
    return retorno;
	
}


+(OCoffee*) coffeeByName:(NSString*)_name inContext:(NSManagedObjectContext*)_context
{
    @try{
	NSFetchRequest *fetchRequest = [[OAppDelegate managedObjectModel] fetchRequestFromTemplateWithName:@"CoffeeWithName" substitutionVariables:[NSDictionary dictionaryWithObject:_name forKey:@"COFFEENAME"]];
	NSError *error = nil;
	NSArray *fetchedArray = [_context executeFetchRequest:fetchRequest error:&error];
	
    return [fetchedArray lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/CoreData/Ocoffee/coffeByName");
    }

}


@end
