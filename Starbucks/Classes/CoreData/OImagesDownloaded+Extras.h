//
//  OImagesDownloaded+Extras.h
//  Starbucks
//
//  Created by Santi Belloso López on 12/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OImagesDownloaded.h"

@interface OImagesDownloaded (Extras)
+ (OImagesDownloaded *)imageWithName:(NSString *)_imageName context:(NSManagedObjectContext *)_context;
@end
