//
//  OStoresAdressCell.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresAdressCell.h"

@implementation OStoresAdressCell



#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


#pragma mark - Properties

@synthesize nameLabel = mNameLabel;
@synthesize addressLabel = mAddressLabel;
@synthesize areaLabel = mAreaLabel;
@synthesize cityLabel = mCityLabel;
@synthesize postalcodeLabel = mPostalCodeLabel;


#pragma mark - UITableViewCell Methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
