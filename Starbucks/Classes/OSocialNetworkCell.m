//
//  OSocialNetworkCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OSocialNetworkCell.h"
#import "OStoresDetailViewController.h"

@implementation OSocialNetworkCell
#define RGBToFloat(f) (f/255.0)
#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		// Initialization code
	}
	return self;
}


#pragma mark - Properties

@synthesize titleLabel = mTitleLabel;
@synthesize iconImageView = mIconImageView;
@synthesize disclosureImageView = mDisclosureImageView;
@synthesize delegate = mDelegate;
@synthesize tag = mTag;

@synthesize imageView = mImageView;
@synthesize myswitch = mMySwitch;
@synthesize StateOnLabel=mStatusOnLabel;


- (BOOL) showDisclosure
{
    return mShowDisclosure;
}

- (void) setShowDisclosure:(BOOL)_showDisclosure
{
    @try{
    mShowDisclosure = _showDisclosure;
    
    if (mShowDisclosure)
        [mDisclosureImageView setHidden:NO];
    else
        [mDisclosureImageView setHidden:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSocialNetworkCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}


#pragma mark IBACtions

-(IBAction)buttonPressed:(id)sender
{	
	if(mTag==10)
	{
		[(OStoresDetailViewController *)mDelegate addToMyStores:nil];
	}
}


#pragma mark - ViewController Methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

@end

