//
//  ODrinksCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODrink.h"
#import "DYImageView.h"
#import "ODrinkSubcategoryViewController.h"
@interface ODrinksCell : UITableViewCell
{
	DYImageView * mThumbnailImageView;
	UIButton * mAvailableButton;
	UILabel * mTitleLabel;
	BOOL mIndexPathPair;
	int mIndex;
	ODrinkSubcategoryViewController * mDelegate;
	
}

@property (nonatomic, strong) IBOutlet DYImageView * thumbnailImageView;
@property (nonatomic, strong) IBOutlet UIButton * avalaibleButton;
@property (nonatomic, strong) IBOutlet UILabel * titleLabel;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;
@property (nonatomic, strong) ODrinkSubcategoryViewController * delegate;


- (void)updateSchedule:(int)_state;
-(IBAction)makeDrinkButtonPressed:(id)sender;
@end
