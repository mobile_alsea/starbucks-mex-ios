//
//  OStoresViewController.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ADClusterMapView.h"
#import "OAppDelegate.h"
#import "OBaseViewController.h"
#import "OStoresFiltersViewController.h"
#import "ARGeoViewController.h"

typedef enum {
    OSControllerModeNone = 0,
    OSControllerModeFilter,
    OSControllerModeDetail,
    OSControllerModeAR
} OSControllerMode;

#define BOX_WIDTH 150
#define BOX_HEIGHT 100

#define VIEWPORT_WIDTH_RADIANS .5
#define VIEWPORT_HEIGHT_RADIANS .7392

@interface OStoresViewController : OBaseViewController
<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,
UISearchBarDelegate, ADClusterMapViewDelegate, CLLocationManagerDelegate,
UIAccelerometerDelegate, ARViewDelegate, OStoresFiltersViewControllerDelegate>
{
    OAppDelegate *mAppDelegate;
	UINavigationBar *mNavBar;
	UISearchBar  *mSearchBar;
    UISegmentedControl *mSegmentedControl;
    NSPredicate *mPredicate;
	
	CLLocation *mLastUserLocation;
    
    NSMutableArray *mServicesArray;
	BOOL mIsActualizedOneTime;
	BOOL mIsActualizeView;
}

@property (nonatomic, strong) NSMutableArray *servicesArray;
@property (nonatomic, assign) BOOL isActualizeView;
@property (nonatomic, strong) IBOutlet UISearchBar	*searchBar;
@property (nonatomic, strong) IBOutlet UINavigationBar	*navBar;


- (IBAction) btnUserLocationSelected:(id)_sender;
- (void) reloadMapAnnotations;
- (void) executeFetch;

@end
