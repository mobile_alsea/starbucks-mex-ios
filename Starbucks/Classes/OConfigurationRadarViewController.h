//
//  OConfigurationRadarViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"


@class DNScrollSlider;
	
@interface OConfigurationRadarViewController : OBaseViewController
{
	DNScrollSlider *shotCountSlider;
}


@property (nonatomic, strong) IBOutlet DNScrollSlider *shotCountSlider;

- (IBAction)shotCountChanged:(DNScrollSlider *)sender;


@end
