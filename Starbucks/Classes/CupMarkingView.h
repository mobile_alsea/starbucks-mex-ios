//
//  CupMarkingView.h
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CupMarkingView : UIView {
	UILabel *label;
	BOOL strikethrough;
	UIImageView *strikethroughView;
}

@property (nonatomic) BOOL strikethrough;

@property (strong, nonatomic, readonly) UILabel *label;

@end
