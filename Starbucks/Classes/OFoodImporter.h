//
//  OFoodImporter.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBaseImporter.h"

@interface OFoodImporter : OBaseImporter
{

	NSMutableDictionary *mCurrentFoodDict;
    NSMutableDictionary *mCurrentAlergenoDict;
    NSMutableDictionary *mCurrentCombinationDict;
    
    NSMutableArray *mFoodAlergenoArray;
    NSMutableArray *mFoodCombinationArray;
	
    BOOL mIsAlergeno;
    BOOL mIsCombination;
}

- (void) insertIntoContext:(NSDictionary *)_item;

@end
