//
//  OOptionsCodeViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OOptionsCodeViewController.h"
#import "OGenericCell.h"
#import <DYCore/files/DYFileNameManager.h>
#import "OSetPassCodeViewController.h"
@interface OOptionsCodeViewController ()

@end

@implementation OOptionsCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OWebViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:NSLocalizedString(@"Opciones Código", nil)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - UITableView Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 40;
}



- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	static NSString *CellIdentifier = @"OGenericCell";
    
    OGenericCell *cell = (OGenericCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
		
    }
    
    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
	
	
	switch (_indexPath.row) 
	{
		case 0:
		{
			cell.titleLabel.text = @"Deshabilitar Código";
			[cell setShowDisclosure:NO];
		}
			break;
		case 1:
		{
			cell.titleLabel.text = @"Cambiar Código";
			[cell setShowDisclosure:YES];
		}
			break;
		default:
			break;
	}
	
	
        
    return cell;
	return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OWebViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
    
	
	
	switch (_indexPath.row) 
	{
		case 0:
		{
			[[NSUserDefaults standardUserDefaults] setObject:@"-1" forKey:@"Code"];
			[[NSUserDefaults standardUserDefaults] synchronize];

			[self.navigationController popToRootViewControllerAnimated:YES];
		}
			break;
		case 1:
		{
			//Invocar a llamar crear codigo.
			OSetPassCodeViewController *detailViewController = [[OSetPassCodeViewController alloc] initWithNibName:@"OSetPassCodeViewController" bundle:nil];
			detailViewController.isNew = YES;
			detailViewController.passwordSavedInFile=@"";
			[self.navigationController pushViewController:detailViewController animated:YES];
		}
			break;
		default:
			break;
	}
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OWebViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
	
}




@end
