//
//  OMyFavoritesCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMyFavoritesCell.h"
#import "OMyFavoritesViewController.h"

#define RGBToFloat(f) (f/255.0)
@implementation OMyFavoritesCell

@synthesize  titleLabel = mTitleLabel;
@synthesize  howManyLabel = mHowManyLabel;
@synthesize  iconImageView = mIconImageView;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
   
    
    if (_indexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}

@end
