//
//  OStoresPersonDetailViewController.h
//  Starbucks
//
//  Created by Isabelo Pamies López on 30/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "OBaseViewController.h"

@class DYVerticalAligmentLabel, OStore;

@interface OStoresPersonDetailViewController : OBaseViewController
{
	UILabel						*mNameLabel;
	DYVerticalAligmentLabel		*mAddressLabel;
	UIImageView	*mPhotoView;
	
	UILabel		*mEmailLabel;
	UILabel		*mPhoneLabel;
	
	ABRecordRef mPerson;
	OStore		*mStore;
	
	IBOutlet UITableView * mTableView;
	int mNumberOfPhones;
	int mNumberOfMails;
	int mInternalCount;
	NSString * mNumberPhone;
	
}

@property (nonatomic, strong) IBOutlet UILabel						*nameLabel;
@property (nonatomic, strong) IBOutlet DYVerticalAligmentLabel		*addressLabel;
@property (nonatomic, strong) IBOutlet UIImageView	*photoView;
@property (nonatomic, strong) IBOutlet UILabel		*emailLabel;
@property (nonatomic, strong) IBOutlet UILabel		*phoneLabel;

- (void) setStore:(OStore *)_store;
- (void) setPerson:(ABRecordRef) person;
- (void)sendActionButtonPressed:(id)sender withType:(int) type andIndex:(int)index;


- (IBAction)actionSendMail:(int) index;
- (IBAction)actionSendSMS: (int) index; 
- (IBAction)actionCallPhone: (int) index;

@end

@interface OStoresPersonDetailViewController (MessageComposeDelegateMethods) <MFMessageComposeViewControllerDelegate> 
@end

@interface OStoresPersonDetailViewController (MailComposeDelegateMethods) <MFMailComposeViewControllerDelegate> 
@end
