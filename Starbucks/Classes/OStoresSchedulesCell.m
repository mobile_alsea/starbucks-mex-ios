//
//  OStoresSchedulesCell.m
//  Starbucks
//
//  Created by Adrián Caramés on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresSchedulesCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation OStoresSchedulesCell


#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


#pragma mark - Properties

@synthesize dayLabel = mDayLabel;
@synthesize scheduleLabel = mScheduleLabel;

- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}


#pragma mark - UITableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
