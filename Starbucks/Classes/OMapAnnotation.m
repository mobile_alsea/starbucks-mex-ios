//
//  OMapAnnotation.m
//  Starbucks
//
//  Created by Adrián Caramés Ramos on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMapAnnotation.h"

@implementation OMapAnnotation


#pragma mark - MKAnnotation Methods

- (CLLocationCoordinate2D)coordinate {
    CLLocationCoordinate2D location;
    location.latitude = [self.storeInfo[@"latitude"] doubleValue];
	location.longitude = [self.storeInfo[@"longitude"] doubleValue];
    return location;
}

- (NSString *)title {
    return self.storeInfo[@"name"];
}

- (NSString *)subtitle {
    return self.storeInfo[@"address"];
}


@end
