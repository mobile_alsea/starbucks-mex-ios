//
//  OMyFavoritesCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 06/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OMyFavoritesViewController.h"
@interface OMyFavoritesCell : UITableViewCell
{
	UILabel * mTitleLabel;
	UILabel * mHowManyLabel;
	UIImageView * mIconImageView;



}

@property(nonatomic, strong) IBOutlet UILabel * titleLabel;
@property(nonatomic, strong) IBOutlet UILabel * howManyLabel;
@property(nonatomic, strong) IBOutlet UIImageView * iconImageView;


- (void) setIndexPathPair:(BOOL)_indexPathPair;

@end
