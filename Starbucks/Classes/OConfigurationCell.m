//
//  OConfigurationCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 27/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OConfigurationCell.h"
#import <DYCore/files/DYFileNameManager.h>
#import "OStoresDetailViewController.h"

@implementation OConfigurationCell

#define RGBToFloat(f) (f/255.0)
#pragma mark - Constructors

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
if (self)
{
	// Initialization code
}
return self;
}


#pragma mark - Properties

@synthesize titleLabel = mTitleLabel;
@synthesize iconImageView = mIconImageView;
@synthesize disclosureImageView = mDisclosureImageView;
@synthesize delegate = mDelegate;
@synthesize tag = mTag;
@synthesize nickNameTextField = mNickNameTextField;


- (BOOL) showDisclosure
{
    return mShowDisclosure;
}

- (void) setShowDisclosure:(BOOL)_showDisclosure
{
    mShowDisclosure = _showDisclosure;
    
    if (mShowDisclosure)
        [mDisclosureImageView setHidden:NO];
    else
        [mDisclosureImageView setHidden:YES];
}

- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}


#pragma mark IBACtions

-(IBAction)buttonPressed:(id)sender
{
	@try{
	if(mTag==10)
	{
       
		[(OStoresDetailViewController *)mDelegate addToMyStores:nil];		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oConfigurationCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - ViewController Methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

@end

