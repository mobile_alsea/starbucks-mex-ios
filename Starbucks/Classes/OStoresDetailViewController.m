//
//  OStoresDetailViewController.m
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresDetailViewController.h"
#import "OGenericCell.h"
#import "OStoresViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "OStoresAdressCell.h"
#import "OStoresScheduleViewController.h"
#import "OStoresPersonDetailViewController.h"
#import "ONotImplementedViewController.h"
#import "OMYFavorites+Extras.h"
#import "OMyFavorites.h"
#import "OSchedule.h"
#import "OHomeViewController.h"

#import "ARCameraViewController.h"

@interface OStoresDetailViewController ()

- (void) showSharingLocation;
- (void) showInviteToFriend;

@end


#define RGBToFloat(f) (f/255.0)

@implementation OStoresDetailViewController


#pragma mark - Constructors

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - Properties
@synthesize storeSelected = mStoreSelected;


#pragma mark - ViewController Methods

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];

    [self.navigationItem setTitle:NSLocalizedString(@"Detalle de Tienda", nil)];
	
	mFavorite =[OMyFavorites favoriteWithName:mStoreSelected.name context:mAddingContext];
	
	if(mFavorite!=nil)
	{ 
		mIsAdded = YES;
		[mAddingContext deleteObject:mFavorite];
		mAddStore = @"Eliminar de mis Tiendas";
	}
	else
	{
		mIsAdded = NO;
		mAddStore = @"Añadir a mis Tiendas";
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if(self.comesFromModal) {
		CGRect tableFrame = CGRectInset(self.view.bounds, 10.0, 10.0);
		mTableView.frame = tableFrame;
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark IBActions

-(void)addToMyStores:(id)sender
{
    @try{
	if(mIsAdded==YES)
	{
		//Delete from bd
		
		[mAddingContext deleteObject:mFavorite];
		mAddStore = @"Añadir a mis Tiendas";
		
		((OGenericCell *) [mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]]).titleLabel.text = mAddStore;
		NSError * error = nil;
		[mAddingContext save:&error];
		mIsAdded = NO;
		
	}
	else
	{
		NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyFavorites" inManagedObjectContext:mAddingContext];
		
		mFavorite = [[OMyFavorites alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:mAddingContext];
		mFavorite.type = @"1";
		mFavorite.name = mStoreSelected.name;
		
		NSError * error = nil;
		[mAddingContext save:&error];
		mAddStore = @"Eliminar de mis Tiendas";
		((OGenericCell *) [mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]]).titleLabel.text = mAddStore;
		mIsAdded = YES;
		
		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - Private Methods

- (void)btnDoneSelected:(id)_sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void) showSharingLocation
{
    @try{
	UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:@"Compartir mi ubicación" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil 
											   otherButtonTitles:@"Publicar en Facebook", @"Publicar en Twitter", @"Envía un mensaje",nil];
	
	[asheet showInView:self.view];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) showInviteToFriend
{
    @try{
	ABPeoplePickerNavigationController *peoplePickeNavController = [[ABPeoplePickerNavigationController alloc] init];
	[peoplePickeNavController.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
	peoplePickeNavController.searchDisplayController.searchBar.tintColor = [UIColor blackColor];
	peoplePickeNavController.peoplePickerDelegate = self;


        [self presentViewController:peoplePickeNavController animated:YES completion:nil];
        
        
	peoplePickeNavController.navigationBar.topItem.title = @"Todos los contactos";
        

        
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    if ([mStoreSelected.services count] > 0)
        return 3;
    else
        return 2;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{    
    switch (_section)
    {
        case 0:
            return 3;
            break;
        case 1:
            return 2;
            break;
        case 2:
            return [mStoreSelected.services count];
            break;
        default:
            return 0;
            break;
    };
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    id cell;
    static NSString *CellIdentifier;
    
    if (_indexPath.section == 0 && _indexPath.row == 0)
    {
        CellIdentifier = @"OStoresAdressCell";
        cell = (OStoresAdressCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    else
    {
        CellIdentifier = @"OGenericCell";
        cell = (OGenericCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
        //        cell.textField.delegate = self;
    }
    
    if (_indexPath.section != 0 || _indexPath.row != 0)
        [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
    
    switch (_indexPath.section)
    {
        case 0:
            switch (_indexPath.row)
            {
                case 0:
                    [[(OStoresAdressCell *)cell nameLabel] setText:mStoreSelected.name];
                    [[(OStoresAdressCell *)cell addressLabel] setText:mStoreSelected.address];
                    [[(OStoresAdressCell *)cell areaLabel] setText:mStoreSelected.area];
                    [[(OStoresAdressCell *)cell cityLabel] setText:mStoreSelected.city];
                    [[(OStoresAdressCell *)cell postalcodeLabel] setText:mStoreSelected.postalCode];
                    break;
                case 1:
                    [[(OGenericCell *)cell titleLabel] setText:NSLocalizedString(@"Horarios", nil)];
                    [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:@"Hours.png"]];
                    [(OGenericCell *)cell setShowDisclosure:YES];
                    break;
                case 2:
                    [[(OGenericCell *)cell titleLabel] setText:NSLocalizedString(@"Obtener dirección", nil)];
                    [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:@"Directions.png"]];
                    [(OGenericCell *)cell setShowDisclosure:YES];
                    break;
                case 3:
                    /*[[(OGenericCell *)cell titleLabel] setText:NSLocalizedString(@"Mejorar info de la tienda", nil)];
                    [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:@"Feedback.png"]];
                    [(OGenericCell *)cell setShowDisclosure:NO];
                    break;*/
                    default:
                        break;
            }
            break;
        case 1:
            switch (_indexPath.row)
            {
                case 0:
                    [[(OGenericCell *)cell titleLabel] setText:NSLocalizedString(@"Invitar a un Amigo", nil)];
                    [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:@"InviteFriend.png"]];
                    [(OGenericCell *)cell setShowDisclosure:YES];
                    break;
               /* case 1:
                    [[(OGenericCell *)cell titleLabel] setText:NSLocalizedString(@"Compartir mi Ubicación", nil)];
                    [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:@"ShareMyLocation.png"]];
                    [(OGenericCell *)cell setShowDisclosure:YES];
                    break;*/
                case 1:
                    [[(OGenericCell *)cell titleLabel] setText:mAddStore];
                    [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:@"My.png"]];
                    [(OGenericCell *)cell setShowDisclosure:NO];
					((OGenericCell *)cell).tag = 10;
					((OGenericCell *)cell).delegate = self;
                    
                    break;

                default:
                    break;
            }
            break;
        case 2:
            
            [[(OGenericCell *)cell titleLabel] setText:[[[mStoreSelected.services allObjects] objectAtIndex:_indexPath.row] name]];
            [(OGenericCell *)cell setShowDisclosure:NO];
			[(OGenericCell*) cell setUserInteractionEnabled:NO];

            [[(OGenericCell *)cell iconImageView] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Amenity%@.png",[[[[mStoreSelected.services allObjects] objectAtIndex:_indexPath.row] name] stringByReplacingOccurrencesOfString:@" " withString:@""]]]];
            
            NSLog(@"%@",[NSString stringWithFormat:@"Amenity%@.png",[[[[mStoreSelected.services allObjects] objectAtIndex:_indexPath.row] name] stringByReplacingOccurrencesOfString:@" " withString:@""]]);
            break;
        default:
            break;
    }
    
    return cell;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    
    @try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
    
    //    OStoresDetailViewController *detailViewController = [[OStoresDetailViewController alloc] initWithNibName:@"OStoresDetailViewController" bundle:nil];
    //    [self.navigationController pushViewController:detailViewController animated:YES];
    //    [detailViewController release];
    
    if (_indexPath.section == 0 && _indexPath.row == 1)
    {
        OStoresScheduleViewController *storeScheduleController = [[OStoresScheduleViewController alloc] initWithNibName:@"OStoresScheduleViewController" bundle:nil];
		storeScheduleController.comesFromModal = self.comesFromModal;
        [self.navigationController pushViewController:storeScheduleController animated:YES];
        storeScheduleController.storeSelected = mStoreSelected;
    }
    else if (_indexPath.section == 0 && _indexPath.row == 2)
    {NSString* versionNum = [[UIDevice currentDevice] systemVersion];
        NSString *nativeMapScheme = @"maps.apple.com";
        if ([versionNum compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
            nativeMapScheme = @"maps.google.com";
        
        NSString *mapUrl =nil;
		
		CLLocation *userLocation = nil;
		if([self.delegate respondsToSelector:@selector(storesDetailControllerRequestLocation:)])
			userLocation = [self.delegate storesDetailControllerRequestLocation:self];
		
        if(userLocation != nil)
        {
            mapUrl= [NSString stringWithFormat:@"http://%@/maps?saddr=%f,%f&daddr=%f,%f",nativeMapScheme, [userLocation coordinate].latitude, [userLocation coordinate].longitude, [mStoreSelected.latitude doubleValue], [mStoreSelected.longitude doubleValue]];
        }
        else{
            mapUrl= [NSString stringWithFormat:@"http://%@/geo?q=%f,%f",nativeMapScheme,  [mStoreSelected.latitude doubleValue], [mStoreSelected.longitude doubleValue]];
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mapUrl]];
    }
	else if (_indexPath.section == 1 && _indexPath.row == 0)
	{
		[self showInviteToFriend];
	}
	else if (_indexPath.section == 1 && _indexPath.row == 1)
	{
		[self addToMyStores:nil];
	}
	/*else if (_indexPath.section == 1 && _indexPath.row == 1)
	{
		[self showSharingLocation];
	}*/
	
	/*
    else
    {
        ONotImplementedViewController *notImplementedController = [[ONotImplementedViewController alloc] initWithNibName:@"ONotImplementedViewController" bundle:nil];
        [self.navigationController pushViewController:notImplementedController animated:YES];
        [notImplementedController release];
    }
	*/
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/stores/OStoresDetailViewController/tableView");
    
   
    
        UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
    }
}

-(CGFloat)tableView:(UITableView *)_tableView heightForHeaderInSection:(NSInteger)_section
{
    switch (_section)
    {
        case 0:
            return 0;
            break;
        case 1:
            return 20;
            break;
        case 2:
            return 50;
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    if (_indexPath.section == 0 && _indexPath.row == 0)
        return 80;
    else
        return 44;
}

-(UIView *)tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)_section
{
    @try{
    UIView *headerView = [[UIView alloc] init];
    [headerView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(30.0) green:RGBToFloat(30.0) blue:RGBToFloat(30.0) alpha:1.0]];
    
    switch (_section)
    {
        case 0:
            return nil;
            break;
        case 1:{
            [headerView setFrame:CGRectMake(0, 0, 300, 20)];
            return headerView;
        }break;
        case 2:{
            [headerView setFrame:CGRectMake(0, 0, 300, 50)];
            UILabel *servicesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 150, 40)];
            [servicesLabel setText:NSLocalizedString(@"SERVICIOS", nil)];
            [servicesLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
            [servicesLabel setTextAlignment:UITextAlignmentCenter];
            [servicesLabel setTextColor:[UIColor colorWithRed:RGBToFloat(30.0) green:RGBToFloat(30.0) blue:RGBToFloat(30.0) alpha:1.0]];
            [headerView addSubview:servicesLabel];
            return headerView;;
        }break;
            
        default:
            return nil;
            break;
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


@end


@implementation OStoresDetailViewController (ActionSheetDelegateMethods)

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
	
}

@end

@implementation OStoresDetailViewController (PeoplePickerNavigationControllerDelegateMethods)

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    @try{
	OStoresPersonDetailViewController *vcontroller = [[OStoresPersonDetailViewController alloc] initWithNibName:@"OStoresPersonDetailViewController" bundle:nil];
	[vcontroller setStore:mStoreSelected];
	[vcontroller setPerson:person];
	[self.navigationController pushViewController:vcontroller animated:YES];
	
	[self dismissModalViewControllerAnimated:YES];
	
	return NO;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresDetailViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
	[self dismissModalViewControllerAnimated:YES];
}

@end