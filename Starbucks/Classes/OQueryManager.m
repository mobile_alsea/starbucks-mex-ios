//
//  OQueryManager.m
//  Starbucks
//
//  Created by Santi Belloso López on 27/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OQueryManager.h"
#import <DYCore/categories/regex/NSString_regex.h>
#import "AppDelegate.h"

static NSString *kServiceURL = @"http://api.starbucks.mx:8080/replicador/services/AlseaValueImpWS.AlseaValueImpWSHttpSoap12Endpoint/";

static NSString *kRequestBody = @"<?xml version=\"1.0\" encoding=\"utf-8\"?><soapenv:Envelope xmlns:impl=\"http://impl.service.replicador.alsea.com.mx\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Header><wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsse:UsernameToken wsu:Id=\"UsernameToken-22\"><wsse:Username>bob</wsse:Username><wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">bobPW</wsse:Password><wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">moDQbMH0JeBhfD9y1HR/g==</wsse:Nonce><wsu:Created>2012-09-26T05:47:08.758Z</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><impl:%@>%@<impl:password>?</impl:password><impl:Uid>12345678912345678912345</impl:Uid></impl:%@></soapenv:Body></soapenv:Envelope>";

static NSString *kResponseSuffix = @"Response";


@interface OQueryManager (privateMethods)

- (void) queryService:(NSString *)_service params:(NSDictionary *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate;

@end

@implementation OQueryManager

@synthesize datasource = mDataSource;

+ (void) queryService:(NSString *)_service params:(NSDictionary *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate
{
    @try{
	[[OQueryManager sharedInstance] queryService:_service params:_params delegate:_delegate];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OQueryManager.m/queryService");
    }
}


+ (void) queryServiceWithData:(NSString *)_service params:(NSData *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate
{
	@try{
	[[OQueryManager sharedInstance] queryServiceWithData:_service params:_params delegate:_delegate];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OQueryManager.m/queryServiceWithData");
    }
}



- (id) init
{
    @try{
	self = [super init];
	if (self != nil)
	{
		DYSOAPManager *instance = [DYSOAPManager sharedInstance];
		instance.dataSource = self;
		instance.preprocessDelegate = self;
	}
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OQueryManager.m/init");
    }
}


@end


@implementation OQueryManager (OSOAPManagerDataSource)



- (NSString*) SOAPBaseURLForQuery:(NSString*)_query
{
	return kServiceURL;
	
}

- (NSString*) SOAPQueryFormatForQuery:(NSString*)_query
{
	return kRequestBody;
	
	
}



@end


@implementation OQueryManager (OSOAPManagerPreprocessDelegate)

- (NSDictionary*) preprocessResponse:(NSDictionary*)_response forQuery:(NSString*)_query
{
	return [[[[_response objectForKey:@"soapenv:Envelope"] objectForKey:@"soapenv:Body"] objectForKey:[NSString stringWithFormat:@"ns:%@%@", _query, kResponseSuffix]] objectForKey:@"ns:return"];
	
}



- (BOOL) shouldCacheResponse:(NSDictionary*)_response forQuery:(NSString*)_query
{
	return NO;
	
}

@end


@implementation OQueryManager (privateMethods)

- (void) queryService:(NSString *)_service params:(NSDictionary *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate
{
    @try{
	
	[DYSOAPManager callFunction:_service params:_params delegate:_delegate];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OQueryManager.m/queryservices");
    }
}

- (void) queryServiceWithData:(NSString *)_service params:(NSData *) _params delegate:(id<DYSOAPManagerDelegate>)_delegate
{
	@try{
	[DYSOAPManager callFunction:_service data:_params delegate:_delegate];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/DataModel/OQueryManager.m/queryServiceWithData");
    }
}






@end

