//
//  OConfigurationRadarViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OConfigurationRadarViewController.h"
#import "DNScrollSlider.h"
#import <DYCore/files/DYFileNameManager.h>
@interface OConfigurationRadarViewController ()

@end

@implementation OConfigurationRadarViewController
@synthesize shotCountSlider;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationRadarViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	   [self.navigationItem setTitle:NSLocalizedString(@"Localizador", nil)];
	NSMutableArray *numbers = [[NSMutableArray alloc] initWithObjects:@"No filtrar",@"1 km",@"2 km",@"5 km",@"10 km",@"20 km",@"50 km",@"100 km",@"1000 km", nil];
	shotCountSlider.valueNames = numbers;

	
	NSString * readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"km"];
		
	
	if(readValue==nil || [readValue isEqualToString:@"-1"])shotCountSlider.valueIndex= 0;
	if([readValue isEqualToString:@"1"])shotCountSlider.valueIndex= 1;
	if([readValue isEqualToString:@"2"])shotCountSlider.valueIndex= 2;
	if([readValue isEqualToString:@"5"])shotCountSlider.valueIndex= 3;
	if([readValue isEqualToString:@"10"])shotCountSlider.valueIndex= 4;
	if([readValue isEqualToString:@"20"])shotCountSlider.valueIndex= 5;
	if([readValue isEqualToString:@"50"])shotCountSlider.valueIndex= 6;
	if([readValue isEqualToString:@"100"])shotCountSlider.valueIndex= 7;
	if([readValue isEqualToString:@"1000"])shotCountSlider.valueIndex= 8;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OConfigurationRadarViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)shotCountChanged:(DNScrollSlider *)sender {
	
	NSString * value = @"";
	switch (sender.valueIndex) {
		case 0:
			value = @"-1";
			break;
		case 1:
			value = @"1";
			break;
		case 2:
			value = @"2";
			break;
		case 3:
			value = @"5";
			break;
		case 4:
			value = @"10";
			break;
		case 5:
			value = @"20";
			break;
		case 6:
			value = @"50";
			break;
		case 7:
			value = @"100";
			break;
		case 8:
			value = @"1000";
			break;
		default:
			break;
	}
	
	[[NSUserDefaults standardUserDefaults] setObject:value forKey:@"km"];
	[[NSUserDefaults standardUserDefaults] synchronize];

}


@end
