//
//  OTwitterViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OTwitterViewController.h"
#import "OShareTwitter.h"
@interface OTwitterViewController ()

@end

@implementation OTwitterViewController
@synthesize drinkSelected = mDrinkSelected;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideModal:) name:@"Starbucks-SOCIAL-SEND" object:nil];
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OTwitterViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"Starbucks-SOCIAL-SEND" object:nil];

}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	/*
	 mPreString = [[NSString alloc] initWithFormat:@"%@ 01 Acabo de guardar mi bebida %@ de Starbucks Coffee mediante su aplicación móvil http://goo.gl/r2WGv",mTextView.text,mDrinkSelected.name];
	mCounterLabel.text = [NSString stringWithFormat:@"%d",150-mPreString.length];
	*/

	[self.navigationItem setTitle:NSLocalizedString(@"facebook", nil)];
	UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Post", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(postToTwitter:)];
    [self.navigationItem setRightBarButtonItem:btnSave];
	
	
	
	UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cerrar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(hideModal:)];
    [self.navigationItem setLeftBarButtonItem:btnClose];
	
	UIImageView * mImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navbar_twitter.png"]];
	[self.navigationItem setTitleView:mImageView];
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OTwitterViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)hideModal:(id)sender
{
	
	[self dismissModalViewControllerAnimated:YES];
	
}

-(IBAction)postToTwitter:(id)sender
{
	
	[self sendByTwitter];
	
}


- (void) sendByTwitter
{
	SHKItem *item = [self itemToShare];	
	if(item!=nil)
		[OShareTwitter shareItem:item];
}

- (SHKItem *) itemToShare
{
    @try{
	SHKItem *item = nil;

    //MBT 10Dic2012
    //NSString * sended = [NSString stringWithFormat:@"%@. Guardé mi bebida %@ mediante la aplicación de Starbucks http://goo.gl/r2WGv",mTextView.text,mDrinkSelected.name];

	NSString * sended = [NSString stringWithFormat:@"%@. Acabo de guardar mi bebida %@ de Starbucks Coffee mediante su aplicación móvil.",mTextView.text,mDrinkSelected.name];

	
        
        item=[[SHKItem alloc]init];
	item.text = sended;
	item.title = mDrinkSelected.name;
        
              
	[item setCustomValue:item.text forKey:@"status"];
	[item setCustomValue:item.title forKey:@"title"];

	return item;
        

    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OTwitterViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}


- (BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)_range replacementText:(NSString *)_text
{
    
	//int counterCharacters = 150 -( _textView.text.length + mPreString.length + _text.length);
	
	if (([_textView.text length] + mPreString.length) <30 || _text.length == 0)
	{
		/*if(_text.length == 0)
		{
			mCounterLabel.text = [NSString stringWithFormat:@"%d", counterCharacters-1];
		
		}
		else 
		{
			mCounterLabel.text = [NSString stringWithFormat:@"%d", counterCharacters];
			
			
		}*/
		return YES;	
	}
	else
		return NO;
	
	

}

-(void) textViewDidChange:(UITextView *)_textView
{

	int counterCharacters = 30 -(_textView.text.length + mPreString.length );
	mCounterLabel.text = [NSString stringWithFormat:@"%d",counterCharacters];

}


@end
