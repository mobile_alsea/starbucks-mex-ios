//
//  OShareTwitter.m
//  Starbucks
//
//  Created by Santi Belloso López on 24/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OShareTwitter.h"	

#define kStatusSuccess      1
#define kStatusError       -1
#define kStatusNotSend      0
#define kStatusNotConfig   -2


@implementation OShareTwitter

+ (NSString *) twitterConsumerKey
{
	return @"7HUdeSHXeFSGb1Pxf5qrw";
}

+ (NSString *) twitterSecret
{
	return @"3LQsgvD46vEfh9wYs2UNQ9UKjtLWCHsU6SUtYy0g";
}

+ (NSString *) twitterCallbackUrl
{
	return @"http://starbucsPrueba.com";
}

+ (BOOL) twitterUseXAuth
{
	return NO;
}

+ (NSString *) twitterUsername
{
	return @"";
}

// Bit.ly (for shortening URLs on Twitter) - http://bit.ly/account/register - after signup: http://bit.ly/a/your_api_key
+ (NSString *) bitLyLogin
{
	return @"dylvianpruebasproduccion";
}

+ (NSString *) bitLyKey
{
	return @"R_31fb65f484dbd1871a754c6418a78bc0";
}

- (id)init
{
    @try{
	if (self = [super init])
	{	
		// OAUTH		
		self.consumerKey = [OShareTwitter twitterConsumerKey];		
		self.secretKey = [OShareTwitter twitterSecret];
 		self.authorizeCallbackURL = [NSURL URLWithString:[OShareTwitter twitterCallbackUrl]];// HOW-TO: In your Twitter application settings, use the "Callback URL" field.  If you do not have this field in the settings, set your application type to 'Browser'.
		
		// XAUTH
		self.xAuth = [OShareTwitter twitterUseXAuth];
		
		
		// -- //
		
		
		// You do not need to edit these, they are the same for everyone
	    self.authorizeURL = [NSURL URLWithString:@"https://api.twitter.com/oauth/authorize"];
	    self.requestURL = [NSURL URLWithString:@"https://api.twitter.com/oauth/request_token"];
	    self.accessURL = [NSURL URLWithString:@"https://api.twitter.com/oauth/access_token"]; 
		self.quiet = YES;
	}	
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Social Network/OshareTwitter.m/init");
    }
}

//- (NSURL *) authorizeCallbackURL
//{
//	return [NSURL URLWithString:[OShareTwitter twitterCallbackUrl]];
//}

+ (NSArray *)authorizationFormFields
{
    @try{
	if ([[OShareTwitter twitterUsername] isEqualToString:@""])
		return [super authorizationFormFields];
	
	return [NSArray arrayWithObjects:
			[SHKFormFieldSettings label:SHKLocalizedString(@"Username") key:@"username" type:SHKFormFieldTypeText start:nil],
			[SHKFormFieldSettings label:SHKLocalizedString(@"Password") key:@"password" type:SHKFormFieldTypePassword start:nil],
			[SHKFormFieldSettings label:SHKLocalizedString(@"Follow %@", [OShareTwitter twitterUsername]) key:@"followMe" type:SHKFormFieldTypeSwitch start:SHKFormFieldSwitchOn],			
			nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Social Network/OshareTwitter.m/authorizationFormFields");
    }
}

- (void)sendStatus
{
    @try{
	OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://api.twitter.com/1/statuses/update.json"]
																	consumer:consumer
																	   token:accessToken
																	   realm:nil
														   signatureProvider:nil];
	
	[oRequest setHTTPMethod:@"POST"];
	
	OARequestParameter *statusParam = [[OARequestParameter alloc] initWithName:@"status"
																		 value:[item customValueForKey:@"status"]];
	NSArray *params = [NSArray arrayWithObjects:statusParam, nil];
	[oRequest setParameters:params];
	
	OAAsynchronousDataFetcher *fetcher = [OAAsynchronousDataFetcher asynchronousFetcherWithRequest:oRequest
																						  delegate:self
																				 didFinishSelector:@selector(sendStatusTicket:didFinishWithData:)
																				   didFailSelector:@selector(sendStatusTicket:didFailWithError:)];	
	
	[fetcher start];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Social Network/OshareTwitter.m/sendStatus");
    }
}

- (BOOL) shouldAutoShare
{
	return YES;
}



#pragma mark -

- (void)shortenURL
{
    @try{
	if (![SHK connected])
	{
		[item setCustomValue:[NSString stringWithFormat:@"%@ %@", item.title, [item.URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forKey:@"status"];
		[self showTwitterForm];		
		return;
	}
	
	if (!quiet)
		[[SHKActivityIndicator currentIndicator] displayActivity:SHKLocalizedString(@"Shortening URL...")];
	
	self.request = [[SHKRequest alloc] initWithURL:[NSURL URLWithString:[NSMutableString stringWithFormat:@"http://api.bit.ly/v3/shorten?login=%@&apikey=%@&longUrl=%@&format=txt",
																		  [OShareTwitter bitLyLogin],
																		  [OShareTwitter bitLyKey],																		  
																		  SHKEncodeURL(item.URL)
																		  ]]
											 params:nil
										   delegate:self
								 isFinishedSelector:@selector(shortenURLFinished:)
											 method:@"GET"
										  autostart:YES];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Social Network/OshareTwitter.m/shortURL");
    }
}

- (void)sendDidFinish
{	
	[super sendDidFinish];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SOCIAL-SEND" object:nil];
}

- (void)sendDidFailWithError:(NSError *)error
{
	[super sendDidFailWithError:error];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SOCIAL-SEND" object:nil];
}


//- (void)sharer:(SHKSharer *)sharer failedWithError:(NSError *)error shouldRelogin:(BOOL)shouldRelogin
//{
//	[SHK logoutOfService:@"SHKTwitter"];
//	[SHK logoutOfService:@"OShareTwitter"];
//	[OShareTwitter logout];
//	[super sharer:sharer failedWithError:error shouldRelogin:shouldRelogin];
//}

@end
