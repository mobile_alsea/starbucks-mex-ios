//
//  ODrinksImporter.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinksImporter.h"
#import "OAppDelegate.h"
#import "ODrink.h"
#import "ODrink+Extras.h"
#import <DYCore/files/DYFileNameManager.h>

@implementation ODrinksImporter
#pragma mark private methods

- (NSData *) getData
{
    @try{
        
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kDrinkURL]];
	return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/getdata");
    }
    
}

- (void) didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDict
{
    @try{
    if ([_elementName isEqualToString:@"bebidas"])
    {
        // Remove old stores
		
        NSArray *drinks = [ODrink drinksInContext:mAddingContext];
        [mAppDelegate removeObjects:drinks fromContext:mAddingContext];
        [mAddingContext processPendingChanges];
		NSString *pListPath = [DYFileNameManager pathForFile:@"Identifier.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
		NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
		NSString * ReadValue = [dictionary valueForKey:@"id"];
		mId = [ReadValue intValue];
		
		
    }
	else if ([_elementName isEqualToString:@"bebida"])
	{
		mCurrentDrinkDict = [[NSMutableDictionary alloc] init];
        
        mIsShot = NO;
        mIsPersonalization = NO;
	}
	
    else if ([_elementName isEqualToString:@"personalizacion"])
    {
        mIsPersonalization = YES;
    }
	/*else if ([_elementName isEqualToString:@"shot"]) {
		
		mCurrentShotDict = [[NSMutableDictionary alloc] init];
	}
    else if ([_elementName isEqualToString:@"personalizaciones"])
    {
        mDrinkPersonalizationArray = [[NSMutableArray alloc] init];
        mIsPersonalization = YES;
    }
    else if ([_elementName isEqualToString:@"personalizacion"])
    {
        mCurrentDrinkDict = [[NSMutableDictionary alloc] init];
    }*/
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/didstartelement");
    }
	
}

- (void) didEndElement:(NSString *)_elementName withElementValue:(NSString *)_elementValue
{
    @try{
	if ([_elementName isEqualToString:@"bebidas"])
	{
		NSString * value = [NSString stringWithFormat:@"%d",mId];
		NSString *pListPath = [DYFileNameManager pathForFile:@"Identifier.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
		NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:value,@"id", nil];
		[dictionary writeToFile:pListPath atomically:YES];

	}
	else if ([_elementName isEqualToString:@"personalizacion"])
    {
        mIsPersonalization = NO;
    }
	else if ([_elementName isEqualToString:@"bebida"])
	{
        [self insertIntoContext:mCurrentDrinkDict];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"nombre"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"name"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"descripcion"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"largedescription"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"disponibilidad"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"available"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"descripcioncorta"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"shortdescription"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"temperatura"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"temperature"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"categoria"])
	{
		
		if(mIsPersonalization==NO)
		{
			[mCurrentDrinkDict setObject:_elementValue forKey:@"category"];
		}
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"subcategoria"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"subcategory"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"marcadovaso"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"glassmark"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"imagen"])
	{
		[mCurrentDrinkDict setObject:_elementValue forKey:@"image"];
	}
	/*else if (_elementValue != nil && [_elementName isEqualToString:@"shot"])
	{
		[mCurrentShotDict setObject:_elementValue forKey:@"name"];
        [mDrinkShotArray addObject:mCurrentShotDict];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"personalizacion"])
	{
		[mCurrentPersonalizationDict setObject:_elementValue forKey:@"name"];
        [mDrinkPersonalizationArray addObject:mCurrentPersonalizationDict];
	}*/
	
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/didendelement");
    }
}

#pragma mark - CoreData Methods

- (void) insertIntoContext:(NSDictionary *)_item
{
    @try{
    NSEntityDescription *drinkEntity = [NSEntityDescription entityForName:@"Drinks" inManagedObjectContext:mAddingContext];
	
	ODrink *drink = [[ODrink alloc] initWithEntity:drinkEntity insertIntoManagedObjectContext:mAddingContext];
    
	mId++;
	NSString * value = [NSString stringWithFormat:@"%d",mId];
	
	drink.ide = value;
	drink.name = [_item objectForKey:@"name"];
    drink.largedescription = [_item objectForKey:@"largedescription"];
	drink.shortdescription = [_item objectForKey:@"shortdescription"];
	drink.available = [_item objectForKey:@"available"];
	drink.temperature = [_item objectForKey:@"temperature"];
	drink.category = [_item objectForKey:@"category"];
	drink.subcategory =[_item objectForKey:@"subcategory"];
	drink.glassmark = [_item objectForKey:@"glassmark"];
	drink.image = [_item objectForKey:@"image"];
	
    
	
	/*for (NSDictionary *shotDict in mDrinkShotArray)
    {   
        NSEntityDescription *shotEntity = [NSEntityDescription entityForName:@"Shots" inManagedObjectContext:mAddingContext];
        OShot * shot = [[OShot alloc] initWithEntity:shotEntity insertIntoManagedObjectContext:mAddingContext];
		shot.name = [shotDict objectForKey:@"name"];
		
        
        [drink addShotsObject:shot];
        [shot release];
    }*/
    
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/insertintocontext");
    }
}


#pragma mark - Delegate Methods

- (void) importerDidFinishAllItems:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishAllItems:withData:withIdentifier:)])
		[mDelegate importerDidFinishAllItems:self withData:mAddingContext withIdentifier:@"ODrinksImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/importerdidFinishAllitems");
    }
}

- (void) importerDidFinishItem:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishItem:withData:withIdentifier:)])
		[mDelegate importerDidFinishItem:self withData:mAddingContext withIdentifier:@"ODrinksImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/importerdidfinishitem");
    }
}

- (void) importerDidFail:(NSError *)_error
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFail:withError:withIdentifier:)])
		[mDelegate importerDidFail:self withError:_error withIdentifier:@"ODrinksImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/drinksImporter/importerDidFail");
    }
}

@end
