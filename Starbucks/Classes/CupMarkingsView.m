//
//  CupMarkingsView.m
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "CupMarkingsView.h"

//#import "CustomDrink.h"
#import "CupMarking.h"
#import "DNLinearLayoutView.h"
#import "CupMarkingView.h"

@implementation CupMarkingsView

@synthesize decafContainer;
@synthesize shotsContainer;
@synthesize syrupContainer;
@synthesize milkContainer;
@synthesize customContainer;
@synthesize drinkContainer;
//@synthesize drink;
@synthesize CupMarkings = mCupMarkings;


+ (CupMarkingsView *)viewFromNib {
    @try{
	NSArray *rootObjects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
	for (NSObject *object in rootObjects) {
		if ([object isKindOfClass:[self class]]) {
			return (CupMarkingsView *)object;
		}
	}
	return nil;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Networks/CupMarkingsView.m/viewNib");
    }
}

- (void)awakeFromNib {
	scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 6, 4, 6);
}

- (void)close {
	[self removeFromSuperview];
}

- (UIView *)cupMarkingViewForCupMarking:(CupMarking *)cupMarking {
    @try{
	CupMarkingView *view = [[CupMarkingView alloc] initWithFrame:CGRectZero];
	view.label.text = cupMarking.text;
	view.strikethrough = cupMarking.strikethrough;
	[view sizeToFit];
	return view;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Networks/CupMarkingsView.m/cupMarkingViewForCup");
    }
}


- (NSArray *)boxes {
	return [NSArray arrayWithObjects:decafContainer, shotsContainer, syrupContainer, milkContainer, customContainer, drinkContainer, nil];
}

- (void)reload {
    @try{
	NSArray *cupMarkings = mCupMarkings;
	NSArray *boxes = [self boxes];
	
	//Max width is the maximum width of any layoutview
	CGFloat maxWidth = 0;
	for (NSUInteger i=0; i<6; i++) {
		NSArray *boxMarkings = [cupMarkings objectAtIndex:i];
		DNLinearLayoutView *layoutView = [boxes objectAtIndex:i];
		[layoutView removeAllSubviews];
		
		for (CupMarking *marking in boxMarkings) {
			[layoutView addSubview:[self cupMarkingViewForCupMarking:marking]];
		}
		//Keep the view centered as we change its size
		[layoutView sizeToFit];
		maxWidth = MAX(maxWidth, layoutView.bounds.size.width);
	}
	//Maxwidth should be at least the width of the scrollview
	maxWidth = MAX(maxWidth, scrollView.bounds.size.width);
	//Adjust scroll area
	scrollView.contentSize = CGSizeMake(maxWidth, scrollView.bounds.size.height);
	//Make sure we start in the center
	scrollView.contentOffset = CGPointMake(maxWidth/2 - scrollView.bounds.size.width / 2, 0);
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Networks/CupMarkingsView.m/reload");
    }
}

- (void)layoutSubviews {
	@try{
	CGRect contentFrame = CGRectZero;
	contentFrame.size = scrollView.contentSize;
	contentView.frame = contentFrame;
	
	for (UIView *boxView in [self boxes]) {
		//keep boxes centered
		CGPoint boxViewCenter = boxView.center;
		boxViewCenter.x = contentView.bounds.size.width / 2;
		boxView.center = boxViewCenter;
	}
	[super layoutSubviews];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Cup Networks/CupMarkingsView.m/layoutSubViews");
    }
	
}



@end
