//
//  OConfigurationCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OConfigurationCell : UITableViewCell<UITextFieldDelegate>
{
    UILabel *mTitleLabel;
    UIImageView *mIconImageView;
    UIImageView *mDisclosureImageView;
	UITextField * mNickNameTextField;
	
	
    BOOL mShowDisclosure;
    BOOL mIndexPathPair;
	int mTag;
	id __weak mDelegate;
}

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) IBOutlet UIImageView *disclosureImageView;
@property (nonatomic, strong) IBOutlet UITextField * nickNameTextField;
@property (nonatomic, assign) BOOL showDisclosure;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int tag;
@property (nonatomic, weak) id delegate;

-(IBAction)buttonPressed:(id)sender;

@end
