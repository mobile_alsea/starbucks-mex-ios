//
//  OConfigurationViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 10/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OSignUpRewardsViewController.h"
#import "OSignInRewardsViewController.h"

@interface OConfigurationViewController : OBaseViewController <UITextFieldDelegate>
{
	IBOutlet UITableView * mAplicacionTableView;
	IBOutlet UITableView * mCuentaTableView;
	IBOutlet UITableView * mSocialNetwork;



}
@end
