//
//  OWebViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OWebViewController.h"

@interface OWebViewController ()

@end

@implementation OWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OWebViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}


-(void) viewWillDisappear:(BOOL)animated
{

	[super viewWillDisappear:animated];
	[mWebView stopLoading];

}


- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	NSString *urlAddress = @"http://rewards.starbucks.mx/mobile/";
	
	
	NSURL *url = [NSURL URLWithString:urlAddress];
	
	//URL Requst Object
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	
	//Load the request in the UIWebView.
	[mWebView loadRequest:requestObj];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OWebViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
