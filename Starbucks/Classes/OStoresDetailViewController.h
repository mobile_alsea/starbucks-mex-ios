//
//  OStoresDetailViewController.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OStore.h"
#import "OAppDelegate.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "OMyFavorites.h"
#import "OBaseViewController.h"

@class OStoresDetailViewController;

@protocol OStoresDetailViewControllerDelegate <NSObject>
- (CLLocation *)storesDetailControllerRequestLocation:(OStoresDetailViewController *)storesDetailController;
@end

@interface OStoresDetailViewController : OBaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    id __weak mDelegate;
    
    OStore *mStoreSelected;
	OMyFavorites * mFavorite;
	NSString * mAddStore;
	NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	BOOL mIsAdded;
	IBOutlet UITableView * mTableView;
    
}

@property (nonatomic, weak) id <OStoresDetailViewControllerDelegate> delegate;
@property (nonatomic, strong) OStore *storeSelected;
@property (assign, nonatomic) BOOL comesFromModal;

-(void)addToMyStores:(id)sender;

@end

@interface OStoresDetailViewController (ActionSheetDelegateMethods) <UIActionSheetDelegate>
@end

@interface OStoresDetailViewController (PeoplePickerNavigationControllerDelegateMethods) <ABPeoplePickerNavigationControllerDelegate>
@end