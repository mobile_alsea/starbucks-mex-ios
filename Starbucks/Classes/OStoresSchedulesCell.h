//
//  OStoresSchedulesCell.h
//  Starbucks
//
//  Created by Adrián Caramés on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OStoresSchedulesCell : UITableViewCell
{
    UILabel *mDayLabel;
    UILabel *mScheduleLabel;
    
    BOOL mIndexPathPair;
}

@property (nonatomic, strong) IBOutlet UILabel *dayLabel;
@property (nonatomic, strong) IBOutlet UILabel *scheduleLabel;
@property (nonatomic, assign) BOOL indexPathPair;

@end
