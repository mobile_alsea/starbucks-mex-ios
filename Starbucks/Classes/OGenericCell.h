//
//  OGenericCell.h
//  Starbucks
//
//  Created by Adrián Caramés on 09/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OStoresDetailViewController.h"

@interface OGenericCell : UITableViewCell
{
    UILabel *mTitleLabel;
    UIImageView *mIconImageView;
    UIImageView *mDisclosureImageView;
    
    BOOL mShowDisclosure;
    BOOL mIndexPathPair;
	int mTag;
	id __weak mDelegate;
}

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) IBOutlet UIImageView *disclosureImageView;
@property (nonatomic, assign) BOOL showDisclosure;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int tag;
@property (nonatomic, weak) id delegate;

-(IBAction)buttonPressed:(id)sender;


@end
