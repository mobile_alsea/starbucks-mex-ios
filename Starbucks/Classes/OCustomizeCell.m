//
//  OCustomizeCell.m
//  Starbucks
//
//  Created by Santi Belloso López on 18/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCustomizeCell.h"
#define RGBToFloat(f) (f/255.0)

@implementation OCustomizeCell
@synthesize nameLabel = mNameLabel;
@synthesize ingredientLabel = mIngredienteLabel;
@synthesize plusButton = mPlusButton;
@synthesize minusButton = mMinusButton;
@synthesize isActive = mIsActive;
@synthesize delegate = mDelegate;
@synthesize index = mIndex;
@synthesize indexPathPair = mIndexPathPair;
@synthesize valuesArray = mValuesArray;
@synthesize count = mCount;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
	
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
	{
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
		
		
	}
    else
	{
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
		
	}
	
}

- (IBAction) minusButtonClick:(id)sender
{
    @try{
	if(mCount!=0)
	{
		mCount--;
		mIngredienteLabel.text = [mValuesArray objectAtIndex:mCount];
		[mPlusButton setEnabled:YES];
	
	}
	else 
	{
		[mMinusButton setHidden:YES];
		mIngredienteLabel.text=@"";
		[mPlusButton setEnabled:YES];
	}

[mDelegate updateArraywithIndex:mIndex];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCutomizeCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}
- (IBAction) plusButtonClick:(id)sender
{
    @try{
	if(mCount==-1)
	{
		mCount++;
		[mMinusButton setHidden:NO];
		[mPlusButton setEnabled:YES];
		mIngredienteLabel.text = [mValuesArray objectAtIndex:mCount];
	
	}
	else 
	{
	
		if(mCount<[mValuesArray count]-1)
		{
			mCount++;
			[mMinusButton setHidden:NO];
			[mPlusButton setEnabled:YES];
			mIngredienteLabel.text = [mValuesArray objectAtIndex:mCount];
			if(mCount==[mValuesArray count]-1) [mPlusButton setEnabled:NO];
		
		}
		
	}
	[mDelegate updateArraywithIndex:mIndex];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OCustomizeCell");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


@end
