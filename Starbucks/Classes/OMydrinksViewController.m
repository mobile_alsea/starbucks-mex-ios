//
//  OMydrinksViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OMydrinksViewController.h"
#import "OMyDrinksCell.h"
#import "OSavedDrinkViewController.h"
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OHomeViewController.h"
@interface OMydrinksViewController ()

@end

@implementation OMydrinksViewController
@synthesize tableView = mTableView;
@synthesize favoritesButton = mFavoritesButton;
@synthesize friendsButton = mFriendsButton;
@synthesize segmentedBackgroundImageView = mSegmentedBackgroundImageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
		
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/initWithNibName");
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

/*-(void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
	
}*/

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	
	mTypeOrderSelected = 1;
	[self.navigationItem setTitle:NSLocalizedString(@"Mis bebidas", nil)];
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Editar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnEditSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
	[self executeFetch];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/viewdidload");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{

	[super viewWillAppear:animated];
	if(mAddingContext!=nil)
	{
		mAddingContext = nil;
	
	}

	mAddingContext = [[NSManagedObjectContext alloc] init];
	[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
	[self executeFetch];
	[mTableView reloadData];

}

- (void) executeFetch
{
    @try{
    mFetchedResultsController = nil;
    
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error])
	{
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    [mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/executeFetch");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
}

- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		//[mFetchedResultsController release];
		//mFetchedResultsController=nil;
		return  mFetchedResultsController;
	}
	
    NSFetchRequest *fetchRequest;
	
	
	if(mTypeOrderSelected==1)
	{
	
		fetchRequest = [OMyDrinks myDrinksUserTypeRequest:@"1" context:mAddingContext];

	
	}
	else
	{
		fetchRequest = [OMyDrinks myDrinksUserTypeRequest:@"2" context:mAddingContext];
	}
    
		
	// Create the sort descriptors array.
	
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"ide" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:mAddingContext sectionNameKeyPath:nil cacheName:nil];
    
    mFetchedResultsController.delegate = self;
	
	//[sellerDescriptor release];
	//[sortDescriptors release];
	
	return mFetchedResultsController;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/fetchResultscontroller");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}




- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) configureCell:(id)_cell atIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	OMyDrinksCell * cell = (OMyDrinksCell*) _cell;
			
	OMyDrinks * drink = [self.fetchedResultsController objectAtIndexPath:_indexPath];
	
			cell.titleLabel.text = drink.name;
			cell.nickNameLabel.text = drink.nickName;
			UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
			[cell.avalaibleButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
			[cell.avalaibleButton setTitle:drink.available forState:UIControlStateNormal];
			
			OImagesDownloaded * image = [OImagesDownloaded imageWithName:drink.image context:mAddingContext];
			if(image==nil || image.image==nil)
			{
				[cell.thumbnailImageView setUrl:[NSURL URLWithString:drink.image]];
			}
			else
			{
				[cell.thumbnailImageView setImage:[UIImage imageWithData:image.image]];
			}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/configurecell");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

	
}


- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/didfinishloadimage");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

	
	
}



#pragma mark - Private Methods

- (void)btnDoneSelected:(id)_sender
{
    @try{
	[mTableView setEditing:NO animated:YES];
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Editar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnEditSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/btnDondeselected");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


}



- (void)btnEditSelected:(id)_sender
{
    @try{
    [mTableView setEditing:YES animated:YES];
	UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Hecho", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(btnDoneSelected:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/btnEditSelected");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


}




#pragma mark IBActions Methods

-(IBAction)btnFavoritesSelected:(id)sender
{
    @try{
	mFavoritesButton.selected = YES;
	mFriendsButton.selected = NO;
	mTypeOrderSelected=1;
	[mSegmentedBackgroundImageView setImage:[UIImage imageNamed:@"SectionHeaderLeftSelected.png"]];
	
	[self executeFetch];
	[mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/favoritesSelecetc");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
}
-(IBAction)btnFriendsSelected:(id)sender
{
    @try{
	mFavoritesButton.selected = NO;
	mFriendsButton.selected = YES;
	mTypeOrderSelected=2;
	[mSegmentedBackgroundImageView setImage:[UIImage imageNamed:@"SectionHeaderRightSelected.png"]];
	[self executeFetch];
	[mTableView reloadData];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/btnFriendsSelected");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}




#pragma mark - UITableView Methods

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{

	return @"Eliminar";

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
	@try{
	int numberOfRows =[[[self.fetchedResultsController sections] objectAtIndex:_section] numberOfObjects];

        int RowsNumber=5;
        int TableWidth=320;
       if(mAppDelegate.window.frame.size.height==480)
       {  RowsNumber=5;
           TableWidth=320;
       }else if(mAppDelegate.window.frame.size.height==568)
       {   RowsNumber=6;
         TableWidth=410;
       }else if(mAppDelegate.window.frame.size.height==1024)
       {   RowsNumber=12;
	  TableWidth=700;
       }
           if(numberOfRows<=RowsNumber)
	{
		_tableView.frame = CGRectMake(10, 45, 300, numberOfRows*65);
	}
	else
	{
		_tableView.frame = CGRectMake(10, 45, 300, TableWidth);
	
	}
	
	return numberOfRows;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/nuberOfsectionsInTableView");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OMyDrinksCell";
    
    OMyDrinksCell *cell = (OMyDrinksCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
    }
    
	cell.thumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-bebidas-low.png"];

	[self configureCell:cell atIndexPath:_indexPath];
 
    
    return cell;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/tableview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
	OSavedDrinkViewController *detailViewController = [[OSavedDrinkViewController alloc] initWithNibName:@"OSavedDrinkViewController" bundle:nil];

	OMyDrinks * drinkSelected = [self.fetchedResultsController objectAtIndexPath:_indexPath];
	detailViewController.drinkSelected = drinkSelected;
	[self.navigationController pushViewController:detailViewController animated:YES];
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/tableview");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	//TODO: cambiar de orden en el arrayList de objetos el from y ponerlo en el to
	
	/*switch (mTypeOrderSelected) {
		case 1:
			[mDrinkMeArray exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
			break;
			case 2:
			[mDrinkFriendsArray exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
			break;
			
		default:
			break;
	}*/
	
	
	
	
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	@try{
	if (editingStyle == UITableViewCellEditingStyleDelete) 
	{

				[tableView beginUpdates];
				[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
								 withRowAnimation:UITableViewRowAnimationFade];

				[mAddingContext deleteObject:[[mFetchedResultsController fetchedObjects] objectAtIndex:indexPath.row]];
				[mAddingContext save:nil];
				[self executeFetch];
				[tableView endUpdates];
				
				[tableView reloadData];
	
	}
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OMyDrinksViewCntroller/tableView");
        
        
        OHomeViewController  *regresa =[[OHomeViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

	
	/*[tableView beginUpdates];
	[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
					 withRowAnimation:UITableViewRowAnimationFade];
	OMyFavorites * mFavorite =[OMyFavorites favoriteWithName:((OMyFavorites*)[mFavoritesArray objectAtIndex:indexPath.row]).name context:mAddingContext];
	[mAddingContext deleteObject:mFavorite];
	[mAddingContext save:nil];
	[mFavoritesArray removeObjectAtIndex:indexPath.row];
	
	[tableView endUpdates];
	[tableView reloadData];*/
	
}    

@end
