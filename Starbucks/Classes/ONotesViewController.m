//
//  ONotesViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ONotesViewController.h"
#import <CoreLocation/CoreLocation.h>
#define RGBToFloat(f) (f/255.0)
@interface ONotesViewController ()

@end

@implementation ONotesViewController
@synthesize  userLabel = mUserLabel;
@synthesize drinkSelected = mDrinkSelected;
@synthesize delegate = mDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/onotesViewController");
        
        
        OSavedDrinkViewController   *regresa =[[OSavedDrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	[self.navigationItem setTitle:NSLocalizedString(@"Notas", nil)];
	UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Guardar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(SaveButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:btnSave];
	
	mTextView.text = mDrinkSelected.notes;
	mTextField.text = mDrinkSelected.nickName;
	if(mDrinkSelected.user==nil || [mDrinkSelected.user isEqualToString:@""])
	{
		mUserLabel.text = @"busca contacto";
	}
	else mUserLabel.text = mDrinkSelected.user;
	
	
	if([mTextView.text isEqualToString:@""])
	{
		mTextView.text = @"¿Algo en especial?";
	}
	else mTextView.textColor = [UIColor blackColor];
		
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OnotesViewcontroller");
        
        
        OSavedDrinkViewController   *regresa =[[OSavedDrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
	
	
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)addUserButtonPressed:(id)sender
{
    @try{
	[mTextView resignFirstResponder];
	[mTextField resignFirstResponder];

		ABPeoplePickerNavigationController *peoplePickeNavController = [[ABPeoplePickerNavigationController alloc] init];
		[peoplePickeNavController.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
		peoplePickeNavController.searchDisplayController.searchBar.tintColor = [UIColor blackColor];
		peoplePickeNavController.peoplePickerDelegate = self;
		[self presentModalViewController:peoplePickeNavController animated:YES];
	peoplePickeNavController.navigationBar.topItem.title = @"Todos los contactos";
	
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OnotesviewController");
        
        
        OSavedDrinkViewController   *regresa =[[OSavedDrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(IBAction)SaveButtonPressed:(id)sender
{
    @try{
	OMyDrinks * drink = [OMyDrinks myDrinkById:mDrinkSelected.ide inContext:mAddingContext];
	
	NSString * nickName = @"";
	if([mTextField.text isEqualToString:@"Ingresa nombre"]) nickName = @"Mi favorito";
	else nickName = mTextField.text;
	NSString * user =@"";
	if(![mUserLabel.text isEqualToString:@"busca contacto"]) user = mUserLabel.text;
	
	drink.nickName = nickName;
	drink.user = user;
	if([mTextView.text isEqualToString:@"¿Algo en especial?"])drink.notes=@"";
	else drink.notes = mTextView.text;

	
	if([mUserLabel.text isEqualToString:@"busca contacto"])
	{
		drink.userType = @"1";
	
	}
	else drink.userType = @"2";
	
	[mAddingContext save:nil];
	[mDelegate.tableView reloadData];
	
	[mDelegate setDrinkSelected:drink];
	
	
	[self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OnotesViewController");
        
        
        OSavedDrinkViewController   *regresa =[[OSavedDrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark TextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	if([textView.text isEqualToString:@"¿Algo en especial?"])
	{	
		textView.text = @"";
		textView.textColor = [UIColor blackColor];
	}
	
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
	textField.placeholder=@"";
	
}

@end



@implementation ONotesViewController (PeoplePickerNavigationControllerDelegateMethods)

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    @try{
	NSString *firstName = (NSString *) CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
    if(firstName==nil) firstName=@"";
	NSString * lastName = (NSString *) CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
	if(lastName!=nil && lastName.length>0)
		mUserLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
	else mUserLabel.text = firstName;
	mUserLabel.textColor = [UIColor blackColor];
	[self dismissModalViewControllerAnimated:YES];
	
	return NO;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OnotesViewController");
        
        
        OSavedDrinkViewController   *regresa =[[OSavedDrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
	[self dismissModalViewControllerAnimated:YES];
}

@end