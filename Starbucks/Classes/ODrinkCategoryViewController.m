//
//  ODrinkCategoryViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 11/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ODrinkCategoryViewController.h"
#import "ODrink.h"
#import "OAppDelegate.h"
#import "ODrink+Extras.h"
#import "OGenericCell.h"
#import "ODrinkSubcategoryViewController.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "UIImageExtras.h"
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "ODrinkDetailViewController.h"
#import "OCustomizeDrinkViewController.h"
#import "ODrinkViewController.h"


@interface ODrinkCategoryViewController ()

@end

@implementation ODrinkCategoryViewController
@synthesize fetchedResultsController = mFetchedResultsController;
@synthesize tableView = mTableView;
@synthesize isHot = mIsHot;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mOpenFlowSelected = 0;
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	if(mIsHot)[self.navigationItem setTitle:NSLocalizedString(@"Tipos de bebidas calientes", nil)];
    //MBT 10Dic2012
	else [self.navigationItem setTitle:NSLocalizedString(@"Tipos de bebidas frías", nil)];
	// Load Data
	NSString * temperature =@"";
	if(mIsHot) temperature =@"Caliente";
	else temperature = @"Frío";
	
	mDrinksArray = [[NSArray alloc] initWithArray:[ODrink drinksByTemperature:temperature inContext:mAddingContext]];
	
    [self executeFetch];
	
	[mOpenFlowView setNumberOfImages:[mDrinksArray count]];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

-(void) viewWillAppear:(BOOL)animated
{
    @try{
        [super viewWillAppear:animated];
        if(!isOS6())
        {
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
            UIViewController * prueba = [[UIViewController alloc] init];
            [prueba.view setHidden:YES];
            [self presentModalViewController:prueba animated:NO];
            [self dismissModalViewControllerAnimated:NO];
        }
	
	
	
	if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight || self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	{
		[self.navigationController.navigationBar setHidden:YES];
		
		//mOpenFlowView.frame = CGRectMake(80, 0, mOpenFlowView.frame.size.width, mOpenFlowView.frame.size.height);
		[mCoverFlowDetailView setHidden:NO];
		[mProductTitleLabel setHidden:NO];
		[mDetailView setHidden:YES];
	}
	else
	{
		[self.navigationController.navigationBar setHidden:NO];
		[mCoverFlowDetailView setHidden:YES];
		[mProductTitleLabel setHidden:YES];
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
	{
		
		[self.navigationController.navigationBar setHidden:YES];
	}
	
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[mOpenFlowView setBounds:mCoverFlowDetailView.bounds];


}
-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[mOpenFlowView setBounds:mCoverFlowDetailView.bounds];
	if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	{
		[self.navigationController.navigationBar setHidden:YES];
		
		//mOpenFlowView.frame = CGRectMake(80, 0, mOpenFlowView.frame.size.width, mOpenFlowView.frame.size.height);
		[mCoverFlowDetailView setHidden:NO];
		[mProductTitleLabel setHidden:NO];
	}
	else
	{
		[self.navigationController.navigationBar setHidden:NO];
		[mCoverFlowDetailView setHidden:YES];
		[mProductTitleLabel setHidden:YES];
	}
	
	
}


- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
}



- (void) configureCell:(OGenericCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{
	
    _cell.titleLabel.text = [[mSectionsArray objectAtIndex:_indexPath.section] objectAtIndex:_indexPath.row];
	_cell.titleLabel.frame = CGRectMake(15, 11, 249, 21);
    
}

- (void) executeFetch
{
    @try{
    mFetchedResultsController = nil;
    
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error])
	{
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
	/*****Configuracion array de subcategorias****/
	
	if(mSectionsArray!=nil)
	{
		mSectionsArray = nil;
	}
	
	mSectionsArray = [[NSMutableArray alloc] initWithCapacity:0];
	//int sections =[[self.fetchedResultsController sections] count];
	for(int i=0;i<[[self.fetchedResultsController sections] count];i++)
	{
        
        
		NSMutableArray * subcategoryArray = [[NSMutableArray alloc] initWithCapacity:0];
		
		for(int j=0;j<[[[self.fetchedResultsController sections] objectAtIndex:i] numberOfObjects];j++)
		{
           
             
			ODrink * drink = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
  NSLog(@"%@", drink.category);
            
            
            
			BOOL isRepeat = NO;
			for(int h=0;h<[subcategoryArray count] && (isRepeat==NO);h++)
			{
				if([[subcategoryArray objectAtIndex:h] isEqualToString:drink.subcategory])
				{
					isRepeat=YES;
				}
			
			}
			
			if(isRepeat==NO)
			{
                
                [subcategoryArray addObject:drink.subcategory];
                
          
                
				
			}
			
		}
        
       
        
		[subcategoryArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		[mSectionsArray addObject:subcategoryArray];
	}
	
	/******************************************/
    [mTableView reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	
    
	NSString * temperature =@"";
	if(mIsHot) temperature =@"Caliente";
	else temperature = @"Frío";
	NSFetchRequest *fetchRequest;
	fetchRequest = [ODrink drinksInContext:[OAppDelegate managedObjectContext] withTemperature:temperature];
	
	// Create the sort descriptors array.
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
	NSSortDescriptor *sellerDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor,sellerDescriptor2, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:@"category" cacheName:nil];
    mFetchedResultsController.delegate = self;
	
	
	
	
	
	
	return mFetchedResultsController;
        
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

#pragma mark - UITableView Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
//	int sections = [[self.fetchedResultsController sections] count];
	return [[self.fetchedResultsController sections] count];
	
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 15;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{ 
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	ODrink *  drink = (ODrink*)[[sectionInfo objects] objectAtIndex:0];
	return  drink.category;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    @try{
	NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
	UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 15);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:12];
    label.text = sectionTitle;
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 15)];
	view.backgroundColor = [UIColor blackColor];
	[view addSubview:label];
	return view;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    /*id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_section];
    return [sectionInfo numberOfObjects];*/
	return [[mSectionsArray objectAtIndex:_section ] count];
	
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
	static NSString *CellIdentifier = @"OGenericCell";
    
    OGenericCell *cell = (OGenericCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
		
    }
    
    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
	
    [self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
	return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
    
	OGenericCell * cell = (OGenericCell*)[_tableView cellForRowAtIndexPath:_indexPath];
    ODrinkSubcategoryViewController *detailViewController = [[ODrinkSubcategoryViewController alloc] initWithNibName:@"ODrinkSubcategoryViewController" bundle:nil];
	detailViewController.isHot = mIsHot;
	detailViewController.subCategory = cell.titleLabel.text;
	
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

#pragma mark IBActions Methods

-(IBAction)addToMyDrink:(id)sender
	{
        @try{
		if(mIsAdded==YES)
		{
			//Delete from bd
			
			[mAddingContext deleteObject:mFavorite];
			mAddToFavsLabel.text = @"Añadir a mis Bebidas";
			NSError * error = nil;
			[mAddingContext save:&error];
			mIsAdded = NO;
			
		}
		else
		{
			NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyDrinks" inManagedObjectContext:mAddingContext];
			mFavorite = [[OMyDrinks alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:mAddingContext];
			
			ODrink * mDrinkSelected = (ODrink*)[mDrinksArray objectAtIndex:mOpenFlowSelected];
			
			mFavorite.ide = mDrinkSelected.ide;
			mFavorite.available = mDrinkSelected.available;
			mFavorite.category = mDrinkSelected.category;
			mFavorite.glassmark = mDrinkSelected.glassmark;
			mFavorite.image = mDrinkSelected.image;
			mFavorite.largedescription = mDrinkSelected.largedescription;
			mFavorite.name = mDrinkSelected.name;
			mFavorite.shortdescription = mDrinkSelected.shortdescription;
			mFavorite.subcategory = mDrinkSelected.subcategory;
			mFavorite.temperature = mDrinkSelected.temperature;
			mFavorite.user = @"Yo";
			mFavorite.userType = @"1";
			mFavorite.nickName=@"";
			mFavorite.notes=@"";
			NSError * error = nil;
			[mAddingContext save:&error];
			mAddToFavsLabel.text = @"Eliminar de mis Bebidas";
			mIsAdded = YES;
			
		}
        }
        
        
        @catch (NSException *ex) {
            
            NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
            ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
            
            [self.navigationController pushViewController:regresa animated:YES];
            UIAlertView *alert =[[UIAlertView alloc]
                                 initWithTitle:@"No Disponible"
                                 message:@"Opción no disponible"
                                 delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
            [alert show];
            
        }
}

-(IBAction)hideDetailViewButtonPressed:(id)sender
{
    @try{
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	[mDetailView setHidden:YES];
    [UIView commitAnimations];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}



-(IBAction)viewDetailButtonPressed:(id)sender
{
	@try{
	ODrink *drink = (ODrink*)[mDrinksArray objectAtIndex:mOpenFlowSelected];
	
	ODrinkDetailViewController *detailViewController = [[ODrinkDetailViewController alloc] initWithNibName:@"ODrinkDetailViewController" bundle:nil];
	[detailViewController setDrinkSelected:drink];
    detailViewController.delegate = self;	
	[mCoverFlowDetailView setHidden:YES];
	[mProductTitleLabel setHidden:YES];
	[self.navigationController.navigationBar setHidden:NO];
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}


-(IBAction) makeDrinkButtonPressed:(int)_index
{
	@try{
	ODrink *drink = (ODrink*)[mDrinksArray objectAtIndex:mOpenFlowSelected];
	OCustomizeDrinkViewController *detailViewController = [[OCustomizeDrinkViewController alloc] initWithNibName:@"OCustomizeDrinkViewController" bundle:nil];
	[detailViewController setDrinkSelected:drink];
	
	[mCoverFlowDetailView setHidden:YES];
	[mProductTitleLabel setHidden:YES];
	[self.navigationController.navigationBar setHidden:NO];
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}




#pragma mark Openflow Protocol Delegate

// delegate protocol to tell which image is selected
- (void)openFlowView:(AFOpenFlowView *)openFlowView selectionDidChange:(int)index{
	
	ODrink *drink = [mDrinksArray objectAtIndex:index];
	
	mProductTitleLabel.text = drink.name;
	[mDetailView setHidden:YES];
	
}

// setting the image 1 as the default pic
- (UIImage *)defaultImage{
	UIImage * image = [UIImage imageNamed:@"noimage-bebidas-high.png"];
	image = [image cropCenterAndScaleImageToSize:CGSizeMake(200, 200)];
	return image;
}



- (void)openFlowView:(AFOpenFlowView *)openFlowView didTapSelectedImage:(int)index
{
    @try{
	mOpenFlowSelected = index;
	
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	[mDetailView setHidden:NO];  
    [UIView commitAnimations];
	
	ODrink *drink = [mDrinksArray objectAtIndex:index];
	mProductNameDetail.text = drink.name;
	mProductDetailDetail.text = drink.largedescription;
	UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [mAvailableButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [mAvailableButton setTitle:drink.available forState:UIControlStateNormal];
	
	
	mFavorite =[OMyDrinks myDrinkById:drink.ide inContext:mAddingContext];
	
	if(mFavorite!=nil)
	{ 
		mIsAdded = YES;
		[mAddingContext deleteObject:mFavorite];
		mAddToFavsLabel.text = @"Eliminar de mis Bebidas";
	}
	else
	{
		mIsAdded = NO;
		mAddToFavsLabel.text = @"Añadir a mis Bebidas";
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}


- (void)openFlowView:(AFOpenFlowView *)openFlowView requestImageForIndex:(int)index
{
	
	@try{
	ODrink *drink = [mDrinksArray objectAtIndex:index];
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:drink.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[self downloadImage:drink.image cell:nil index:[NSString stringWithFormat:@"%d",index]];
	}
	else
	{
		UIImage * image2 = [UIImage imageWithData:image.image];
		image2 = [image2 cropCenterAndScaleImageToSize:CGSizeMake(200, 200)];
		[mOpenFlowView setImage:image2 forIndex:index];
	}
	if(index==0)mProductTitleLabel.text = drink.name;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

#pragma mark -
#pragma mark Image Download Methods

//- (void) downloadImage:(NSString *)_urlString indexPath:(NSIndexPath *)_indexPath
- (void) downloadImage:(NSString *)_urlString cell:(UITableViewCell *)_cell index:(NSString*) _index
{	
	@try{
	NSMutableDictionary *values = [[NSMutableDictionary alloc] init];
	[values setObject:_urlString forKey:@"URL"];
	
	if(_cell!=nil)[values setObject:_cell forKey:@"Cell"];
	[values setObject:_index forKey:@"Index"];
	
	NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(downloadImageTask:) object:values];
	[mAppDelegate.operationQueue addOperation:invocationOperation];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) downloadImageTask:(NSMutableDictionary *)_values
{
    @try{
	NSString *urlString = [_values objectForKey:@"URL"];
	
	NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]];
	
	
	if (data != nil)
	{	
		[_values setObject:data forKey:@"ImageData"];
		
		
		[self performSelectorOnMainThread:@selector(updateCellImage:) withObject:[NSDictionary dictionaryWithDictionary:_values] waitUntilDone:YES];			
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) updateCellImage:(NSDictionary *)_values
{
    @try{
	NSData *imageData = [_values objectForKey:@"ImageData"];
	//	NSIndexPath *indexPath = [_values objectForKey:@"IndexPath"];
	
	//	id cell = [self.tableView cellForRowAtIndexPath:indexPath];
	id cell = [_values objectForKey:@"Cell"];
	if(cell!=nil)
	{	
		[cell setValue:[UIImage imageWithData:imageData] forKeyPath:@"thumbnailImageView.image"];
	}
	
	
	NSInteger indice = [[_values objectForKey:@"Index"] intValue];
	
	
	UIImage * image = [UIImage imageWithData:imageData];
	image = [image cropCenterAndScaleImageToSize:CGSizeMake(200, 200)];
	[mOpenFlowView setImage:image forIndex:(int)indice];
	
	
	
	ODrink *item = [mDrinksArray objectAtIndex:(int)indice];
	
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = item.image;
	imageObject.image = imageData;
	[mAddingContext save:nil];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}



#pragma mark - NSFetchedResultsController Protocol

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [mTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    @try{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [mTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
					  withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [mTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
					  withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    @try{
    UITableView *tableView = mTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(OGenericCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ODrinkCtaegoryController");
        ODrinkViewController  *regresa =[[ODrinkViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
//        UIAlertView *alert =[[UIAlertView alloc]
//                             initWithTitle:@"No Disponible"
//                             message:@"Opción no disponible"
//                             delegate:self
//                             cancelButtonTitle:@"OK"
//                             otherButtonTitles:nil];
      //  [alert show];
        
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [mTableView endUpdates];
}



@end
