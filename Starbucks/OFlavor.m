//
//  OFlavor.m
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFlavor.h"
#import "OCoffee.h"


@implementation OFlavor

@dynamic name;
@dynamic coffee;

@end
