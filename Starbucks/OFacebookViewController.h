//
//  OFacebookViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 04/10/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OMyDrinks.h"
#import "Facebook.h"
#import "OBaseViewController.h"

@interface OFacebookViewController : OBaseViewController<UITextViewDelegate>
{
	IBOutlet UILabel * mDrinkNameLabel;
	IBOutlet UILabel * mFavoriteNameLabel;
	IBOutlet UITextView * mTextView;
	OMyDrinks * mDrinkSelected;
	Facebook *mFacebook;
}

@property(nonatomic, strong) OMyDrinks * drinkSelected;

@end

@interface OFacebookViewController (FBSessionDelegate) <FBSessionDelegate> 
@end

@interface OFacebookViewController (FBRequestDelegate) <FBRequestDelegate> 
@end
