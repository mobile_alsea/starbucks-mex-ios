//
//  OManagementRewardsCardsViewController.h
//  Starbucks
//
//  Created by Mobile on 10/18/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"
#import "OAddRewardsCardViewController.h"
#import "MBProgressHUD.h"

@interface OManagementRewardsCardsViewController : OBaseViewController <UITableViewDataSource,UITableViewDelegate,NSURLConnectionDelegate,UIAlertViewDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}

@property (strong, nonatomic) IBOutlet UITableView *cardsTableView;
@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (nonatomic, strong) NSArray *rewardsCardsArray;
@property (nonatomic, strong) NSMutableArray *rewardsCardsArrayMutable;
@property (strong, nonatomic) IBOutlet UIView *viewAddCard;
- (UIImage *)imageToFitFromData:(UIView*)view imagen:(UIImage*)nameIMG;
@end
