//
//  ORewardsViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface ORewardsViewController : OBaseViewController
{
	IBOutlet UILabel * mNumberOfStarsLabel;
	NSString * mNumberOfStars;
    NSString * mNumberOfStarsGold;
	NSString * mDateMember;
	IBOutlet UILabel * mNeedLabel;


    
	
}


@property(nonatomic, strong) NSString * numberOfStars;
@property(nonatomic, strong) NSString * numberOfStarsGold;
@property(nonatomic, strong) NSString * dateMember;
@end
