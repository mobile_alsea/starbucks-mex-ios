//
//  OFoodResultViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "AFOpenFlowView.h"
#import "OMyFavorites.h"
@interface OFoodResultViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,AFOpenFlowViewDelegate,AFOpenFlowViewDataSource>
{
	OAppDelegate *mAppDelegate;
	UITableView * mTableView;
	UILabel * mHeaderLabel;
	NSFetchedResultsController *mFetchedResultsController;
	NSString * mCategorySelected;
	IBOutlet AFOpenFlowView * mOpenFlowView;
	IBOutlet UIView * mCoverFlowDetailView;
	IBOutlet UILabel * mProductTitleLabel;
	IBOutlet UILabel * mProductNameDetail;
	IBOutlet UILabel * mProductDetailDetail;
	IBOutlet UIButton * mAvailableButton;
	IBOutlet UIView * mDetailView;
	int mOpenFlowSelected;
	NSManagedObjectContext *mAddingContext;
	NSMutableArray * mFiltersArray;
	IBOutlet UILabel * mAddToFavsLabel;
	OMyFavorites * mFavorite;
	BOOL mIsAdded;
	

}

@property(nonatomic,strong) IBOutlet UITableView * tableView;
@property(nonatomic, strong) NSMutableArray * filtersArray;
@property(nonatomic, strong) IBOutlet UILabel * headerLabel;
@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSString * categorySelected;

-(IBAction)viewDetailButtonPressed:(id)sender;
-(IBAction)hideDetailViewButtonPressed:(id)sender;

@end
