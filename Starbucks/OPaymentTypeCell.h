//
//  OPaymentTypeCell.h
//  Starbucks
//
//  Created by Mobile on 10/22/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYImageView.h"

/*
@class OPaymentTypeCell;
@protocol OPaymentTypeCellDelegate  <NSObject>
- (void) OPaymentTypeCellPressed:(OPaymentTypeCell *)controller;
@end
*/

@interface OPaymentTypeCell : UITableViewCell
{
    UIImageView * mThumbnailImageView;
    UILabel *mLblTitulo;
    UILabel *mLblDescripcion;
    UIButton *mCheckButton;
    BOOL mIndexPathPair;
    int mIndex;
}
@property(nonatomic, strong) IBOutlet UIImageView * thumbnailImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UILabel *lblDescripcion;
@property (strong, nonatomic) IBOutlet UIButton *CheckButton;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;
@end
