//
//  SBCoachingView.m
//  Starbucks
//
//  Created by Victor Soto on 7/22/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import "SBCoachingView.h"

@interface SBCoachingView ()
@property (assign, nonatomic, readwrite) BOOL hideCoachingView;
@end

@implementation SBCoachingView

@synthesize hideCoachingView = _hideCoachingView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.titleLabel.text = NSLocalizedString(@"Realidad Aumentada", nil);
    self.messageLabel.text = NSLocalizedString(@"En posición vertical acceda a Realidad Aumentada", nil);
    self.radioLabel.text = NSLocalizedString(@"No volver a mostrar", nil);
    
    self.shadowView.alpha = 1.0;
}

#pragma mark - Coaching View

+ (id)getView
{
	NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"SBCoachingView" owner:nil options:nil];
	return [nibViews objectAtIndex:0];
}

+ (BOOL)shouldPresentView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"KOSCoachMarkKey:%i",[[defaults objectForKey:KOSCoachMarkKey] boolValue]);
    return ![[defaults objectForKey:KOSCoachMarkKey] boolValue];
}

#pragma mark - Custom Setters

- (void)setHideCoachingView:(BOOL)hideCoachingView
{
    _hideCoachingView = hideCoachingView;
    NSLog(@"hideCoachingView:%i",hideCoachingView);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:hideCoachingView?@YES:@NO forKey:KOSCoachMarkKey];
    [defaults synchronize];
}

#pragma mark - Animation Methods

- (void)show
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];    
    [animation setKeyTimes:frameTimes];    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = 0.35f;
    [self.containerView.layer addAnimation:animation forKey:nil];
}

- (void)dismiss
{
    __weak SBCoachingView *weakSelf = self;
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        if (weakSelf) {
            SBCoachingView *strongSelf = weakSelf;
            [strongSelf removeFromSuperview];
        }
    }];
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = @1.0f;
    fadeAnimation.toValue = @0.0f;
    fadeAnimation.duration = 0.2f;
    [self.layer addAnimation:fadeAnimation forKey:nil];
    self.layer.opacity = 0.0f;
    [CATransaction commit];
}

#pragma mark - Target Action Methods

- (IBAction)stopShowingCoaching:(id)sender
{
    BOOL shouldHideCoaching = self.radioButton.selected;
    self.radioButton.selected = !shouldHideCoaching;
    self.hideCoachingView = !shouldHideCoaching;
}

- (IBAction)close:(id)sender
{
    [self dismiss];
}

@end
