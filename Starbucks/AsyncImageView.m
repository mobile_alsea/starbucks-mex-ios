//
//  AsyncImageView.m
//  IphoneEEEM
//
//  Created by Juan Pablo  Gonzalez Hermosillo Cires on 04/03/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "AsyncImageView.h"


// This class demonstrates how the URL loading system can be used to make a UIView subclass
// that can download and display an image asynchronously so that the app doesn't block or freeze
// while the image is downloading. It works fine in a UITableView or other cases where there
// are multiple images being downloaded and displayed all at the same time. 

@implementation AsyncImageView

@synthesize viewCargando;
@synthesize activityView;
@synthesize miURL;

- (void)dealloc {
	[connection cancel]; //in case the URL is still downloading
}

- (void)loadImageFromURLMain{
	[self loadImageFromURL:miURL];
}

- (void)loadImageFromURL:(NSURL*)url {
	
	@try {
		NSString *urlString = [url absoluteString];
		if ([urlString length]==0){
            return;}

		
		if (connection!=nil){
            [connection cancel];
            connection = nil;
        } //in case we are downloading a 2nd image
		if (data!=nil) { data = nil; }
		
		NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
		connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
		//TODO error handling, what if connection is nil?
		
		viewCargando = [[UIView alloc] initWithFrame:self.bounds];
		
		[viewCargando setBackgroundColor:[UIColor colorWithRed:((float) 250 / 255.0f) green:((float) 250 / 255.0f) blue:((float) 250 / 255.0f) alpha:0.9]];
		int left = (self.frame.size.width - 16)/2;
		if (left < 0){
			left=20;
		}
		int top = (self.frame.size.height - 16)/2;
		if (top < 0){
			top=20;
		}
		activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(left, top, 16, 16)];
		activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
		[activityView startAnimating];
		activityView.hidesWhenStopped=YES;
		[viewCargando addSubview:activityView];
		[self addSubview:viewCargando];
		
	} @catch (NSException* ex) {
		NSLog(@"error: %@",ex);
		[activityView stopAnimating];
		activityView.hidesWhenStopped=YES;
	}
}

//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	@try {
		
		if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
		[data appendData:incrementalData];
		
	} @catch (NSException* ex) {
		NSLog(@"error: %@",ex);
		[activityView stopAnimating];
		activityView.hidesWhenStopped=YES;

		UIApplication* app2 = [UIApplication sharedApplication];
		app2.networkActivityIndicatorVisible = NO;
	}		
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	
	@try {
		//so self data now has the complete image
		connection=nil;
		if ([[self subviews] count]>0) {
			//then this must be another image, the old one is still in subviews
			[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
		}
		
		[viewCargando removeFromSuperview];
				
		//make an image view for the image
		UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:data]];
        
        if(CGSizeEqualToSize(imageView.image.size, CGSizeZero))
        {
            imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_adddefault.png"]];
        }
        
        
		//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
		imageView.contentMode = UIViewContentModeScaleAspectFit;
		//imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth || UIViewAutoresizingFlexibleHeight );
		[self addSubview:imageView];
		imageView.frame = self.bounds;
		[imageView setNeedsLayout];
		[self setNeedsLayout];
		data=nil;
		
	} @catch (NSException* ex) {
		//NSLog(@"error: %@",ex);
		[activityView stopAnimating];
		activityView.hidesWhenStopped=YES;
	}
	
    UIApplication* app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = NO;
}

//just in case you want to get the image directly, here it is in subviews
- (UIImage*) image {
	UIImageView* iv = [[self subviews] objectAtIndex:0];
	return [iv image];
}

@end