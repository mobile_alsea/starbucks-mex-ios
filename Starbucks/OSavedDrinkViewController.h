//
//  OSavedDrinkViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OShareCell.h"
#import "ODrinkDescriptionCell.h"
#import "CupMarkingsView.h"
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OAppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "OBaseViewController.h"
@class CupMarkingsView;

@interface OSavedDrinkViewController : OBaseViewController<UIActionSheetDelegate,OShareCellDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
{

	UITableView * mTableView;
	NSArray *animationImages;
	NSArray *reverseAnimationImages;
	UIImageView * imageView; 
	BOOL mIsReverse;
	CupMarkingsView *cupMarkingsView;
	OMyDrinks * mDrinkSelected;
	NSManagedObjectContext *mAddingContext;
	OAppDelegate * mAppDelegate;
	UIImageView * imageView2;

}

@property(nonatomic, strong) IBOutlet UITableView * tableView;
@property (nonatomic, strong) NSArray *reverseAnimationImages;
@property (nonatomic, strong) NSArray *animationImages;
@property (nonatomic, strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) IBOutlet UIImageView * imageView2;
@property (nonatomic, strong) OMyDrinks * drinkSelected;


-(IBAction)setCupRotation:(id)sender;
- (void)viewCupMarkings;
- (void)hideCupMarkings;


@end
