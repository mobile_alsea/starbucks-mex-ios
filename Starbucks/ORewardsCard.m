//
//  ORewardsCard.m
//  Starbucks
//
//  Created by Mobile on 11/1/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "ORewardsCard.h"

@implementation ORewardsCard
@synthesize cardNumber;
@synthesize idCard;
@synthesize loyaltyLevel;
@synthesize alias;
@synthesize isPrincipal;
@synthesize activatedDate;
@synthesize balance;
@synthesize imagesArray;
@synthesize lastUpdate;

-(id) init{
    self.idCard=@"";
    self.cardNumber=@"";
    self.loyaltyLevel=@"";
    self.alias=@"";
    self.isPrincipal = nil;
    self.activatedDate=@"";
    self.balance=@"";
    self.imagesArray = nil;
    self.lastUpdate = @"";
    
    return  self;
}

@end
