//
//  SettingsPagaTodo.m
//  Starbucks
//
//  Created by Mobile on 11/20/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "SettingsPagaTodo.h"

@implementation SettingsPagaTodo

#define DESARROLLO @"desarrollo"

#ifdef DESARROLLO
#define URL_SWITCH_KM @"https://karma-marketing.com/pagatodomobile/starbucks/settings.json"
#define URL_WS_SMART @"http://201.155.112.175:4090/Servicio.svc/JSON/"
#define URL_WS_REWARDS @"http://ec2-54-219-59-225.us-west-1.compute.amazonaws.com:9000/"
//#define URL_WS_REWARDS @"http://54.219.59.225:9000/"
#define CERT_KM @"795b5bc110c96063133ba738bcea8f96220fbd4be7061265169efa1d5578e982"
#define CERT_WS_SMART @"795b5bc110c96063133ba738bcea8f96220fbd4be7061265169efa1d5578e982"
#define CERT_WS_REWARDS @"795b5bc110c96063133ba738bcea8f96220fbd4be7061265169efa1d5578e982"
#define URL_APP_STORE @"itms-apps://itunes.apple.com/mx/app/Starbucks-mexico/id570779372?mt=8"
#define LAST_DATE_SWITCH @"2013-10-10 00:00"
#else
#define URL_SWITCH_KM @"https://karma-marketing.com/pagatodomobile/starbucks/settings.json"
#define URL_WS_REWARDS @"http://ec2-54-219-59-225.us-west-1.compute.amazonaws.com:9000/"
#define URL_WS_SMART @"http://ec2-54-219-59-225.us-west-1.compute.amazonaws.com/"
#define CERT_KM @"795b5bc110c96063133ba738bcea8f96220fbd4be7061265169efa1d5578e982"
#define CERT_WS_SMART @"795b5bc110c96063133ba738bcea8f96220fbd4be7061265169efa1d5578e982"
#define CERT_WS_REWARDS @"795b5bc110c96063133ba738bcea8f96220fbd4be7061265169efa1d5578e982"
#define URL_APP_STORE @"itms-apps://itunes.apple.com/mx/app/Starbucks-mexico/id570779372?mt=8"
#define LAST_DATE_SWITCH @"2013-10-10 00:00"
#endif


+ (NSString *)getURLSwitch{
    return URL_SWITCH_KM;
}
+ (NSString *)getURLWSSmart{
    return URL_WS_SMART;
}
+ (NSString *)getURLWSRewards{
    return URL_WS_REWARDS;
}
+ (NSString *)getCertWSSmart{
    return CERT_WS_SMART;
}
+ (NSString *)getCertWSRewards{
    return CERT_WS_REWARDS;
}
+ (NSString *)getCertKM{
    return CERT_KM;
}
+ (NSString *)getURLAppStore{
    return URL_APP_STORE;
}
+ (NSString *)getDateSettings{
    return LAST_DATE_SWITCH;
}

@end
