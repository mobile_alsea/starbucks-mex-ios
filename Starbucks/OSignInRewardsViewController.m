//
//  OSignInRewardsViewController.m
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OSignInRewardsViewController.h"
#import "MBProgressHUD.h"

@interface OSignInRewardsViewController ()

@property (strong, nonatomic) MBProgressHUD *HUD;
@end

@implementation OSignInRewardsViewController
@synthesize mUSerTextField;
@synthesize mPasswordTextField;
@synthesize SignInButton;
@synthesize HUD;
@synthesize receivedData;

#pragma mark - NSURL Connection
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView *dialogo = [[UIAlertView alloc] init];
    [dialogo setDelegate:self];
    [dialogo setTitle:@"¡Lo sentimos!"];
    [dialogo setMessage:[error localizedDescription]];
    [dialogo addButtonWithTitle:@"OK"];
    [dialogo show];
}

// success conectivity
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [HUD hide:YES];
    NSError *error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    
    int idRespuesta = [[results objectForKey:@"id"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        NSArray *userCardsArray = (NSArray *)[results objectForKey:@"tarjetas"];
        if ([userCardsArray count] > 0) {
            [[NSUserDefaults standardUserDefaults] setObject:(NSArray *)[results objectForKey:@"tarjetas"] forKey:@"userCardsArray"];
            
            for (int i=0; i<[userCardsArray count]; i++) {
                NSDictionary *userCardInformation = (NSDictionary *)[userCardsArray objectAtIndex:i];
                NSString *cardId = (NSString *)[userCardInformation objectForKey:@"idTarjeta"];
                NSString *lastCardBalanceUpdate = [self getCurrentTime];
                NSLog(@"carId:%@",cardId);
                NSLog(@"lastCardBalanceUpdate:%@",lastCardBalanceUpdate);
                [[NSUserDefaults standardUserDefaults] setObject:lastCardBalanceUpdate forKey:cardId];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
        }
        
        NSString *token = (NSString *)[results objectForKey:@"token"];
        NSString *idUsuario = (NSString *)[results objectForKey:@"idUsuario"];
        NSDictionary *userInformation = (NSDictionary *)[results objectForKey:@"datosCliente"];
        
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setObject:idUsuario forKey:@"idUsuario"];
        [[NSUserDefaults standardUserDefaults] setObject:userInformation forKey:@"userInformation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"token:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
        NSLog(@"idUsuario:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"]);
        NSLog(@"userInfromation:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userInformation"]);
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-SING-IN-ACCOUNT-REWARDS" object:nil];
    }
}


- (NSString *)getCurrentTime{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd"];
    NSDateFormatter *format2 = [[NSDateFormatter alloc]init];
    format2.dateFormat = @"MMM/yyyy HH:mm";
    [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"es_MX"]];
    NSString *yourString= [format2 stringFromDate:[[NSDate alloc] init]];
    yourString=[[[yourString substringToIndex:1] uppercaseString] stringByAppendingString:[yourString substringFromIndex:1]];
    yourString = [NSString stringWithFormat:@"%@/%@",[format stringFromDate:[[NSDate alloc] init]], yourString];
    return  yourString;
}





#pragma mark Textfield Delegate

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([mUSerTextField.text length] > 0 && [mPasswordTextField.text length] > 0) {
        SignInButton.enabled = YES;
    }else{
        SignInButton.enabled = NO;
    }
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
        if ([mUSerTextField.text length] > 0 && [mPasswordTextField.text length] > 0){
            SignInButton.enabled = YES;
        }else{
            SignInButton.enabled = NO;
        }
    return YES;
}

#pragma mark Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            /*
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
            */
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OSignInRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
        SignInButton.enabled = NO;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OWebViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


- (void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}

- (void) viewDidAppear:(BOOL)animated
{
    [mUSerTextField becomeFirstResponder];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [mUSerTextField resignFirstResponder];
    [mPasswordTextField resignFirstResponder];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)rememberPasswordPressed:(id)sender {
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)enterButtonPressed:(id)sender {
    if ([mUSerTextField.text length] == 0 || [mPasswordTextField.text length] == 0) {
    
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"Aviso"
                             message:@"Por favor ingresa los datos de tu cuenta."
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
    UIWindow *tempKeyboardWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    HUD=[[MBProgressHUD alloc] initWithWindow:tempKeyboardWindow];
    [tempKeyboardWindow addSubview:HUD];
    [HUD show:YES];
	HUD.detailsLabelText = @"Espere por favor...";
	HUD.labelText = @"Validando datos de la cuenta";
    HUD.dimBackground = YES;
    receivedData = [[NSMutableData alloc] init];
    NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
    validateCardSbx = [validateCardSbx stringByAppendingString:@"login"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:validateCardSbx]];
    
    [request setHTTPMethod:@"POST"];
    NSString *cardSbxPostString = [NSString stringWithFormat: @"email=%@&contrasenia=%@",mUSerTextField.text,mPasswordTextField.text];
    NSData *postCardSbxData = [cardSbxPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postCardSbxLength = [NSString stringWithFormat:@"%d", [postCardSbxData length]];
    [request setValue:postCardSbxLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postCardSbxData];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    [connection start];
}

@end
