//
//  OFoodDetailViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OFood.h"
#import "DYImageView.h"
#import "OMyFavorites.h"
#import "OAppDelegate.h"
#import "OBaseViewController.h"

@interface OFoodDetailViewController : OBaseViewController<UITableViewDelegate,UITableViewDataSource>
{
	id __weak mDelegate;
	OFood *mFoodSelected;
	DYImageView * mThumbnailImageView;
	UILabel * mNameLabel;
	UILabel * mDescriptionLabel;
	IBOutlet UIButton * mPlusButton;
	IBOutlet UIButton * mAvailableButton;
	IBOutlet DYImageView * mPlusDYImageView;
	IBOutlet UIView * mPlusView;
	BOOL mIsAdded;
	OMyFavorites * mFavorite;
	OAppDelegate * mAppDelegate;
	UILabel * mAddFoodLabel;
	NSManagedObjectContext *mAddingContext;
	IBOutlet UITableView * mTableView;
	NSMutableArray * mCombinationsArray;
	IBOutlet UILabel * mCombinationLabel;
	IBOutlet UIImageView * mCombinationImageView;
	IBOutlet UIView * mViewWithAlpha;


}

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) OFood *foodSelected;
@property (nonatomic, strong) IBOutlet UILabel * nameLabel;
@property (nonatomic, strong) IBOutlet UILabel * descriptionLabel;
@property (nonatomic, strong) IBOutlet UIButton * plusButton;
@property (nonatomic, strong) IBOutlet UIButton * availableButton;
@property (nonatomic, strong) IBOutlet DYImageView * thumbnailImageView;
@property (nonatomic, strong) IBOutlet UILabel * addFoodLabel;


-(IBAction)plusButtonSelected:(id)sender;
-(IBAction)dismmisPlusButtonSelected:(id)sender;
-(IBAction)addToMyFood:(id)sender;

@end
