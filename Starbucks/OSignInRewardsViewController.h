//
//  OSignInRewardsViewController.h
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"
#import "DYHorizontalStretchButton.h"

@interface OSignInRewardsViewController : OBaseViewController<NSURLConnectionDelegate>
{
    OAppDelegate *mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
@property (strong, nonatomic) IBOutlet UITextField *mUSerTextField;
@property (strong, nonatomic) IBOutlet UITextField *mPasswordTextField;
@property (strong, nonatomic) IBOutlet DYHorizontalStretchButton *SignInButton;
@property (strong, nonatomic) NSMutableData *receivedData;


- (IBAction)enterButtonPressed:(id)sender;
- (IBAction)rememberPasswordPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

@end
