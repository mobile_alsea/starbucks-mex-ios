//
//  OSavedDrinkViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OSavedDrinkViewController.h"
#import "OShareCell.h"
#import "ODrinkDescriptionCell.h"
#import "ONotesViewController.h"
#import "CupMarkingsView.h"
#import "CupMarking.h"
#import "OCustomization.h"
#import "OCustomization+Extras.h"
#import "OShareTwitter.h"
#import "OShareFacebook.h"
#import "OFacebookViewController.h"
#import "OTwitterViewController.h"
#import <DYCore/categories/hash/NSData_hash.h>
#import <DYCore/files/DYFileNameManager.h>
#import "OMydrinksViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface OSavedDrinkViewController ()

@end

@implementation OSavedDrinkViewController

@synthesize tableView = mTableView;
@synthesize imageView;
@synthesize reverseAnimationImages;
@synthesize animationImages;
@synthesize drinkSelected = mDrinkSelected;
@synthesize imageView2 = imageView2;
//@synthesize favoriteSelected = mFavoriteSelected;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
        mAddingContext = [[NSManagedObjectContext alloc] init];
        [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
    [self.navigationItem setTitle:NSLocalizedString(@"Bebida guardada", nil)];
    [imageView setImage:[UIImage imageNamed:@"1.png"]];
    NSString * name = mDrinkSelected.name;
    name = [name stringByAppendingString:@".png"];
    UIImage * image = [UIImage imageNamed:name];
    if(image==nil)
    {
        //imageView.image = [UIImage imageNamed:@"Cafe standar.png"];
        imageView2.image = [UIImage imageNamed:@"Cafe standar.png"];
        
    }
    else
    {
        //mImageView.image = 
        //imageView.image = image;
        imageView2.image = image;
    }
    
    if([mDrinkSelected.name isEqualToString:@"Espresso Clásico"] || [mDrinkSelected.name isEqualToString:@"Espresso con Panna"] || [mDrinkSelected.name isEqualToString:@"Espresso Macchiato"])
    {
        
        //[mDoCupRotationButton setHidden:YES];
        [imageView setHidden:YES];
        
    }
    else
    { 
        //[mDoCupRotationButton setHidden:NO];
        [imageView setHidden:NO];
    }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
    
    //[imageView setImage:[UIImage imageNamed:@"1.png"]];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    if(_indexPath.row==0)
    {
        static NSString *CellIdentifier = @"ODrinkDescriptionCell";
        ODrinkDescriptionCell *cell = (ODrinkDescriptionCell  *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
        cell.nameLabel.text =mDrinkSelected.name;
        cell.availableLabel.text = mDrinkSelected.available;
        if(mDrinkSelected.user==nil || [mDrinkSelected.user isEqualToString:@""])
        {
            cell.propertyOfLabel.text = @"Yo";
        }
        else cell.propertyOfLabel.text = mDrinkSelected.user;

        return cell;
    
    }
    else
    {
         static NSString *CellIdentifier = @"OShareCell";
        OShareCell *cell = (OShareCell  *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
         cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
        cell.delegate = self;
        return cell;
    
    }
    
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
    if(_indexPath.row==0)
    {
        //TODO llamar al detail.
        [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
        ONotesViewController *detailViewController = [[ONotesViewController alloc] initWithNibName:@"ONotesViewController" bundle:nil];
        detailViewController.drinkSelected = mDrinkSelected;
        detailViewController.delegate=self;
        [self.navigationController pushViewController:detailViewController animated:YES];

    }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
    
        return 75;
    
    }
    else 
    {
        return 44;
    }
    
    
}

- (void) showInClipboard
{
    @try{
   
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"Bebida"
                         message:@"Bebida almacenada"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
        
    }
    @catch (NSException *ex) {
    
    NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/showinClipboard");
        UIAlertView *alert =[[UIAlertView alloc]
                         initWithTitle:@"No Disponible"
                         message:@"Opción no disponible"
                         delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
}

}

/*
#pragma mark Action sheet senders
- (void) showInClipboard
{
    @try{
    NSString *drinkLink = @"";
    drinkLink = [drinkLink stringByAppendingString:@"available="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.available];
    drinkLink = [drinkLink stringByAppendingString:@"&category="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.category];
    drinkLink = [drinkLink stringByAppendingString:@"&glassmark="];
    if(mDrinkSelected.glassmark!=nil) drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.glassmark];
    else drinkLink = [drinkLink stringByAppendingString:@""];
    drinkLink = [drinkLink stringByAppendingString:@"&image="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.image];
    drinkLink = [drinkLink stringByAppendingString:@"&largedescription="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.largedescription];
    drinkLink = [drinkLink stringByAppendingString:@"&name="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.name];
    drinkLink = [drinkLink stringByAppendingString:@"&nickName="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.nickName];
    drinkLink = [drinkLink stringByAppendingString:@"&notes="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.notes];
    drinkLink = [drinkLink stringByAppendingString:@"&shortdescription="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.shortdescription];
    drinkLink = [drinkLink stringByAppendingString:@"&size="];
    if(mDrinkSelected.size!=nil)drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.size];
    else drinkLink = [drinkLink stringByAppendingString:@""];
    
    drinkLink = [drinkLink stringByAppendingString:@"&subcategory="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.subcategory];
    drinkLink = [drinkLink stringByAppendingString:@"&temperature="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.temperature];
    drinkLink = [drinkLink stringByAppendingString:@"&user="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.user];
    drinkLink = [drinkLink stringByAppendingString:@"&userType=2"];
    
    NSSet * shots =(NSSet*) mDrinkSelected.customizations;
    NSArray * shotsArray = [shots allObjects];
    drinkLink = [drinkLink stringByAppendingString:@"&shots="];
    drinkLink = [drinkLink stringByAppendingString:[NSString stringWithFormat:@"%d",[shotsArray count]]];
    for(int i=0;i<[shotsArray count];i++)
    {
        OCustomization * custo  = (OCustomization*)[ shotsArray objectAtIndex:i];
        drinkLink = [drinkLink stringByAppendingString:@"&abbreviature="];
        drinkLink = [drinkLink stringByAppendingString:custo.abbreviature];
        drinkLink = [drinkLink stringByAppendingString:@"&amount="];
        drinkLink = [drinkLink stringByAppendingString:custo.amount];
        drinkLink = [drinkLink stringByAppendingString:@"&category="];
        drinkLink = [drinkLink stringByAppendingString:custo.category];
        drinkLink = [drinkLink stringByAppendingString:@"&subcategory="];
        drinkLink = [drinkLink stringByAppendingString:custo.subcategory];
        
    }
    
    
    
    NSData* data=[drinkLink dataUsingEncoding:NSUTF8StringEncoding];
    data = [data base64Encode];
    
    NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
    NSString * sendString = @"sbucks://";
    sendString = [sendString stringByAppendingString:newStr];
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = sendString;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Bebida copiada al clipboard" message:sendString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertView show];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        [alert release];
    }

}
 */



- (void) sendByEmail
{

    @try{
        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
        if (mailClass != nil)
        {
            // We must always check whether the current device is configured for sending emails
            if ([mailClass canSendMail])
            {
                [self displayComposerSheet];
            }
            else
            {
                [self launchMailAppOnDevice];
            }
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}


- (void) sendByFacebook
{
    @try{
    SHKItem *item = [self itemToShare];	
    if(item!=nil)
        [OShareFacebook shareItem:item];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


}

- (void) sendByTwitter
{
    @try{
    SHKItem *item = [self itemToShare];	
    if(item!=nil)
        [OShareTwitter shareItem:item];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (SHKItem *) itemToShare
{
    @try{
    SHKItem *item = nil;
    
    //MBT 10Dic2012
    //NSString * sended = [NSString stringWithFormat:@"Acabo de guardar mi bebida %@ de Starbucks Coffee mediante su aplicación móvil http://goo.gl/r2WGv",mDrinkSelected.name];
    NSString * sended = [NSString stringWithFormat:@"Acabo de guardar mi bebida %@ de Starbucks Coffee mediante su aplicación móvil.",mDrinkSelected.name];
    
    item = [SHKItem URL:[NSURL URLWithString:@"http://www.google.es"] title:mDrinkSelected.name imgURL:mDrinkSelected.image imgHref:mDrinkSelected.image ];
    item.text = sended;
    item.title = mDrinkSelected.name;
    [item setCustomValue:item.text forKey:@"status"];
    [item setCustomValue:item.title forKey:@"title"];
    
    return item;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

#pragma mark -
#pragma mark Compose Mail


-(void)displayComposerSheet 
{
    @try{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setSubject:NSLocalizedString(@"Añade esta bebida a tus favoritos",nil)];
    
    
    // Set up recipients
    NSArray *toRecipients = [NSArray arrayWithObject:@""]; 
    [picker setToRecipients:toRecipients];

    //NSString *drinkLink = @"sbucks://"; // Link to iTune App link
    NSString *drinkLink = @"";
    drinkLink = [drinkLink stringByAppendingString:@"available="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.available];
    drinkLink = [drinkLink stringByAppendingString:@"&category="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.category];
    drinkLink = [drinkLink stringByAppendingString:@"&glassmark="];
    if(mDrinkSelected.glassmark!=nil) drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.glassmark];
    else drinkLink = [drinkLink stringByAppendingString:@""];
    drinkLink = [drinkLink stringByAppendingString:@"&image="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.image];
    drinkLink = [drinkLink stringByAppendingString:@"&largedescription="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.largedescription];
    drinkLink = [drinkLink stringByAppendingString:@"&name="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.name];
    drinkLink = [drinkLink stringByAppendingString:@"&nickName="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.nickName];
    drinkLink = [drinkLink stringByAppendingString:@"&notes="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.notes];
    drinkLink = [drinkLink stringByAppendingString:@"&shortdescription="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.shortdescription];
    drinkLink = [drinkLink stringByAppendingString:@"&size="];
    if(mDrinkSelected.size!=nil)drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.size];
    else drinkLink = [drinkLink stringByAppendingString:@""];
    
    drinkLink = [drinkLink stringByAppendingString:@"&subcategory="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.subcategory];
    drinkLink = [drinkLink stringByAppendingString:@"&temperature="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.temperature];
    drinkLink = [drinkLink stringByAppendingString:@"&user="];
    drinkLink = [drinkLink stringByAppendingString:mDrinkSelected.user];
    drinkLink = [drinkLink stringByAppendingString:@"&userType=2"];
    
    NSSet * shots =(NSSet*) mDrinkSelected.customizations;
    NSArray * shotsArray = [shots allObjects];
    drinkLink = [drinkLink stringByAppendingString:@"&shots="];
    drinkLink = [drinkLink stringByAppendingString:[NSString stringWithFormat:@"%d",[shotsArray count]]];
    for(int i=0;i<[shotsArray count];i++)
    {
        OCustomization * custo  = (OCustomization*)[ shotsArray objectAtIndex:i];
        drinkLink = [drinkLink stringByAppendingString:@"&abbreviature="];
        drinkLink = [drinkLink stringByAppendingString:custo.abbreviature];
        drinkLink = [drinkLink stringByAppendingString:@"&amount="];
        drinkLink = [drinkLink stringByAppendingString:custo.amount];
        drinkLink = [drinkLink stringByAppendingString:@"&category="];
        drinkLink = [drinkLink stringByAppendingString:custo.category];
        drinkLink = [drinkLink stringByAppendingString:@"&subcategory="];
        drinkLink = [drinkLink stringByAppendingString:custo.subcategory];
        
    }

    
    
    NSData* data=[drinkLink dataUsingEncoding:NSUTF8StringEncoding];
    data = [data base64Encode];
    
    NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
    NSString * sendString = @"sbucks://";
    sendString = [sendString stringByAppendingString:newStr];
    
     NSString *emailBody = [NSString stringWithFormat:@" %@ te envió este %@. Haz click en el link para añadir esto a tus bebidas en la App de Starbucks. </br><a href = '%@'>Añadir bebida</a></p>                     ¿No tienes la App de Starbucks? Bájala desde App Store.",mDrinkSelected.user,mDrinkSelected.name, sendString];
    
    [picker setMessageBody:emailBody isHTML:YES];
    [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            break;
        case MFMailComposeResultSaved:
            
            break;
        case MFMailComposeResultSent:
            
            break;
        case MFMailComposeResultFailed:
            
            break;
        default:
            
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
    @try{
    NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
    NSString *body = @"&body=It is raining in sunny California!";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}




#pragma mark OShareCell delegate

-(void)showActionSheet:(int)sender
{
    @try{
    //[mShareImageView setHidden:NO];
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Enviar por Email",nil),NSLocalizedString(@"Publicar en Facebook",nil) , NSLocalizedString(@"Publicar en Twitter",nil) ,NSLocalizedString(@"Guardar en Galería",nil) , nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popupQuery showInView:self.view];
    //[mShareImageView setHidden:YES];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try{
    switch (buttonIndex) {
        case 0:
            [self sendByEmail];
            break;
        case 1:
        {
        
            OFacebookViewController *controller = [[OFacebookViewController alloc] initWithNibName:@"OFacebookViewController" bundle:nil];
            controller.drinkSelected = mDrinkSelected;
            //controller.delegate = self;
            SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:controller];
            
            if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) 
            {
                UIImage *image = [UIImage imageNamed:@"NavBar.png"];
                [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
                navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
                
                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                    [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
                    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                    [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
                }
            }
            
            
            [self presentModalViewController:navigationController animated:YES];

        }
    
            break;
        case 2:
        {
        
            OTwitterViewController *controller = [[OTwitterViewController alloc] initWithNibName:@"OTwitterViewController" bundle:nil];
            controller.drinkSelected = mDrinkSelected;
            //controller.delegate = self;
            SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:controller];
            
            if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) 
            {
                UIImage *image = [UIImage imageNamed:@"NavBar.png"];
                [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
                navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
                
                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                    [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
                    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                    [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
                }
            }
            
            
            [self presentModalViewController:navigationController animated:YES];
        
        }
            //[self sendByTwitter];
            break;
        case 3:
            [self showInClipboard];
            break;
            
        default:
            break;
    }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }



}

#pragma mark IBAction methods

-(IBAction)setCupRotation:(id)sender
{
    @try{
    if([mDrinkSelected.name isEqualToString:@"Espresso Clásico"] || [mDrinkSelected.name isEqualToString:@"Espresso con Panna"] || [mDrinkSelected.name isEqualToString:@"Espresso Macchiato"])
    {
        
        [self viewCupMarkings];
        
    }
    else
    { 
        [self doCupRotation:mIsReverse];
        
        
    }
    
    
    
    mIsReverse = !mIsReverse;
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


}

#pragma mark cup animation methods

- (NSArray *)animationImages {
    @try{
    if (!animationImages) 
    {
                    NSMutableArray *loadedImages = [NSMutableArray array];
            for (int i=0; i < 8; i++) 
            {
                UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png", i+1]];
                [loadedImages addObject:image];
            }
            animationImages = loadedImages;
    }
        
    
    return animationImages;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (NSArray *)reverseAnimationImages {
    @try{
    if (!reverseAnimationImages) 
    {
        NSEnumerator *reverseEnumerator = [self.animationImages reverseObjectEnumerator];
        NSMutableArray *reverseImagesMutable = [NSMutableArray array];
        for (UIImage *image in reverseEnumerator) 
        {
            [reverseImagesMutable addObject:image];
        }
        self.reverseAnimationImages = reverseImagesMutable;
    }
    return reverseAnimationImages;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}

- (void)setReverseAnimationImages:(NSArray *)value {
    @try{
    if (reverseAnimationImages == value) return;
    reverseAnimationImages = value;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}


- (NSTimeInterval)delayToShowCupMarkings 
{

    return 0.4;

}

- (NSTimeInterval)cupRotationAnimationDuration 
{
    return 0.35;
}

- (void)doCupRotation:(BOOL)show {
    if (!show) {
        imageView.animationImages = self.animationImages;
        imageView.animationDuration = [self cupRotationAnimationDuration];
        imageView.animationRepeatCount = 1;
        imageView.image = [animationImages objectAtIndex:animationImages.count - 1];
        [imageView startAnimating];
        [self performSelector:@selector(viewCupMarkings) withObject:nil afterDelay:[self delayToShowCupMarkings]];
    } else {
        imageView.animationImages = self.reverseAnimationImages;
        imageView.animationDuration = [self cupRotationAnimationDuration];
        imageView.animationRepeatCount = 1;
        imageView.image = [reverseAnimationImages objectAtIndex:reverseAnimationImages.count - 1];
        [imageView startAnimating];
        [self hideCupMarkings];
    }
}


-(NSArray*) cupMarkingsArray
{
    @try{
    NSMutableArray *cupBoxMarkingArrayArray = [NSMutableArray array];
    
    for(int i=0;i<6;i++)
    {
        
        NSArray* marks;
     
        
        switch (i) {
            case 0:
                marks = [[NSArray alloc] initWithArray:[OCustomization customizationWithCategory:@"Cafe" identi:mDrinkSelected.ide context:mAddingContext]];
                break;
            
            case 1:
                marks = [[NSArray alloc] initWithArray:[OCustomization customizationWithCategory:@"Shots" identi:mDrinkSelected.ide context:mAddingContext]];
                break;
            case 2:
                marks = [[NSArray alloc] initWithArray:[OCustomization customizationWithCategory:@"Sirope" identi:mDrinkSelected.ide context:mAddingContext]];
                break;
            case 3:
                marks = [[NSArray alloc] initWithArray:[OCustomization customizationWithCategory:@"Leche" identi:mDrinkSelected.ide context:mAddingContext]];
                break;
            case 4:
                marks = [[NSArray alloc] initWithArray:[OCustomization customizationWithCategory:@"Añadidos" identi:mDrinkSelected.ide context:mAddingContext]];
                break;
            case 5:
                marks = [[NSArray alloc] initWithArray:[OCustomization customizationWithCategory:@"Bebida" identi:mDrinkSelected.ide context:mAddingContext]];
                break;
            default:
                break;
        }
        
        
        
        NSMutableArray *boxArray = [NSMutableArray array];
        if(marks == nil || [marks count]==0)
        {
            if(i==5)
            {
                CupMarking *cupMarking  = [CupMarking new];
               if(mDrinkSelected.glassmark!=nil)
                cupMarking.text = mDrinkSelected.glassmark;
                else
                 cupMarking.text = @"";
                [boxArray addObject:cupMarking];
            }
            else
            {
            CupMarking *cupMarking  = [CupMarking new];
            cupMarking.text = @"";
            [boxArray addObject:cupMarking];
            }
        }
        
        
        for (int j=0; j<[marks count]; j++) 
        {
            
            
          

            
//MBT 20-12-12
            OCustomization * cust = (OCustomization*)[marks objectAtIndex:j];
            CupMarking *cupMarking  = [CupMarking new];
            
            
            if(i==4||i==2)
      
            cupMarking.text=       [NSString stringWithFormat:@"%@%@",cust.amount, cust.abbreviature];
            
            else
                
                cupMarking.text=       [NSString stringWithFormat:@"%@", cust.abbreviature];

            
            
            [boxArray addObject:cupMarking];
            
        }
        [cupBoxMarkingArrayArray addObject:boxArray];
    }
    
    return cupBoxMarkingArrayArray;
    
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}



- (void)viewCupMarkings {
    @try{
    if (!mIsReverse) return [self hideCupMarkings];
    if (!cupMarkingsView) {
        cupMarkingsView = [CupMarkingsView viewFromNib];
        //cupMarkingsView.frame = CGRectMake(150, 0, 156, 359);
        //cupMarkingsView.drink = drink;
        cupMarkingsView.CupMarkings = [self cupMarkingsArray];
        [self.view addSubview:cupMarkingsView];
        [cupMarkingsView reload];
        
        //Animate
        
        //cupMarkingsView.layer.anchorPoint = CGPointMake(0, 0.5);
        cupMarkingsView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        cupMarkingsView.alpha = 0.0;
        cupMarkingsView.center = CGPointMake(260, 160);
        
        [UIView beginAnimations:@"popInCupMarkings" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.15];
        cupMarkingsView.transform = CGAffineTransformIdentity;
        cupMarkingsView.alpha = 1.0;
        [UIView commitAnimations];
    }
    else
    {
        cupMarkingsView.CupMarkings = [self cupMarkingsArray];
    }
    mIsReverse = YES;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

}
- (void)hideCupMarkings {
    @try{
    
    [UIView beginAnimations:@"popInCupMarkings" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.15];
    [UIView setAnimationDidStopSelector:@selector(hideCupMarkingsAnimation:didFinish:context:)];
    [UIView setAnimationDelegate:self];
    cupMarkingsView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    cupMarkingsView.alpha = 0.0;
    [UIView commitAnimations];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OSavedDrinkViewController/initWithNibName");
        
        
        OMydrinksViewController  *regresa =[[OMydrinksViewController  alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:regresa animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }

    
}

- (void)hideCupMarkingsAnimation:(NSString *)name didFinish:(BOOL)finished context:(void *)context {
    [cupMarkingsView removeFromSuperview];
    cupMarkingsView = nil;
    mIsReverse = NO;
}


@end
