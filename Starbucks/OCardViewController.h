//
//  OCardViewController.h
//  Starbucks
//
//  Created by Mobile on 11/1/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ORewardsCard.h"
#import "OBaseViewController.h"

@protocol OCardViewControllerDelegate;

@interface OCardViewController : OBaseViewController
{
    ORewardsCard *card;
    IBOutlet UIView *viewCard;
    id <OCardViewControllerDelegate> delegate;
    
    UIButton *handleButton;
    UIImage *handleButtonImage;
    UIImage *handleButtonBlinkImage;
    NSTimer *handleButtonBlinkTimer;
    NSTimer *toolDrawerFadeTimer;
}

@property (strong, nonatomic) ORewardsCard *card;
@property (strong, nonatomic) IBOutlet UIView *viewCard;
@property (strong, nonatomic) IBOutlet UIButton *ShowReverseCardButton;
@property (nonatomic, assign) id<OCardViewControllerDelegate> delegate;
@property (strong, nonatomic) UIButton *handleButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *ReloadActivityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountLabel;
@property (strong, nonatomic) IBOutlet UIButton *CardReloadButton;
@property (strong, nonatomic) IBOutlet UIButton *CardManagementButton;

- (IBAction)ShowCardReverseButtonPressed;
- (IBAction)ReloadButtonPressed;
- (IBAction)ManageButtonPressed;

@end

@protocol OCardViewControllerDelegate
- (void)OCardViewControllerDidPressReverseButton:(OCardViewController *)controller;
- (void)OCardViewControllerDidReloadButtonPressed:(OCardViewController *)controller;
- (void)OCardViewControllerDidManageButtonPressed:(OCardViewController *)controller;

@end
