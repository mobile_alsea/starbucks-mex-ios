//
//  OCreditCardCell.m
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OCreditCardCell.h"

@implementation OCreditCardCell
@synthesize lblTitulo = mLblTitulo;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize index = mIndex;
@synthesize indexPathPair = mIndexPathPair;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
