//
//  OCoffee.h
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OFlavor, OImage;

@interface OCoffee : NSManagedObject

@property (nonatomic, strong) NSString * availability;
@property (nonatomic, strong) NSString * descrip;
@property (nonatomic, strong) NSString * form;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * profile;
@property (nonatomic, strong) NSSet *flavors;
@property (nonatomic, strong) NSSet *images;
@end

@interface OCoffee (CoreDataGeneratedAccessors)

- (void)addFlavorsObject:(OFlavor *)value;
- (void)removeFlavorsObject:(OFlavor *)value;
- (void)addFlavors:(NSSet *)values;
- (void)removeFlavors:(NSSet *)values;
- (void)addImagesObject:(OImage *)value;
- (void)removeImagesObject:(OImage *)value;
- (void)addImages:(NSSet *)values;
- (void)removeImages:(NSSet *)values;
@end
