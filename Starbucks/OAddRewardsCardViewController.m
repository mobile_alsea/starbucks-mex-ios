//
//  OAddRewardsCardViewController.m
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OAddRewardsCardViewController.h"
#import "OSignUpRewardsViewController.h"
#import "MBProgressHUD.h"
#import "NSData+Base64.h"
#import "NSObject+Sha.h"

#define VALIDATE_CARD_CONECTION 1
#define REGISTER_CARD_CONECTION 2

@interface OAddRewardsCardViewController ()
{
    int conectionType;
}
@property (strong, nonatomic) UIBarButtonItem *addCardButton;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) NSMutableData *receivedData;
@end

@implementation OAddRewardsCardViewController
@synthesize verfifyNumberCodeTextField;
@synthesize cardNumberTextField;
@synthesize addCardButton;
@synthesize cancelButton;
@synthesize receivedData;
@synthesize HUD;
@synthesize invocationFromCardManagementBool;

#pragma mark - NSURL Connection

/*
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    SecTrustRef trust = [protectionSpace serverTrust];
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(trust, 0);
    NSData* serverCertificateData = (__bridge  NSData*)SecCertificateCopyData(certificate);
    NSString *serverCertificateDataHash = [[serverCertificateData base64EncodedString] SHA256];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL areCertificatesEqualPT = ([serverCertificateDataHash isEqualToString:[defaults objectForKey:@"CERT_PT"]]);
    
    if (!areCertificatesEqualPT){
        [connection cancel];
        [HUD hide:YES];
        return NO;
    }
    return YES;
}
*/

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Error"];
    [dialog setMessage:[error localizedDescription]];
    [dialog addButtonWithTitle:@"OK"];
    [dialog show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError * error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"results:%@",results);
    [HUD hide:YES];
    
    int idRespuesta = [[results objectForKey:@"codigo"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        if (conectionType == VALIDATE_CARD_CONECTION) {
            [[NSUserDefaults standardUserDefaults] setObject:(NSString *)[results objectForKey:@"idPreregistro"] forKey:@"idPreregistro"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            OSignUpRewardsViewController *signUpController = [[OSignUpRewardsViewController alloc] initWithNibName:@"OSignUpRewardsViewController" bundle:nil];
            [self.navigationController pushViewController:signUpController animated:YES];
        }else if(conectionType == REGISTER_CARD_CONECTION){
            [self.navigationController popViewControllerAnimated:YES];
            if (invocationFromCardManagementBool == YES) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-ADD-CARD-FROM-MYCARDS" object:nil];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Starbucks-REGISTER-CARD-REWARDS" object:nil];
            }
        }
    }
}

#pragma marl lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            /*
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideModal:) name:@"Starbucks-ADDCARD-REWARDS" object:nil];
            */
        }
        return self;
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/MyDrinks/OFaceViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        [self.navigationItem setTitle:NSLocalizedString(@"Agregar Tarjeta", nil)];
 
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]!= nil) {
            addCardButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Continuar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(addRewardsCard:)];
            [self.navigationItem setRightBarButtonItem:addCardButton];
            
            cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancelar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(removeViewController:)];
            [self.navigationItem setLeftBarButtonItem:cancelButton];
            
        }else{
            addCardButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Continuar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(validateCard:)];
            [self.navigationItem setRightBarButtonItem:addCardButton];
            
            cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancelar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(hideModal:)];
            [self.navigationItem setLeftBarButtonItem:cancelButton];
        }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OAddRewardsCardViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [cardNumberTextField becomeFirstResponder];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [cardNumberTextField resignFirstResponder];
    [verfifyNumberCodeTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//for validate card webservice_rewards

-(IBAction)hideModal:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//for add card webservice_rewards

-(IBAction)removeViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)validateCard:(id)sender
{
    if ([cardNumberTextField.text length] == 0 || [verfifyNumberCodeTextField.text length] == 0) {
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"Aviso"
                             message:@"Por favor ingresa los datos de tu tarjeta."
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    //Validate Card
    UIWindow *tempKeyboardWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    HUD=[[MBProgressHUD alloc] initWithWindow:tempKeyboardWindow];
    [tempKeyboardWindow addSubview:HUD];
    [HUD show:YES];
    HUD.labelText = @"Validando tarjeta";
    HUD.detailsLabelText = @"Espere por favor...";
    HUD.dimBackground = YES;
    receivedData = [[NSMutableData alloc] init];
    NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
    validateCardSbx = [validateCardSbx stringByAppendingString:@"validarTarjeta"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:validateCardSbx]];
    NSLog(@"request:%@",validateCardSbx);
    [request setHTTPMethod:@"POST"];
    NSString *cardSbxPostString = [NSString stringWithFormat: @"NoTarjetaSbx=%@&CodigoVerificador=%@&UdId=%@",cardNumberTextField.text,verfifyNumberCodeTextField.text,[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]];
    NSData *postCardSbxData = [cardSbxPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postCardSbxLength = [NSString stringWithFormat:@"%d", [postCardSbxData length]];
    [request setValue:postCardSbxLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postCardSbxData];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    conectionType = VALIDATE_CARD_CONECTION;
    [connection start];
}

//For add a rewards Card
-(IBAction)addRewardsCard:(id)sender
{
    if ([cardNumberTextField.text length] == 0 || [verfifyNumberCodeTextField.text length] == 0) {
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"Aviso"
                             message:@"Por favor ingresa los datos de tu tarjeta."
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
    //Validate Card
    UIWindow *tempKeyboardWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    HUD=[[MBProgressHUD alloc] initWithWindow:tempKeyboardWindow];
    [tempKeyboardWindow addSubview:HUD];
    [HUD show:YES];
    HUD.labelText = @"Registrando tarjeta";
    HUD.detailsLabelText = @"Espere por favor...";
    HUD.dimBackground = YES;
    receivedData = [[NSMutableData alloc] init];
    NSString *addCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
    addCardSbx = [addCardSbx stringByAppendingString:@"agregarTarjetaRewards"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:addCardSbx]];
    NSLog(@"request:%@",addCardSbx);
    [request setHTTPMethod:@"POST"];
    NSString *cardSbxPostString = [NSString stringWithFormat: @"NoTarjetaSbx=%@&CodigoVerificador=%@",cardNumberTextField.text,verfifyNumberCodeTextField.text];
    NSData *postCardSbxData = [cardSbxPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postCardSbxLength = [NSString stringWithFormat:@"%d", [postCardSbxData length]];
    [request setValue:postCardSbxLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [request setHTTPBody:postCardSbxData];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    conectionType = REGISTER_CARD_CONECTION;
    [connection start];
}


#pragma mark UITextfield delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == cardNumberTextField){
        //detectamos si hay un backspace puesto que también mueven el largo si son tecleados
        int largo= [textField.text length];
        if ([string isEqualToString:@""]) {
            largo = largo - 1;
        }
        
        if(largo < 15)
        {
            [addCardButton setEnabled:NO];
        }else{
            if ([cardNumberTextField.text length]== 16 && [verfifyNumberCodeTextField.text length]==8) {
                [addCardButton setEnabled:YES];
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return (newLength > 16) ? NO : YES;
        }
    }else if(textField == verfifyNumberCodeTextField){
        
        NSUInteger largo = [textField.text length] + [string length] - range.length;
        
        if(largo < 8)
        {
            [addCardButton setEnabled:NO];
        }else{
            if ([cardNumberTextField.text length]== 16 && [verfifyNumberCodeTextField.text length]<=8) {
                [addCardButton setEnabled:YES];
            }
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return (newLength > 8) ? NO : YES;
        }
    }
    return YES;
}




@end
