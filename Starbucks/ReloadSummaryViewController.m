//
//  ReloadSummaryViewController.m
//  Starbucks
//
//  Created by Mobile on 10/24/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "ReloadSummaryViewController.h"
#import "OPaymentTypeCell.h"
#import "OReloadCell.h"
#import "OCreditCardCell.h"
#import "OAddCreditCardViewController.h"

@interface ReloadSummaryViewController ()
@property (strong , nonatomic) NSMutableArray *arrayReloads;

@end

@implementation ReloadSummaryViewController
@synthesize sView;
@synthesize viewReloadSummary;
@synthesize ReloadSummaryTableView;
@synthesize reloadAmount;
@synthesize autoreloadOnBalance;
@synthesize arrayReloads;
@synthesize CancelReloadButton;
@synthesize confirmReloadAmount;
@synthesize HUD;
@synthesize receivedData;

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ReloadSummaryViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Hacer Recarga", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Atras", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Dismiss:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        
        [CancelReloadButton.layer setCornerRadius:4.0f];
        [CancelReloadButton.layer setMasksToBounds:YES];
        
        ReloadSummaryTableView.delegate = self;
        [sView addSubview:viewReloadSummary];
        [sView setContentSize:viewReloadSummary.frame.size];
        
        arrayReloads = [[NSMutableArray alloc] init];
        [arrayReloads addObject:(NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"RELOAD_AMOUNT"]];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"] != nil) {
            [arrayReloads addObject:(NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"]];
        }
        
        [arrayReloads addObject:@"visa terminación 9799"];
        
        NSLog(@"Count:%i",[arrayReloads count]);
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OAddCreditCardViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark DismissViewController

-(IBAction)Dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    return [arrayReloads count];
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    
	@try{
         if ([arrayReloads count] == 3) {
             if (_indexPath.row == 2){
                 static NSString *CellIdentifier0 = @"OCreditCardCell";
                 OCreditCardCell *cell = (OCreditCardCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier0];
                 if (cell == nil){
                     cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier0 owner:nil options:nil] lastObject];
                 }
                 cell.lblTitulo.text =  @"visa terminación 9799";
                 return cell;
             }else if(_indexPath.row == 1){
                 static NSString *CellIdentifier = @"OReloadCell";
                 OReloadCell *cell = (OReloadCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                 if (cell == nil){
                     cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                 }
                 cell.balanceLabel.text = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
                 cell.statusLabel.text = @"Auto recarga en saldo de:";
                 return cell;
             }else{
                 static NSString *CellIdentifier = @"OReloadCell";
                 OReloadCell *cell = (OReloadCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                 if(cell == nil){
                     cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                 }
                 cell.balanceLabel.text = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"RELOAD_AMOUNT"];
                 cell.statusLabel.text = @"Monto de recarga:";
                 return cell;
             }
         }else{
             if (_indexPath.row == 1){
                 static NSString *CellIdentifier0 = @"OCreditCardCell";
                 OCreditCardCell *cell = (OCreditCardCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier0];
                 if (cell == nil){
                     cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier0 owner:nil options:nil] lastObject];
                 }
                 cell.lblTitulo.text =  @"visa terminación 9799";
                 return cell;
             }else{
                 static NSString *CellIdentifier = @"OReloadCell";
                 OReloadCell *cell = (OReloadCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                 if (cell == nil){
                     cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                 }
                 cell.balanceLabel.text = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"RELOAD_AMOUNT"];
                 cell.statusLabel.text = @"Monto de recarga";
                 return cell;
             }
         }

        /*
        //if (_indexPath.row == 1){
            static NSString *CellIdentifier0 = @"OCreditCardCell";
            OCreditCardCell *cell2 = (OCreditCardCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier0];
            if (cell2 == nil){
                cell2 = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier0 owner:nil options:nil] lastObject];
            }
            cell2.lblTitulo.text =  @"visa terminación 9799";
            return cell2;
        }else{
            static NSString *CellIdentifier = @"OReloadCell";
            OReloadCell *cell = (OReloadCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil){
                cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
            }
            cell.balanceLabel.text = @"prueba";
            cell.statusLabel.text = @"Monto de recarga";
        return cell;
        }
        */
        
    }@catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAutoReloadViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
        /*
         [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
         
         OSelectPaymentViewController *detailViewController = [[OSelectPaymentViewController alloc] initWithNibName:@"OSelectPaymentViewController" bundle:nil];
         ;
         [self.navigationController pushViewController:detailViewController animated:YES];
         */
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}



- (IBAction)ReloadConfirmButtonPressed:(id)sender {
    
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Confirma tu constraseña"];
    [dialog setMessage:@"Ingresa tu contraseña de tu cuenta de Starbucks"];
    [dialog addButtonWithTitle:@"Cancel"];
    [dialog addButtonWithTitle:@"Hacer Recarga"];
    dialog.tag = 5;
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [dialog setTransform: moveUp];
    [dialog show];
}

- (IBAction)ReloadCancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
-(void)loquesea {
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    
    receivedData = [[NSMutableData alloc] init];
    
    NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_SMART"];
    validateCardSbx = [validateCardSbx stringByAppendingString:@"Recarga"];
    NSLog(@"VALidate:%@",validateCardSbx);
    
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:reloadTransactionDictionary options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError) {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    } else {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:validateCardSbx] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"12345678" forHTTPHeaderField:@"token"];
    [request setValue:@"10263" forHTTPHeaderField:@"idUsuario"];
    //[request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    //[request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: jsonData];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    [connection start];
}
*/


#pragma mark NSURLConnection

/*
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
 {
 SecTrustRef trust = [protectionSpace serverTrust];
 SecCertificateRef certificate = SecTrustGetCertificateAtIndex(trust, 0);
 NSData* serverCertificateData = (__bridge  NSData*)SecCertificateCopyData(certificate);
 NSString *serverCertificateDataHash = [[serverCertificateData base64EncodedString] SHA256];
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 BOOL areCertificatesEqualPT = ([serverCertificateDataHash isEqualToString:[defaults objectForKey:@"CERT_PT"]]);
 
 if (!areCertificatesEqualPT){
 [connection cancel];
 [HUD hide:YES];
 return NO;
 }
 return YES;
 }
 */

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Error"];
    [dialog setMessage:[error localizedDescription]];
    [dialog addButtonWithTitle:@"OK"];
    [dialog show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError * error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"results:%@",results);
    [HUD hide:YES];
    
    int idRespuesta = [[results objectForKey:@"codigo"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }
}
@end
