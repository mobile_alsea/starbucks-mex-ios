//
//  ORewardsCardCell.m
//  Starbucks
//
//  Created by Mobile on 10/18/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "ORewardsCardCell.h"

#define RGBToFloat(f) (f/255.0)

@implementation ORewardsCardCell
@synthesize balanceLabel = mBalanceLabel;
@synthesize cardInformartionLabel = mCardInformartionLabel;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize index = mIndex;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}


#pragma mark - Private methods


- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
	{
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
		
		
	}
    else
	{
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
		
	}
}


@end
