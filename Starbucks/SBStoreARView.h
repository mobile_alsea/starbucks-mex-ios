//
//  SBStoreARView.h
//  Starbucks
//
//  Created by Ironbit Ninja on 17/07/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SBStoreARView;
@protocol SBStoreARViewDelegate <NSObject>
- (void)storeViewDidSelectShowOrientation:(SBStoreARView *)storeView;
- (void)storeViewDidSelectShowDetail:(SBStoreARView *)storeView;
- (void)storeView:(SBStoreARView *)storeView selected:(BOOL)selected;
@end

@interface SBStoreARView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowView;
@property (weak, nonatomic) IBOutlet UIImageView *openImageView;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UILabel *showDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *showOrientationLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeDistanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *showDetailButton;
@property (weak, nonatomic) IBOutlet UIButton *showOrientationButton;

@property (assign, nonatomic) BOOL isOpen;
@property (weak, nonatomic) id <SBStoreARViewDelegate> delegate;

+ (id)getView;
- (void)storeSelected;

@end
