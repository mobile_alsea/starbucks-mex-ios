//
//  ORecentTransactionsViewController.m
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "ORecentTransactionsViewController.h"
#import "ORecentTransactionCell.h"
#import "OSelectPaymentViewController.h"

@interface ORecentTransactionsViewController ()
@property (strong, nonatomic) NSArray *recentTransactionsArray;
@end

@implementation ORecentTransactionsViewController
@synthesize sView;
@synthesize recentTransactionsView;
@synthesize HUD;
@synthesize receivedData;
@synthesize cardId;
@synthesize cardBalance;
@synthesize cardLastUpdate;
@synthesize dateLabel;
@synthesize balanceLabel;
@synthesize recentTransactionsArray;
@synthesize transactionsTableView;

#pragma mark - NSURL Connection

/*
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
 {
 SecTrustRef trust = [protectionSpace serverTrust];
 SecCertificateRef certificate = SecTrustGetCertificateAtIndex(trust, 0);
 NSData* serverCertificateData = (__bridge  NSData*)SecCertificateCopyData(certificate);
 NSString *serverCertificateDataHash = [[serverCertificateData base64EncodedString] SHA256];
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 BOOL areCertificatesEqualPT = ([serverCertificateDataHash isEqualToString:[defaults objectForKey:@"CERT_PT"]]);
 
 if (!areCertificatesEqualPT){
 [connection cancel];
 [HUD hide:YES];
 return NO;
 }
 return YES;
 }
 */

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Error"];
    [dialog setMessage:[error localizedDescription]];
    [dialog addButtonWithTitle:@"OK"];
    [dialog show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError * error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    [HUD hide:YES];
    
    int idRespuesta = [[results objectForKey:@"codigo"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        recentTransactionsArray = [[NSArray alloc] initWithArray:(NSArray *)[results objectForKey:@"movimientos"]];
        NSLog(@"recentTransactionsArray:%@",recentTransactionsArray);
        NSLog(@"recentTransactionsArray:%i",recentTransactionsArray.count);
        if ([recentTransactionsArray count] > 0) {
            if (recentTransactionsArray.count < 7) {
                int tableviewHeight = [recentTransactionsArray count] * 44;
                transactionsTableView.frame = CGRectMake(8, 140, 304, tableviewHeight);
                
            }else{
                int heightWithLargeTable = 140;
                int tableviewHeight = [recentTransactionsArray count] * 44;
                transactionsTableView.frame = CGRectMake(8, 140, 304, tableviewHeight);
                heightWithLargeTable = heightWithLargeTable + tableviewHeight;
                
                recentTransactionsView.frame = CGRectMake(0.0f, 0.0f, 320.0f, heightWithLargeTable);
                [sView setContentSize:CGSizeMake(0, heightWithLargeTable)];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:recentTransactionsArray forKey:@"userCardTransactions"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.transactionsTableView setHidden:NO];
            [self.transactionsTableView reloadData];
        }
        //necesito procesar el arreglo
    }
}

#pragma mark Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            /*
            appDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            addingContext = [[NSManagedObjectContext alloc] init];
            [addingContext setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
            */
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORecentTransactionsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Mis Tarjetas", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Movimientos Recientes", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Dismiss:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        self.transactionsTableView.delegate = self;
        dateLabel.text = cardLastUpdate;
        double balanceDouble = [cardBalance doubleValue];
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [currencyFormatter setCurrencySymbol:@""];
        balanceLabel.text = [NSString stringWithFormat:@"$%@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:balanceDouble]]];
        [self.transactionsTableView setHidden:YES];
        //recentTransactionsView.frame = CGRectMake(0, 0, 320, 500);
        [sView addSubview:recentTransactionsView];
        [sView setContentSize:recentTransactionsView.frame.size];
        receivedData = [[NSMutableData alloc] init];
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        receivedData = [[NSMutableData alloc] init];
        NSString *cardTransactionSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
        cardTransactionSbx = [cardTransactionSbx stringByAppendingString:@"consultaTransaccionesPorTarjeta"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:cardTransactionSbx]];
        [request setHTTPMethod:@"POST"];
        NSString *cardSbxPostString = [NSString stringWithFormat: @"idTarjeta=%@",cardId];
        NSData *postCardSbxData = [cardSbxPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postCardSbxLength = [NSString stringWithFormat:@"%d", [postCardSbxData length]];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
        [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setValue:postCardSbxLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postCardSbxData];
        NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        [connection start];
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/ORecentTransactionsViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}


-(IBAction)Dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    return [recentTransactionsArray count] + 1;
    //return 4;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ORecentTransactionCell* cell=[transactionsTableView dequeueReusableCellWithIdentifier:@"ORecentTransactionCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ORecentTransactionCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.descriptionLabel.text = (NSString *)[[recentTransactionsArray objectAtIndex:indexPath.row] objectForKey:@"descripcion"];
    cell.dateLabel.text = (NSString *)[[recentTransactionsArray objectAtIndex:indexPath.row] objectForKey:@"fecha"];
    double amountDouble = [[[recentTransactionsArray objectAtIndex:indexPath.row] objectForKey:@"saldo"] doubleValue];
    NSLog(@"%f",amountDouble);
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencySymbol:@""];
    cell.amountLabel.text = [NSString stringWithFormat:@"$%@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:amountDouble]]];
    
    [cell setIndexPathPair:(indexPath.row%2 == 0) ? YES : NO];
    return cell;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
        [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
        
        /*
        OSelectPaymentViewController *detailViewController = [[OSelectPaymentViewController alloc] initWithNibName:@"OSelectPaymentViewController" bundle:nil];
        ;
        [self.navigationController pushViewController:detailViewController animated:YES];
        */
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
	
}

@end
