//
//  SBStoreARView.m
//  Starbucks
//
//  Created by Ironbit Ninja on 17/07/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import "SBStoreARView.h"

CGFloat const kSBStoreARViewSelectionOffset = 77.0f;

@interface SBStoreARView ()

<UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIImage *openImage;
@property (strong, nonatomic) UIImage *closeImage;
@property (assign, nonatomic) BOOL isSelected;


- (IBAction)showOrientation:(id)sender;
- (IBAction)showDetail:(id)sender;
- (IBAction)selectView:(id)sender;


@end

@implementation SBStoreARView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//      Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
	self.openImage = [UIImage imageNamed:@"compass_tienda_abierto"];
	self.closeImage = [UIImage imageNamed:@"compass_tienda_cerrado"];	
	self.isOpen = NO;
	self.isSelected = NO;
	self.clipsToBounds = YES;
	self.detailView.alpha = 0.0f;
	
	self.storeNameLabel.text = @"";
	self.storeAddressLabel.text = @"";
	self.storeDistanceLabel.text = @"";
			
	self.showDetailLabel.text = NSLocalizedString(@"Mostrar Detalle", nil);
	self.showOrientationLabel.text = NSLocalizedString(@"Mostrar Orientación", nil);
		
	self.backgroundColor = [UIColor clearColor];
	self.backgroundImageView.image = [[UIImage imageNamed:@"burbuja"] resizableImageWithCapInsets:UIEdgeInsetsMake(50.0, 10.0, 30.0, 1.0f)];
}

+ (id)getView
{
	NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"SBStoreARView" owner:nil options:nil];
	return [nibViews objectAtIndex:0];
}

#pragma mark - Custom Setters

- (void)setIsOpen:(BOOL)isOpen
{
	_isOpen = isOpen;
	self.openImageView.image = _isOpen?self.openImage:self.closeImage;
}

#pragma mark - Selection Methods

- (void)storeSelected
{
	CGRect viewFrame = self.frame;
	CGFloat offset;
	CGFloat detailAlpha;
	if(self.isSelected) {
		offset = -kSBStoreARViewSelectionOffset;
		detailAlpha = 0.0f;
	} else {
		offset = kSBStoreARViewSelectionOffset;
		detailAlpha = 1.0f;
	}
	viewFrame.size.height += offset;
	self.isSelected = !self.isSelected;
	__weak SBStoreARView *weakSelf = self;
	[UIView animateWithDuration:0.2 animations:^{
		if(weakSelf) {
			SBStoreARView *strongSelf = weakSelf;
			strongSelf.frame = viewFrame;
			strongSelf.detailView.alpha = detailAlpha;
		}
	} completion:^(BOOL finished) {
        if(weakSelf) {
            SBStoreARView *strongSelf = weakSelf;
            if([strongSelf.delegate respondsToSelector:@selector(storeView:selected:)])
                [strongSelf.delegate storeView:strongSelf selected:strongSelf.isSelected];
        }
    }];
}

#pragma mark - Target/Action Methods

- (IBAction)showOrientation:(id)sender {
	[self storeSelected];
	if([self.delegate respondsToSelector:@selector(storeViewDidSelectShowOrientation:)])
		[self.delegate storeViewDidSelectShowOrientation:self];
}

- (IBAction)showDetail:(id)sender {
	[self storeSelected];
	if([self.delegate respondsToSelector:@selector(storeViewDidSelectShowDetail:)])
		[self.delegate storeViewDidSelectShowDetail:self];
}

- (IBAction)selectView:(id)sender {
	[self storeSelected];
}

#pragma mark - UIGestureRecognizer Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	if([gestureRecognizer.view isKindOfClass:[UIButton class]] || [otherGestureRecognizer.view isKindOfClass:[UIButton class]])
		return NO;
	else
		return YES;
}


@end
