//
//  OBenefitsRewardsViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface OBenefitsRewardsViewController : OBaseViewController
{
	IBOutlet UIView * mTitleView;
	IBOutlet UISegmentedControl * mSegmentedControl;
	IBOutlet UIImageView * mImageView;
    IBOutlet UIImageView * mPrueba;
	IBOutlet UIScrollView *mScrollView;

}

-(IBAction)segmentedControlValueChange:(id)sender;

@end
