/**
 * Copyright (c) 2009 Alex Fajkowski, Apparent Logic LLC
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#import <UIKit/UIKit.h>
#import "AFItemView.h"
#import <QuartzCore/QuartzCore.h>


@protocol AFOpenFlowViewDataSource;
@protocol AFOpenFlowViewDelegate;

@interface AFOpenFlowView : UIView {
	id <AFOpenFlowViewDataSource>	__weak dataSource;
	id <AFOpenFlowViewDelegate>	__weak viewDelegate;
	NSMutableSet					*offscreenCovers;
	NSMutableDictionary				*onscreenCovers;
	NSMutableDictionary				*coverImages;
	NSMutableDictionary				*coverImageHeights;
	UIImage							*defaultImage;
	CGFloat							defaultImageHeight;

	UIScrollView					*scrollView;
	int								lowerVisibleCover;
	int								upperVisibleCover;
	int								numberOfImages;
	int								beginningCover;
	
	AFItemView						*selectedCoverView;

	CATransform3D leftTransform, rightTransform;
	
	CGFloat halfScreenHeight;
	CGFloat halfScreenWidth;
	
	Boolean isSingleTap;
	Boolean isDoubleTap;
	Boolean isDraggingACover;
	CGFloat startPosition;
	
	// Variables from Constants
	NSInteger mCoverSpacing;		// COVER_SPACING		80
	NSInteger mCenterCoverOffset;	// CENTER_COVER_OFFSET	70
	double mSideCoverAngle;			// SIDE_COVER_ANGLE		0.90
	NSInteger mSideCoverZPosition;	// SIDE_COVER_ZPOSITION	-80
	double mScaleCover;				// SCALE_COVER			0.90
	double mScaleCenterCover;		// 1.0
	double mTransform_m34;			// TRANSFORM_M34		-0.001
	
	double mReflectionFraction;		// REFLECTION_FRACTION	0.0
	double mReflectionAlpha;		// REFLECTION_ALPHA		0.90
	double mVerticalPositionFactor;	// 1.0
	
	double mSpeedFactor;			// 1.5
}

@property (nonatomic, weak) IBOutlet id <AFOpenFlowViewDataSource> dataSource;
@property (nonatomic, weak) IBOutlet id <AFOpenFlowViewDelegate> viewDelegate;
@property (nonatomic, strong) UIImage *defaultImage;
@property int numberOfImages;

@property (nonatomic)	NSInteger	coverSpacing;
@property (nonatomic)	NSInteger	centerCoverOffset;
@property (nonatomic)	double		sideCoverAngle;
@property (nonatomic)	NSInteger	sideCoverZPosition;
@property (nonatomic)	double		scaleCover;
@property (nonatomic)	double		scaleCenterCover;
@property (nonatomic)	double		transform_m34;
@property (nonatomic)	double		reflectionFraction;
@property (nonatomic)	double		reflectionAlpha;
@property (nonatomic)	double		verticalPositionFactor;
@property (nonatomic)	double		speedFactor;

- (void)setSelectedCover:(int)newSelectedCover;
- (void)centerOnSelectedCover:(BOOL)animated;
- (void)setImage:(UIImage *)image forIndex:(int)index;

- (void) updateTransform;
- (NSInteger) coverUnderPoint:(CGPoint)_point;
- (NSInteger) selectedCover;

@end

@protocol AFOpenFlowViewDelegate <NSObject>
@optional
- (void)openFlowView:(AFOpenFlowView *)openFlowView selectionDidChange:(int)index;
- (void)openFlowView:(AFOpenFlowView *)openFlowView didTapSelectedImage:(int)index;
@end

@protocol AFOpenFlowViewDataSource <NSObject>
- (void)openFlowView:(AFOpenFlowView *)openFlowView requestImageForIndex:(int)index;
- (UIImage *)defaultImage;
@end