/**
 * Copyright (c) 2009 Alex Fajkowski, Apparent Logic LLC
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#import "AFOpenFlowView.h"
#import "AFOpenFlowConstants.h"
#import "AFUIImageReflection.h"


@interface AFOpenFlowView (hidden)

- (void)setUpInitialState;
- (AFItemView *)coverForIndex:(int)coverIndex;
- (void)updateCoverImage:(AFItemView *)aCover;
- (AFItemView *)dequeueReusableCover;
- (void)layoutCovers:(int)selected fromCover:(int)lowerBound toCover:(int)upperBound;
- (void)layoutCover:(AFItemView *)aCover selectedCover:(int)selectedIndex animated:(Boolean)animated;
- (AFItemView *)findCoverOnscreen:(CALayer *)targetLayer;
- (void) calculateCenter;

@end

@implementation AFOpenFlowView (hidden)

- (void)setUpInitialState 
{
    @try{
	// Constants
	mCoverSpacing = COVER_SPACING;
	mCenterCoverOffset = CENTER_COVER_OFFSET;
	mSideCoverAngle = SIDE_COVER_ANGLE;
	mSideCoverZPosition = SIDE_COVER_ZPOSITION;
	mScaleCover = SCALE_COVER;
	mScaleCenterCover = 1.0;
	mTransform_m34 = TRANSFORM_M34;
	mReflectionFraction = REFLECTION_FRACTION;
	mReflectionAlpha = REFLECTION_ALPHA;
	mVerticalPositionFactor = 1.0;
	mSpeedFactor = 1.5;
	
	// Set up the default image for the coverflow.
	self.defaultImage = [self.dataSource defaultImage];
	
	// Create data holders for onscreen & offscreen covers & UIImage objects.
	coverImages = [[NSMutableDictionary alloc] init];
	coverImageHeights = [[NSMutableDictionary alloc] init];
	offscreenCovers = [[NSMutableSet alloc] init];
	onscreenCovers = [[NSMutableDictionary alloc] init];
	
	scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
	scrollView.userInteractionEnabled = NO;
	scrollView.multipleTouchEnabled = NO;
	scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self addSubview:scrollView];
	
	self.multipleTouchEnabled = NO;
	self.userInteractionEnabled = YES;
	self.autoresizesSubviews = YES;
	//self.layer.position=CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
	
	// Initialize the visible and selected cover range.
	lowerVisibleCover = upperVisibleCover = -1;
	selectedCoverView = nil;
	
	[self updateTransform];
	
	// Set some perspective
	CATransform3D sublayerTransform = CATransform3DIdentity;
	sublayerTransform.m34 = mTransform_m34;
	[scrollView.layer setSublayerTransform:sublayerTransform];
	
	[self setBounds:self.frame];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/setupInitial");
    }
}

- (AFItemView *)coverForIndex:(int)coverIndex {
    @try{
	AFItemView *coverView = [self dequeueReusableCover];
	if (!coverView)
	{
		coverView = [[AFItemView alloc] initWithFrame:CGRectZero];
		coverView.coverSpacing = mCoverSpacing;
		coverView.verticalPositionFactor = mVerticalPositionFactor;
	}
	
	coverView.number = coverIndex;
	
	return coverView;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/coverForIndex");
    }
}

- (void)updateCoverImage:(AFItemView *)aCover {
    @try{
	NSNumber *coverNumber = [NSNumber numberWithInt:aCover.number];
	UIImage *coverImage = (UIImage *)[coverImages objectForKey:coverNumber];
	if (coverImage) {
		NSNumber *coverImageHeightNumber = (NSNumber *)[coverImageHeights objectForKey:coverNumber];
		if (coverImageHeightNumber)
			[aCover setImage:coverImage originalImageHeight:[coverImageHeightNumber floatValue] reflectionFraction:mReflectionFraction];
	} else {
		[aCover setImage:defaultImage originalImageHeight:defaultImageHeight reflectionFraction:mReflectionFraction];
		[self.dataSource openFlowView:self requestImageForIndex:aCover.number];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/updateCoverImage");
    }
}

- (AFItemView *)dequeueReusableCover {
    @try{
	AFItemView *aCover = [offscreenCovers anyObject];
	if (aCover) {
		[offscreenCovers removeObject:aCover];
	}
	return aCover;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/dequeueReusableCover");
    }
}

- (void)layoutCover:(AFItemView *)aCover selectedCover:(int)selectedIndex animated:(Boolean)animated  {
    @try{
	int coverNumber = aCover.number;
	CATransform3D newTransform;
	CGFloat newZPosition = mSideCoverZPosition;
	CGPoint newPosition;
	
	newPosition.x = halfScreenWidth + aCover.horizontalPosition;
	newPosition.y = halfScreenHeight + aCover.verticalPosition;
	if (coverNumber < selectedIndex) {
		newPosition.x -= mCenterCoverOffset;
		newTransform = leftTransform;
	} else if (coverNumber > selectedIndex) {
		newPosition.x += mCenterCoverOffset;
		newTransform = rightTransform;
	} else {
		newZPosition = 0;
		//newTransform = CATransform3DIdentity;
		newTransform = CATransform3DMakeScale(mScaleCenterCover, mScaleCenterCover, mScaleCenterCover);
	}
	
	if (animated) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
		[UIView setAnimationBeginsFromCurrentState:YES];
	}
	
	aCover.layer.transform = newTransform;
	aCover.layer.zPosition = newZPosition;
	aCover.layer.position = newPosition;
	
	if (animated) {
		[UIView commitAnimations];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/layoutCover");
    }
}

- (void)layoutCovers:(int)selected fromCover:(int)lowerBound toCover:(int)upperBound {
    @try{
	AFItemView *cover;
	NSNumber *coverNumber;
	for (int i = lowerBound; i <= upperBound; i++) {
		coverNumber = [[NSNumber alloc] initWithInt:i];
		cover = (AFItemView *)[onscreenCovers objectForKey:coverNumber];
		[self layoutCover:cover selectedCover:selected animated:YES];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/layoutCovers");
    }
}

- (AFItemView *)findCoverOnscreen:(CALayer *)targetLayer {
    @try{
	// See if this layer is one of our covers.
	NSEnumerator *coverEnumerator = [onscreenCovers objectEnumerator];
	AFItemView *aCover = nil;
	while (aCover = (AFItemView *)[coverEnumerator nextObject])
		if ([[aCover.imageView layer] isEqual:targetLayer])
			break;
	
	return aCover;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/findCoverOnscreen");
    }
}

- (void) calculateCenter {
	halfScreenWidth = self.bounds.size.width / 2;
	halfScreenHeight = self.bounds.size.height / 2;
}

@end


@implementation AFOpenFlowView
@synthesize dataSource, viewDelegate, numberOfImages, defaultImage;

@synthesize coverSpacing = mCoverSpacing;
@synthesize centerCoverOffset = mCenterCoverOffset;
@synthesize sideCoverAngle = mSideCoverAngle;
@synthesize sideCoverZPosition = mSideCoverZPosition;
@synthesize scaleCover = mScaleCover;
@synthesize scaleCenterCover = mScaleCenterCover;
@synthesize transform_m34 = mTransform_m34;
@synthesize reflectionFraction = mReflectionFraction;
@synthesize reflectionAlpha = mReflectionAlpha;
@synthesize verticalPositionFactor = mVerticalPositionFactor;
@synthesize speedFactor = mSpeedFactor;

#define COVER_BUFFER 6

- (void)awakeFromNib {
	[self setUpInitialState];
}

- (id)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
		[self setUpInitialState];
	}
	
	return self;
}

- (void)dealloc {
	
	[offscreenCovers removeAllObjects];
	
	[onscreenCovers removeAllObjects];
	
}

- (void)setBounds:(CGRect)newSize {
    @try{
	[super setBounds:newSize];
	
	[self calculateCenter];

	int lowerBound = MAX(-1, selectedCoverView.number - COVER_BUFFER);
	int upperBound = MIN(self.numberOfImages - 1, selectedCoverView.number + COVER_BUFFER);

	[self layoutCovers:selectedCoverView.number fromCover:lowerBound toCover:upperBound];
	[self centerOnSelectedCover:NO];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/setBounds");
    }
}

- (int)numberOfImages
{
    return numberOfImages;
}

- (void)setNumberOfImages:(int)newNumberOfImages
{
    @try{
	if(newNumberOfImages<numberOfImages)
	{
		for(NSInteger i=newNumberOfImages;i<=numberOfImages;i++)
		{
			AFItemView *cover = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:i]];
			if(cover!=nil)
			{
				[offscreenCovers addObject:cover];
				[cover removeFromSuperview];
				[onscreenCovers removeObjectForKey:[NSNumber numberWithInt:cover.number]];
			}
		}
	}
	
	numberOfImages = newNumberOfImages;
	scrollView.contentSize = CGSizeMake(newNumberOfImages * mCoverSpacing + self.bounds.size.width, self.bounds.size.height);

	int lowerBound = MAX(0, selectedCoverView.number - COVER_BUFFER);
	int upperBound = MIN(self.numberOfImages - 1, selectedCoverView.number + COVER_BUFFER);
	
	if (selectedCoverView)
		[self layoutCovers:selectedCoverView.number fromCover:lowerBound toCover:upperBound];
	else
		[self setSelectedCover:0];
	
	[self centerOnSelectedCover:NO];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/setNumberOfImages");
    }
}

- (void)setDefaultImage:(UIImage *)newDefaultImage {
    @try{
	defaultImageHeight = newDefaultImage.size.height;
	defaultImage = [newDefaultImage addImageReflection:mReflectionFraction alpha:mReflectionAlpha];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/setDefaultImage");
    }
}

- (void)setImage:(UIImage *)image forIndex:(int)index {
    @try{
	// Create a reflection for this image.
	UIImage *imageWithReflection = [image addImageReflection:mReflectionFraction alpha:mReflectionAlpha];
	NSNumber *coverNumber = [NSNumber numberWithInt:index];
	[coverImages setObject:imageWithReflection forKey:coverNumber];
	[coverImageHeights setObject:[NSNumber numberWithFloat:image.size.height] forKey:coverNumber];
	
	// If this cover is onscreen, set its image and call layoutCover.
	AFItemView *aCover = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:index]];
	if (aCover) {
		[aCover setImage:imageWithReflection originalImageHeight:image.size.height reflectionFraction:mReflectionFraction];
		[self layoutCover:aCover selectedCover:selectedCoverView.number animated:NO];
	}
        
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/setImage");
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    @try{
	//NSLog(@"touchesBegan");
	
	CGPoint startPoint = [[touches anyObject] locationInView:self];
	isDraggingACover = NO;
	
	// Which cover did the user tap?
	CALayer *targetLayer = (CALayer *)[scrollView.layer hitTest:startPoint];
	AFItemView *targetCover = [self findCoverOnscreen:targetLayer];
	isDraggingACover = (targetCover != nil);

	beginningCover = selectedCoverView.number;
	// Make sure the user is tapping on a cover.
	startPosition = (startPoint.x / mSpeedFactor) + scrollView.contentOffset.x;	// 1.5
	
	if (isSingleTap)
		isDoubleTap = YES;
		
	isSingleTap = ([touches count] == 1);
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/touchesBegan");
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    @try{
	//NSLog(@"touchesMoved");
	
	isSingleTap = NO;
	isDoubleTap = NO;
	
	// Only scroll if the user started on a cover.
	if (!isDraggingACover)
		return;
	
	CGPoint movedPoint = [[touches anyObject] locationInView:self];
	CGFloat offset = startPosition - (movedPoint.x / mSpeedFactor);	// 1.5
	CGPoint newPoint = CGPointMake(offset, 0);
	scrollView.contentOffset = newPoint;
	int newCover = offset / mCoverSpacing;
	if (newCover != selectedCoverView.number) {
		if (newCover < 0)
			[self setSelectedCover:0];
		else if (newCover >= self.numberOfImages)
			[self setSelectedCover:self.numberOfImages - 1];
		else
			[self setSelectedCover:newCover];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/tocuhesMoved");
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    @try{
		
	//NSLog(@"touchesEnded");
	
	UITouch *touch = [touches anyObject];
	
	if (isSingleTap && [touch tapCount]==1) {
		// Which cover did the user tap?
		CGPoint targetPoint = [[touches anyObject] locationInView:self];
		CALayer *targetLayer = (CALayer *)[scrollView.layer hitTest:targetPoint];
		AFItemView *targetCover = [self findCoverOnscreen:targetLayer];
		if (targetCover){
			if (targetCover.number != selectedCoverView.number){
				[self setSelectedCover:targetCover.number];
			}
			else{
				if ([self.viewDelegate respondsToSelector:@selector(openFlowView:didTapSelectedImage:)])
					[self.viewDelegate openFlowView:self didTapSelectedImage:selectedCoverView.number];
			}
		}
	}
	[self centerOnSelectedCover:YES];
	
	// And send the delegate the newly selected cover message.
	if (beginningCover != selectedCoverView.number)
		if ([self.viewDelegate respondsToSelector:@selector(openFlowView:selectionDidChange:)])
			[self.viewDelegate openFlowView:self selectionDidChange:selectedCoverView.number];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/tocuhesEnded");
    }
}

- (void)centerOnSelectedCover:(BOOL)animated {
    @try{
	CGPoint selectedOffset = CGPointMake(mCoverSpacing * selectedCoverView.number, 0);
	[scrollView setContentOffset:selectedOffset animated:animated];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/centerOnSelectCover");
    }
}

- (void)setSelectedCover:(int)newSelectedCover {
    @try{
	if (selectedCoverView && (newSelectedCover == selectedCoverView.number))
		return;
	
	AFItemView *cover = nil;
	int newLowerBound = MAX(0, newSelectedCover - COVER_BUFFER);
	int newUpperBound = MIN(self.numberOfImages - 1, newSelectedCover + COVER_BUFFER);
	if (!selectedCoverView) {
		// Allocate and display covers from newLower to newUpper bounds.
		for (int i=newLowerBound; i <= newUpperBound; i++) {
			cover = [self coverForIndex:i];
			[onscreenCovers setObject:cover forKey:[NSNumber numberWithInt:i]];
			[self updateCoverImage:cover];
			[scrollView.layer addSublayer:cover.layer];
			//[scrollView addSubview:cover];
			[self layoutCover:cover selectedCover:newSelectedCover animated:NO];
		}
		
		lowerVisibleCover = newLowerBound;
		upperVisibleCover = newUpperBound;
		selectedCoverView = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:newSelectedCover]];
		
		return;
	}
	
	// Check to see if the new & current ranges overlap.
	if ((newLowerBound > upperVisibleCover) || (newUpperBound < lowerVisibleCover)) {
		// They do not overlap at all.
		// This does not animate--assuming it's programmatically set from view controller.
		// Recycle all onscreen covers.
		AFItemView *cover;
		for (int i = lowerVisibleCover; i <= upperVisibleCover; i++) {
			cover = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:i]];
			[offscreenCovers addObject:cover];
			[cover removeFromSuperview];
			[onscreenCovers removeObjectForKey:[NSNumber numberWithInt:cover.number]];
		}
			
		// Move all available covers to new location.
		for (int i=newLowerBound; i <= newUpperBound; i++) {
			cover = [self coverForIndex:i];
			[onscreenCovers setObject:cover forKey:[NSNumber numberWithInt:i]];
			[self updateCoverImage:cover];
			[scrollView.layer addSublayer:cover.layer];
		}

		lowerVisibleCover = newLowerBound;
		upperVisibleCover = newUpperBound;
		selectedCoverView = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:newSelectedCover]];
		[self layoutCovers:newSelectedCover fromCover:newLowerBound toCover:newUpperBound];
		
		return;
	} else if (newSelectedCover > selectedCoverView.number) {
		// Move covers that are now out of range on the left to the right side,
		// but only if appropriate (within the range set by newUpperBound).
		for (int i=lowerVisibleCover; i < newLowerBound; i++) {
			cover = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:i]];
			if (upperVisibleCover < newUpperBound) {
				// Tack it on the right side.
				upperVisibleCover++;
				cover.number = upperVisibleCover;
				[self updateCoverImage:cover];
				[onscreenCovers setObject:cover forKey:[NSNumber numberWithInt:cover.number]];
				[self layoutCover:cover selectedCover:newSelectedCover animated:NO];
			} else {
				// Recycle this cover.
				[offscreenCovers addObject:cover];
				[cover removeFromSuperview];
			}
			[onscreenCovers removeObjectForKey:[NSNumber numberWithInt:i]];
		}
		lowerVisibleCover = newLowerBound;
		
		// Add in any missing covers on the right up to the newUpperBound.
		for (int i=upperVisibleCover + 1; i <= newUpperBound; i++) {
			cover = [self coverForIndex:i];
			[onscreenCovers setObject:cover forKey:[NSNumber numberWithInt:i]];
			[self updateCoverImage:cover];
			[scrollView.layer addSublayer:cover.layer];
			[self layoutCover:cover selectedCover:newSelectedCover animated:NO];
		}
		upperVisibleCover = newUpperBound;
	} else {
		// Move covers that are now out of range on the right to the left side,
		// but only if appropriate (within the range set by newLowerBound).
		for (int i=upperVisibleCover; i > newUpperBound; i--) {
			cover = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:i]];
			if (lowerVisibleCover > newLowerBound) {
				// Tack it on the left side.
				lowerVisibleCover --;
				cover.number = lowerVisibleCover;
				[self updateCoverImage:cover];
				[onscreenCovers setObject:cover forKey:[NSNumber numberWithInt:lowerVisibleCover]];
				[self layoutCover:cover selectedCover:newSelectedCover animated:NO];
			} else {
				// Recycle this cover.
				[offscreenCovers addObject:cover];
				[cover removeFromSuperview];
			}
			[onscreenCovers removeObjectForKey:[NSNumber numberWithInt:i]];
		}
		upperVisibleCover = newUpperBound;
		
		// Add in any missing covers on the left down to the newLowerBound.
		for (int i=lowerVisibleCover - 1; i >= newLowerBound; i--) {
			cover = [self coverForIndex:i];
			[onscreenCovers setObject:cover forKey:[NSNumber numberWithInt:i]];
			[self updateCoverImage:cover];
			[scrollView.layer addSublayer:cover.layer];
			//[scrollView addSubview:cover];
			[self layoutCover:cover selectedCover:newSelectedCover animated:NO];
		}
		lowerVisibleCover = newLowerBound;
	}

	if (selectedCoverView.number > newSelectedCover)
		[self layoutCovers:newSelectedCover fromCover:newSelectedCover toCover:selectedCoverView.number];
	else if (newSelectedCover > selectedCoverView.number)
		[self layoutCovers:newSelectedCover fromCover:selectedCoverView.number toCover:newSelectedCover];
	
	selectedCoverView = (AFItemView *)[onscreenCovers objectForKey:[NSNumber numberWithInt:newSelectedCover]];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/setSelectedCover");
    }
}

- (NSInteger) coverUnderPoint:(CGPoint)_point
{
    @try{
	NSInteger dev = NSNotFound;
	
	CALayer *targetLayer = (CALayer *)[scrollView.layer hitTest:_point];
	AFItemView *targetCover = [self findCoverOnscreen:targetLayer];
	if (targetCover)
		dev = targetCover.number;
	
	return dev;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AFOpenFlowView.m/coverUnderPoint");
    }
}

- (void) updateTransform
{
	// Set up the cover's left & right transforms.
	leftTransform = CATransform3DIdentity;
	leftTransform = CATransform3DRotate(leftTransform, mSideCoverAngle, 0.0f, 1.0f, 0.0f);
	leftTransform = CATransform3DScale(leftTransform, mScaleCover, mScaleCover, 1);
	
	rightTransform = CATransform3DIdentity;
	rightTransform = CATransform3DRotate(rightTransform, mSideCoverAngle, 0.0f, -1.0f, 0.0f);
	rightTransform = CATransform3DScale(rightTransform, mScaleCover, mScaleCover, 1);
}

- (NSInteger) selectedCover
{
	return selectedCoverView.number;
}

@end