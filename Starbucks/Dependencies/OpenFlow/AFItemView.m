/**
 * Copyright (c) 2009 Alex Fajkowski, Apparent Logic LLC
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#import "AFItemView.h"
#import <QuartzCore/QuartzCore.h>
#import "AFOpenFlowConstants.h"


@implementation AFItemView
@synthesize number, imageView, horizontalPosition, verticalPosition;
@synthesize verticalPositionFactor = mVerticalPositionFactor;

@synthesize coverSpacing = mCoverSpacing;

- (id)initWithFrame:(CGRect)frame {
    @try{
	if (self = [super initWithFrame:frame]) {
		self.opaque = YES;
		self.backgroundColor = NULL;
		verticalPosition = 0;
		horizontalPosition = 0;
		mVerticalPositionFactor = 1.0;
		
		// Image View
		imageView = [[UIImageView alloc] initWithFrame:frame];
		imageView.opaque = YES;
		[self addSubview:imageView];
		
		mCoverSpacing = COVER_SPACING;
		
		//Too slow
//		self.layer.shadowOffset = CGSizeMake(0,3);
//		self.layer.shadowColor = [UIColor blackColor].CGColor;
//		self.layer.shadowOpacity = 0.3;
	}
	
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AfItem.m/initWithFrame");
    }
}

- (void)setImage:(UIImage *)newImage originalImageHeight:(CGFloat)imageHeight reflectionFraction:(CGFloat)reflectionFraction {
    @try{
	[imageView setImage:newImage];
	verticalPosition = (imageHeight * reflectionFraction / 2) * mVerticalPositionFactor;
	originalImageHeight = imageHeight;
	if(newImage!=nil)
		self.frame = CGRectMake(0, 0, newImage.size.width, newImage.size.height);
	else
		self.frame = CGRectZero;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AfItem.m/setImage");
    }
}


- (int)number
{
    return number;
}

- (void)setNumber:(int)newNumber {
    @try{
	//horizontalPosition = COVER_SPACING * newNumber;
	horizontalPosition = mCoverSpacing * newNumber;
	number = newNumber;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AfItem.m/setNumber");
    }
}

- (CGSize)calculateNewSize:(CGSize)baseImageSize boundingBox:(CGSize)boundingBox {
    @try{
	CGFloat boundingRatio = boundingBox.width / boundingBox.height;
	CGFloat originalImageRatio = baseImageSize.width / baseImageSize.height;
	
	CGFloat newWidth;
	CGFloat newHeight;
	if (originalImageRatio > boundingRatio) {
		newWidth = boundingBox.width;
		newHeight = boundingBox.width * baseImageSize.height / baseImageSize.width;
	} else {
		newHeight = boundingBox.height;
		newWidth = boundingBox.height * baseImageSize.width / baseImageSize.height;
	}
	
	return CGSizeMake(newWidth, newHeight);
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AfItem.m/calculateNewSize");
    }
}

- (void)setFrame:(CGRect)newFrame {
    @try{
	[super setFrame:newFrame];
	[imageView setFrame:newFrame];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Dependencies/OpenFlow/AfItem.m/setFrame");
    }
}


@end