//
//  OCofeesImporter.m
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCofeesImporter.h"
#import "OAppDelegate.h"
#import "OCoffee.h"
#import "OFlavor.h"
#import "OImage.h"
#import "OCoffee+Extras.h"
@implementation OCofeesImporter


#pragma mark private methods

- (NSData *) getData
{
    @try{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kCofeesURL]];
	return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/cofeesimporter/getdata");
    }
}

- (void) didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDict
{
    @try{
    if ([_elementName isEqualToString:@"cafes"])
    {
        // Remove old stores
        NSArray *cofees = [OCoffee coffeesInContext:mAddingContext];
        [mAppDelegate removeObjects:cofees fromContext:mAddingContext];
        [mAddingContext processPendingChanges];
    }
	else if ([_elementName isEqualToString:@"cafe"])
	{
		mCurrentCoffeeDict = [[NSMutableDictionary alloc] init];
        
        mIsFlavor = NO;
        mIsImage = NO;
	}
	
    else if ([_elementName isEqualToString:@"sabores"])
    {
        mCoffeeFlavorArray = [[NSMutableArray alloc] init];
        mIsFlavor = YES;
    }
	else if ([_elementName isEqualToString:@"sabor"]) {
		
		mCurrentFlavorDict = [[NSMutableDictionary alloc] init];
	}
    else if ([_elementName isEqualToString:@"imagenes"])
    {
        mCoffeeImageArray = [[NSMutableArray alloc] init];
        mIsImage = YES;
    }
    else if ([_elementName isEqualToString:@"imagen"])
    {
        mCurrentImageDict = [[NSMutableDictionary alloc] init];
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/disstartelement");
    }

}

- (void) didEndElement:(NSString *)_elementName withElementValue:(NSString *)_elementValue
{
    @try{
	if ([_elementName isEqualToString:@"cafe"])
	{
        [self insertIntoContext:mCurrentCoffeeDict];
	}
   // else if (_elementValue != nil && [_elementName isEqualToString:@"sabores"])
   // {
     //   [mCoffeeFlavorArray addObject:mCurrentFlavorDict];
		
//    }
//    else if (_elementValue != nil && [_elementName isEqualToString:@"imagenes"])
//    {
//        [mCoffeeImageArray addObject:mCurrentImageDict];
//    }
	else if (_elementValue != nil && [_elementName isEqualToString:@"nombre"])
	{
            [mCurrentCoffeeDict setObject:_elementValue forKey:@"name"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"descripcion"])
	{
		[mCurrentCoffeeDict setObject:_elementValue forKey:@"descrip"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"disponibilidad"])
	{
		[mCurrentCoffeeDict setObject:_elementValue forKey:@"availability"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"perfil"])
	{
		[mCurrentCoffeeDict setObject:_elementValue forKey:@"profile"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"forma"])
	{
		[mCurrentCoffeeDict setObject:_elementValue forKey:@"form"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"imagenprincipal"])
	{
		[mCurrentCoffeeDict setObject:_elementValue forKey:@"image"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"imagen"])
	{
		[mCurrentImageDict setObject:_elementValue forKey:@"name"];
        [mCoffeeImageArray addObject:mCurrentImageDict];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"sabor"])
	{
		[mCurrentFlavorDict setObject:_elementValue forKey:@"name"];
        [mCoffeeFlavorArray addObject:mCurrentFlavorDict];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/didendelement");
    }
  
}

#pragma mark - CoreData Methods

- (void) insertIntoContext:(NSDictionary *)_item
{
    @try{
    NSEntityDescription *coffeeEntity = [NSEntityDescription entityForName:@"Coffees" inManagedObjectContext:mAddingContext];
	
	OCoffee *coffee = [[OCoffee alloc] initWithEntity:coffeeEntity insertIntoManagedObjectContext:mAddingContext];
    
	coffee.name = [_item objectForKey:@"name"];
    coffee.descrip = [_item objectForKey:@"descrip"];
	coffee.availability = [_item objectForKey:@"availability"];
	coffee.profile = [_item objectForKey:@"profile"];
	coffee.form = [_item objectForKey:@"form"];
	coffee.image = [_item objectForKey:@"image"];
   
    

	for (NSDictionary *flavourDict in mCoffeeFlavorArray)
    {   
        NSEntityDescription *flavourEntity = [NSEntityDescription entityForName:@"Flavors" inManagedObjectContext:mAddingContext];
        OFlavor * flavour = [[OFlavor alloc] initWithEntity:flavourEntity insertIntoManagedObjectContext:mAddingContext];
			NSLog(@"%@",[flavourDict objectForKey:@"name"]);
			flavour.name = [flavourDict objectForKey:@"name"];
		
        
        [coffee addFlavorsObject:flavour];
    }
	
	for (NSDictionary *imageDict in mCoffeeImageArray)
    {   
        NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"Images" inManagedObjectContext:mAddingContext];
        OImage * image = [[OImage alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
        NSLog(@"%@",[imageDict objectForKey:@"name"]);
        image.name = [imageDict objectForKey:@"name"];
		
        
        [coffee addImagesObject:image];
    }
    
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/insertintocontext");
    }
}


#pragma mark - Delegate Methods

- (void) importerDidFinishAllItems:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishAllItems:withData:withIdentifier:)])
		[mDelegate importerDidFinishAllItems:self withData:mAddingContext withIdentifier:@"OCoffeesImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/importerdidfinish");
    }
}

- (void) importerDidFinishItem:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishItem:withData:withIdentifier:)])
		[mDelegate importerDidFinishItem:self withData:mAddingContext withIdentifier:@"OCoffeesImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/importerdidfinish");
    }
}

- (void) importerDidFail:(NSError *)_error
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFail:withError:withIdentifier:)])
		[mDelegate importerDidFail:self withError:_error withIdentifier:@"OCoffeesImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/importerdidfail");
    }
}

@end
