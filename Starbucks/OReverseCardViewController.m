//
//  OReverseCardViewController.m
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OReverseCardViewController.h"

@interface OReverseCardViewController ()

@end

@implementation OReverseCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
     self.navigationController.navigationBar.hidden = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ShowFrontCard:(id)sender {
    [self setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self dismissModalViewControllerAnimated:YES];
}
@end
