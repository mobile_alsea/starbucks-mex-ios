//
//  OSharedConstants.h
//  Starbucks
//
//  Created by specktro on 27/02/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#define KOSCoachMarkKey                             @"coachMarkKey"
#define kOSARAllowed                                @"ARAllowed"
#define kOSLockARDismissKey							@"lockARDismissKey"
#define kOSAREnabled								@"AREnabled"
#define kOSARDisabled								@"ARDisabled"
#define kOSDismissLockFromController				@"DismissLockFromController"