//
//  OBillingCardCell.h
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYImageView.h"

@interface OBillingCardCell : UITableViewCell
{
    UIImageView * thumbnailImageView;
    UILabel *mLblTitulo;
    BOOL mIndexPathPair;
    int mIndex;
}
@property(nonatomic, strong) IBOutlet UIImageView * thumbnailImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;
@end
