//
//  SBCoachingView.h
//  Starbucks
//
//  Created by Victor Soto on 7/22/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBCoachingView : UIView

@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *radioLabel;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;

+ (id)getView;
+ (BOOL)shouldPresentView;

- (void)show;
- (void)dismiss;

@end
