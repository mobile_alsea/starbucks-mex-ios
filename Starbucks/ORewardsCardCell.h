//
//  ORewardsCardCell.h
//  Starbucks
//
//  Created by Mobile on 10/18/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DYImageView.h"

@interface ORewardsCardCell : UITableViewCell
{
    DYImageView * mThumbnailImageView;
    UILabel * mBalanceLabel;
    UILabel * mCardInformartionLabel;
    BOOL mIndexPathPair;
    int mIndex;
}

@property(nonatomic, strong) IBOutlet UIView * thumbnailImageView;
@property(nonatomic, strong) IBOutlet UILabel * balanceLabel;
@property(nonatomic, strong) IBOutlet UILabel * cardInformartionLabel;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;

@end
