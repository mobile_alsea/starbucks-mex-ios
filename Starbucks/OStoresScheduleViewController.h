//
//  OStoresScheduleViewController.h
//  Starbucks
//
//  Created by Adrián Caramés on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OStore+Extras.h"
#import "OBaseViewController.h"

@interface OStoresScheduleViewController : OBaseViewController <UITableViewDataSource>
{
    UITableView *mTableView;
    
    OStore *mStoreSelected;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) OStore *storeSelected;
@property (assign, nonatomic) BOOL comesFromModal;

@end
