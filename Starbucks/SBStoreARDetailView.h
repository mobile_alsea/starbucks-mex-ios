//
//  SBStoreARDetailView.h
//  Starbucks
//
//  Created by Ironbit Ninja on 19/07/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBStoreARDetailView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *openImageView;
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeDistanceLabel;

@property (assign, nonatomic) BOOL isOpen;

+ (id)getView;

@end
