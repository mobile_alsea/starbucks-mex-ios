//
//  OFoodResultViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFoodResultViewController.h"
#import "OFoodCell.h"
#import "OFoodDetailViewController.h"
#import "OFood.h"
#import "OFood+Extras.h"
#import "OAppDelegate.h"
#import "UIImageExtras.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OMyFavorites.h"
#import "OMyFavorites+Extras.h"

#define kDefaultHeigth			416
#define kDefaultHeaderHeight	0

@interface OFoodResultViewController ()

@end

@implementation OFoodResultViewController
@synthesize tableView = mTableView;
@synthesize headerLabel = mHeaderLabel;
@synthesize fetchedResultsController = mFetchedResultsController;
@synthesize categorySelected = mCategorySelected;
@synthesize filtersArray = mFiltersArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mOpenFlowSelected = 0;
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}


- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	
	
	if([mFiltersArray count]>0)
	{
	
		UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ToolbarFilterOn.png"] style:UIBarButtonItemStyleBordered target:self action:nil];
		btnFilter.enabled =NO;
    
    
		[self.navigationItem setRightBarButtonItem:btnFilter];
	}
	
	
	mAppDelegate = (OAppDelegate *) [UIApplication sharedApplication].delegate;
	[self.navigationItem setTitle:mCategorySelected];
	
	[self executeFetch];

	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];

    [mOpenFlowView setNumberOfImages:[sectionInfo numberOfObjects]];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated
{
    @try{
		[super viewWillAppear:animated];
        if(!isOS6())
        {
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
            UIViewController * prueba = [[UIViewController alloc] init];
            [prueba.view setHidden:YES];
            [self presentModalViewController:prueba animated:NO];
            [self dismissModalViewControllerAnimated:NO];
        }
		
	
	
	if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight || self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	{
		[self.navigationController.navigationBar setHidden:YES];
		
		//mOpenFlowView.frame = CGRectMake(80, 0, mOpenFlowView.frame.size.width, mOpenFlowView.frame.size.height);
		[mCoverFlowDetailView setHidden:NO];
		[mProductTitleLabel setHidden:NO];
		[mDetailView setHidden:YES];
	}
	else
	{
		[self.navigationController.navigationBar setHidden:NO];
		[mCoverFlowDetailView setHidden:YES];
		[mProductTitleLabel setHidden:YES];
	}

    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	if(mOpenFlowView!=nil)	[mOpenFlowView setBounds:mCoverFlowDetailView.bounds];

}


-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[mOpenFlowView setBounds:mCoverFlowDetailView.bounds];
	if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
	{
		[self.navigationController.navigationBar setHidden:YES];
		
		/*CGRect coverFrame = CGRectMake(0, kDefaultHeaderHeight, mCoverFlowDetailView.frame.size.width, kDefaultHeigth - kDefaultHeaderHeight);
		[mCoverFlowDetailView setFrame:coverFrame];
		[mCoverFlowDetailView setBounds:coverFrame];*/
		[mCoverFlowDetailView setHidden:NO];
		[mProductTitleLabel setHidden:NO];
	}
	else
	{
		[self.navigationController.navigationBar setHidden:NO];
		/*CGRect coverFrame = CGRectMake(0, kDefaultHeaderHeight, mCoverFlowDetailView.frame.size.width, kDefaultHeigth - kDefaultHeaderHeight);
		[mCoverFlowDetailView setFrame:coverFrame];
		[mCoverFlowDetailView setBounds:coverFrame];*/
		[mCoverFlowDetailView setHidden:YES];
		[mProductTitleLabel setHidden:YES];
	}
	

}

- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}



- (void) configureCell:(OFoodCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    OFood *food = [self.fetchedResultsController objectAtIndexPath:_indexPath];
    
    _cell.titleLabel.text = food.name;
	
	UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [_cell.avalaibleButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [_cell.avalaibleButton setTitle:food.availability forState:UIControlStateNormal];
	
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:food.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		NSURL * url = [NSURL URLWithString:food.image];
		
		if(url!=nil)[_cell.thumbnailImageView setUrl:url];
	}
	else
	{
		
		[_cell.thumbnailImageView setImage:[UIImage imageWithData:image.image]];

	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) executeFetch
{
    mFetchedResultsController = nil;
    
	NSError *error;

	if (![[self fetchedResultsController] performFetch:&error])
	{
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}

    [mTableView reloadData];
    
}


- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	
    NSFetchRequest *fetchRequest;
	
	if(mFiltersArray!=nil && [mFiltersArray count]>0)
	{
	
		fetchRequest = [OFood foodWithCategory:mCategorySelected andAlergenos:mFiltersArray context:[OAppDelegate managedObjectContext]];
		
	}
	else 	fetchRequest = [OFood foodWithCategory:mCategorySelected context:[OAppDelegate managedObjectContext]];
	
	
	// Create the sort descriptors array.
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    mFetchedResultsController.delegate = self;
	return mFetchedResultsController;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}




#pragma mark UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OFoodCell";
  //  OFood *item = [self.fetchedResultsController objectAtIndexPath:_indexPath];

    OFoodCell *cell = (OFoodCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
    }
    
    // Setting Cell
	cell.index = _indexPath.row;
	cell.thumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-alimentos-low.png"];
	[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
    [cell updateSchedule:0];
	//if (item.image != nil && item.imageIcon == nil)
	//	[self downloadImage:item.image cell:cell index:[NSString stringWithFormat:@"%d",_indexPath.row]];
	
	// Configure cell

	[self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
	OFood *food = [self.fetchedResultsController objectAtIndexPath:_indexPath];

	OFoodDetailViewController *detailViewController = [[OFoodDetailViewController alloc] initWithNibName:@"OFoodDetailViewController" bundle:nil];
	[detailViewController setFoodSelected:food];
    detailViewController.delegate = self;	

	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

	
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

		return 65;

}

#pragma mark Openflow Protocol Delegate

// delegate protocol to tell which image is selected
- (void)openFlowView:(AFOpenFlowView *)openFlowView selectionDidChange:(int)index{
	
    @try{
	OFood *food = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];

	mProductTitleLabel.text = food.name;
	[mDetailView setHidden:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

// setting the image 1 as the default pic
- (UIImage *)defaultImage{
	UIImage * image = [UIImage imageNamed:@"noimage-alimentos-high.png"];
	image = [image cropCenterAndScaleImageToSize:CGSizeMake(200, 200)];
	return image;
}



- (void)openFlowView:(AFOpenFlowView *)openFlowView didTapSelectedImage:(int)index
{
    @try{
	mOpenFlowSelected = index;
	
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	[mDetailView setHidden:NO];  
    [UIView commitAnimations];
	
	OFood *food = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
	mProductNameDetail.text = food.name;
	mProductDetailDetail.text = food.largedescrip;
	UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [mAvailableButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [mAvailableButton setTitle:food.availability forState:UIControlStateNormal];
	
	mFavorite =[OMyFavorites favoriteWithName:food.name context:mAddingContext];
	
	if(mFavorite!=nil)
	{ 
		mIsAdded = YES;
		[mAddingContext deleteObject:mFavorite];
		mAddToFavsLabel.text = @"Eliminar de mis Alimentos";
	}
	else
	{
		mIsAdded = NO;
		mAddToFavsLabel.text = @"Añadir a mis Alimentos";
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
	
}


- (void)openFlowView:(AFOpenFlowView *)openFlowView requestImageForIndex:(int)index
{
    @try{
	
		OFood *food = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:food.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[self downloadImage:food.image cell:nil index:[NSString stringWithFormat:@"%d",index]];
	}
	else
	{
		UIImage * image2 = [UIImage imageWithData:image.image];
		image2 = [image2 cropCenterAndScaleImageToSize:CGSizeMake(200, 200)];
		[mOpenFlowView setImage:image2 forIndex:index];
	}
	if(index==0)mProductTitleLabel.text = food.name;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

#pragma mark -
#pragma mark Image Download Methods

//- (void) downloadImage:(NSString *)_urlString indexPath:(NSIndexPath *)_indexPath
- (void) downloadImage:(NSString *)_urlString cell:(UITableViewCell *)_cell index:(NSString*) _index
{	

    @try{
	NSMutableDictionary *values = [[NSMutableDictionary alloc] init];
	[values setObject:_urlString forKey:@"URL"];
	
	if(_cell!=nil)[values setObject:_cell forKey:@"Cell"];
	[values setObject:_index forKey:@"Index"];
	
	NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(downloadImageTask:) object:values];
	[mAppDelegate.operationQueue addOperation:invocationOperation];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) downloadImageTask:(NSMutableDictionary *)_values
{
    @try{
	NSString *urlString = [_values objectForKey:@"URL"];
	
	NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]];
	
	
	if (data != nil)
	{	
		[_values setObject:data forKey:@"ImageData"];
		
		
		[self performSelectorOnMainThread:@selector(updateCellImage:) withObject:[NSDictionary dictionaryWithDictionary:_values] waitUntilDone:YES];			
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void) updateCellImage:(NSDictionary *)_values
{
    
    @try{
	NSData *imageData = [_values objectForKey:@"ImageData"];
	//	NSIndexPath *indexPath = [_values objectForKey:@"IndexPath"];
	
	//	id cell = [self.tableView cellForRowAtIndexPath:indexPath];
	id cell = [_values objectForKey:@"Cell"];
	if(cell!=nil)
	{	
		[cell setValue:[UIImage imageWithData:imageData] forKeyPath:@"thumbnailImageView.image"];
	}
	
	
	NSInteger indice = [[_values objectForKey:@"Index"] intValue];
	
	
	UIImage * image = [UIImage imageWithData:imageData];
	image = [image cropCenterAndScaleImageToSize:CGSizeMake(200, 200)];
	[mOpenFlowView setImage:image forIndex:(int)indice];

	
	
	OFood *item = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:(int)indice inSection:0]];
	
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = item.image;
	imageObject.image = imageData;
	[mAddingContext save:nil];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

#pragma mark IBactions


-(IBAction)hideDetailViewButtonPressed:(id)sender
{
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
	[mDetailView setHidden:YES];
    [UIView commitAnimations];
}

-(IBAction)addToMyFood:(id)sender
{
    @try{
	OFood *food = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:mOpenFlowSelected inSection:0]];
	if(mIsAdded==YES)
	{
		//Delete from bd
		
		[mAddingContext deleteObject:mFavorite];
		mAddToFavsLabel.text = @"Añadir a mis Alimentos";
		NSError * error = nil;
		[mAddingContext save:&error];
		mIsAdded = NO;
		
	}
	else
	{
		NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyFavorites" inManagedObjectContext:mAddingContext];
		mFavorite = [[OMyFavorites alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:mAddingContext];
		mFavorite.type = @"3";
		mFavorite.name = food.name;
		NSError * error = nil;
		[mAddingContext save:&error];
		mAddToFavsLabel.text = @"Eliminar de mis Alimentos";
		mIsAdded = YES;
		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


-(IBAction)viewDetailButtonPressed:(id)sender
{

	@try{
	OFood *food = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:mOpenFlowSelected inSection:0]];
	
	OFoodDetailViewController *detailViewController = [[OFoodDetailViewController alloc] initWithNibName:@"OFoodDetailViewController" bundle:nil];
	[detailViewController setFoodSelected:food];
    detailViewController.delegate = self;	
	[mCoverFlowDetailView setHidden:YES];
	[mProductTitleLabel setHidden:YES];
	[self.navigationController.navigationBar setHidden:NO];
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}




@end
