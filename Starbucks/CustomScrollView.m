//
//  CustomScrollView.m
//  Starbucks
//
//  Created by Mac Mindbits on 05/02/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import "CustomScrollView.h"

@implementation CustomScrollView

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
[self.nextResponder touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesMoved:touches withEvent:event];
    if(!self.dragging)
        [[self nextResponder] touchesMoved:touches withEvent:event];
    
 }

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    if(!self.dragging)
      [[self nextResponder] touchesEnded:touches withEvent:event];
}


@end
