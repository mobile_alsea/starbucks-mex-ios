//
//  OShareCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OShareCellDelegate

-(void)showActionSheet:(int)sender;

@end



@interface OShareCell : UITableViewCell
{
	 id<OShareCellDelegate> mdelegate;
}

@property(nonatomic,strong) id<OShareCellDelegate> delegate;


-(IBAction)showActionSheet:(int)sender;

@end
