//
//  ORefreshBalanceViewController.h
//  Starbucks
//
//  Created by Mobile on 10/22/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"

@interface ORefreshBalanceViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
- (IBAction)AddCardButtonPressed:(id)sender;
@end
