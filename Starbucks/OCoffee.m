//
//  OCoffee.m
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OCoffee.h"
#import "OFlavor.h"
#import "OImage.h"


@implementation OCoffee

@dynamic availability;
@dynamic descrip;
@dynamic form;
@dynamic image;
@dynamic name;
@dynamic profile;
@dynamic flavors;
@dynamic images;

@end
