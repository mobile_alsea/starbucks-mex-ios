//
//  OManagementRewardsCardsViewController.m
//  Starbucks
//
//  Created by Mobile on 10/18/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OManagementRewardsCardsViewController.h"
#import "ORewardsCardCell.h"
#import "AsyncImageView.h"

#define CONNECTION_REMOVE_CARDS 1
#define CONNECTION_GET_ALL_CARDS 2

@interface OManagementRewardsCardsViewController ()
{
    int conecctionType;
}
@property (strong, nonatomic) UIAlertView *removeCardAlertView;
@end

@implementation OManagementRewardsCardsViewController
@synthesize HUD;
@synthesize receivedData;
@synthesize removeCardAlertView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAllUserCards:) name:@"Starbucks-ADD-CARD-FROM-MYCARDS" object:nil];
    }
    return self;
}

-(IBAction) getAllUserCards:(id) sender{
    receivedData = [[NSMutableData alloc] init];
    NSString *strURL= [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
    strURL = [strURL stringByAppendingString:@"consultaTarjetas"];
    NSURL *url = [[NSURL alloc] initWithString:strURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:25.0];
    [request setHTTPMethod:@"GET"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    conecctionType = CONNECTION_GET_ALL_CARDS;
    [connection start];
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Edit:)];
        [self.navigationItem setRightBarButtonItem:btnSave];
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OManagementRewardsCardsViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
    _cardsTableView.backgroundColor = [UIColor blackColor];
    _cardsTableView.backgroundView.backgroundColor = [UIColor blackColor];
    _rewardsCardsArrayMutable = [[NSMutableArray alloc] initWithArray:_rewardsCardsArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Edit:(id)sender{
    
    UIBarButtonItem *editButton = (UIBarButtonItem *)sender;
    
    if (_cardsTableView.editing) {
        [_cardsTableView setEditing:NO];
        [editButton setTitle:@"Edit"];
    }
    else{
        [_cardsTableView setEditing:YES];
        [editButton setTitle:@"Done"];
    }
}

- (IBAction)addCard:(id)sender
{
    NSLog(@"Add Card");
    OAddRewardsCardViewController *controller = [[OAddRewardsCardViewController alloc] initWithNibName:@"OAddRewardsCardViewController" bundle:nil];
    controller.invocationFromCardManagementBool = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark OAddRewardsCardViewControllerDelegate

- (void) OAddRewardsCardViewControllerDidClose:(OAddRewardsCardViewController *)controller
{
    NSLog(@"OADDREWARDSCARDS DELEGATE DID CLOSE DEBERIA DE AGREGAR UNA NUEVA TARJETA");
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) OAddRewardsCardViewControllerDidCancel:(OAddRewardsCardViewController *)controller
{
    NSLog(@"OADDREWARDSCARDS DELEGATE");
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_rewardsCardsArrayMutable count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ORewardsCardCell* cell=[_cardsTableView dequeueReusableCellWithIdentifier:@"ORewardsCardCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ORewardsCardCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    double val = [[[_rewardsCardsArrayMutable objectAtIndex:indexPath.row] objectForKey:@"saldo"] doubleValue];
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencySymbol:@""];
    cell.balanceLabel.text = [NSString stringWithFormat:@"$%@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:val]]];
    
    NSString *cardNumber = [NSString stringWithFormat:@"%@",[[_rewardsCardsArrayMutable objectAtIndex:indexPath.row] objectForKey:@"numeroTarjeta"]];
    cell.cardInformartionLabel.text = [NSString stringWithFormat:@"%@ (%@)",[[_rewardsCardsArrayMutable objectAtIndex:indexPath.row] objectForKey:@"alias"], [cardNumber substringFromIndex:([cardNumber length] - 4)]];
    
    //cell.thumbnailImageView.i
    NSString *imageURL = @"http://54.219.59.225:9000/card-small.jpg";
    AsyncImageView* asyncImage1 = [[AsyncImageView alloc] initWithFrame:cell.thumbnailImageView.bounds];
    asyncImage1.clipsToBounds = YES;
    NSURL* url = [NSURL URLWithString:imageURL];
    [asyncImage1 loadImageFromURL:url];
    [cell.thumbnailImageView addSubview:asyncImage1];
    
    return cell;
}

- (UIImage *)imageToFitFromData:(UIView*)view imagen:(UIImage*)nameIMG{
    UIGraphicsBeginImageContext(view.frame.size);
    [nameIMG drawInRect:view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _cardsTableView.frame.size.width, 50)];
    [viewFooter addSubview:_viewAddCard];
    return viewFooter;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 50;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If row is deleted, remove it from the list.
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *cardNumber = [NSString stringWithFormat:@"%@",[[_rewardsCardsArrayMutable objectAtIndex:indexPath.row] objectForKey:@"numeroTarjeta"]];
        cardNumber = [NSString stringWithFormat:@"Estas a punto de dar de baja la tarjeta con la terminación:(%@)",[cardNumber substringFromIndex:([cardNumber length] - 4)]];
        removeCardAlertView = [[UIAlertView alloc] init];
        [removeCardAlertView setDelegate:self];
        [removeCardAlertView setTitle:@"Aviso"];
        [removeCardAlertView setMessage:cardNumber];
        [removeCardAlertView addButtonWithTitle:@"Cancelar"];
        [removeCardAlertView addButtonWithTitle:@"Continuar"];
        [removeCardAlertView show];
    }
}

#pragma mark - NSURL Connection

/*
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
 {
 SecTrustRef trust = [protectionSpace serverTrust];
 SecCertificateRef certificate = SecTrustGetCertificateAtIndex(trust, 0);
 NSData* serverCertificateData = (__bridge  NSData*)SecCertificateCopyData(certificate);
 NSString *serverCertificateDataHash = [[serverCertificateData base64EncodedString] SHA256];
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 BOOL areCertificatesEqualPT = ([serverCertificateDataHash isEqualToString:[defaults objectForKey:@"CERT_PT"]]);
 
 if (!areCertificatesEqualPT){
 [connection cancel];
 [HUD hide:YES];
 return NO;
 }
 return YES;
 }
 */

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Error"];
    [dialog setMessage:[error localizedDescription]];
    [dialog addButtonWithTitle:@"OK"];
    [dialog show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError * error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    [HUD hide:YES];
    
    int idRespuesta = [[results objectForKey:@"codigo"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        if (conecctionType == CONNECTION_REMOVE_CARDS) {
            NSIndexPath *myIndexPath = [self.cardsTableView indexPathForSelectedRow];
            [_rewardsCardsArrayMutable removeObjectAtIndex:myIndexPath.row];
            [_cardsTableView reloadData];
            
        }else if (conecctionType == CONNECTION_GET_ALL_CARDS){
            NSArray *cardsArray = (NSArray *)[results objectForKey:@"tarjetas"];
            [[NSUserDefaults standardUserDefaults] setObject:cardsArray forKey:@"userCardsArray"];
            _rewardsCardsArrayMutable = [[NSMutableArray alloc] initWithArray:cardsArray];
            [self.cardsTableView reloadData];
        }
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:@"Starbucks-ADD-CARD-FROM-MYCARDS"];
}

#pragma mark UIAlertViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == removeCardAlertView) {
        if (buttonIndex == 1) {
            
            NSIndexPath *myIndexPath = [self.cardsTableView indexPathForSelectedRow];
            HUD = [[MBProgressHUD alloc] initWithView:self.view];
            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.dimBackground = YES;
            HUD.detailsLabelText = @"Espere por favor...";
            HUD.labelText = @"Eliminando tarjeta";
            [HUD show:YES];
            
            receivedData = [[NSMutableData alloc] init];
            NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
            validateCardSbx = [validateCardSbx stringByAppendingString:@"eliminarTarjetaRewards"];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:validateCardSbx]];
            [request setHTTPMethod:@"POST"];
            NSString *cardNumber = [NSString stringWithFormat:@"%@",[[_rewardsCardsArrayMutable objectAtIndex:myIndexPath.row] objectForKey:@"numeroTarjeta"]];
            NSString *cardSbxPostString = [NSString stringWithFormat: @"IdTarjeta=%@",cardNumber];
            NSData *postCardSbxData = [cardSbxPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postCardSbxLength = [NSString stringWithFormat:@"%d", [postCardSbxData length]];
            [request setValue:postCardSbxLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postCardSbxData];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
            [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
            conecctionType = CONNECTION_REMOVE_CARDS;
            [connection start];
        }
    }
}

@end