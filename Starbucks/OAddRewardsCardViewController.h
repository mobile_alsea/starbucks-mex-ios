//
//  OAddRewardsCardViewController.h
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"

@interface OAddRewardsCardViewController : OBaseViewController <UITextFieldDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
    BOOL invocationFromCardManagementBool;
}
@property (strong, nonatomic) IBOutlet UITextField *cardNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *verfifyNumberCodeTextField;
@property (nonatomic) BOOL invocationFromCardManagementBool;
@end
