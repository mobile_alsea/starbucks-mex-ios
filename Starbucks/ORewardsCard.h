//
//  ORewardsCard.h
//  Starbucks
//
//  Created by Mobile on 11/1/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ORewardsCard : NSObject
{
    NSNumber *idCard;
    NSString *cardNumber;
    NSString *loyaltyLevel;
    NSString *alias;
    BOOL *isPrincipal;
    NSString *activatedDate;
    NSString *balance;
    NSArray *imagesArray;
    NSString *lastUpdate;
}
@property (strong, nonatomic) NSNumber *idCard;
@property (strong, nonatomic) NSString *cardNumber;
@property (strong, nonatomic) NSString *loyaltyLevel;
@property (strong, nonatomic) NSString *alias;
@property (nonatomic) BOOL *isPrincipal;
@property (strong, nonatomic) NSString *activatedDate;
@property (strong, nonatomic) NSString *balance;
@property (strong, nonatomic) NSString *lastUpdate;
@property (strong, nonatomic) NSArray *imagesArray;
@end
