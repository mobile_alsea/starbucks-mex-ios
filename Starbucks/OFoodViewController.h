//
//  OFoodViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@interface OFoodViewController : OBaseViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
{

	UITableView * mTableView;
	NSFetchedResultsController *mFetchedResultsController;
	NSMutableArray * mCategoriesArray;
	IBOutlet UIButton * mFilterButton;
	NSMutableArray * mFiltersSelectedArray;



}

@property(nonatomic, strong) IBOutlet UITableView * tableView;
@property (nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSMutableArray * filtersSelectedArray;

-(IBAction)filterButtonPressed:(id)sender;
@end
