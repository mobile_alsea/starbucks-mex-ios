//
//  OReloadCardViewController.h
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"
#import "DYHorizontalStretchButton.h"
#import "MBProgressHUD.h"

@interface OReloadCardViewController : OBaseViewController <UITableViewDataSource,UITableViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSString *cardIdToReload;
@property (strong, nonatomic) NSString *lastUpdate;
@property (strong, nonatomic) NSString *cardBalance;
@property (strong, nonatomic) NSString *cardNumber;

@property (strong, nonatomic) IBOutlet UILabel *cardBalanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastUpdatedLabel;


@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UIView *viewReloadCard;
@property (strong, nonatomic) IBOutlet UIToolbar *UserAmountToolbar;

@property (strong, nonatomic) IBOutlet UIButton *firstAmountButton;
@property (strong, nonatomic) IBOutlet UIButton *secondAmountButton;
@property (strong, nonatomic) IBOutlet UIButton *thirdAmountButton;
@property (strong, nonatomic) IBOutlet UIButton *fourthAmountButton;
//payment type section
@property (strong, nonatomic) IBOutlet UIButton *PaymentTypeButton;
@property (strong, nonatomic) IBOutlet UILabel *PaymentTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *PaymentTypeInstructionLabel;
@property (strong, nonatomic) IBOutlet UITableView *PaymentTypeTableView;
//autoreload section
@property (strong, nonatomic) IBOutlet UILabel *AutoreloadTitleLabel;
@property (strong, nonatomic) IBOutlet UISwitch *AutoreloadSwitch;
@property (strong, nonatomic) IBOutlet UILabel *AutoreloadInstructionLabel;
@property (strong, nonatomic) IBOutlet UIButton *firstButtonBelowAmount;
@property (strong, nonatomic) IBOutlet UIButton *secondButtonBelowAmount;
@property (strong, nonatomic) IBOutlet UIButton *thirdButtonBelowAmount;
@property (strong, nonatomic) IBOutlet UIButton *fourthButtonBelowAmount;
@property (strong, nonatomic) IBOutlet UIButton *OneDayReloadButton;
@property (strong, nonatomic) IBOutlet DYHorizontalStretchButton *AutoReloadButton;
//picker views
@property (strong, nonatomic) IBOutlet UIPickerView *UserAmountPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *UserReloadDayPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *UserMinimumBalancePickerView;

- (IBAction)ReloadOneDayButtonPressed:(id)sender;
- (IBAction)ReloadCardButtonPressed:(id)sender;
- (IBAction)ValueChanged:(id)sender;
- (IBAction)SetButtonAmountTapped:(id)sender;
- (IBAction)SetUserAmountReload:(id)sender;
- (IBAction)CancelAmountToolbarPressed:(id)sender;
- (IBAction)DoneAmountToolbarPressed:(id)sender;
@end
