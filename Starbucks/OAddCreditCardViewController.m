//
//  OAddCreditCardViewController.m
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OAddCreditCardViewController.h"
#import "OAddressCreditCardViewController.h"
#import "OBillingCardCell.h"
#import "ReloadSummaryViewController.h"

@interface OAddCreditCardViewController ()
{
    int senderButton;
}
@property (strong, nonatomic) NSMutableArray *arrayCreditCardsTypes;
@property (strong, nonatomic) NSMutableArray *arrayYears;
@property (strong, nonatomic) NSMutableArray *arrayMonths;
@property (strong, nonatomic) NSString *cardExpiryDate;
@end

@implementation OAddCreditCardViewController
@synthesize sView;
@synthesize viewCreditCardInformation;
@synthesize CreditCardBillingTableView;
@synthesize CreditCardExpireDatePickerView;
@synthesize CreditCardTypePickerView;
@synthesize CreditCardSwitch;
@synthesize arrayCreditCardsTypes;
@synthesize CreditCardToolbar;
@synthesize creditCardNameTextField;
@synthesize creditCardNumberTextField;
@synthesize creditCardSecurityNumberTextField;
@synthesize creditCardAliasTextField;
@synthesize receivedData;
@synthesize HUD;
@synthesize cardNumber;
@synthesize amountOnReload;
@synthesize cardExpiryDate;
@synthesize ContinueButton;
@synthesize ExpirationDateLabel;
@synthesize CardTypeLabel;
@synthesize arrayYears;
@synthesize arrayMonths;

bool existsInputErrorOnCreditaCard = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAddCreditCardViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Medio de Pago", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Atras", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Dismiss:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        [sView addSubview:viewCreditCardInformation];
        viewCreditCardInformation.frame= CGRectMake(0, 0, 320, 480 + 50);
        [sView setContentSize:viewCreditCardInformation.frame.size];
        ContinueButton.enabled = NO;
        arrayCreditCardsTypes = [[NSMutableArray alloc] initWithCapacity:0];
        [arrayCreditCardsTypes addObject:@"Visa"];
        [arrayCreditCardsTypes addObject:@"Mastercard"];
        [arrayCreditCardsTypes addObject:@"AMEX"];
        
        
        arrayYears = [[NSMutableArray alloc] initWithCapacity:0];
        arrayMonths = [[NSMutableArray alloc] initWithCapacity:0];
        
        for (int i= 2012; i<2022; i++) {
            [arrayYears addObject:[NSString stringWithFormat:@"%i",i+1]];
        }
        
        [arrayMonths addObject:@"01 - Enero"];
        [arrayMonths addObject:@"02 - Febrero"];
        [arrayMonths addObject:@"03 - Marzo"];
        [arrayMonths addObject:@"04 - Abril"];
        [arrayMonths addObject:@"05 - Mayo"];
        [arrayMonths addObject:@"06 - Junio"];
        [arrayMonths addObject:@"07 - Julio"];
        [arrayMonths addObject:@"08 - Agosto"];
        [arrayMonths addObject:@"09 - Septiembre"];
        [arrayMonths addObject:@"10 - Octubre"];
        [arrayMonths addObject:@"11 - Noviembre"];
        [arrayMonths addObject:@"12 - Diciembre"];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CreditCardExpireDatePickerView.frame = CGRectMake(0.0f,screenRect.size.height - 300.0f, 320.0f, 162.0f);
        CreditCardTypePickerView.frame = CGRectMake(0.0f,screenRect.size.height - 300.0f, 320.0f, 162.0f);
        CreditCardToolbar.frame = CGRectMake(0.0f, screenRect.size.height - 344.0f, 320.0f, 44.0f);
        [CreditCardTypePickerView reloadAllComponents];
        [CreditCardExpireDatePickerView reloadAllComponents];
        CreditCardTypePickerView.tag = 0;
        CreditCardExpireDatePickerView.tag = 1;
        [CreditCardExpireDatePickerView setHidden:YES];
        [CreditCardTypePickerView setHidden:YES];
        [CreditCardToolbar setHidden:YES];
    
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        ExpirationDateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissKeyboard)];
        
        [sView addGestureRecognizer:tap];
        [sView setScrollEnabled:YES];
        
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OAddCreditCardViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(void)dismissKeyboard {
    [creditCardAliasTextField resignFirstResponder];
    [creditCardNameTextField resignFirstResponder];
    [creditCardNumberTextField resignFirstResponder];
    [creditCardSecurityNumberTextField resignFirstResponder];
}

-(IBAction)Dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    if (thePickerView == CreditCardTypePickerView) {
        return 1;
    }else if(thePickerView == CreditCardExpireDatePickerView) {
        return 2;
    }
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (thePickerView.tag == 0) {
        if (arrayCreditCardsTypes!=nil) {
            return [arrayCreditCardsTypes count];
        }
    }else if(thePickerView == CreditCardExpireDatePickerView) {
        if (component == 0) {
            return [arrayMonths count];
        }else if (component == 1){
            return [arrayYears count];
        }
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (thePickerView== CreditCardTypePickerView) {
        if (arrayCreditCardsTypes!=nil) {
            return [arrayCreditCardsTypes objectAtIndex:row];
        }
    }else if (thePickerView == CreditCardExpireDatePickerView){
        if (component == 0) {
            return [arrayMonths objectAtIndex:row];
        }else if (component == 1){
            return [arrayYears objectAtIndex:row];
        }
    }
    return @"";
}

- (void) getExpirationDateFromPickerView
{
    int month = [CreditCardExpireDatePickerView selectedRowInComponent:0];
    int year = [CreditCardExpireDatePickerView selectedRowInComponent:1];
    NSString *monthString = (NSString *)[arrayMonths objectAtIndex:month];
    NSString *yearString = (NSString *)[arrayYears objectAtIndex:year];
    ExpirationDateLabel.text = [NSString stringWithFormat:@"%@/%@",monthString,yearString];
    yearString =[yearString substringFromIndex:[yearString length]-2];
    monthString = [NSString stringWithFormat:@"%02d",month+1];
    cardExpiryDate  = [NSString stringWithFormat:@"%@/%@",monthString,yearString];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView== CreditCardExpireDatePickerView){
        [self getExpirationDateFromPickerView];
    }
}

- (IBAction)ToolbarCreditCardDonePressed:(id)sender {
    if (senderButton == 0) {
        CardTypeLabel.text = [NSString stringWithFormat:@"%@",(NSString *)[self.arrayCreditCardsTypes objectAtIndex:[self.CreditCardTypePickerView selectedRowInComponent:0]]];
                [CreditCardTypePickerView setHidden:YES];
    }else{
        [self getExpirationDateFromPickerView];
        [CreditCardExpireDatePickerView setHidden:YES];
    }
    [CreditCardToolbar setHidden:YES];
}

- (IBAction)ToolbarCreditCardCancelPressed:(id)sender {
    if (senderButton == 0) {
        [CreditCardTypePickerView setHidden:YES];
    }else{
        [CreditCardExpireDatePickerView setHidden:YES];
    }
    [CreditCardToolbar setHidden:YES];
}

- (IBAction)SelectCreditCardType:(id)sender {
    [self dismissKeyboard];
    
    [CreditCardToolbar setHidden:NO];
    senderButton = 0;
    [CreditCardTypePickerView setHidden:NO];
}

- (IBAction)SelectCreditCardExpiryDate:(id)sender {
    [self dismissKeyboard];
    [CreditCardToolbar setHidden:NO];
    senderButton = 1;
    [CreditCardExpireDatePickerView setHidden:NO];
}

#pragma mark CreditCardSwitch Value Changed

- (IBAction)SelectSaveCreditCard:(id)sender {
    if (CreditCardSwitch.on == YES) {
        [[NSUserDefaults standardUserDefaults] setObject:@"sí" forKey:@"PAYMENT_TIPE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PAYMENT_TIPE"];
    }
}

#pragma mark UITextfield delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == creditCardNumberTextField){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 16) ? NO : YES;
    }else if(textField == creditCardSecurityNumberTextField){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 4) ? NO : YES;
    }
    [self verificarTexto:textField];
    
    return YES;
}


-(void)verificarTexto:(id)sender{
    int empty = 0;
    for(id x in [viewCreditCardInformation subviews]){
        if([x isKindOfClass:[UITextField class]]){
            UITextField *v = (UITextField *)x;
            if (v.text.length == 0 ) {
                empty++;
            }
        }
        
    }
    if (empty == 0) {
        ContinueButton.enabled = YES;
        NSLog(@"correcto");
    }else{
        ContinueButton.enabled = NO;
            NSLog(@"incorrecto");
    }
}

#pragma mark Continue Reload

- (IBAction)SetSaveCreditCardButtonPressed:(id)sender {
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    
    receivedData = [[NSMutableData alloc] init];
    
    NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_SMART"];
    validateCardSbx = [validateCardSbx stringByAppendingString:@"Recarga"];
    NSLog(@"VALidate:%@",validateCardSbx);
    
    int now = [[NSDate date] timeIntervalSince1970];
    NSString *referenceID = [NSString stringWithFormat:@"%i",now];
    NSArray *objetos = [NSArray new];
    NSArray *llaves = [NSArray new];
    
    NSNumber *storeCreditCard  = [NSNumber numberWithBool:CreditCardSwitch.on];
    NSLog(@"stroreCreditCard:%@",storeCreditCard);
    objetos = [NSArray arrayWithObjects:@"STARBUCKS",@"Movil",cardNumber,@"MXN",amountOnReload,creditCardNumberTextField.text,creditCardSecurityNumberTextField.text,cardExpiryDate,@"1",referenceID,CardTypeLabel.text,[[NSUserDefaults standardUserDefaults] objectForKey:@"userInformation"],nil];
    llaves = [NSArray arrayWithObjects:@"Cadena",@"TipoDispositivo",@"TarjetaCadena",@"Modena",@"MontoRecarga",@"TarjetaBancaria",@"CVV",@"FechaVencimiento",@"GuardarTarjeta",@"IDReferencia",@"TipoBanco",@"DetalleCliente",nil];

    NSDictionary *reloadTransactionDictionary = [NSDictionary dictionaryWithObjects:objetos forKeys:llaves];
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:reloadTransactionDictionary options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError) {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    } else {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:validateCardSbx] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"12345678" forHTTPHeaderField:@"token"];
    [request setValue:@"10263" forHTTPHeaderField:@"idUsuario"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: jsonData];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    [connection start];
}

@end
