//
//  OSignUpRewardsViewController.h
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"
#import "OAddRewardsCardViewController.h"
#import "DYHorizontalStretchButton.h"

@interface OSignUpRewardsViewController : OBaseViewController <UITextFieldDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
    int intErrorName;
    int intErrorEmail;
    int intErrorPassword;
}
@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UIView *viewSignUpRegister;
//UITextfield's
@property (strong, nonatomic) IBOutlet UILabel *NameErrorLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet UILabel *BirthdayLabel;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *secondAddressTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *stateTextField;
@property (strong, nonatomic) IBOutlet UITextField *postalCodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *dateButton;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;

@property (strong, nonatomic) IBOutlet UISwitch *newsletterSwitch;
@property (strong, nonatomic) IBOutlet UIDatePicker *dateMonthPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;

//Error labels
@property (strong, nonatomic) IBOutlet UILabel *emailErrorLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordsErrorLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordInstructionsLabel;
@property (strong, nonatomic) IBOutlet DYHorizontalStretchButton *SignUpButton;

//
//views
@property (strong, nonatomic) IBOutlet UIView *nameErrorView;
@property (strong, nonatomic) IBOutlet UIView *nameView;
@property (strong, nonatomic) IBOutlet UIView *emailErrorView;
@property (strong, nonatomic) IBOutlet UIView *emailView;
@property (strong, nonatomic) IBOutlet UIView *passwordErrorView;
@property (strong, nonatomic) IBOutlet UIView *passwordView;
@property (strong, nonatomic) IBOutlet UIView *birthdayView;
@property (strong, nonatomic) IBOutlet UIView *moreInfoView;
@property (strong, nonatomic) IBOutlet UIView *suscribeView;


- (IBAction)ValueChangedNewsletterSwitch:(id)sender;
- (IBAction)SelectBirthday:(id)sender;
- (IBAction)OpenBirthdayPickerView:(id)sender;
- (IBAction)ValueChangedBirthdayPickerView:(id)sender;


@property (strong, nonatomic) IBOutlet UITextField *CountryTextField;

@end
