//
//  OReloadCell.m
//  Starbucks
//
//  Created by Mobile on 10/22/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OReloadCell.h"

@implementation OReloadCell

@synthesize statusLabel = mStatusLabel;
@synthesize indexPathPair = mIndexPathPair;
@synthesize index = mIndex;
@synthesize balanceLabel = mBalanceLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
