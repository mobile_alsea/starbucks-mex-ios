//
//  ORecentTransactionsViewController.h
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"
#import "MBProgressHUD.h"
#import "OCreditCardCell.h"

@interface ORecentTransactionsViewController : OBaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    OAppDelegate * appDelegate;
    NSManagedObjectContext *addingContext;
}
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) IBOutlet UIView *recentTransactionsView;
@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UITableView *transactionsTableView;
@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSNumber *cardId;
@property (strong, nonatomic) NSString *cardLastUpdate;
@property (strong, nonatomic) NSString *cardBalance;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@end
