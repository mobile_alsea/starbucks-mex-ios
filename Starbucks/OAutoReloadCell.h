//
//  OAutoReloadCell.h
//  Starbucks
//
//  Created by Mobile on 10/22/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OAutoReloadCell : UITableViewCell
{
    UILabel * mStatusLabel;
    UISwitch *mSwitchAutoReload;
    BOOL mIndexPathPair;
    int mIndex;
}
@property (nonatomic, strong) IBOutlet UILabel * statusLabel;
@property (nonatomic, strong) IBOutlet UISwitch *switchAutoReload;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;
@end
