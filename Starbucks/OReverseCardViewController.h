//
//  OReverseCardViewController.h
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"

@class OReverseCardViewController;
@protocol OReverseCardViewControllerClose <NSObject>
- (void)OReverseCardDidClose:(OReverseCardViewController *)controller;

@end

@interface OReverseCardViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *CoffeViewScan;
@property (strong, nonatomic) id <OReverseCardViewControllerClose> delegate;
- (IBAction)ShowFrontCard:(id)sender;

@end
