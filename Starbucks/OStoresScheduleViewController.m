//
//  OStoresScheduleViewController.m
//  Starbucks
//
//  Created by Adrián Caramés on 24/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresScheduleViewController.h"
#import "OSToresSchedulesCell.h"
#import "OSchedule.h"

@interface OStoresScheduleViewController ()

@end

@implementation OStoresScheduleViewController


#pragma mark - Constructors

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark - Properties

@synthesize tableView = mTableView;
@synthesize storeSelected = mStoreSelected;


#pragma mark - UIViewController Methods

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationItem setTitle:NSLocalizedString(@"Horarios", nil)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if(self.comesFromModal) {
		CGRect tableFrame = CGRectInset(self.view.bounds, 10.0, 10.0);
		mTableView.frame = tableFrame;
	}
}

#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    return [mStoreSelected.schedules count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OStoresSchedulesCell";
    
    OStoresSchedulesCell *cell = (OStoresSchedulesCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
        //        cell.textField.delegate = self;
    }
    
    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
//    [cell updateSchedule:0];
    
    OSchedule *schedule = [mStoreSelected.schedulesArray objectAtIndex:_indexPath.row];
    
    [cell.dayLabel setText:schedule.name];
	if([schedule.startTime length] > 0 && [schedule.endTime length] > 0)
		[cell.scheduleLabel setText:[NSString stringWithFormat:@"%@ a %@", schedule.startTime, schedule.endTime]];
	
	else if([schedule.startTime length] > 0)
		cell.scheduleLabel.text = schedule.startTime;
	
	else if([schedule.endTime length] > 0)
		cell.scheduleLabel.text = schedule.endTime;
	
	else
		cell.scheduleLabel.text = @"";
    
    return cell;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OStoresScheduleViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


@end
