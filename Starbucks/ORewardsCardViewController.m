//
//  ORewardsCardViewController.m
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "ORewardsCardViewController.h"
#import "OReverseCardViewController.h"
#import "OReloadCardViewController.h"
#import "ORecentTransactionsViewController.h"
#import "OAutoReloadViewController.h"
#import "ZXingObjC.h"
#import "UIDevice+IdentifierAddition.h"

#define CONNECTION_GET_ALL_CARDS 1
#define CONNECTION_RELOAD_CARD_BALANCE 2
#define CONNECTION_GET_USER_INFORMATION 3

@interface ORewardsCardViewController ()
{
    int conectionType;
    int cardPosition;
}
@property (strong, nonatomic) NSTimer * timer;
@property (strong, nonatomic) NSArray *rewardsCardsArray;
@property (strong, nonatomic) NSMutableArray *rewardsCardsMutableArray;
@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSNumber *idCardToReloadBalance;
@property (strong, nonatomic) NSDictionary *currentCard;
@end

@implementation ORewardsCardViewController
@synthesize viewIntroduction;
@synthesize viewReverseCard;
@synthesize CupImageView;
@synthesize pageControl;
@synthesize rewardsCardScrollView;
@synthesize timer;
//
@synthesize ReloadActivityIndicator;
@synthesize CardManagementButton;
@synthesize CardReloadButton;
@synthesize dateLabel;
@synthesize amountLabel;
@synthesize rewardsCardsArray;
@synthesize receivedData;
@synthesize idCardToReloadBalance;
@synthesize rewardsCardsMutableArray;
@synthesize currentCard;
@synthesize HUD;

int intActual = 1;
int intTotal=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            /*
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
            */
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showUserAccountData:) name:@"Starbucks-SING-IN-ACCOUNT-REWARDS" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showUserAccountData:) name:@"Starbucks-REGISTER-ACCOUNT-REWARDS" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNewUserAccountCards:) name:@"Starbucks-REGISTER-CARD-REWARDS" object:nil];
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserInformationNeedToReload:) name:@"Starbucks-GET-USER-INFORMATION" object:nil];
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsCardViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Mis Tarjetas", nil)];
        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addCard:)];
        [self.navigationItem setRightBarButtonItem:btnAdd];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Ver Todas", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(viewAll:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        rewardsCardScrollView.delegate = self;
        rewardsCardScrollView.scrollEnabled = YES;
        [ReloadActivityIndicator setHidden:YES];
        [CardReloadButton.layer setCornerRadius:4.0f];
        [CardReloadButton.layer setMasksToBounds:YES];
        [CardManagementButton.layer setCornerRadius:4.0f];
        [CardManagementButton.layer setMasksToBounds:YES];
        
        
        rewardsCardsArray = [[NSArray alloc] init];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"] != nil) {
            [self showUserAccountData:nil];
            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.dimBackground = YES;
            receivedData = [[NSMutableData alloc] init];
            NSString *strURL= [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
            strURL = [strURL stringByAppendingString:@"consultaTarjetas"];
            NSURL *url = [[NSURL alloc] initWithString:strURL];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            //[request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
            //[request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
            conectionType = CONNECTION_GET_ALL_CARDS;
            NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
            [connection start];
        }
    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OSignUpRewardsViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

-(void)threadStartAnimating:(id)data
{
    [ReloadActivityIndicator setHidden:NO];
    [ReloadActivityIndicator startAnimating];
}

- (void) threadStopAnimating
{
    [ReloadActivityIndicator stopAnimating];
    [ReloadActivityIndicator setHidden:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"token:%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]);
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"] == nil) {
        [self.view addSubview:viewIntroduction];
        self.navigationController.navigationBar.hidden = YES;
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger nearestNumber = lround(fractionalPage);
    pageControl.currentPage = nearestNumber;
    intActual = pageControl.currentPage;
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self ShowCurrentCardInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)viewAll:(id)sender
{
    OManagementRewardsCardsViewController *controller = [[OManagementRewardsCardsViewController alloc] initWithNibName:@"OManagementRewardsCardsViewController" bundle:nil];
    [controller setRewardsCardsArray:rewardsCardsArray];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark Add Starbucks Rewards Card

-(IBAction)addCard:(id)sender
{
    OAddRewardsCardViewController *controller = [[OAddRewardsCardViewController alloc] initWithNibName:@"OAddRewardsCardViewController" bundle:nil];
    controller.invocationFromCardManagementBool = NO;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark ButtonSignUpPressed Present Card Validation

- (IBAction)SignUpButtonPressed:(id)sender {
    
    OAddRewardsCardViewController *controller = [[OAddRewardsCardViewController alloc] initWithNibName:@"OAddRewardsCardViewController" bundle:nil];
    
    SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:controller];
    
    if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] )
    {
        UIImage *image = [UIImage imageNamed:@"NavBar.png"];
        [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
            [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
            [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        }
    }
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark ButtonSignInPressed "Login"

- (IBAction)SignInButtonPressed:(id)sender {

    OSignInRewardsViewController *detailViewController = [[OSignInRewardsViewController alloc] initWithNibName:@"OSignInRewardsViewController" bundle:nil];
    detailViewController.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:detailViewController];
    
    if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] )
    {
        UIImage *image = [UIImage imageNamed:@"NavBar.png"];
        [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
            [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
            [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        }
    }
    navigationController.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark OCardViewController When reverse button is pressed

- (void) OCardViewControllerDidPressReverseButton:(OCardViewController *)controller
{
    _PDF417ImageView.backgroundColor = [UIColor colorWithRed:235/255.0f green:235/255.0f  blue:235/255.0f  alpha:1];
    NSDictionary *dicCampos = (NSDictionary *)[rewardsCardsArray objectAtIndex:pageControl.currentPage];
    NSString *tarjeta = (NSString *)[dicCampos objectForKey:@"numeroTarjeta"];
    NSError* error = nil;
    ZXMultiFormatWriter* writer = [ZXMultiFormatWriter writer];
    ZXBitMatrix* result = [writer encode:tarjeta format:kBarcodeFormatPDF417 width:201 height:110 hints:nil error:&error];
    
    NSString* errorMessage = [error localizedDescription];
    
    if (result) {
        // display barcode on screen
        /*
         
         
         
         */
        UIImage *croppedImage = [self imageByCropping:[UIImage imageWithCGImage: [[ZXImage imageWithMatrix:result] cgimage]] toRect:CGRectMake(35, 38, 200, 42)];
        _PDF417ImageView.image = croppedImage;
        
    } else {
        _PDF417ImageView.image = nil;
        NSLog(@"%@",errorMessage);
    }
    
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
                           [self.view addSubview:viewReverseCard];
                       }
                    completion:^(BOOL finished){
                        
                        [self cupRotatesOnXAxis:nil];
                        timer = [NSTimer scheduledTimerWithTimeInterval:1.8
                                                                 target:self
                                                               selector:@selector(cupRotatesOnXAxis:)
                                                               userInfo:nil
                                                                repeats:YES];
                    }
     ];
}

- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    return cropped;
}

- (IBAction)ShowReverseCardButtonPressed:(id)sender
{
    [timer invalidate];
    timer = nil;
    [UIView transitionWithView:self.view duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
                           [viewReverseCard removeFromSuperview];
                       }
                    completion:NULL];
}


#pragma mark Cup Rotates on Y Axis while Reverse Card is showing

- (IBAction)cupRotatesOnXAxis:(id)sender{
    
    CATransform3D flip = CATransform3DMakeRotation(M_PI, 0.0, 1.0, 0.0);
    if (CATransform3DEqualToTransform(CupImageView.layer.transform, flip))
    {
        flip = CATransform3DMakeRotation(M_PI,0.0,0.0, 0.0);
        
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2.0];
    
    CupImageView.layer.transform = flip;
    
    [UIView commitAnimations];
    
}

- (IBAction)ReloadButtonPressed
{
    NSDictionary *currentCardDictionary = (NSDictionary *)[rewardsCardsArray objectAtIndex:pageControl.currentPage];
    OReloadCardViewController *reloadViewController = [[OReloadCardViewController alloc] initWithNibName:@"OReloadCardViewController" bundle:nil];
    reloadViewController.cardIdToReload = (NSString *)[currentCardDictionary objectForKey:@"idTarjeta"];
    reloadViewController.lastUpdate = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:(NSString *)[currentCardDictionary objectForKey:@"idTarjeta"]];
    reloadViewController.cardBalance = (NSString *)[currentCardDictionary objectForKey:@"saldo"];
    reloadViewController.cardNumber  = (NSString *)[currentCardDictionary objectForKey:@"numeroTarjeta"];
    [self.navigationController pushViewController:reloadViewController animated:YES];
}

#pragma mark Manage Card Button Pressed

- (IBAction)ManageButtonPressed
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Actualizar Saldo", @"Movimientos Recientes",@"Agregar Tarjeta a Passbook", nil];
	popupQuery.actionSheetStyle = UIActionSheetStyleDefault;
    [popupQuery showInView:self.view];
}

#pragma mark CardViewController Delegate

- (void) OCardViewControllerDidReloadButtonPressed:(OCardViewController *)controller
{
    OReloadCardViewController *reloadViewController = [[OReloadCardViewController alloc] initWithNibName:@"OReloadCardViewController" bundle:nil];
    [self.navigationController pushViewController:reloadViewController animated:YES];
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSDictionary *currentCardDictionary = (NSDictionary *)[rewardsCardsArray objectAtIndex:pageControl.currentPage];
    idCardToReloadBalance =(NSNumber *)[currentCardDictionary objectForKey:@"idTarjeta"];
    cardPosition = pageControl.currentPage;
    if (buttonIndex == 0) {
        //obtener la tarjeta en la posición especifica y actualizarle sólo el campo de la fecha y el saldo
        [self threadStartAnimating:nil];
        receivedData = [[NSMutableData alloc] init];
        NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_REWARDS"];
        validateCardSbx = [validateCardSbx stringByAppendingString:@"consultaSaldoTarjeta"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:validateCardSbx]];
        [request setHTTPMethod:@"POST"];
        NSString *cardSbxPostString = [NSString stringWithFormat: @"idTarjeta=%@",idCardToReloadBalance];
        NSData *postCardSbxData = [cardSbxPostString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postCardSbxLength = [NSString stringWithFormat:@"%d", [postCardSbxData length]];
        [request setValue:postCardSbxLength forHTTPHeaderField:@"Content-Length"];
        //[request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
        //[request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postCardSbxData];
        NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        conectionType = CONNECTION_RELOAD_CARD_BALANCE;
        [connection start];
        
    }else if (buttonIndex == 1){
        NSString *balance = (NSString *)[currentCardDictionary objectForKey:@"saldo"];
        NSString *lastUpdate = (NSString *)[currentCardDictionary objectForKey:@"fechaActualizacion"];
        if (lastUpdate == nil) {
            lastUpdate = [NSString stringWithFormat:@"Actualizado el %@",(NSString *)[currentCardDictionary objectForKey:@"fechaActivacion"]];
        }
        ORecentTransactionsViewController *recentTransactionsViewController = [[ORecentTransactionsViewController alloc] initWithNibName:@"ORecentTransactionsViewController" bundle:nil];
        recentTransactionsViewController.cardId = idCardToReloadBalance;
        recentTransactionsViewController.cardBalance = balance;
        recentTransactionsViewController.cardLastUpdate = lastUpdate;
        [self.navigationController pushViewController:recentTransactionsViewController animated:YES];
        
    }else if (buttonIndex == 2){
        /*
        OAutoReloadViewController *transactionViewController = [[OAutoReloadViewController alloc] initWithNibName:@"OAutoReloadViewController" bundle:nil];
        [self.navigationController pushViewController:transactionViewController animated:YES];
        NSLog(@"Auto reload");
        */
    }else if (buttonIndex == 3){
        NSLog(@"Add card to passbook");
    }else{
        NSLog(@"Default");
    }
    
}


- (NSString *)getCurrentTime{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd"];
    NSDateFormatter *format2 = [[NSDateFormatter alloc]init];
    format2.dateFormat = @"MMM/yyyy HH:mm";
    [format2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"es_MX"]];
     NSString *yourString= [format2 stringFromDate:[[NSDate alloc] init]];
     yourString=[[[yourString substringToIndex:1] uppercaseString] stringByAppendingString:[yourString substringFromIndex:1]];
     yourString = [NSString stringWithFormat:@"%@/%@",[format stringFromDate:[[NSDate alloc] init]], yourString];
     return  yourString;
}

- (NSString *)getCurrencyMode:(NSString *)value
{
    double valueDouble = [value doubleValue];
    NSNumberFormatter *currencyFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencySymbol:@""];
    NSString *amountValueWithFormat =[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:valueDouble]];
    return amountValueWithFormat;
}

- (void)getUserInformationNeedToReload
{
    receivedData = [[NSMutableData alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strURL= [defaults objectForKey:@"URL_WS_REWARDS"];
    strURL = [strURL stringByAppendingString:@"consultaDatosUsuario"];
    NSURL *url = [[NSURL alloc] initWithString:strURL];
        
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    conectionType = CONNECTION_GET_USER_INFORMATION;
    [connection start];
}

-(IBAction) getNewUserAccountCards:(id) sender{
    receivedData = [[NSMutableData alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strURL= [defaults objectForKey:@"URL_WS_REWARDS"];
    strURL = [strURL stringByAppendingString:@"consultaTarjetas"];
    NSURL *url = [[NSURL alloc] initWithString:strURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:25.0];
    [request setHTTPMethod:@"GET"];
    [request setValue:[defaults objectForKey:@"idUsuario"] forHTTPHeaderField:@"idUsuario"];
    [request setValue:[defaults objectForKey:@"token"] forHTTPHeaderField:@"token"];
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    conectionType = CONNECTION_GET_ALL_CARDS;
    [connection start];
}

-(IBAction) showUserAccountData:(id) sender
{
    self.navigationController.navigationBar.hidden = NO;
    [viewIntroduction removeFromSuperview];
    intTotal = 0;
    rewardsCardsArray =(NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"userCardsArray"];
    
    int intLeft=0;
    for (int i=0; i<[rewardsCardsArray count]; i++) {
        NSDictionary *userCardInformation = (NSDictionary *)[rewardsCardsArray objectAtIndex:i];
        
        NSString *balance = (NSString *)[userCardInformation objectForKey:@"numeroTarjeta"];
        ORewardsCard *card;
        OCardViewController *cController;
        card = [[ORewardsCard alloc] init];
        card.idCard = (NSNumber *)[userCardInformation objectForKey:@"idTarjeta"];
        card.cardNumber = (NSString *)[userCardInformation objectForKey:@"numeroTarjeta"];
        card.loyaltyLevel = (NSString *)[userCardInformation objectForKey:@"nivelLealtad"];
        card.alias = (NSString *)[userCardInformation objectForKey:@"alias"];
        card.isPrincipal = (BOOL *)[userCardInformation objectForKey:@"esPrincipal"];
        card.activatedDate = (NSString *)[userCardInformation objectForKey:@"fechaActivacion"];
        card.balance = balance;
        card.imagesArray = (NSArray *)[userCardInformation objectForKey:@"imagen"];
        cController = [[OCardViewController alloc] initWithNibName:@"OCardViewController" bundle:[NSBundle mainBundle]];
        cController.card = card;
        cController.view.frame = CGRectMake(intLeft, 0, 320, 200);
        cController.delegate = self;
        intLeft = intLeft + 320;
        [rewardsCardScrollView addSubview:cController.view];
        intTotal++;
    }
    
    pageControl.numberOfPages = intTotal;
    
    rewardsCardScrollView.delegate = self;
    pageControl.currentPage=0;
    [rewardsCardScrollView setContentSize:CGSizeMake(intLeft, 200)];
    intActual = 1;
    [self ShowCurrentCardInformation];
}

- (void) ShowCurrentCardInformation
{
    [dateLabel setHidden:NO];
    [amountLabel setHidden:NO];
    NSDictionary *cardArray = (NSDictionary *)[rewardsCardsArray objectAtIndex:pageControl.currentPage];
    NSString *balance = (NSString *)[cardArray objectForKey:@"saldo"];
    NSString *idCard = (NSString *)[cardArray objectForKey:@"idTarjeta"];
    amountLabel.text = [NSString stringWithFormat:@"$ %@",[self getCurrencyMode:balance]];
    dateLabel.text = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:idCard];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:idCard]);
}

/*
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
 {
 SecTrustRef trust = [protectionSpace serverTrust];
 SecCertificateRef certificate = SecTrustGetCertificateAtIndex(trust, 0);
 NSData* serverCertificateData = (__bridge  NSData*)SecCertificateCopyData(certificate);
 NSString *serverCertificateDataHash = [[serverCertificateData base64EncodedString] SHA256];
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 BOOL areCertificatesEqualPT = ([serverCertificateDataHash isEqualToString:[defaults objectForKey:@"CERT_PT"]]);
 
 if (!areCertificatesEqualPT){
 [connection cancel];
 [HUD hide:YES];
 return NO;
 }
 return YES;
 }
 */

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Error"];
    [dialog setMessage:[error localizedDescription]];
    [dialog addButtonWithTitle:@"OK"];
    [dialog show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError * error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    int idRespuesta = [[results objectForKey:@"codigo"] intValue];
    
    if (conectionType == CONNECTION_GET_ALL_CARDS) {
        [HUD hide:YES];
        if (idRespuesta == 0) {
            NSArray *cardsArray = (NSArray *)[results objectForKey:@"tarjetas"];
            [[NSUserDefaults standardUserDefaults] setObject:cardsArray forKey:@"userCardsArray"];
            rewardsCardsArray =(NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"userCardsArray"];
            for (int i=0; i<[rewardsCardsArray count]; i++) {
                NSDictionary *userCardInformation = (NSDictionary *)[rewardsCardsArray objectAtIndex:i];
                NSString *cardId = (NSString *)[userCardInformation objectForKey:@"idTarjeta"];
                NSString *lastCardBalanceUpdate = [self getCurrentTime];
                NSLog(@"carId:%@",cardId);
                NSLog(@"lastCardBalanceUpdate:%@",lastCardBalanceUpdate);
                [[NSUserDefaults standardUserDefaults] setObject:lastCardBalanceUpdate forKey:cardId];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [self showUserAccountData:nil];
        }
    }else if(conectionType == CONNECTION_RELOAD_CARD_BALANCE) {
        
        NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
        
        if (idRespuesta != 0) {
            UIAlertView *dialogo = [[UIAlertView alloc] init];
            [dialogo setDelegate:self];
            [dialogo setTitle:@"Aviso"];
            [dialogo setMessage:mensaje];
            [dialogo addButtonWithTitle:@"OK"];
            [dialogo show];
        }else{
            NSString *valor = @"10.57";
            NSArray *arraySaldos = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:@"userCardsArray"];
            rewardsCardsMutableArray = [[NSMutableArray alloc] initWithArray:arraySaldos];
            NSMutableDictionary *oldCardInfoDictionary =[[NSMutableDictionary alloc] initWithDictionary:[rewardsCardsMutableArray objectAtIndex:cardPosition]];
            [oldCardInfoDictionary setObject:[self getCurrencyMode:valor] forKey:@"saldo"];
            //[oldCardInfoDictionary setObject:yourString forKey:@"fechaActualizacion"];
            [rewardsCardsMutableArray replaceObjectAtIndex:cardPosition withObject:[NSDictionary dictionaryWithDictionary:oldCardInfoDictionary]];
            amountLabel.text = [self getCurrencyMode:valor];
            dateLabel.text = [self getCurrentTime];
            [[NSUserDefaults standardUserDefaults] setObject:(NSArray *)rewardsCardsMutableArray forKey:@"userCardsArray"];
            NSString *cardId = (NSString *)[oldCardInfoDictionary objectForKey:@"idTarjeta"];
            NSString *lastCardBalanceUpdate = [self getCurrentTime];
            [[NSUserDefaults standardUserDefaults] setObject:lastCardBalanceUpdate forKey:cardId];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSLog(@"carId:%@",cardId);
            NSLog(@"lastCardBalanceUpdate:%@",lastCardBalanceUpdate);
            rewardsCardsArray = [[NSArray alloc] initWithArray:(NSArray *)rewardsCardsMutableArray];
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userCardsArray"]);
            
            [self threadStopAnimating];
            [self ShowCurrentCardInformation];
        }
    }else if(conectionType == CONNECTION_GET_USER_INFORMATION){
        if (idRespuesta == 0) {
            NSDictionary *userInformation = (NSDictionary *)[results objectForKey:@"datosCliente"];
            [[NSUserDefaults standardUserDefaults] setObject:userInformation forKey:@"userInformation"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (void) dealloc{
    [super dealloc];
    [[NSNotificationCenter defaultCenter]removeObserver:@"Starbucks-SING-IN-ACCOUNT-REWARDS"];
    [[NSNotificationCenter defaultCenter]removeObserver:@"Starbucks-REGISTER-ACCOUNT-REWARDS"];
    [[NSNotificationCenter defaultCenter]removeObserver:@"Starbucks-REGISTER-CARD-REWARDS"];
    //[[NSNotificationCenter defaultCenter]removeObserver:@"Starbucks-GET-USER-INFORMATION"];
}

@end
