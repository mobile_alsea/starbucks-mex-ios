//
//  SBNavigationController.m
//  Starbucks
//
//  Created by Ironbit Ninja on 11/07/13.
//  Copyright (c) 2013 Dylvian. All rights reserved.
//

#import "SBNavigationController.h"

@interface SBNavigationController ()

@end

@implementation SBNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//      Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//      Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
//     Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    self.navigationController.navigationBar.translucent = NO;
}

#pragma mark - Rotation

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
