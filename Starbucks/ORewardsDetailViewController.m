//
//  ORewardsDetailViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ORewardsDetailViewController.h"
#import "OBenefitsRewardsViewController.h"
@interface ORewardsDetailViewController ()

@end

@implementation ORewardsDetailViewController
@synthesize numberOfStars = mNumberOfStars;
@synthesize numberOfStarsGold = mNumberOfStarsGold;
@synthesize actualLevel = mActualLevel;
@synthesize dateMember = mDateMember;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsDetailsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	
	if([mActualLevel isEqualToString: @"Welcome Level"]){
    {
        mColorNivel.textColor = [UIColor blackColor];
        mColorStar.image = [UIImage imageNamed:@"rewards_detail_star.png"];
        
		mDetailLabel.text = @"Acumularás una Star por cada vez que pagues con tu Starbucks Card registrada en cualquier tienda participante. ¡Esperamos verte pronto!";
        
        
     }
    
    }else if([mActualLevel isEqualToString:@"Green Level"]){
    {
        //mColorNivel.textColor = [UIColor greenColor];
        mColorStar.image = [UIImage imageNamed:@"greenstar.png"];
    
		mDetailLabel.text = @"¡Continúa acumulando Stars! ¿No fue tan difícil o sí? Disfruta de tus nuevos beneficios.";
        
	} 
    
    }else if([mActualLevel isEqualToString:@"Gold Level"])
    {
        mColorNivel.textColor= [UIColor brownColor];
        mColorStar.image = [UIImage imageNamed:@"goldstar.png"];
       
		mDetailLabel.text = @"¡Llegaste al Gold Level! Queremos agradecerte por ser un gran cliente Starbucks. Estamos felices de que seamos tu elección.";
    }   
	
    
		[self.navigationItem setTitle:NSLocalizedString(@"Recompensas", nil)];
	
	
	[self setDate];
	mActualLevelLabel.text = mActualLevel;
	
	UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithTitle:@"Beneficios"  style:UIBarButtonItemStyleBordered target:self action:@selector(goToBenefits:)];
	
	mDateMemberLabel.text = mDateMember;
	
    [self.navigationItem setRightBarButtonItem:btnFilter];
	
	int number = [mNumberOfStars intValue];
    
	
	if([mActualLevel isEqualToString:@"Welcome Level"]){
	    {
		mTotalLabel.text = mNumberOfStars;
		mNextLevelLabel.text = @"Green Level";
		mFaltanLabel.text = [NSString stringWithFormat:@"%d",5-number];
        
        mImagenWelcome.image = [UIImage imageNamed:@"welcome.png"];
            [mImagenWelcome setFrame:CGRectMake(mImagenWelcome.frame.origin.x, mImagenWelcome.frame.origin.y, (37/5)*number, mImagenWelcome.frame.size.height)];
            
        }
    	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome.png"]]];
        
        
    
    } else if([mActualLevel isEqualToString:@"Green Level"]){
	    {
		mTotalLabel.text = mNumberOfStars;
		mNextLevelLabel.text = @"Gold Level";
		mFaltanLabel.text = [NSString stringWithFormat:@"%d",30-number];
            
        mImagenWelcome.image = [UIImage imageNamed:@"welcome.png"];
            [mImagenWelcome setFrame:CGRectMake(mImagenWelcome.frame.origin.x, mImagenWelcome.frame.origin.y, (37/1), mImagenWelcome.frame.size.height)];
            
        mImagenGreen.image = [UIImage imageNamed:@"green.png"];
            [mImagenGreen setFrame:CGRectMake(mImagenGreen.frame.origin.x, mImagenGreen.frame.origin.y, (68/30)*number, mImagenGreen.frame.size.height)];
            
        }
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"green.png"]]];
	
        
    
    } else if([mActualLevel isEqualToString:@"Gold Level"])
        {
        int prueba =([mNumberOfStars intValue] - 30) % 15;
        int prueba2 = 15 - prueba;
        
        if (prueba == 0)
            {
              mNextLevelLabel.text = [NSString stringWithFormat:@"Tienes una bebida 15 Stars Gratis"];
                
                mImagenWelcome.image = [UIImage imageNamed:@"welcome.png"];
                [mImagenWelcome setFrame:CGRectMake(mImagenWelcome.frame.origin.x, mImagenWelcome.frame.origin.y, (37/1), mImagenWelcome.frame.size.height)];
                
                mImagenGreen.image = [UIImage imageNamed:@"green.png"];
                [mImagenGreen setFrame:CGRectMake(mImagenGreen.frame.origin.x, mImagenGreen.frame.origin.y, (68/1), mImagenGreen.frame.size.height)];
                
                mImagenGold.image = [UIImage imageNamed:@"gold.png"];
                [mImagenGold setFrame:CGRectMake(mImagenGold.frame.origin.x, mImagenGold.frame.origin.y, (89/1), mImagenGold.frame.size.height)];
            }
        
        else{
              mNextLevelLabel.text = [NSString stringWithFormat:@"Bebida Gratis"];                    
              mFaltanLabel.text = [NSString stringWithFormat:@"%d", prueba2];
              mTotalLabel.text = [NSString stringWithFormat:@"%d", prueba];
            
        
        mImagenWelcome.image = [UIImage imageNamed:@"welcome.png"];
            [mImagenWelcome setFrame:CGRectMake(mImagenWelcome.frame.origin.x, mImagenWelcome.frame.origin.y, (37/1), mImagenWelcome.frame.size.height)];
            
        mImagenGreen.image = [UIImage imageNamed:@"green.png"];
            [mImagenGreen setFrame:CGRectMake(mImagenGreen.frame.origin.x, mImagenGreen.frame.origin.y, (68/1), mImagenGreen.frame.size.height)];
            
        mImagenGold.image = [UIImage imageNamed:@"gold.png"];
            [mImagenGold setFrame:CGRectMake(mImagenGold.frame.origin.x, mImagenGold.frame.origin.y, (89/1), mImagenGold.frame.size.height)];
            
            }
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gold.png"]]];
        
        }
    
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsDetailsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    // Do any additional setup after loading the view from its nib.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) setDate
{

	NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:MM:SS"];
 
    NSString* str = [formatter stringFromDate:date];
    [mDateLabel setText:str];
    
    
    
  

}

-(IBAction) goToBenefits:(id) sender
{
    @try{
	OBenefitsRewardsViewController *benefitsViewController = [[OBenefitsRewardsViewController alloc] initWithNibName:@"OBenefitsRewardsViewController" bundle:nil];
	
	
	SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:benefitsViewController];
	
	[self presentModalViewController:navigationController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsDetailsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

@end
