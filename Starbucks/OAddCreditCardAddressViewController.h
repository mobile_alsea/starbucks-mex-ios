//
//  OAddCreditCardAddressViewController.h
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"

@interface OAddCreditCardAddressViewController : OBaseViewController
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UIView *viewAddAddressCreditCardInformation;

@end
