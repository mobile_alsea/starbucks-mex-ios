//
//  OCofeesImporter.h
//  Starbucks
//
//  Created by Santi Belloso López on 27/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBaseImporter.h"


@interface OCofeesImporter : OBaseImporter
{

	NSMutableDictionary *mCurrentCoffeeDict;
    NSMutableDictionary *mCurrentFlavorDict;
    NSMutableDictionary *mCurrentImageDict;
    
    NSMutableArray *mCoffeeFlavorArray;
    NSMutableArray *mCoffeeImageArray;
	
    BOOL mIsFlavor;
    BOOL mIsImage;

}

- (void) insertIntoContext:(NSDictionary *)_item;

@end
