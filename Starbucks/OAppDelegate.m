//
//  OAppDelegate.m
//  Starbucks
//
//  Created by Adrián Caramés on 08/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OAppDelegate.h"
#import "OHomeViewController.h"
#import "OServiceManager.h"
#import "SHKConfiguration.h"
#import <ShareKit/Sharers/Services/Facebook/SHKFacebook.h>
#import "OShareConfigurator.h"
#import "OShareFacebook.h"
#import <DYCore/categories/hash/NSData_hash.h>
#import "OMyDrinks.h"
#import "OMyDrinks+Extras.h"
#import "OCustomization.h"
#import <DYCore/files/DYFileNameManager.h>
#import "ODrinkViewController.h"
#import "OStoresViewController.h"
#import "OFoodViewController.h"
#import "ORewardsViewController.h"
#import "OInitRewardsViewController.h"
#import "OSetPassCodeViewController.h"
#import "OMyFavoritesViewController.h"
#import "OCoffeeViewController.h"
#import "OMydrinksViewController.h"
#import "OConfigurationViewController.h"
#import "ORewardsCardViewController.h"
#import "SettingsPagaTodo.h"
#import "UIDevice+IdentifierAddition.h"

#define RGBToFloat(f) (f/255.0)


@implementation OAppDelegate

#pragma mark - Appearance Methods

- (void)setCustomAppearance
{
    UINavigationBar *navBarAppearance = [UINavigationBar appearance];
    //[navBarAppearance setTintColor:[UIColor colorWithRed:RGBToFlosoat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
    [navBarAppearance setBackgroundImage:[UIImage imageNamed:@"NavBar"] forBarMetrics:UIBarMetricsDefault];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        //[navBarAppearance setBackgroundImage:[UIImage imageNamed:@"NavBari7"] forBarMetrics:UIBarMetricsDefault];
        [navBarAppearance setBackgroundImage:[UIImage imageNamed:@"NavBar"] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];

    }
}

#pragma mark - Properties

@synthesize window = _window;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize operationQueue = mOperationQueue;
@synthesize navigationController = mNavigationController;
@synthesize tabController=tabBarController;


#pragma mark - Application Delegate Methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setCustomAppearance];
	
	DefaultSHKConfigurator *configurator = [[OShareConfigurator alloc] init];
	[SHKConfiguration sharedInstanceWithConfigurator:configurator];
		
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    mOperationQueue = [[NSOperationQueue alloc] init];
    
//  Setting HomeController
    NSDictionary *controllers = @{@1:[[SBNavigationController alloc] initWithRootViewController:[[OMyFavoritesViewController alloc] initWithNibName:@"OMyFavoritesViewController" bundle:nil]],
                                  @2:[[SBNavigationController alloc] initWithRootViewController:[[OCoffeeViewController alloc] initWithNibName:@"OCoffeeViewController" bundle:nil]],
                                  @3:[[SBNavigationController alloc] initWithRootViewController:[[OFoodViewController alloc] initWithNibName:@"OFoodViewController" bundle:nil]],
                                  @4:[[SBNavigationController alloc] initWithRootViewController:[[ODrinkViewController alloc] initWithNibName:@"ODrinkViewController" bundle:nil]],
                                  @5:[[SBNavigationController alloc] initWithRootViewController:[[OInitRewardsViewController alloc] initWithNibName:@"OInitRewardsViewController" bundle:nil]],
                                  @6:[[SBNavigationController alloc] initWithRootViewController:[[OMydrinksViewController alloc] initWithNibName:@"OMydrinksViewController" bundle:nil]],
                                  @7:[[SBNavigationController alloc] initWithRootViewController:[[OStoresViewController alloc] initWithNibName:@"OStoresViewController" bundle:nil]],
                                  @8:[[SBNavigationController alloc] initWithRootViewController:[[OConfigurationViewController alloc] initWithNibName:@"OConfigurationViewController" bundle:nil]],
                                  @13:[[SBNavigationController alloc] initWithRootViewController:[[ORewardsCardViewController alloc] initWithNibName:@"ORewardsCardViewController" bundle:nil]]};
    
    
    OHomeViewController *homeViewController = [[OHomeViewController alloc] initWithNibName:@"OHomeViewController" bundle:nil];
    homeViewController.controllers = controllers;
    self.navigationController = [[SBNavigationController alloc] initWithRootViewController:homeViewController];
 
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FECHA_CONFIGURACION"] == nil) {
        [defaults setObject:[[UIDevice currentDevice] universallyUniqueDeviceIdentifier] forKey:@"UUID"];
        [defaults setObject:[SettingsPagaTodo getURLWSRewards] forKey:@"URL_WS_REWARDS"];
        [defaults setObject:[SettingsPagaTodo getURLWSSmart] forKey:@"URL_WS_SMART"];
        [defaults setObject:[SettingsPagaTodo getURLSwitch] forKey:@"URL_SWITCH"];
        [defaults setObject:[SettingsPagaTodo getDateSettings] forKey:@"DATE_SWITCH"];
        [defaults setObject:[SettingsPagaTodo getURLAppStore] forKey:@"URL_APP_STORE"];
        [defaults setObject:[SettingsPagaTodo getCertWSSmart] forKey:@"DIGEST_CERT_WS_SMART_CERT_IOS"];
        [defaults setObject:[SettingsPagaTodo getCertWSRewards] forKey:@"DIGEST_CERT_WS_REWARDS_CERT_IOS"];
        [defaults setObject:[SettingsPagaTodo getCertKM] forKey:@"DIGEST_CERT_KM_IOS"];
        [defaults setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey] forKey:@"VERSION"];
        [defaults synchronize];
    }

    self.window.rootViewController = mNavigationController;
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    return YES;
}


- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return (UIInterfaceOrientationMaskAll);
}


- (void)applicationWillResignActive:(UIApplication *)application
{ 
// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//NSManagedObjectContext *moc = [self managedObjectContext];
    
    #pragma marl IMPORTANTE DESCOMENTAR
    [OServiceManager updateFullData];
		
	NSString * readValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"Code"];
	if(readValue != nil && ![readValue isEqualToString:@"-1"])
	{
		OSetPassCodeViewController *detailViewController = [[OSetPassCodeViewController alloc] initWithNibName:@"OSetPassCodeViewController" bundle:nil];
		detailViewController.passwordSavedInFile = readValue;
		detailViewController.modal=YES;
		[self.window.rootViewController presentModalViewController:detailViewController animated:YES];
	}
}

- (void)applicationWillTerminate:(UIApplication *)application
{
// Saves changes in the application's managed object context before the application terminates.		
    [self saveContext];
}


#pragma mark - Private Methods

- (void)saveContext
{
    @try{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/saveContext");
    }
}

- (void) saveManagedObjectContext:(NSManagedObjectContext *)_context
{
    @try{
    if (_context == nil)
        _context = _managedObjectContext;
    
	NSError * outError;
	if (![_context save:&outError])
	{
		NSLog(@"Unresolved error %@, %@", outError, [outError userInfo]);
		exit(-1);
	}
    else
    {
        NSLog(@"Save OK");
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/saveManagedObject");
    }
}

- (void)mergeMainContextWithContext:(NSNotification *)_saveNotification
{
    @try{
    if([NSThread isMainThread]==NO)
    {
        [self performSelectorOnMainThread:@selector(mergeMainContextWithContext:) withObject:_saveNotification waitUntilDone:YES];
        return;
    }
    
	[self.managedObjectContext mergeChangesFromContextDidSaveNotification:_saveNotification];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/mergeMainContextWithContext");
    }
}

- (void)removeObjects:(NSArray *)_objects fromContext:(NSManagedObjectContext *)_context
{
    @try{
    for (NSManagedObject *item in _objects)
        [_context deleteObject:item];
    
    NSLog(@"Eliminados: %i", [_objects count]);
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/removeObjects");
    }
}


#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    @try{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/managedObjectContext");
    }
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    @try{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Starbucks" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/managedObjectmodel");
    }
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    @try{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSError *error = nil;
    NSString * path = [DYFileNameManager pathForFile:@"Starbucks.sqlite" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:NO];
    NSURL *storeURL = [NSURL fileURLWithPath:path];
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/persistentSotreCoordinator");
    }
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    @try{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/applicationDocumentdirectory");
    }
}

+ (NSPersistentStoreCoordinator *)coordinator {
    OAppDelegate *delegate = (OAppDelegate *)[[UIApplication sharedApplication] delegate];
    return delegate.persistentStoreCoordinator;
}

+ (NSManagedObjectContext *) managedObjectContext
{
    @try{
    OAppDelegate *delegate = (OAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return delegate.managedObjectContext;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/mangedObjectContext");
    }
}

+ (NSManagedObjectModel *) managedObjectModel
{
    @try{
    OAppDelegate *delegate = (OAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return delegate.managedObjectModel;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/managedObjectmodel");
    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    @try{
	if (!url) {  return NO; }
	
	NSString* scheme = [url scheme];
	NSString* prefix = [NSString stringWithFormat:@"fb%@", SHKCONFIG(facebookAppId)];
	if ([scheme hasPrefix:prefix])
		return [OShareFacebook handleOpenURL:url];
	
	
	else
	{
		NSString *urlString = [url absoluteString];
		urlString = [urlString stringByReplacingOccurrencesOfString:@"sbucks://" withString:@""];
		
		NSData* data=[urlString dataUsingEncoding:NSUTF8StringEncoding];
		data = [data base64Decode];
		
		NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
		[self saveDrinkWithString:newStr];
		
        
	}
	
    
    return YES;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/application");
    }
    
}


-(void) saveDrinkWithString:(NSString*)_savedDrinkString
{
    
	@try{
	
	NSString *pListPath = [DYFileNameManager pathForFile:@"Identifier.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
	NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
	
	NSString * ReadValue = [dictionary valueForKey:@"id"];
	
	int mId = [ReadValue intValue];
	
	mId++;
	
	NSMutableDictionary * drinkDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];
	NSArray *listItems = [_savedDrinkString componentsSeparatedByString:@"&"];
	BOOL salir = NO;
	int numberOfShots = 0;
	int indexOfShotsElement=0;
	for(int i=0;i<[listItems count] && !salir;i++)
	{
		NSArray * listElements = [[listItems objectAtIndex:i] componentsSeparatedByString:@"="];
		if([[listElements objectAtIndex:0 ] isEqualToString:@"shots"])
		{
			
			salir = YES;
			numberOfShots = [[listElements objectAtIndex:1] intValue];
			indexOfShotsElement = i+1;
		}
		else
		{
			[drinkDictionary setValue:[listElements objectAtIndex:1] forKey:[listElements objectAtIndex:0]];
		}
		
	}
	NSMutableArray * shotsArray =[[NSMutableArray alloc] initWithCapacity:0];
	if(indexOfShotsElement!=0)
	{
		
		for(int j=0;j<numberOfShots;j++)
		{
            
			NSMutableDictionary * shotDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];
			for(int i=indexOfShotsElement;i<indexOfShotsElement+4;i++)
			{
				NSArray * listElements = [[listItems objectAtIndex:i] componentsSeparatedByString:@"="];
				
				[shotDictionary setValue:[listElements objectAtIndex:1] forKey:[listElements objectAtIndex:0]];
			}
			[shotsArray addObject:shotDictionary];
			indexOfShotsElement +=4;
		}
	}
	NSEntityDescription *myDrinksEntity = [NSEntityDescription entityForName:@"MyDrinks" inManagedObjectContext:[self managedObjectContext]];
	
	OMyDrinks * mDrinkCustomize= [[OMyDrinks alloc] initWithEntity:myDrinksEntity insertIntoManagedObjectContext:[self managedObjectContext]];
	
	mDrinkCustomize.ide = [NSString stringWithFormat:@"%d",mId];
	mDrinkCustomize.available = [drinkDictionary valueForKey:@"available"];
	mDrinkCustomize.category = [drinkDictionary valueForKey:@"category"];
	mDrinkCustomize.glassmark = [drinkDictionary valueForKey:@"glassmark"];
	mDrinkCustomize.image = [drinkDictionary valueForKey:@"image"];
	mDrinkCustomize.largedescription = [drinkDictionary valueForKey:@"largedescription"];
	mDrinkCustomize.name =[drinkDictionary valueForKey:@"name"];
	mDrinkCustomize.shortdescription = [drinkDictionary valueForKey:@"shortdescription"];
	mDrinkCustomize.subcategory = [drinkDictionary valueForKey:@"subcategory"];
	mDrinkCustomize.temperature =[drinkDictionary valueForKey:@"temperature"];
	mDrinkCustomize.user = [drinkDictionary valueForKey:@"user"];
	mDrinkCustomize.userType =[drinkDictionary valueForKey:@"userType"];
	mDrinkCustomize.notes =[drinkDictionary valueForKey:@"notes"];
	mDrinkCustomize.nickName=[drinkDictionary valueForKey:@"nickName"];
	mDrinkCustomize.size = [drinkDictionary valueForKey:@"size"];
	
	
	
	for(int i=0;i<numberOfShots;i++)
	{
		
		
		NSEntityDescription *myCustomizationEntity = [NSEntityDescription entityForName:@"Customization" inManagedObjectContext:[self managedObjectContext]];
		OCustomization * customization = [[OCustomization alloc] initWithEntity:myCustomizationEntity insertIntoManagedObjectContext:[self managedObjectContext]];
		customization.ide =[NSString stringWithFormat:@"%d",mId];
		customization.abbreviature = [[shotsArray objectAtIndex:i] valueForKey:@"abbreviature"];
		customization.amount = [[shotsArray objectAtIndex:i] valueForKey:@"amount"];
		customization.category = [[shotsArray objectAtIndex:i] valueForKey:@"category"];
		customization.subcategory = [[shotsArray objectAtIndex:i] valueForKey:@"subcategory"];
		
		[mDrinkCustomize addCustomizationsObject:customization];
		
	}
    
	NSError * error = nil;
	[[self managedObjectContext] save:&error];
	
	
	NSMutableDictionary* dictionary2 = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
	
	
	NSString * value = [NSString stringWithFormat:@"%d",mId];
	[dictionary2 setObject:value forKey:@"id"];
	[dictionary2 writeToFile:pListPath atomically:YES];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/saveDrinkWithString");
    }
}

@end


#pragma mark -

@implementation UINavigationBar (UINavigationBarCategory)

- (void)drawRect:(CGRect)rect
{
    @try{
	UIImage *image = [UIImage imageNamed: @"NavBar.png"];
	[image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAppDelegate.m/drawRect");
    }
}

@end
