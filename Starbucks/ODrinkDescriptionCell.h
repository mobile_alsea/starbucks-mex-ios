//
//  ODrinkDescriptionCell.h
//  Starbucks
//
//  Created by Santi Belloso López on 14/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ODrinkDescriptionCell : UITableViewCell
{

	UILabel * mNameLabel;
	UILabel * mAvaliableLabel;
	UILabel * mUserLabel;
	UILabel * mPropertyOfLabel;
	
	NSString * mName;
	NSString * mAvailable;
	NSString * mUser;

}

@property(nonatomic, strong) IBOutlet UILabel * nameLabel;
@property(nonatomic, strong) IBOutlet UILabel * availableLabel;
@property(nonatomic, strong) IBOutlet UILabel * userLabel;
@property(nonatomic, strong) IBOutlet UILabel * propertyOfLabel;
@property(nonatomic, strong) NSString * name;
@property(nonatomic, strong) NSString * available;
@property(nonatomic, strong) NSString * user;

@end
