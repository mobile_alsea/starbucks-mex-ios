//
//  OAppDelegate.h
//  Starbucks
//
//  Created by Adrián Caramés on 08/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kVersionsURL @"http://200.53.147.250/cms/xml/versiones.xml"
#define kStoresURL	 @"http://200.53.147.250/cms/xml/tiendas.xml"
#define kCofeesURL	 @"http://200.53.147.250/cms/xml/cafes.xml"
#define kFoodURL	 @"http://200.53.147.250/cms/xml/comidas.xml"
#define kDrinkURL	 @"http://200.53.147.250/cms/xml/bebidas.xml"


@interface OAppDelegate : UIResponder <UIApplicationDelegate>
{
    SBNavigationController *mNavigationController;
	NSOperationQueue *mOperationQueue;
    UITabBarController *tabBarController;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *   managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) SBNavigationController *navigationController;
@property (nonatomic, readonly)	NSOperationQueue *operationQueue;


@property (nonatomic, strong) UITabBarController *tabController;

- (void)saveContext;
- (void)saveManagedObjectContext:(NSManagedObjectContext *)_context;
- (void)removeObjects:(NSArray *)_objects fromContext:(NSManagedObjectContext *)_context;
- (NSURL *)applicationDocumentsDirectory;

+ (NSPersistentStoreCoordinator *)coordinator;
+ (NSManagedObjectContext *)managedObjectContext;
+ (NSManagedObjectModel *) managedObjectModel;

@end
