//
//  OPaymentTypeCell.m
//  Starbucks
//
//  Created by Mobile on 10/22/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OPaymentTypeCell.h"
#define RGBToFloat(f) (f/255.0)

@implementation OPaymentTypeCell
@synthesize lblTitulo = mLblTitulo;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize index = mIndex;
@synthesize indexPathPair = mIndexPathPair;
@synthesize CheckButton = mCheckButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL) indexPathPair
{
    return mIndexPathPair;
}

- (void) setIndexPathPair:(BOOL)_indexPathPair
{
    mIndexPathPair = _indexPathPair;
    
    if (mIndexPathPair)
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    else
        [self.contentView setBackgroundColor:[UIColor colorWithRed:RGBToFloat(238.0) green:RGBToFloat(238.0) blue:RGBToFloat(238.0) alpha:1.0]];
}

@end
