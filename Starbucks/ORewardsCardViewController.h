//
//  ORewardsCardViewController.h
//  Starbucks
//
//  Created by Mobile on 10/17/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBaseViewController.h"
#import "OAppDelegate.h"
#import "OSignUpRewardsViewController.h"
#import "OSignInRewardsViewController.h"
#import "OAddRewardsCardViewController.h"
#import "OManagementRewardsCardsViewController.h"
#import "OCardViewController.h"
#import "MBProgressHUD.h"

@interface ORewardsCardViewController : OBaseViewController<UIActionSheetDelegate,UIScrollViewDelegate,NSURLConnectionDelegate,OCardViewControllerDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
@property (strong, nonatomic) IBOutlet UIView *viewIntroduction;
@property (strong, nonatomic) IBOutlet UIView *viewReverseCard;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *rewardsCardScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *CupImageView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *ReloadActivityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountLabel;
@property (strong, nonatomic) IBOutlet UIButton *CardReloadButton;
@property (strong, nonatomic) IBOutlet UIButton *CardManagementButton;
@property (strong, nonatomic) MBProgressHUD *HUD;

@property (strong, nonatomic) IBOutlet UIImageView *PDF417ImageView;

- (IBAction)ReloadButtonPressed;
- (IBAction)ManageButtonPressed;

- (IBAction)SignUpButtonPressed:(id)sender;
- (IBAction)SignInButtonPressed:(id)sender;
- (IBAction)ShowReverseCardButtonPressed:(id)sender;

- (IBAction)cupRotatesOnXAxis:(id)sender;
@end
