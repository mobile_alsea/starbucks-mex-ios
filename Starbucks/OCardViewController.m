//
//  OCardViewController.m
//  Starbucks
//
//  Created by Mobile on 11/1/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OCardViewController.h"
#import "AsyncImageView.h"

@interface OCardViewController ()
@property (strong, nonatomic) NSTimer *handleButtonBlinkTimer;
- (void)blinkTabButton;
- (void)flipTabButtonImage:(NSTimer*)theTimer;
@end

@implementation OCardViewController
@synthesize delegate;
@synthesize viewCard;
@synthesize card;
@synthesize ShowReverseCardButton;

/*
@synthesize ReloadActivityIndicator;
@synthesize handleButton;
@synthesize CardManagementButton;
@synthesize CardReloadButton;
@synthesize dateLabel;
@synthesize amountLabel;
*/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    handleButtonImage = [self createTabButtonImageWithFillColor:[UIColor colorWithWhite:1.0 alpha:0.25]];
    handleButtonBlinkImage = [self createTabButtonImageWithFillColor:[UIColor whiteColor]];
    self.handleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.handleButton.frame = CGRectMake(235.0, 143.0, 30.0, 30.0);
    self.handleButton.center = CGPointMake(15.0, 15.0);
    self.handleButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.handleButton setImage:handleButtonImage forState:UIControlStateNormal];
    [self.handleButton addTarget:self action:@selector(ShowCardReverseButtonPressed) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.handleButton];
     
     
    toolDrawerFadeTimer = [NSTimer scheduledTimerWithTimeInterval:self.durationToFade
    target:self
    selector:@selector(fadeAway:)
    userInfo:nil
    repeats:NO];
    */
    
    NSString *imageURL = @"http://54.219.59.225:9000/card-small.jpg";
    
    //NSLog(@"%i",[card.imagesArray count]);
    
    /*
    for (int i=0; i<[card.imagesArray count]; i++) {
        NSDictionary *imagesDictionary = (NSDictionary *)[card.imagesArray objectAtIndex:i];
        
        if (i==0) {
            imageURL = (NSString *)[imagesDictionary objectForKey:@"chica"];
        }
    }
    */
    
    AsyncImageView* asyncImage1 = [[AsyncImageView alloc] initWithFrame:viewCard.bounds];
    asyncImage1.contentMode = UIViewContentModeScaleAspectFit;
    asyncImage1.clipsToBounds = YES;
    NSURL* url = [NSURL URLWithString:imageURL];
    [asyncImage1 loadImageFromURL:url];
    [viewCard addSubview:asyncImage1];
    //viewCard.layer.cornerRadius = 10.0f;
    asyncImage1.backgroundColor = [UIColor clearColor];
    
    /*
    dateLabel.text = [NSString stringWithFormat:@"Hasta el %@", card.activatedDate];
    if ((id)card.balance!= [NSNull null]) {
        double val = [card.balance doubleValue];
    
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [currencyFormatter setCurrencySymbol:@""];
    amountLabel.text = [NSString stringWithFormat:@"$%@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:val]]];
    }else{
        amountLabel.text = @"";
    }
    
    amountLabel.text = [NSString stringWithFormat:@"$%@", card.balance];
    [CardReloadButton.layer setCornerRadius:4.0f];
    [CardReloadButton.layer setMasksToBounds:YES];
    [CardManagementButton.layer setCornerRadius:4.0f];
    [CardManagementButton.layer setMasksToBounds:YES];
    
    [self threadStartAnimating:nil];
    [self performSelector:@selector(threadStopAnimating) withObject:self afterDelay:1.5];
   */ 
    
}


- (void)viewWillAppear:(BOOL)animated
{
     //[self threadStartAnimating:nil];
     //[self performSelector:@selector(threadStopAnimating) withObject:self afterDelay:10.0];
}

/*
- (void)fadeAway:(NSTimer*)theTimer{
    toolDrawerFadeTimer = nil;
    if (self.alpha == 1.0){
        [UIView animateWithDuration:0.5
							  delay:0.0
							options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
						 animations:^{ self.alpha = 0.5; }
						 completion:nil];
    }
}
*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Draw the cheveron button in either the filled or empty state;
- (UIImage *)createTabButtonImageWithFillColor:(UIColor *)fillColor{
    UIGraphicsBeginImageContext(CGSizeMake(24.0, 24.0));
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6].CGColor);
    
    CGContextSetFillColorWithColor(ctx, fillColor.CGColor);
    CGContextSetLineWidth(ctx, 2.0);
    
    CGRect circle = CGRectMake(2.0, 2.0, 20.0, 20.0);
    // Draw filled circle
    CGContextFillEllipseInRect(ctx, circle);
    
    // Stroke circle
    CGContextAddEllipseInRect(ctx, circle);
    CGContextStrokePath(ctx);
    
    // Stroke Chevron
    CGFloat chevronOffset = 4.0;
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, 12.0 - chevronOffset, 12.0 - chevronOffset);
    CGContextAddLineToPoint(ctx, 12.0 + chevronOffset, 12.0);
    CGContextAddLineToPoint(ctx, 12.0 - chevronOffset, 12.0 + chevronOffset);
    CGContextAddLineToPoint(ctx, 12.0 - chevronOffset, 12 - chevronOffset);
    CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextFillPath(ctx);
    CGContextStrokePath(ctx);
    
    UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return buttonImage;
}


- (void)flipTabButtonImage:(NSTimer*)theTimer{
    if (self.handleButton.imageView.image == handleButtonBlinkImage){
        self.handleButton.imageView.image = handleButtonImage;
    } else {
        self.handleButton.imageView.image = handleButtonBlinkImage;
    }
}

- (void)resetTabButton{
    if (handleButtonBlinkTimer != nil){
        if ([handleButtonBlinkTimer isValid]){
            [handleButtonBlinkTimer invalidate];
        }
        
        handleButtonBlinkTimer = nil;
    }
    
    self.handleButton.imageView.image = handleButtonImage;
}

- (void)blinkTabButton{
    handleButtonBlinkTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target:self
                                                            selector:@selector(flipTabButtonImage:)
                                                            userInfo:nil
                                                             repeats:YES];
}

#pragma mark OCardControllerDelegate is call

- (IBAction)ShowCardReverseButtonPressed{
    [self.delegate OCardViewControllerDidPressReverseButton:self];
}

- (IBAction)ReloadButtonPressed{
    [self.delegate OCardViewControllerDidReloadButtonPressed:self];
}

- (IBAction)ManageButtonPressed{
    [self.delegate OCardViewControllerDidManageButtonPressed:self];
}

/*
-(void)threadStartAnimating:(id)data
{
    [ReloadActivityIndicator setHidden:NO];
    [ReloadActivityIndicator startAnimating];
}

- (void) threadStopAnimating
{
    [ReloadActivityIndicator stopAnimating];
    [ReloadActivityIndicator setHidden:YES];
}
*/

@end
