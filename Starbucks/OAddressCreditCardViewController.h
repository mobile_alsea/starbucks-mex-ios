//
//  OAddressCreditCardViewController.h
//  Starbucks
//
//  Created by Mobile on 10/23/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"

@interface OAddressCreditCardViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
}
@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UIView *viewAdrressInformation;
@property (strong, nonatomic) IBOutlet UITableView *AddressInformationTableView;
- (IBAction)AddAddressButtonPressed:(id)sender;

@end
