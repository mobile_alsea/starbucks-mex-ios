//
//  OFoodDetailViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFoodDetailViewController.h"
#import "OAppDelegate.h"
#import "OMYFavorites+Extras.h"
#import "OImagesDownloaded.h"
#import "OImagesDownloaded+Extras.h"
#import "OCombinationCoffeeCell.h"
#import "OCoffeeDetailViewController.h"
#import "OCoffee+Extras.h"
#import "OCombinations.h"

@interface OFoodDetailViewController ()

@end

@implementation OFoodDetailViewController
@synthesize foodSelected = mFoodSelected;
@synthesize delegate = mDelegate;
@synthesize nameLabel = mNameLabel;
@synthesize descriptionLabel = mDescriptionLabel;
@synthesize plusButton = mPlusButton;
@synthesize availableButton = mAvailableButton;
@synthesize thumbnailImageView = mThumbnailImageView;
@synthesize addFoodLabel = mAddFoodLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
		mAddingContext = [[NSManagedObjectContext alloc] init];
		[mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
		
		
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

/*-(void) dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
	
}*/

-(void) viewWillAppear:(BOOL)animated
{
    @try{
        [super viewWillAppear:animated];
        //if(!isOS6())
        /*
        {
            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
            UIViewController * prueba = [[UIViewController alloc] init];
            [prueba.view setHidden:YES];
            [self presentModalViewController:prueba animated:NO];
            [self dismissModalViewControllerAnimated:NO];
        }
         */
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
        
    
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLoadingImage:) name:DYImageViewDidFinishLoadingNotification object:nil];
	[self.navigationController.navigationBar setHidden:NO];
	UIImage *stateSummer = [UIImage imageNamed:@"DrinkSizeButtonUnselected.png"];
    [mAvailableButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [mAvailableButton setTitle:NSLocalizedString(@"Todo el año", nil) forState:UIControlStateNormal];
	
	
	NSMutableArray * array = [[NSMutableArray alloc] initWithCapacity:0];
	NSSet * combinations = (NSSet*) mFoodSelected.combinations;
	NSArray * combinationsArray = [combinations allObjects];
	
	for(int i=0;i<[combinationsArray count];i++)
	{
		OCombinations * comb  = (OCombinations*)[ combinationsArray objectAtIndex:i];
		[array addObject:comb.name];
		
	}
	
	NSArray *coffeNames =  [array copy];
	
	 mCombinationsArray = [OCoffee coffeesByCombinationNames:coffeNames inContext:mAddingContext];
	if(mCombinationsArray==nil || [mCombinationsArray count]==0)
	{
		[mTableView setHidden:YES];
		[mCombinationLabel setHidden:YES];
		[mCombinationImageView setHidden:YES];
	
	}
	else
	{ 
		[mCombinationLabel setHidden:NO];
		[mCombinationImageView setHidden:NO];
		[mTableView setHidden:NO];
	}
	[mTableView reloadData];
	
	
	
	
	mThumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-alimentos-low.png"];
	mPlusDYImageView.failureImage = [UIImage imageNamed:@"noimage-alimentos-high.png"];
		[self.navigationItem setTitle:NSLocalizedString(@"Alimento", nil)];
	
	mNameLabel.text = mFoodSelected.name;
	mDescriptionLabel.text = mFoodSelected.largedescrip;
    
    [mThumbnailImageView setAnimationStyle:DYImageViewAnimationStyleWhite];
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:mFoodSelected.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[mThumbnailImageView setUrl:[NSURL URLWithString:mFoodSelected.image]];
	}
	else
	{
		[mThumbnailImageView setImage:[UIImage imageWithData:image.image]];
	}
	
	
	mFavorite =[OMyFavorites favoriteWithName:mFoodSelected.name context:mAddingContext];
	
	if(mFavorite!=nil)
	{ 
		mIsAdded = YES;
		[mAddingContext deleteObject:mFavorite];
		mAddFoodLabel.text = @"Eliminar de mis Alimentos";
	}
	else
	{
		mIsAdded = NO;
		mAddFoodLabel.text = @"Añadir a mis Alimentos";
	}
    }


    @catch (NSException *ex) {
        
        NSLog(@"Exception - oFoodDetailViewcontroller");
        
        OBaseViewController *comidacon =[[OBaseViewController alloc]initWithNibName:nil bundle:nil];
        
        [self.navigationController pushViewController:comidacon animated:YES];
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }


    // Do any additional setup after loading the view from its nib.
}


- (void) didFinishLoadingImage: (NSNotification*)_notification
{
    @try{
	if([NSThread isMainThread]==NO)
	{
		[self performSelectorOnMainThread:@selector(didFinishLoadingImage:) withObject:_notification waitUntilDone:YES];
		return;
	}
	NSDictionary * user_info =  _notification.userInfo;
	
	NSString * url = [[user_info objectForKey:@"url"]absoluteString];
	UIImage * image =  [user_info objectForKey:@"image"];
	NSData * data = UIImagePNGRepresentation(image);
	NSEntityDescription *imageEntity = [NSEntityDescription entityForName:@"ImagesDownloaded" inManagedObjectContext:mAddingContext];
	
	OImagesDownloaded *imageObject = [[OImagesDownloaded alloc] initWithEntity:imageEntity insertIntoManagedObjectContext:mAddingContext];
	imageObject.name = url;
	imageObject.image = data;
	[mAddingContext save:nil];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) configureCell:(OCombinationCoffeeCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    OCoffee *coffee = (OCoffee*)[mCombinationsArray objectAtIndex:_indexPath.row];
    
    _cell.titleLabel.text = coffee.name;
	
	UIImage *stateSummer = [UIImage imageNamed:@"StoreList_Contact_bkgnd.png"];
    [_cell.avalaibleButton setBackgroundImage:stateSummer forState:UIControlStateNormal];
    [_cell.avalaibleButton setTitle:coffee.availability forState:UIControlStateNormal];
	
	
	
	NSSet * seter = mFoodSelected.combinations;
	NSArray * combinations = [seter allObjects];
	
	OCombinations * combination = (OCombinations*)[combinations objectAtIndex:_indexPath.row];
	
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:coffee.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		NSURL * url = [NSURL URLWithString:combination.image];
		
		if(url!=nil)[_cell.thumbnailImageView setUrl:url];
	}
	else
	{
		
		[_cell.thumbnailImageView setImage:[UIImage imageWithData:image.image]];
		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}



-(IBAction)plusButtonSelected:(id)sender
{
    @try{
	/*[UIView animateWithDuration:0.5f
						  delay:0.0f
						options:UIViewAnimationCurveLinear 
					 animations:^{
						 mViewWithAlpha.alpha = 0.5;
					 }
					 completion:nil];*/
	
	
	
	[mPlusView setHidden:NO];
	//[mPlusDYImageView setAnimationStyle:DYImageViewAnimationStyleWhite];
	
	OImagesDownloaded * image = [OImagesDownloaded imageWithName:mFoodSelected.image context:mAddingContext];
	if(image==nil || image.image==nil)
	{
		[mPlusDYImageView setUrl:[NSURL URLWithString:mFoodSelected.image]];
	}
	else
	{
		[mPlusDYImageView setImage:[UIImage imageWithData:image.image]];
	}
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	

}
-(IBAction)dismmisPlusButtonSelected:(id)sender
{
	[mPlusView setHidden:YES];
	[mPlusDYImageView setImage:nil];

}

-(IBAction)addToMyFood:(id)sender
{
    @try{
	if(mIsAdded==YES)
	{
		//Delete from bd
		
		[mAddingContext deleteObject:mFavorite];
		mAddFoodLabel.text = @"Añadir a mis Alimentos";
		NSError * error = nil;
		[mAddingContext save:&error];
		mIsAdded = NO;
		
	}
	else
	{
		NSEntityDescription *myFavoriteEntity = [NSEntityDescription entityForName:@"MyFavorites" inManagedObjectContext:mAddingContext];
		mFavorite = [[OMyFavorites alloc] initWithEntity:myFavoriteEntity insertIntoManagedObjectContext:mAddingContext];
		mFavorite.type = @"3";
		mFavorite.name = mNameLabel.text;
		NSError * error = nil;
		[mAddingContext save:&error];
		mAddFoodLabel.text = @"Eliminar de mis Alimentos";
		mIsAdded = YES;
		
	}
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

#pragma mark UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    NSSet * seter = (NSSet*)mFoodSelected.combinations;
	
    return [[seter allObjects] count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
    static NSString *CellIdentifier = @"OCombinationCoffeeCell";
    //OCoffee *item = (OCoffee*)[mCombinationsArray objectAtIndex:_indexPath.row];
	
    OCombinationCoffeeCell *cell = (OCombinationCoffeeCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
    }
    
    
	cell.index = _indexPath.row;
	cell.thumbnailImageView.failureImage = [UIImage imageNamed:@"noimage-alimentos-low.png"];
	[cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
    [cell updateSchedule:0];
	
	
	[self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
	@try{
	[_tableView deselectRowAtIndexPath:_indexPath animated:YES];
	OCoffee *item = (OCoffee*)[mCombinationsArray objectAtIndex:_indexPath.row];
	
	OCoffeeDetailViewController *detailViewController = [[OCoffeeDetailViewController alloc] initWithNibName:@"OCoffeeDetailViewController" bundle:nil];
	[detailViewController setCoffeeSelected:item];
    detailViewController.delegate = self;	
	
	[self.navigationController pushViewController:detailViewController animated:YES];
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OFoodResultViewcontroller");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	return 65;
	
}



@end
