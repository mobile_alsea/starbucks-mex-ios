//
//  OInitRewardsViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OInitRewardsViewController.h"
#import "OBenefitsRewardsViewController.h"
#import "OLoginRewardsViewController.h"
#import <DYCore/files/DYFileNameManager.h>
#import "ORewardsViewController.h"

@interface OInitRewardsViewController ()

@end

@implementation OInitRewardsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OInitRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
	[self.navigationController.navigationBar setHidden:NO];
	
	[self.navigationItem setTitle:NSLocalizedString(@"My Starbucks Rewards", nil)];
	
	UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithTitle:@"Beneficios"  style:UIBarButtonItemStyleBordered target:self action:@selector(goToBenefits:)];
	
    
    
    [self.navigationItem setRightBarButtonItem:btnFilter];
    // Do any additional setup after loading the view from its nib.
        
        // Load Sesion Saved

        
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OInitRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark methods IBAction

-(IBAction) goToBenefits:(id) sender
{
	OBenefitsRewardsViewController *benefitsViewController = [[OBenefitsRewardsViewController alloc] initWithNibName:@"OBenefitsRewardsViewController" bundle:nil];
	
	
	SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:benefitsViewController];
	
	[self presentModalViewController:navigationController animated:YES];

	
}

-(IBAction)EnterButtonPressed:(id)sender
{
    
    NSString *pListPath = [DYFileNameManager pathForFile:@"RewardsUserInfo.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
    if(dictionary == nil)
    {
        
    }
    else
    {
        BOOL infoOk=YES;
        for (NSString* key in dictionary) {
            NSString *value = [dictionary objectForKey:key];
            
            if(value==nil || value.length==0)
            {
                infoOk=NO;
            }
            
        }
        
        if(infoOk)
        {
            
            ORewardsViewController *rewardViewController = [[ORewardsViewController alloc] initWithNibName:@"ORewardsViewController" bundle:nil];
            
            NSString *DateMember = [dictionary objectForKey:@"DateMember" ];
            
            NSString *Stars = [dictionary objectForKey:@"Stars" ];
            
            NSString *StarsGold = [dictionary objectForKey:@"StarsGold" ];
            
            rewardViewController.dateMember =  DateMember;
            
            rewardViewController.numberOfStars = Stars;
            
            rewardViewController.numberOfStarsGold = StarsGold;
            
            
            SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:rewardViewController];
            [navigationController.navigationBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"NavBar.png"]]];
            [self presentModalViewController:navigationController animated:YES];
            
            
            return;
            
        }
        else{
            OLoginRewardsViewController *loginViewController = [[OLoginRewardsViewController alloc] initWithNibName:@"OLoginRewardsViewController" bundle:nil];
            [self.navigationController pushViewController:loginViewController animated:YES];
        }
    }


    
    
    

}

@end
