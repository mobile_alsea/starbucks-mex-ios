//
//  OReloadCardViewController.m
//  Starbucks
//
//  Created by Mobile on 10/21/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import "OReloadCardViewController.h"
#import "OSelectPaymentViewController.h"
#import "OPaymentTypeCell.h"
#import "OCreditCardCell.h"
#import "ReloadSummaryViewController.h"
#import "OAddCreditCardViewController.h"

#define RGBToFloat(f) (f/255.0)

@interface OReloadCardViewController ()
{
    int senderButton;
    int heightPoint;
}
@property (strong, nonatomic) NSMutableArray *arrayReloadAvailableAmounts;
@property (strong, nonatomic) NSMutableArray *arrayMinimumBalanceAmounts;
@property (strong, nonatomic) NSMutableArray *arrayReloadAvailableDays;
@end

@implementation OReloadCardViewController
@synthesize sView;
@synthesize viewReloadCard;
@synthesize AutoReloadButton;
@synthesize firstAmountButton;
@synthesize secondAmountButton;
@synthesize thirdAmountButton;
@synthesize fourthAmountButton;
@synthesize AutoreloadInstructionLabel;
@synthesize AutoreloadSwitch;
@synthesize UserAmountPickerView;
@synthesize firstButtonBelowAmount;
@synthesize secondButtonBelowAmount;
@synthesize thirdButtonBelowAmount;
@synthesize fourthButtonBelowAmount;
@synthesize UserAmountToolbar;
@synthesize arrayReloadAvailableAmounts;
@synthesize arrayReloadAvailableDays;
@synthesize arrayMinimumBalanceAmounts;
@synthesize OneDayReloadButton;
@synthesize UserReloadDayPickerView;
@synthesize UserMinimumBalancePickerView;
@synthesize AutoreloadTitleLabel;

@synthesize PaymentTypeTableView;
@synthesize PaymentTypeInstructionLabel;
@synthesize PaymentTypeLabel;
@synthesize PaymentTypeButton;


@synthesize cardIdToReload;
@synthesize lastUpdate;

@synthesize cardBalanceLabel;
@synthesize lastUpdatedLabel;
@synthesize cardBalance;
@synthesize cardNumber;
@synthesize receivedData;
@synthesize HUD;


bool didChangeReloadAmount = 0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
            mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
            mAddingContext = [[NSManagedObjectContext alloc] init];
            [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
        }
        return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OReloadCardViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    @try{
        [super viewDidLoad];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
            self.edgesForExtendedLayout = UIRectEdgeNone;
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.navigationItem setTitle:NSLocalizedString(@"Recargar Tarjeta", nil)];
        UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Atras", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Dismiss:)];
        [self.navigationItem setLeftBarButtonItem:btnClose];
        
        cardBalanceLabel.text = [NSString stringWithFormat:@"$ %@",[self getCurrencyMode:cardBalance]];
        lastUpdatedLabel.text = [NSString stringWithFormat:@"actualizado el\n%@",lastUpdate];
        
        PaymentTypeTableView.layer.cornerRadius = 4.0;
        PaymentTypeTableView.layer.masksToBounds= YES;
        
        [firstAmountButton.layer setCornerRadius:4.0f];
        [firstAmountButton.layer setMasksToBounds:YES];
        [secondAmountButton.layer setCornerRadius:4.0f];
        [secondAmountButton.layer setMasksToBounds:YES];
        [thirdAmountButton.layer setCornerRadius:4.0f];
        [thirdAmountButton.layer setMasksToBounds:YES];
        [fourthAmountButton.layer setCornerRadius:4.0f];
        [fourthAmountButton.layer setMasksToBounds:YES];
        
        [firstButtonBelowAmount.layer setCornerRadius:4.0f];
        [firstButtonBelowAmount.layer setMasksToBounds:YES];
        [secondButtonBelowAmount.layer setCornerRadius:4.0f];
        [secondButtonBelowAmount.layer setMasksToBounds:YES];
        [thirdAmountButton.layer setCornerRadius:4.0f];
        [thirdAmountButton.layer setMasksToBounds:YES];
        [fourthAmountButton.layer setCornerRadius:4.0f];
        [fourthAmountButton.layer setMasksToBounds:YES];
        [OneDayReloadButton.layer setCornerRadius:4.0f];
        [OneDayReloadButton.layer setMasksToBounds:YES];
        
        arrayReloadAvailableAmounts = [[NSMutableArray alloc] initWithCapacity:0];
        [arrayReloadAvailableAmounts addObject:@"100.00"];
        [arrayReloadAvailableAmounts addObject:@"150.00"];
        [arrayReloadAvailableAmounts addObject:@"200.00"];
        [arrayReloadAvailableAmounts addObject:@"250.00"];
        [arrayReloadAvailableAmounts addObject:@"300.00"];
        [arrayReloadAvailableAmounts addObject:@"350.00"];
        [arrayReloadAvailableAmounts addObject:@"400.00"];
        [arrayReloadAvailableAmounts addObject:@"450.00"];
        [arrayReloadAvailableAmounts addObject:@"500.00"];
        [arrayReloadAvailableAmounts addObject:@"550.00"];
        [arrayReloadAvailableAmounts addObject:@"600.00"];
        [arrayReloadAvailableAmounts addObject:@"650.00"];
        [arrayReloadAvailableAmounts addObject:@"700.00"];
        [arrayReloadAvailableAmounts addObject:@"750.00"];
        [arrayReloadAvailableAmounts addObject:@"800.00"];
        [arrayReloadAvailableAmounts addObject:@"850.00"];
        [arrayReloadAvailableAmounts addObject:@"900.00"];
        [arrayReloadAvailableAmounts addObject:@"950.00"];
        [arrayReloadAvailableAmounts addObject:@"1000.00"];
        
        arrayMinimumBalanceAmounts = [[NSMutableArray alloc] initWithCapacity:0];
        [arrayMinimumBalanceAmounts addObject:@"100.00"];
        [arrayMinimumBalanceAmounts addObject:@"200.00"];
        [arrayMinimumBalanceAmounts addObject:@"300.00"];
        [arrayMinimumBalanceAmounts addObject:@"400.00"];
        [arrayMinimumBalanceAmounts addObject:@"600.00"];
        [arrayMinimumBalanceAmounts addObject:@"700.00"];
        [arrayMinimumBalanceAmounts addObject:@"800.00"];
        [arrayMinimumBalanceAmounts addObject:@"900.00"];
        [arrayMinimumBalanceAmounts addObject:@"1000.00"];
        
        arrayReloadAvailableDays = [[NSMutableArray alloc] initWithCapacity:0];
        for (int i= 0; i<29; i++) {
            if (i==28) {
                [arrayReloadAvailableDays addObject:@"último día del mes"];
            }else{
                [arrayReloadAvailableDays addObject:[NSString stringWithFormat:@"%i",i+1]];
            }
        }
        
        [firstAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [secondAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [thirdAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [fourthAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        [firstButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [secondButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [thirdButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [fourthButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

        [OneDayReloadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        UserAmountPickerView.frame = CGRectMake(0.0f,screenRect.size.height - 300.0, 320.0f, 162.0f);
        UserReloadDayPickerView.frame = CGRectMake(0.0f,screenRect.size.height - 300.0, 320.0f, 162.0f);
        UserMinimumBalancePickerView.frame = CGRectMake(0.0f,screenRect.size.height - 300.0, 320.0f, 162.0f);
        UserAmountToolbar.frame = CGRectMake(0.0f, screenRect.size.height - 344.0f, 320.0f, 44.0f);
        [UserAmountPickerView reloadAllComponents];
        UserAmountPickerView.tag = 0;
        UserReloadDayPickerView.tag = 1;
        UserMinimumBalancePickerView.tag = 2;
        [UserReloadDayPickerView reloadAllComponents];
        [UserMinimumBalancePickerView reloadAllComponents];
        AutoReloadButton.frame = CGRectMake(10, 325, 300, 37);
        [UserAmountPickerView setHidden:YES];
        [UserAmountToolbar setHidden:YES];
        [UserReloadDayPickerView setHidden:YES];
        [UserMinimumBalancePickerView setHidden:YES];
        AutoreloadSwitch.enabled = NO;
        //AutoReloadButton.enabled = NO;
        
        PaymentTypeTableView.delegate = self;
        [sView addSubview:viewReloadCard];
        [sView setContentSize:viewReloadCard.frame.size];
        [self RemoveAutoReloadSettingsFromView];
        
        heightPoint = 0;
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PAYMENT_TYPE"] == nil) {
            //esta funcionando
            //PaymentTypeTableView.userInteractionEnabled = NO;
            //AutoReloadButton.enabled = NO;
            [AutoreloadSwitch setOn:NO];
            [self ShowPaymentTypeSettings];
            heightPoint = 185;
            PaymentTypeLabel.frame = CGRectMake(16.0f, heightPoint, 87.0f,21.0f);
            PaymentTypeInstructionLabel.frame = CGRectMake(102.0f, heightPoint, 133.0f,21.0f);
            PaymentTypeTableView.frame = CGRectMake(20.0f, heightPoint + 29, 280.0f, 40.0f);
            //autoreload section
            AutoreloadTitleLabel.frame = CGRectMake(20.0f, heightPoint + 73, 280.0f, 40.0f);
            AutoreloadSwitch.frame = CGRectMake(251.0f, heightPoint + 79, 280.0f, 40.0f);
            AutoreloadInstructionLabel.frame = CGRectMake(20.0f, heightPoint + 112, 280.0f, 21.0f);
            firstButtonBelowAmount.frame = CGRectMake(20.0f, heightPoint + 138, 60.0f, 40.0f);
            secondButtonBelowAmount.frame = CGRectMake(93.0f, heightPoint + 138, 60.0f, 40.0f);
            thirdButtonBelowAmount.frame = CGRectMake(167.0f, heightPoint + 138, 60.0f, 40.0f);
            fourthButtonBelowAmount.frame = CGRectMake(240.0f, heightPoint + 138, 60.0f, 40.0f);
            OneDayReloadButton.frame = CGRectMake(20.0f, heightPoint + 186, 280.0f, 40.0f);
            AutoReloadButton.frame = CGRectMake(10.0f, heightPoint + 130, 300.0f, 37.0f);
            
        }else{
            //[self HidePaymentTypeSection];
            [self ShowPaymentTypeSettings];
            //no tiene medio de pago
            heightPoint = 185;
             //payment section
             PaymentTypeLabel.frame = CGRectMake(16.0f, heightPoint, 87.0f,21.0f);
             PaymentTypeInstructionLabel.frame = CGRectMake(102.0f, heightPoint, 133.0f,21.0f);
             PaymentTypeTableView.frame = CGRectMake(20.0f, heightPoint + 29, 280.0f, 40.0f);
            [AutoreloadSwitch setOn:NO];
            //autoreload section
            AutoreloadTitleLabel.frame = CGRectMake(20.0f, heightPoint -6.0f, 280.0f, 40.0f);
            AutoreloadSwitch.frame = CGRectMake(251.0f, heightPoint, 280.0f, 40.0f);
            AutoreloadInstructionLabel.frame = CGRectMake(20.0f, heightPoint + 28, 280.0f, 21.0f);
            firstButtonBelowAmount.frame = CGRectMake(20.0f, heightPoint + 54, 60.0f, 40.0f);
            secondButtonBelowAmount.frame = CGRectMake(93.0f, heightPoint + 54, 60.0f, 40.0f);
            thirdButtonBelowAmount.frame = CGRectMake(167.0f, heightPoint + 54, 60.0f, 40.0f);
            fourthButtonBelowAmount.frame = CGRectMake(240.0f, heightPoint + 54, 60.0f, 40.0f);
            OneDayReloadButton.frame = CGRectMake(20.0f, heightPoint + 102, 280.0f, 40.0f);
            AutoReloadButton.frame = CGRectMake(10.0f, heightPoint + 70, 300.0f, 37.0f);
        }
        
        
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.dimBackground = YES;
        
        receivedData = [[NSMutableData alloc] init];
        
        NSString *validateCardSbx = [[NSUserDefaults standardUserDefaults] objectForKey:@"URL_WS_SMART"];
        validateCardSbx = [validateCardSbx stringByAppendingString:@"ConsultaMediosPago"];
        
        NSLog(@"VALidate:%@",validateCardSbx);
        NSArray *objetos = [NSArray new];
        NSArray *llaves = [NSArray new];
        /*
         objetos = [NSArray arrayWithObjects:@"STARBUCKS",@"10263",@"12345678",@"Movil",nil];
         llaves = [NSArray arrayWithObjects:@"Cadena",@"IDUsuario",@"Token",@"TipoDispositivo",nil];
         */
        
        objetos = [NSArray arrayWithObjects:@"STARBUCKS",@"Movil",nil];
        llaves = [NSArray arrayWithObjects:@"Cadena",@"TipoDispositivo",nil];
        
        NSDictionary *reloadTransactionDictionary = [NSDictionary dictionaryWithObjects:objetos forKeys:llaves];
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:reloadTransactionDictionary options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        
        if(!jsonSerializationError) {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Serialized JSON: %@", serJSON);
        } else {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:validateCardSbx] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"12345678" forHTTPHeaderField:@"token"];
        [request setValue:@"10263" forHTTPHeaderField:@"idUsuario"];
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: jsonData];
        NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        [connection start];

    }
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Sign/OAddCreditCardViewController");
        
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    
}

- (IBAction)SetButtonAmountTapped:(id)sender
{
    AutoreloadSwitch.enabled = YES;
    //AutoReloadButton.enabled = YES;
    if (sender == firstAmountButton) {
        firstAmountButton.selected = YES;
        secondAmountButton.selected = NO;
        thirdAmountButton.selected = NO;
        fourthAmountButton.selected = NO;
        [fourthAmountButton setTitle:@"Otro" forState:UIControlStateNormal];
        [firstAmountButton setBackgroundColor:[UIColor darkGrayColor]];
        [secondAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        didChangeReloadAmount = YES;
        PaymentTypeTableView.userInteractionEnabled = YES;
        [PaymentTypeTableView reloadData];
    }else if (sender == secondAmountButton)
    {
        firstAmountButton.selected = NO;
        secondAmountButton.selected = YES;
        thirdAmountButton.selected = NO;
        fourthAmountButton.selected = NO;
        [fourthAmountButton setTitle:@"Otro" forState:UIControlStateNormal];
        [firstAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondAmountButton setBackgroundColor:[UIColor darkGrayColor]];
        [thirdAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        didChangeReloadAmount = YES;
        PaymentTypeTableView.userInteractionEnabled = YES;
        [PaymentTypeTableView reloadData];
    }else if (sender == thirdAmountButton )
    {
        firstAmountButton.selected = NO;
        secondAmountButton.selected = NO;
        thirdAmountButton.selected = YES;
        fourthAmountButton.selected = NO;
        [fourthAmountButton setTitle:@"Otro" forState:UIControlStateNormal];
        [firstAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdAmountButton setBackgroundColor:[UIColor darkGrayColor]];
        [fourthAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        didChangeReloadAmount = YES;
        PaymentTypeTableView.userInteractionEnabled = YES;
        [PaymentTypeTableView reloadData];
    }else if( sender == fourthAmountButton)
    {
        firstAmountButton.selected = NO;
        secondAmountButton.selected = NO;
        thirdAmountButton.selected = NO;
        fourthAmountButton.selected = YES;
        [firstAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthAmountButton setBackgroundColor:[UIColor darkGrayColor]];
        didChangeReloadAmount = YES;
        PaymentTypeTableView.userInteractionEnabled = YES;
        [PaymentTypeTableView reloadData];
    }else if( sender == firstButtonBelowAmount)
    {
        firstButtonBelowAmount.selected = YES;
        secondButtonBelowAmount.selected = NO;
        thirdButtonBelowAmount.selected = NO;
        fourthButtonBelowAmount.selected = NO;
        OneDayReloadButton.selected = NO;
        [fourthButtonBelowAmount setTitle:@"Otro" forState:UIControlStateNormal];
        [firstButtonBelowAmount setBackgroundColor:[UIColor darkGrayColor]];
        [secondButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [OneDayReloadButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    }else if( sender == secondButtonBelowAmount)
    {
        firstButtonBelowAmount.selected = NO;
        secondButtonBelowAmount.selected = YES;
        thirdButtonBelowAmount.selected = NO;
        fourthButtonBelowAmount.selected = NO;
        [fourthButtonBelowAmount setTitle:@"Otro" forState:UIControlStateNormal];
        OneDayReloadButton.selected = NO;
        [firstButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondButtonBelowAmount setBackgroundColor:[UIColor darkGrayColor]];
        [thirdButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [OneDayReloadButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    }else if( sender == thirdButtonBelowAmount)
    {
        firstButtonBelowAmount.selected = NO;
        secondButtonBelowAmount.selected = NO;
        thirdButtonBelowAmount.selected = YES;
        fourthButtonBelowAmount.selected = NO;
        [fourthButtonBelowAmount setTitle:@"Otro" forState:UIControlStateNormal];
        OneDayReloadButton.selected = NO;
        [firstButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdButtonBelowAmount setBackgroundColor:[UIColor darkGrayColor]];
        [fourthButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [OneDayReloadButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    }else if( sender == fourthButtonBelowAmount)
    {
        firstButtonBelowAmount.selected = NO;
        secondButtonBelowAmount.selected = NO;
        thirdButtonBelowAmount.selected = NO;
        fourthButtonBelowAmount.selected = YES;
        OneDayReloadButton.selected = NO;
        [firstButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthButtonBelowAmount setBackgroundColor:[UIColor darkGrayColor]];
        [OneDayReloadButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    }
}

- (IBAction)ReloadOneDayButtonPressed:(id)sender {
    firstButtonBelowAmount.selected = NO;
    secondButtonBelowAmount.selected = NO;
    thirdButtonBelowAmount.selected = NO;
    fourthButtonBelowAmount.selected = NO;
    OneDayReloadButton.selected = YES;
    [UserReloadDayPickerView setHidden:NO];
    [UserAmountToolbar setHidden:NO];
    [firstButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    [secondButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    [thirdAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    [fourthButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
    [OneDayReloadButton setBackgroundColor:[UIColor darkGrayColor]];
}


- (IBAction)SetUserAmountReload:(id)sender
{
    [UserAmountToolbar setHidden:NO];
    if (sender == fourthAmountButton) {
        senderButton = 0;
        [UserAmountPickerView setHidden:NO];
    }else if(sender ==fourthButtonBelowAmount){
        senderButton = 1;
        [UserMinimumBalancePickerView setHidden:NO];
    }
}

#pragma mark ToolBar Cancel Button Pressed

- (IBAction)CancelAmountToolbarPressed:(id)sender {
    
    if (![UserAmountPickerView isHidden]) {
        
        if ([firstAmountButton isSelected]) {
           [firstAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([secondAmountButton isSelected]) {
            [secondAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([thirdAmountButton isSelected]) {
            [thirdAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([thirdAmountButton isSelected]) {
            [thirdAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([fourthAmountButton isSelected]) {
            [fourthAmountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }
        [UserAmountPickerView setHidden:YES];
        [UserAmountToolbar setHidden:YES];
    }else if(![UserReloadDayPickerView isHidden])  {
        OneDayReloadButton.selected = YES;
        [UserReloadDayPickerView setHidden:YES];
        [UserAmountToolbar setHidden:YES];
    }else if(![UserMinimumBalancePickerView isHidden])  {
        if ([firstButtonBelowAmount isSelected]) {
            [firstButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([secondButtonBelowAmount isSelected]) {
            [secondButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([thirdButtonBelowAmount isSelected]) {
            [thirdButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([thirdButtonBelowAmount isSelected]) {
            [thirdButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }else if ([fourthButtonBelowAmount isSelected]) {
            [fourthButtonBelowAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        }

        [UserMinimumBalancePickerView setHidden:YES];
        [UserAmountToolbar setHidden:YES];
    }
}

#pragma mark ToolBar Done Button Pressed

- (IBAction)DoneAmountToolbarPressed:(id)sender {
    
    if (![UserAmountPickerView isHidden]) {
    
        NSInteger row = [UserAmountPickerView selectedRowInComponent:0];
        NSString *reloadAmount = [arrayReloadAvailableAmounts objectAtIndex:row];
        NSArray *arrayReloadAmount = [reloadAmount componentsSeparatedByString:@"."];
        reloadAmount = (NSString *)[arrayReloadAmount firstObject];
        [UserAmountPickerView setHidden:YES];
        [UserAmountToolbar setHidden:YES];
        if (senderButton ==0) {
            [fourthAmountButton setTitle:[NSString stringWithFormat:@"$ %@",reloadAmount] forState:UIControlStateSelected];
            firstAmountButton.selected = NO;
            secondAmountButton.selected = NO;
            thirdAmountButton.selected = NO;
            fourthAmountButton.selected = YES;
            [firstAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
            [secondAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
            [thirdAmountButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
            [fourthAmountButton setBackgroundColor:[UIColor darkGrayColor]];
        }
    }else if(![UserReloadDayPickerView isHidden])  {
        NSInteger row = [UserReloadDayPickerView selectedRowInComponent:0];
        NSString *reloadDay = [arrayReloadAvailableDays objectAtIndex:row];
        
        OneDayReloadButton.selected = YES;
        NSLog(@"row:%i",row);
        if (row == [arrayReloadAvailableDays count] -1) {
                [OneDayReloadButton setTitle:[NSString stringWithFormat:@"Recargar el %@",reloadDay] forState:UIControlStateSelected];
             }else{
                 [OneDayReloadButton setTitle:[NSString stringWithFormat:@"Recargar el día %@ de cada mes",reloadDay] forState:UIControlStateSelected];
             }
        [UserReloadDayPickerView setHidden:YES];
        [UserAmountToolbar setHidden:YES];

        [firstButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [secondButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [thirdButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [fourthButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        [OneDayReloadButton setBackgroundColor:[UIColor darkGrayColor]];
        
    }else if (![UserMinimumBalancePickerView isHidden]){
    
        if (senderButton == 1)
        {
            NSInteger row = [UserAmountPickerView selectedRowInComponent:0];
            NSString *reloadOnMinimumAmount = [arrayReloadAvailableAmounts objectAtIndex:row];
            NSArray *arrayReloadOnMinimumAmount = [reloadOnMinimumAmount componentsSeparatedByString:@"."];
            reloadOnMinimumAmount = (NSString *)[arrayReloadOnMinimumAmount firstObject];
            [UserMinimumBalancePickerView setHidden:YES];
            [UserAmountToolbar setHidden:YES];
            [fourthButtonBelowAmount setTitle:[NSString stringWithFormat:@"$ %@",reloadOnMinimumAmount] forState:UIControlStateSelected];
            firstButtonBelowAmount.selected = NO;
            secondButtonBelowAmount.selected = NO;
            thirdButtonBelowAmount.selected = NO;
            fourthButtonBelowAmount.selected = YES;
            OneDayReloadButton.selected = NO;
            
            [firstButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
            [secondButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
            [thirdButtonBelowAmount setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
            [fourthButtonBelowAmount setBackgroundColor:[UIColor darkGrayColor]];
            [OneDayReloadButton setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0]];
        }
    }
}


#pragma mark DismissViewController

-(IBAction)Dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UISwitch On Value Changed

- (IBAction)ValueChanged:(id)sender
{
    if (AutoreloadSwitch.on == YES) {
        [UIView animateWithDuration:0.2
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGRect frame ;
                             frame = AutoReloadButton.frame;
                             if (![PaymentTypeTableView isHidden]) {
                                 frame.origin.y =415;
                             }else{
                                frame.origin.y =377;
                             }
                             AutoReloadButton.frame = frame;
                             AutoReloadButton.alpha = 1.0;

                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             [self ShowAutoReloadSettings];
                         }];
        
        //[[NSUserDefaults standardUserDefaults] setObject:@"sí" forKey:@"PAYMENT_TIPE"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
        //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PAYMENT_TIPE"];
        
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self RemoveAutoReloadSettingsFromView];
                             CGRect frame ;
                             frame = AutoReloadButton.frame;
                             if (![PaymentTypeTableView isHidden]) {
                                 frame.origin.y = 325;
                             }else{
                                 frame.origin.y =270;
                             }
                             
                             AutoReloadButton.frame = frame;
                             AutoReloadButton.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PAYMENT_TIPE"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Show Payment type section

- (void)ShowPaymentTypeSettings
{
    [PaymentTypeButton setHidden:YES];
    [PaymentTypeLabel setHidden:NO];
    [PaymentTypeInstructionLabel setHidden:NO];
    [PaymentTypeTableView setHidden:NO];
}

#pragma mark Hide Payment type section
- (void) HidePaymentTypeSection
{
    [PaymentTypeButton setHidden:NO];
    [PaymentTypeLabel setHidden:YES];
    [PaymentTypeInstructionLabel setHidden:YES];
    [PaymentTypeTableView setHidden:YES];
}

- (void)ShowFirstTypeSettings
{
    [PaymentTypeButton setHidden:NO];
    [PaymentTypeLabel setHidden:NO];
    [PaymentTypeInstructionLabel setHidden:YES];
    [PaymentTypeTableView setHidden:YES];
}

#pragma mark Hide Autoreload Settings

- (void)RemoveAutoReloadSettingsFromView
{
    [AutoreloadInstructionLabel setHidden:YES];
    [firstButtonBelowAmount setHidden:YES];
    [secondButtonBelowAmount setHidden:YES];
    [thirdButtonBelowAmount setHidden:YES];
    [fourthButtonBelowAmount setHidden:YES];
    [OneDayReloadButton setHidden:YES];
}

#pragma mark Show Autoreload Settings

- (void)ShowAutoReloadSettings
{
    [AutoreloadInstructionLabel setHidden:NO];
    [firstButtonBelowAmount setHidden:NO];
    [secondButtonBelowAmount setHidden:NO];
    [thirdButtonBelowAmount setHidden:NO];
    [fourthButtonBelowAmount setHidden:NO];
    [OneDayReloadButton setHidden:NO];
}

#pragma mark - UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    
	@try{
        switch (_tableView.tag) {
            case 0:
            {
               
                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PAYMENT_TYPE"] == nil) {
                    
                    static NSString *CellIdentifier = @"OPaymentTypeCell";
                    
                    OPaymentTypeCell *cell = (OPaymentTypeCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                    }
                    cell.lblTitulo.text = @"Selecciona un medio de pago";
                    
                    if (didChangeReloadAmount == NO) {
                        cell.alpha = 0.7;
                    }else{
                        cell.alpha = 1.0;
                        [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
                    }
                    return cell;
                
                }else{
                    static NSString *CellIdentifier = @"OCreditCardCell";
                    
                    OCreditCardCell *cell = (OCreditCardCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
                    }
                    
                    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
                    return cell;
                }
            }
                break;
            default:
                break;
        }
        
        return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OAutoReloadViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
        [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
        
        if (didChangeReloadAmount == NO) {
            UIAlertView *alert =[[UIAlertView alloc]
                                 initWithTitle:@"Aviso"
                                 message:@"Para dar de alta un medio de pago debes seleccionar un monto para la recarga."
                                 delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
            [alert show];
            
            return;
        }

        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PAYMENT_TYPE"] == nil) {
            NSString *amountOnReload;
            if ([firstAmountButton isSelected]) {
                amountOnReload = firstAmountButton.titleLabel.text;
            }else if ([secondAmountButton isSelected]) {
                amountOnReload = secondAmountButton.titleLabel.text;
            }else if ([thirdAmountButton isSelected]) {
                amountOnReload = thirdAmountButton.titleLabel.text;
            }else if ([fourthAmountButton isSelected]) {
                amountOnReload = fourthAmountButton.titleLabel.text;
            }
            NSLog(@"amountOnReload:%@",amountOnReload);
            amountOnReload = [self getCurrencyMode:amountOnReload];
            OAddCreditCardViewController *detailViewController = [[OAddCreditCardViewController alloc] initWithNibName:@"OAddCreditCardViewController" bundle:nil];
            ;
            detailViewController.cardNumber = cardNumber;
            detailViewController.amountOnReload = amountOnReload;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }else{
            OSelectPaymentViewController *detailViewController = [[OSelectPaymentViewController alloc] initWithNibName:@"OSelectPaymentViewController" bundle:nil];
            ;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
	
}

#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (thePickerView.tag == 0) {
        if (arrayReloadAvailableAmounts!=nil) {
            return [arrayReloadAvailableAmounts count];
        }
    }else if(thePickerView.tag == 1){
        if (arrayReloadAvailableDays!=nil) {
            return [arrayReloadAvailableDays count];
        }
    }else if (thePickerView.tag == 2){
        if (arrayMinimumBalanceAmounts!=nil) {
            return [arrayMinimumBalanceAmounts count];
        }
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (thePickerView.tag == 0) {
        if (arrayReloadAvailableAmounts!=nil) {
            return [arrayReloadAvailableAmounts objectAtIndex:row];
        }
    }else if(thePickerView.tag == 1){
        if (arrayReloadAvailableDays!=nil) {
            return [arrayReloadAvailableDays objectAtIndex:row];
        }
    }else if (thePickerView.tag == 2) {
        if (arrayMinimumBalanceAmounts!=nil) {
            return [arrayMinimumBalanceAmounts objectAtIndex:row];
        }
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
}

- (IBAction)ReloadCardButtonPressed:(id)sender {

    NSString *amountOnReload;
    
    if ([firstAmountButton isSelected]) {
        amountOnReload = firstAmountButton.titleLabel.text;
        //[[NSUserDefaults standardUserDefaults] setObject:firstAmountButton.titleLabel.text forKey:@"RELOAD_AMOUNT"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
    }else if ([secondAmountButton isSelected]) {
        amountOnReload = secondAmountButton.titleLabel.text;
        //[[NSUserDefaults standardUserDefaults] setObject:secondAmountButton.titleLabel.text forKey:@"RELOAD_AMOUNT"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
    }else if ([thirdAmountButton isSelected]) {
        amountOnReload = thirdAmountButton.titleLabel.text;
        //[[NSUserDefaults standardUserDefaults] setObject:thirdAmountButton.titleLabel.text forKey:@"RELOAD_AMOUNT"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
    }else if ([fourthAmountButton isSelected]) {
        amountOnReload = fourthAmountButton.titleLabel.text;
        //[[NSUserDefaults standardUserDefaults] setObject:fourthAmountButton.titleLabel.text forKey:@"RELOAD_AMOUNT"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSLog(@"amountOnReload:%@",amountOnReload);
    amountOnReload = [self getCurrencyMode:amountOnReload];
    
    if (AutoreloadSwitch.on == YES) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"AUTOMATIC_RELOAD"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([firstButtonBelowAmount isSelected]) {
            [[NSUserDefaults standardUserDefaults] setObject:firstButtonBelowAmount.titleLabel.text forKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else if ([secondButtonBelowAmount isSelected]) {
            [[NSUserDefaults standardUserDefaults] setObject:secondButtonBelowAmount.titleLabel.text forKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else if ([thirdButtonBelowAmount isSelected]) {
            [[NSUserDefaults standardUserDefaults] setObject:thirdButtonBelowAmount.titleLabel.text forKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else if ([fourthButtonBelowAmount isSelected]) {
            [[NSUserDefaults standardUserDefaults] setObject:fourthButtonBelowAmount.titleLabel.text forKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else if ([OneDayReloadButton isSelected]) {
            [[NSUserDefaults standardUserDefaults] setObject:OneDayReloadButton.titleLabel.text forKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AUTOMATIC_RELOAD"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RELOAD_AMOUNT_ON_BALANCE_BELOW"];
    }
    
    if ([PaymentTypeTableView isHidden]) {
        
        OAddCreditCardViewController *addCrediCardController = [[OAddCreditCardViewController alloc] initWithNibName:@"OAddCreditCardViewController" bundle:nil];
        ;
        [self.navigationController pushViewController:addCrediCardController animated:YES];
        
    }else{
        ReloadSummaryViewController *detailViewController = [[ReloadSummaryViewController alloc] initWithNibName:@"ReloadSummaryViewController" bundle:nil];
    ;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

- (NSString *)getCurrencyMode:(NSString *)value
{
    double valueDouble = [value doubleValue];
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setCurrencySymbol:@""];
    NSString *amountValueWithFormat =[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:valueDouble]];
    return amountValueWithFormat;
}


#pragma mark NSURLConnection

/*
 - (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
 {
 SecTrustRef trust = [protectionSpace serverTrust];
 SecCertificateRef certificate = SecTrustGetCertificateAtIndex(trust, 0);
 NSData* serverCertificateData = (__bridge  NSData*)SecCertificateCopyData(certificate);
 NSString *serverCertificateDataHash = [[serverCertificateData base64EncodedString] SHA256];
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 BOOL areCertificatesEqualPT = ([serverCertificateDataHash isEqualToString:[defaults objectForKey:@"CERT_PT"]]);
 
 if (!areCertificatesEqualPT){
 [connection cancel];
 [HUD hide:YES];
 return NO;
 }
 return YES;
 }
 */

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Error"];
    [dialog setMessage:[error localizedDescription]];
    [dialog addButtonWithTitle:@"OK"];
    [dialog show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError * error;
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error];
    NSLog(@"results:%@",results);
    [HUD hide:YES];
    
    int idRespuesta = [[results objectForKey:@"codigo"] intValue];
    NSString *mensaje = (NSString *)[results objectForKey:@"descripcion"];
    
    if (idRespuesta != 0) {
        UIAlertView *dialogo = [[UIAlertView alloc] init];
        [dialogo setDelegate:self];
        [dialogo setTitle:@"Aviso"];
        [dialogo setMessage:mensaje];
        [dialogo addButtonWithTitle:@"OK"];
        [dialogo show];
    }else{
        
    }
}





@end
