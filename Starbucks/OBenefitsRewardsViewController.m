//
//  OBenefitsRewardsViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OBenefitsRewardsViewController.h"
#define RGBToFloat(f) (f/255.0)
@interface OBenefitsRewardsViewController ()

@end

@implementation OBenefitsRewardsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OInitRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    @try{
    [super viewDidLoad];
		[self.navigationItem setTitleView:mTitleView];
	UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithTitle:@"Finalizar"  style:UIBarButtonItemStyleBordered target:self action:@selector(hideModal:)];
	  [self.navigationItem setRightBarButtonItem:btnFilter];
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
	mImageView.image = [UIImage imageNamed:@"rewards_benefit_1.png"];
	mImageView.frame = CGRectMake(0, 0, 320, 349);
	[mScrollView setContentSize:CGSizeMake(320, 349)];
	[mScrollView addSubview:mImageView];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OInitRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction) hideModal:(id) sender
{
	[self dismissModalViewControllerAnimated:YES];
	
}

-(IBAction)segmentedControlValueChange:(id)sender
{
    @try{
	switch (mSegmentedControl.selectedSegmentIndex) 
	{
		case 0:
		{
			mImageView.image = [UIImage imageNamed:@"rewards_benefit_1.png"];
			mImageView.frame = CGRectMake(0, 0, 320, 349);
			[mScrollView setContentSize:CGSizeMake(320, 349)];
			[mScrollView addSubview:mImageView];
		
		}
			break;
		case 1:
		{
			mImageView.image = [UIImage imageNamed:@"rewards_benefits_2.png"];
			mImageView.frame = CGRectMake(0, 0, 320, 700);
			[mScrollView setContentSize:CGSizeMake(320, 700)];
			[mScrollView addSubview:mImageView];
			
		}
			break;
		case 2:
		{
			mImageView.image = [UIImage imageNamed:@"rewards_benefits_3.png"];
			mImageView.frame = CGRectMake(0, 0, 320, 700);
			[mScrollView setContentSize:CGSizeMake(320, 700)];
			[mScrollView addSubview:mImageView];
			
		}
			break;
			
		default:
			break;
	}
	
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/OInitRewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}


@end
