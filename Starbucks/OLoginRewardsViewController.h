//
//  OLoginRewardsViewController.h
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DYXML/soap/DYSOAPManager.h>
#import "OBaseViewController.h"

@interface OLoginRewardsViewController : OBaseViewController<DYSOAPManagerDelegate>
{
	IBOutlet UITextField * mUSerTextField;
	IBOutlet UITextField * mPasswordTextField;


}


-(IBAction)enterButtonPressed:(id)sender;
-(IBAction)rememberPasswordPressed:(id)sender;

@end
