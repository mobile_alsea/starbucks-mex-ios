//
//  ReloadSummaryViewController.h
//  Starbucks
//
//  Created by Mobile on 10/24/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAppDelegate.h"
#import "OBaseViewController.h"
#import "DYHorizontalStretchButton.h"
#import "MBProgressHUD.h"

@interface ReloadSummaryViewController : OBaseViewController <UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate,NSURLConnectionDelegate>
{
    OAppDelegate * mAppDelegate;
    NSManagedObjectContext *mAddingContext;
    NSString *reloadAmount;
    NSString *autoreloadOnBalance;
}
@property (strong, nonatomic) IBOutlet UIScrollView *sView;
@property (strong, nonatomic) IBOutlet UIView *viewReloadSummary;
@property (strong, nonatomic) IBOutlet UITableView *ReloadSummaryTableView;
@property (strong, nonatomic) NSString *reloadAmount;
@property (strong, nonatomic) NSString *autoreloadOnBalance;
@property (strong, nonatomic) IBOutlet DYHorizontalStretchButton *CancelReloadButton;
@property (strong, nonatomic) UIAlertView *confirmReloadAmount;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSDictionary *reloadTransactionData;
@property (strong, nonatomic) NSString *cardBalance;
@property (strong, nonatomic) NSString *lastUpdate;
@property (strong, nonatomic) NSString *cardType;
@property (strong, nonatomic) NSString *cardNumber;
@property (strong, nonatomic) NSString *amountToReload;
@property (strong, nonatomic) NSString *amountToReloadOnMinimumBalance;


- (IBAction)ReloadConfirmButtonPressed:(id)sender;
- (IBAction)ReloadCancelButtonPressed:(id)sender;

@end
