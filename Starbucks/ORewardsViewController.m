//
//  ORewardsViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 26/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "ORewardsViewController.h"
#import "ORewardsDetailViewController.h"
#import <DYCore/files/DYFileNameManager.h>

#define RGBToFloat(f) (f/255.0)
@interface ORewardsViewController ()

@end

@implementation ORewardsViewController
@synthesize numberOfStars = mNumberOfStars;
@synthesize numberOfStarsGold = mNumberOfStarsGold;
@synthesize dateMember = mDateMember;
 int x=22,y=22;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad

{
    @try{
    [super viewDidLoad];
	[self.navigationItem setTitle:NSLocalizedString(@"Recompensas", nil)];
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:RGBToFloat(62.0) green:RGBToFloat(142.0) blue:RGBToFloat(22.0) alpha:1.0]];
	UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithTitle:@"Detalles"  style:UIBarButtonItemStyleBordered target:self action:@selector(goToDetail:)];
	    [self.navigationItem setRightBarButtonItem:btnFilter];
	
        
        
        
        
        
        
        
        // izquierdo
	UIBarButtonItem *btnExit = [[UIBarButtonItem alloc] initWithTitle:@"Cerrar sesión"  style:UIBarButtonItemStyleBordered target:self action:@selector(exitPressed:)];
	[self.navigationItem setLeftBarButtonItem:btnExit];
    
    
    if ([mNumberOfStars intValue] <5){ mNeedLabel.text = [NSString stringWithFormat:@"Necesitas %d Stars más para el Green Level",5 - [mNumberOfStars intValue]], mNumberOfStarsLabel.text = mNumberOfStars;
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome.png"]]]; 
        
  
    } else if (([mNumberOfStars intValue] >=5) && ([mNumberOfStars intValue] <30)) { mNeedLabel.text = [NSString stringWithFormat:@"Necesitas %d Stars más para el Gold Level",30 - [mNumberOfStars intValue]], mNumberOfStarsLabel.text = mNumberOfStars;
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"green.png"]]];
    }
    
    else if ([mNumberOfStars intValue] >=30)
            {
            int prueba =([mNumberOfStars intValue] - 30) % 15;
            int prueba2 = 15 - prueba;
            if (prueba == 0 )
                {
                    mNeedLabel.text = [NSString stringWithFormat:@"¡FELICIDADES! Ganaste 1 bebida 15 Stars"];
                }else{
                    mNeedLabel.text = [NSString stringWithFormat:@"Necesitas %d stars más para Bebida Gratis", prueba2], mNumberOfStarsLabel.text = [NSString stringWithFormat:@"%d", prueba];
                }
            [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gold.png"]]];
            }
    
        
    [self doAnimation];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    // Do any additional setup after loading the view from its nib.
}



-(void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	//[self doAnimation];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction) exitPressed:(id) sender
{
    NSString *pListPath = [DYFileNameManager pathForFile:@"RewardsUserInfo.plist" directory:NSApplicationSupportDirectory mode:@"w" copyFromBundle:YES];
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pListPath];
    
    if(dictionary!=nil)
    {
        [dictionary setObject:@"" forKey:@"UserName"];
        [dictionary setObject:@"" forKey:@"Password"];
        [dictionary setObject:@"" forKey:@"Stars"];
        [dictionary setObject:@"" forKey:@"StarsGold"];
        [dictionary setObject:@"" forKey:@"DateMember"];
        [dictionary writeToFile:pListPath atomically:YES];
        
    }

    
	[self dismissModalViewControllerAnimated:YES];
	
}


-(IBAction) goToDetail:(id) sender
{
    @try{
	ORewardsDetailViewController *detailViewController = [[ORewardsDetailViewController alloc] initWithNibName:@"ORewardsDetailViewController" bundle:nil];
	detailViewController.numberOfStars = mNumberOfStars;
	detailViewController.dateMember = mDateMember;
	
	if([mNumberOfStars intValue]<5)detailViewController.actualLevel = @"Welcome Level";
	else
	{
		if([mNumberOfStars intValue]<30)detailViewController.actualLevel = @"Green Level";
        
        
		else detailViewController.actualLevel = @"Gold Level";
		
	} 

	[self.navigationController pushViewController:detailViewController animated:YES];
			
				
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/ORewardsViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

//MBT 17-12-12
-(float)random_: (int)rand
{
    
    
    
    switch (rand) {
        case 0:
            return M_PI;
            break;
        case 1:
             return (-1.0 / 180.0 * 3.14);
            break;
            
        case 2:
               return (1.0 / 180.0 * 3.14);
            break;
            
        case 3:
            return (1.0 / 180.0 * 3.14);
            break;
            
        default:
            break;
    }
    
    return 0.0;
}

//MBT 17-12-12
-(float)randstar: (int)rand
{
    
    
    
    switch (rand) {
        case 0:
            return 0;
            break;
        case 1:
            return  .3;
            break;
            
        case 2:
            return  -.3;
            break;
            
        
      
    }
    return 0.0;
    
    
}



-(void)StarsAnimation: (int) TotalStars
{
    
    float factor = 0.0;
	float row = 0;
   // TotalStars=29;
    for(int i=0;i<TotalStars;i++)
    {
        //MBT 17-12-12
            
         
                
                float initx,inity,endx,endy;
                
                UIImageView * mImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goldstar.png"]];
                
                if(TotalStars==29 && i>=24)
                {
                    switch (i) {
                        case 24:
                            
                            initx = 180.0;
                            inity = -30.0;
                            endx = 110.0-factor*1.5+[self random_ : (arc4random() % 3)];
                            endy = 210 - (24*factor)-5;
                            int r = (arc4random() % 4);
                            float w = [self random_ : r];
                            mImageView.transform = CGAffineTransformMakeRotation(w);
                            
                            break;
                        case 25:
                            initx = 180.0;
                            inity = -30.0;
                            endx = 129.0-factor*.3-[self random_ : (arc4random() % 3)];
                            endy = 210 - (24*factor)-2;
                            int r2 = (arc4random() % 4);
                            float w2 = [self random_ : r2];
                            mImageView.transform = CGAffineTransformMakeRotation(w2);
                            
                            break;
                            
                        case 26:
                            
                            initx = 180.0;
                            inity = -30.0;
                            endx = 151.0+factor*.3+[self random_ : (arc4random() % 3)];
                            endy = 210 - (24*factor)-9;
                            //   CGAffineTransformMakeRotation(16 * M_PI/180);
                            int r3 = (arc4random() % 4);
                            float w3 = [self random_ : r3];
                            mImageView.transform = CGAffineTransformMakeRotation(w3);
                            
                            
                            break;
                            
                        case 27:
                            initx = 180.0;
                            inity = -30.0;
                            endx = 172-3.0+factor*1.6+[self random_ : (arc4random() % 3)];
                            endy = 210 - (24*factor);
                            
                            
                            int r4 = (arc4random() % 4);
                            float w4 = [self random_ : r4];
                            mImageView.transform = CGAffineTransformMakeRotation(w4);
                            
                            
                            break;
                            
                        case 28:
                            
                            initx = 180.0;
                            inity = -30.0;
                            endx = 191-3.0+factor*1.7+[self random_ : (arc4random() % 3)];
                            endy = 210 - (24*factor)-6;
                            
                            
                            int r5 = (arc4random() % 4);
                            float w5 = [self random_ : r5];
                            mImageView.transform = CGAffineTransformMakeRotation(w5);
                            factor +=1.0;
                            
                            break;
                            
                            
                            
                    }
                }
                else{
                    
                    if(i%4==0)
                    {
                        initx = 180.0;
                        inity = -30.0;
                        endx = 114.0-factor*1.5+[self random_ : (arc4random() % 3)];
                        endy = 210 - (24*factor)-5;
                        int r = (arc4random() % 4);
                        float w = [self random_ : r];
                        mImageView.transform = CGAffineTransformMakeRotation(w);
                        
                    }
                    if(i%4==1)
                    {
                        initx = 180.0;
                        inity = -30.0;
                        endx = 139.0-factor*.25-[self random_ : (arc4random() % 3)];
                        endy = 210 - (24*factor)-2;
                        int r = (arc4random() % 4);
                        float w = [self random_ : r];
                        mImageView.transform = CGAffineTransformMakeRotation(w);
                        
                        
                    }
                    if(i%4==2)
                    {
                        initx = 180.0;
                        inity = -30.0;
                        endx = 161.0+factor*.3+[self random_ : (arc4random() % 3)];
                        endy = 210 - (24*factor)-9;
                        //   CGAffineTransformMakeRotation(16 * M_PI/180);
                        int r = (arc4random() % 4);
                        float w = [self random_ : r];
                        mImageView.transform = CGAffineTransformMakeRotation(w);
                        
                        
                        //    mImageView.transform = CGAffineTransformMakeTranslation(2.5, 2.0);
                    }
                    
                    
                    if(i%4==3)
                    {
                        initx = 180.0;
                        inity = -30.0;
                        endx = 183-3.0+factor*1.7+[self random_ : (arc4random() % 3)];
                        endy = 210 - (24*factor);
                        factor +=1.0;
                        
                        int r = (arc4random() % 4);
                        float w = [self random_ : r];
                        mImageView.transform = CGAffineTransformMakeRotation(w);
                        
                        
                    }
                    
                    
                }
                
                
                
                
                
                mImageView.frame = CGRectMake(initx, inity, 26, 26);
                // mImageView.transform = CGAffineTransformMakeRotation(M_PI);
                [self.view addSubview:mImageView];
                
                double delayInSeconds = (double)row;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [UIView animateWithDuration:1.8f
                                          delay:0.42f
                                        options:UIViewAnimationOptionCurveLinear
                                     animations:^{
                                         mImageView.frame = CGRectMake(endx, endy, x, y);
                                         //	mImageView.transform = CGAffineTransformIdentity;
                                     }
                                     completion:nil];
                });
                
                
                
                
                
                row = row+0.4;
            
            
        
        
    }
    
    
}

//MBT 17-12-12

-(void) doAnimation
{
   
   
    
    int TotalStars = [mNumberOfStars intValue];
    {
     if (TotalStars != 0)
       {
        int GoldLevel = ([mNumberOfStars intValue] - 30) % 15;
        {
            
            if (TotalStars <= 30)
        {
            
            [self StarsAnimation:TotalStars];
        }else
        {
              [self StarsAnimation:GoldLevel];
        }
            
        }
           
       }  
    } 
}  

@end
