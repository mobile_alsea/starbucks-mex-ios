//
//  main.m
//  Starbucks
//
//  Created by Adrián Caramés on 08/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OAppDelegate.h"

int main(int argc, char *argv[])
{
    @try {
    @autoreleasepool {
        int retVal = 0;
       NSString *classString = NSStringFromClass([OAppDelegate class]);
       retVal = UIApplicationMain(argc, argv, nil, classString);
        return retVal;
    }
    
    }
        @catch (NSException *ex) {
              
                NSLog(@"Exception - %@",[ex description]);
                
            }
    
}
