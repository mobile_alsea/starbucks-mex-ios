//
//  OFlavor.h
//  Starbucks
//
//  Created by Santi Belloso López on 05/09/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OCoffee;

@interface OFlavor : NSManagedObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) OCoffee *coffee;

@end
