//
//  OFoodViewController.m
//  Starbucks
//
//  Created by Santi Belloso López on 28/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OFoodViewController.h"
#import "OFood.h"
#import "OAppDelegate.h"
#import "OFood+Extras.h"
#import "OGenericCell.h"
#import "OFiltersViewController.h"
#import "OFoodResultViewController.h"

@interface OFoodViewController ()

@end

@implementation OFoodViewController
@synthesize fetchedResultsController = mFetchedResultsController;
@synthesize tableView = mTableView;
@synthesize filtersSelectedArray = mFiltersSelectedArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @try{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    @try{
        [super viewWillAppear:animated];
//        if(!isOS6())
//        {
//            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
//            UIViewController * prueba = [[UIViewController alloc] init];
//            [prueba.view setHidden:YES];
//            [self presentModalViewController:prueba animated:NO];
//            [self dismissModalViewControllerAnimated:NO];
//        }
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoofViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
	
}

- (void)viewDidLoad
{
    @try {
        
   

    
    [super viewDidLoad];
	[self.navigationItem setTitle:NSLocalizedString(@"Alimentos", nil)];
	
	//UIBarButtonItem * endButtonItem = [[UIBarButtonItem alloc] initWithCustomView:mFilterButton];
    //self.navigationItem.rightBarButtonItem =endButtonItem;
	
	//TODO
//	UIBarButtonItem *btnFilter = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ToolbarFilter.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(filterButtonPressed:)];
    
    
    //[self.navigationItem setRightBarButtonItem:btnFilter];
	
	
	if(mCategoriesArray!=nil)
	{
		mCategoriesArray = nil;
	}
	
	NSArray * arrayOfDictionaries =[[NSArray alloc] initWithArray:[OFood categoriesOfFoodInContext:[OAppDelegate managedObjectContext]]];
	
	for(int i=0;i<[arrayOfDictionaries count];i++)
	{
		NSDictionary * dict = [arrayOfDictionaries objectAtIndex:i];
		
		if(mCategoriesArray==nil) mCategoriesArray = [[NSMutableArray alloc] initWithObjects:[dict objectForKey:@"category"], nil];
		else [mCategoriesArray addObject:[dict objectForKey:@"category"]];
	
	}
	[mTableView reloadData];
	// Load Data
    [self executeFetch];

    // Do any additional setup after loading the view from its nib.
        
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) configureCell:(OGenericCell *)_cell atIndexPath:(NSIndexPath *)_indexPath
{

    _cell.titleLabel.text = [mCategoriesArray objectAtIndex:_indexPath.row];
	_cell.titleLabel.frame = CGRectMake(15, 11, 249, 21);
}

- (void) executeFetch
{
    @try{
    mFetchedResultsController = nil;
    
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error])
	{
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    
    [mTableView reloadData];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (NSFetchedResultsController *)fetchedResultsController
{
    @try{
	if (mFetchedResultsController != nil)
	{
		return mFetchedResultsController;
	}
	
    NSFetchRequest *fetchRequest;
	
    
	fetchRequest = [OFood foodInContextFetchRequest:[OAppDelegate managedObjectContext]];
	
	// Create the sort descriptors array.
	
	NSSortDescriptor *sellerDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sellerDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	// Create and initialize the fetch results controller.
	mFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[OAppDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
    mFetchedResultsController.delegate = self;
	

	return mFetchedResultsController;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

#pragma mark IBAction Methods

-(IBAction)filterButtonPressed:(id)sender
{
    @try{
	OFiltersViewController *controller = [[OFiltersViewController alloc] initWithNibName:@"OFiltersViewController" bundle:nil];
    
    SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:controller];
	controller.delegate = self;
	controller.filtersSelected = mFiltersSelectedArray;
    if ([navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"NavBar.png"];
        [navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
		navigationController.navigationBar.tintColor = [UIColor colorWithRed:60.0/255.0 green:140.0/255.0 blue:20.0/255.0 alpha:1.0];
    }

    [self presentModalViewController:navigationController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}



#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{	
	return 1;
}


- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)_section
{   
	if(mCategoriesArray == nil) return 0;
    return [mCategoriesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    static NSString *CellIdentifier = @"OGenericCell";
    
    OGenericCell *cell = (OGenericCell *) [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil] lastObject];
		
    }
    
    [cell setIndexPathPair:(_indexPath.row%2 == 0) ? YES : NO];
	
    [self configureCell:cell atIndexPath:_indexPath];
    
    return cell;
	return nil;
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)_indexPath
{
    @try{
    [_tableView deselectRowAtIndexPath:_indexPath animated:YES];
    
    /*OCoffee *coffee = [self.fetchedResultsController objectAtIndexPath:_indexPath];*/
    OFoodResultViewController *detailViewController = [[OFoodResultViewController alloc] initWithNibName:@"OFoodResultViewController" bundle:nil];
	;
	detailViewController.categorySelected = [mCategoriesArray objectAtIndex:_indexPath.row];
	detailViewController.filtersArray = mFiltersSelectedArray;
	
    
    
    //detailViewController.delegate = self;
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/oFoodViewController");
        
        UIAlertView *alert =[[UIAlertView alloc]
                             initWithTitle:@"No Disponible"
                             message:@"Opción no disponible"
                             delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
        [alert show];
        
    }

	
}


@end
