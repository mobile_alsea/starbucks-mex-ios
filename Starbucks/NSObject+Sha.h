//
//  NSObject+Sha.h
//  NearPlaces
//
//  Created by Hebert Adair Rojas Delgado on 21/03/13.
//  Copyright (c) 2013 UNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Sha)

- (NSString*) SHA256;
@end
