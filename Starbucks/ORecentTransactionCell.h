//
//  ORecentTransactionCell.h
//  Starbucks
//
//  Created by Mobile on 10/22/13.
//  Copyright (c) 2013 Ironbit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ORecentTransactionCell : UITableViewCell
{
    UILabel * mDateLabel;
    UILabel * mDescriptionLabel;
    UILabel * mAmountLabel;
    BOOL mIndexPathPair;
    int mIndex;
}

@property(nonatomic, strong) IBOutlet UILabel * dateLabel;
@property(nonatomic, strong) IBOutlet UILabel * descriptionLabel;
@property(nonatomic, strong) IBOutlet UILabel * amountLabel;
@property (nonatomic, assign) BOOL indexPathPair;
@property (nonatomic, assign) int index;


@end
