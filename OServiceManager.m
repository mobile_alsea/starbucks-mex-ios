//
//  OServiceManager.m
//  Starbucks
//
//  Created by Isabelo Pamies López on 31/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OServiceManager.h"

#import <DYCore/operation/DYOperationQueue.h>
#import <DYXML/xml/DYXMLParser.h>
#import <DYXML/dom/DYDOMParser.h>

#import "OAppDelegate.h"
#import "OCofeesImporter.h"
#import "OStoresImporter.h"
#import "OFoodImporter.h"
#import "ODrinksImporter.h"
#import "OUpdate+Extras.h"

#import <DYCore/operation/DYDispatch.h>

@interface OServiceManager (PrivateMethods)

- (void) processVersions:(NSDictionary *)_response;
- (void) commitService:(NSString *)_service;
- (void) mergeChanges:(NSManagedObjectContext *)_context;

- (void) downloadVersions;
- (void) downloadCoffee:(NSDictionary *)_attrDict;
- (void) downloadStores:(NSDictionary *)_attrDict;
- (void) downloadFood:(NSDictionary *)_attrDict;

- (void) downloadFinish;

@end

@implementation OServiceManager

@synthesize importerCount = mImporterCount;
@synthesize force = mForce;


- (id) init
{
    @try{
	self = [super init];
	if(self!=nil)
	{
		OAppDelegate *appDelegate = (OAppDelegate *) [UIApplication sharedApplication].delegate;
		mContext = [[NSManagedObjectContext alloc] init];
		[mContext setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
		mServicesDict = [[NSMutableDictionary alloc] initWithCapacity:0];
	}
	
	return self;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/int");
    }
}

+ (void) updateFullData
{
    @try{
	[OServiceManager updateFullData:NO];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/updateFull");
    }
}

+ (void) updateFullData:(BOOL)_force
{
    @try{
	[[DYOperationQueue sharedInstance] cancelAllOperations];
	
	OServiceManager *sm = [OServiceManager sharedInstance];
	sm.importerCount = 0;
	sm.force = _force;
	[DYOperationQueue addTarget:sm selector:@selector(downloadVersions) object:nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/updatefulldata");
    }
}

- (void) downloadVersions
{
    @try{
    DYDOMParser *domParser = [[DYDOMParser alloc] init];
    [DYXMLParser parseXMLURL:[NSURL URLWithString:kVersionsURL] delegate:domParser];
    
    [self performSelectorOnMainThread:@selector(processVersions:) withObject:[NSDictionary dictionaryWithDictionary:domParser.dom] waitUntilDone:NO];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/downloadVersions");
    }
}

- (void) downloadCoffee:(NSDictionary *)_attrDict
{
    @try{
	NSString *nameService = [[_attrDict objectForKey:@"nombre"] objectForKey:@"value"];
	mCoffeesImporter = [[OCofeesImporter alloc] init];
	mCoffeesImporter.serviceName = nameService;
	mCoffeesImporter.delegate = self;
	[DYOperationQueue addOperation:mCoffeesImporter];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/downloadcoffe");
    }
}

- (void) downloadDrink:(NSDictionary *)_attrDict
{
    @try{
	NSString *nameService = [[_attrDict objectForKey:@"nombre"] objectForKey:@"value"];
	mDrinkImporter = [[ODrinksImporter alloc] init];
	mDrinkImporter.serviceName = nameService;
	mDrinkImporter.delegate = self;
	[DYOperationQueue addOperation:mDrinkImporter];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/downloadDrink");
    }
}


- (void) downloadStores:(NSDictionary *)_attrDict
{
    @try{
	NSString *nameService = [[_attrDict objectForKey:@"nombre"] objectForKey:@"value"];
	mStoresImporter = [[OStoresImporter alloc] init];
	mStoresImporter.serviceName = nameService;
    mStoresImporter.delegate = self;
    [DYOperationQueue addOperation:mStoresImporter];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/downloadStores");
    }
}

- (void) downloadFood:(NSDictionary *)_attrDict
{
    @try{
	NSString *nameService = [[_attrDict objectForKey:@"nombre"] objectForKey:@"value"];
	mFoodImporter = [[OFoodImporter alloc] init];
	mFoodImporter.serviceName = nameService;
    mFoodImporter.delegate = self;
    [DYOperationQueue addOperation:mFoodImporter];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/downloadFood");
    }
}

- (void) processVersions:(NSDictionary *)_response
{
    @try{
	if(_response == nil || [_response count] <= 0)
	{
		[self downloadFinish];
		return;
	}
	
	NSArray *sectionsArray = [[_response objectForKey:@"secciones"] objectForKey:@"seccion"];
	for (NSDictionary *sect in sectionsArray)
	{
        DYOperationQueue *queue = [DYOperationQueue sharedInstance];
        queue.maxConcurrentOperationCount = 1;
        
		NSString *nameService = [[sect objectForKey:@"nombre"] objectForKey:@"value"];
		NSInteger version = [[[sect objectForKey:@"version"] objectForKey:@"value"] integerValue];
		[mServicesDict setObject:[NSNumber numberWithInteger:version] forKey:nameService];
		
		if([OUpdate isUnsynchronizedToService:nameService version:version context:mContext] || mForce == YES)
		{
			if([nameService isEqualToString:@"cafes"])
			{
				++mImporterCount;
				[self downloadCoffee:sect];
			}
			else if([nameService isEqualToString:@"tiendas"])
			{
				++mImporterCount;
				[self downloadStores:sect];
			}
			else if([nameService isEqualToString:@"comidas"])
			{
				++mImporterCount;
				[self downloadFood:sect];
			}
			else if([nameService isEqualToString:@"bebidas"])
			{
				++mImporterCount;
				[self downloadDrink:sect];
			}
		}
	}
	// Si no hay servicios sin sincronizar
	if(mImporterCount <= 0)
	{
		[self downloadFinish];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/processVersions");
    }
}

- (void) commitService:(NSString *)_service
{
    @try{
	OUpdate *up = [OUpdate updateWithName:_service context:mContext];
	if(up == nil)
	{
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"Update" inManagedObjectContext:mContext];
		up = [[OUpdate alloc] initWithEntity:entity insertIntoManagedObjectContext:mContext];
		up.name = _service;
	}
	up.version = (NSNumber *) [mServicesDict objectForKey:_service];
	[self mergeChanges:mContext];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/commitService");
    }
}

- (void) mergeChanges:(NSManagedObjectContext *)_context
{
    @try{
	OAppDelegate *appDelegate = (OAppDelegate *) [UIApplication sharedApplication].delegate;
	NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
	[dnc addObserver:appDelegate selector:@selector(mergeMainContextWithContext:) name:NSManagedObjectContextDidSaveNotification object:_context];
	[appDelegate saveManagedObjectContext:_context];
	[dnc removeObserver:appDelegate name:NSManagedObjectContextDidSaveNotification object:_context];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/mergeChanges");
    }
}

- (void) downloadFinish
{
    @try{
	[[NSNotificationCenter defaultCenter] postNotificationName:kFinishImporterNotification object:nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/downloadFinish");
    }
}

@end


#pragma mark - BaseImporter Delegate Methods
@implementation OServiceManager (BaseImporterDelegateMethods)

- (void) importerDidFinishAllItems:(id)_importer withData:(id)_data withIdentifier:(NSString *)_identifier
{
    @try{
	NSManagedObjectContext *addingContext = (NSManagedObjectContext *)_data;
	OBaseImporter *bimporter = (OBaseImporter *) _importer;
	[self mergeChanges:addingContext];
	[self commitService:bimporter.serviceName];
	
	--mImporterCount;
	if(mImporterCount <= 0)
	{
		[self downloadFinish];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/importerDidFinish");
    }
}

- (void) importerDidFinishItem:(id)_importer withData:(id)_data withIdentifier:(NSString *)_identifier
{
	
}

- (void) importerDidFail:(id)_importer withError:(NSError *)_error withIdentifier:(NSString *)_identifier
{
    @try{
    NSLog(@"%@",[_error description]);
	
	--mImporterCount;
	if(mImporterCount <= 0)
	{
		[self downloadFinish];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/serviceManager/importerDidFail");
    }
}

@end
