//
//  OServiceManager.h
//  Starbucks
//
//  Created by Isabelo Pamies López on 31/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DYCore/singleton/DYSingletonObject.h>
#import "OBaseImporter.h"

#import "OCofeesImporter.h"
#import "ODrinksImporter.h"
#import "OStoresImporter.h"
#import "OFoodImporter.h"

#define kFinishImporterNotification @"STARBUCKS_FINISH_IMPORTER"

@interface OServiceManager : DYSingletonObject
{
	NSManagedObjectContext	*mContext;
	NSMutableDictionary		*mServicesDict;
	
	NSInteger				mImporterCount;
	BOOL					mForce;
    
    OCofeesImporter *mCoffeesImporter;
	ODrinksImporter *mDrinkImporter;
	OStoresImporter *mStoresImporter;
    OFoodImporter *mFoodImporter;
}

@property (assign) NSInteger importerCount;
@property (assign) BOOL		 force;

+ (void) updateFullData;
+ (void) updateFullData:(BOOL)_force;

@end

@interface OServiceManager (BaseImporterDelegateMethods) <BaseImporterDelegate>
@end
