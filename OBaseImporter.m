//
//  OBaseImporter.m
//  Starbucks
//
//  Created by Adrián Caramés Ramos on 22/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OBaseImporter.h"


@implementation OBaseImporter



#pragma mark - Constructors

- (id) init
{
    self = [super init];
    if (self)
    {       
        mAddingContext = [[NSManagedObjectContext alloc] init];
    }
    return self;
}


#pragma mark - Properties

@synthesize delegate = mDelegate;
@synthesize addingContext = mAddingContext;
@synthesize serviceName = mServiceName;


- (NSString *) requestURL
{
    return nil;
}


#pragma mark - NSOperation Methods

- (void) main
{
    @try{
    mAppDelegate = (OAppDelegate *)[UIApplication sharedApplication].delegate;
    
    [mAddingContext setPersistentStoreCoordinator:mAppDelegate.persistentStoreCoordinator];
    
//    mDateFormatter = [[NSDateFormatter alloc] init];
//    [mDateFormatter setDateFormat:@"dd/MM/yyyy"];
//    [mDateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//    [mDateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    NSData *data = [self getData];
    
    [mAddingContext lock];
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser parse];
    [mAddingContext unlock];
    
    [self performSelectorOnMainThread:@selector(importerDidFinishAllItems:) withObject:mAddingContext waitUntilDone:YES];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/ObaseImporter");
    }
}



#pragma mark - Delegate Methods

- (void) importerDidFail:(NSError *)_error
{}

- (void) didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDict
{}

- (void) didEndElement:(NSString *)_elementName withElementValue:(NSString *)_elementValue
{}


#pragma mark - Private Methods

- (NSData *) getData
{
    return nil;
}


#pragma mark - XML Delegate Methods

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)_string
{
    @try{
	if (mCurrentString == nil)
		mCurrentString = [[NSMutableString alloc] init];
	
	[mCurrentString appendString:[_string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/parser");
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    @try{
	[self importerDidFail:parseError];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/parser");
    }
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)_elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)_attributeDict
{
    @try{
    [self didStartElement:_elementName attributes:_attributeDict];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/parser");
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)_elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    @try{
      [self didEndElement:_elementName withElementValue:mCurrentString];
	
	mCurrentString = nil;
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/parser");
    }
}



@end
