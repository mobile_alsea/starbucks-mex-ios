#import "ARCameraViewController.h"

@interface ARCameraViewController ()
@end

@implementation ARCameraViewController

- (id)init
{
	self = [super init];
	if(self) {
		self.arGeoController = [[ARGeoViewController alloc] init];
		self.arGeoController.scaleViewsBasedOnDistance = NO;
		self.arGeoController.maximumScaleDistance = 0.0;
		self.arGeoController.minimumScaleFactor = 1.0;
	}
	return self;
}

- (void)viewDidLoad
{	
	[super viewDidLoad];
	[self setCaptureManager:[[CaptureSessionManager alloc] init]];
	[[self captureManager] addVideoInput];
	[[self captureManager] addVideoPreviewLayer];
	CGRect layerRect = [[[self view] layer] bounds];
	[[[self captureManager] previewLayer] setBounds:layerRect];
	[[[self captureManager] previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                                                CGRectGetMidY(layerRect))];
	[[[self view] layer] addSublayer:[[self captureManager] previewLayer]];
	
	[self addChildViewController:self.arGeoController];
	[self.view addSubview:self.arGeoController.view];
	self.arGeoController.view.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    if(![[self.captureManager captureSession] isRunning])
        [[self.captureManager captureSession] startRunning];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

#pragma mark - Rotation

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end

