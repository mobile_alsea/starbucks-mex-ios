#import <UIKit/UIKit.h>
#import "CaptureSessionManager.h"
#import "ARGeoViewController.h"

@interface ARCameraViewController : UIViewController

@property (strong, nonatomic) ARGeoViewController *arGeoController;
@property (strong) CaptureSessionManager *captureManager;

@end
