//
//  ARGeoViewController.m
//  ARKitDemo
//
//  Created by Zac White on 8/2/09.
//  Copyright 2009 Zac White. All rights reserved.
//

#import "ARGeoViewController.h"

#import "ARGeoCoordinate.h"

@implementation ARGeoViewController

@synthesize centerLocation;

#pragma mark - Properties
- (void)setCenterLocation:(CLLocation *)newLocation {
	centerLocation = newLocation;
	
	for (ARGeoCoordinate *geoLocation in self.coordinates) {
		if ([geoLocation isKindOfClass:[ARGeoCoordinate class]]) {
			[geoLocation calibrateUsingOrigin:centerLocation];
			
			if (geoLocation.radialDistance > self.maximumScaleDistance) {
				self.maximumScaleDistance = geoLocation.radialDistance;
			}
		}
	}
}

//#pragma mark - UIViewController LifeCycle Selectors
//- (id)init {
//    if ((self = [super init])) {
//        self.locationManager = [[CLLocationManager alloc] init];
//        self.locationManager.headingFilter = kCLHeadingFilterNone;
//		self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    }
//
//    return self;
//}
//
//#pragma mark - Public Selectors
//- (void)startListening {
//    [super startListening];
//    
//    [self.locationManager startUpdatingLocation];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    self.centerLocation = newLocation;
//}

@end
