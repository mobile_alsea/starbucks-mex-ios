//
//  ARKViewController.h
//  ARKitDemo
//
//  Created by Zac White on 8/1/09.
//  Copyright 2009 Zac White. All rights reserved.
//

#import "ARCoordinate.h"

@class ARViewController;

@protocol ARViewDelegate

- (UIView *)viewForCoordinate:(ARCoordinate *)coordinate;
- (id)objectWithManagedObjectID:(NSManagedObjectID *)managedObjectID;
- (id)inforForStoreWithManagedObjectID:(NSManagedObjectID *)managedObjectID;
@optional
- (void)didSelectStoreWithObjectID:(NSManagedObjectID *)storeID;

@end


@interface ARViewController : UIViewController <UIAccelerometerDelegate> {
	UIAccelerometer                     *accelerometerManager;
	
	ARCoordinate                        *centerCoordinate;
	
	BOOL                                scaleViewsBasedOnDistance;
	double                              maximumScaleDistance;
	double                              minimumScaleFactor;
	
	//defaults to 20hz;
	double                              updateFrequency;
	
	BOOL                                rotateViewsBasedOnPerspective;
	double                              maximumRotationAngle;
	
@private
	NSTimer                             *_updateTimer;
	UIView                              *ar_overlayView;
}

@property (strong) NSMutableArray *coordinates;
@property (strong) NSMutableArray *coordinateViews;

@property (nonatomic) BOOL scaleViewsBasedOnDistance;
@property (nonatomic) double maximumScaleDistance;
@property (nonatomic) double minimumScaleFactor;
@property (nonatomic) BOOL rotateViewsBasedOnPerspective;
@property (nonatomic) double maximumRotationAngle;
@property (nonatomic) double updateFrequency;

//adding coordinates to the underlying data model.
- (void)addCoordinate:(ARCoordinate *)coordinate;
- (void)addCoordinate:(ARCoordinate *)coordinate animated:(BOOL)animated;
- (void)addCoordinates:(NSArray *)newCoordinates;

//removing coordinates
- (void)removeCoordinate:(ARCoordinate *)coordinate;
- (void)removeCoordinate:(ARCoordinate *)coordinate animated:(BOOL)animated;
- (void)removeCoordinates:(NSArray *)coordinates;
- (void)removeAllCoordinates;

- (void)startListening;
- (void)stopListening;
- (void)reloadCoordinatesWithCoordinates:(NSArray *)coordinates;
- (void)updateLocations:(NSTimer *)timer;
- (CGPoint)pointInView:(UIView *)realityView forCoordinate:(ARCoordinate *)coordinate;
- (BOOL)viewportContainsCoordinate:(ARCoordinate *)coordinate;

// Fake CLLocationManagerDelegate Method  Forwarding
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading;

@property (nonatomic, weak) NSObject<ARViewDelegate> *delegate;
@property (strong) ARCoordinate *centerCoordinate;
@property (nonatomic, strong) UIAccelerometer *accelerometerManager;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (assign, nonatomic) BOOL blockEnumeration;
@property (assign, nonatomic) BOOL isShowingCompass;
@property (assign, nonatomic) BOOL isShowingDetail;

@end
