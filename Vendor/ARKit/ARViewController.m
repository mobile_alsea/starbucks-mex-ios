//
//  ARKViewController.m
//  ARKitDemo
//
//  Created by Zac White on 8/1/09.
//  Copyright 2009 Zac White. All rights reserved.
//

#import "ARViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SBStoreARView.h"
#import "SBStoreARDetailView.h"
#import "ARGeoCoordinate.h"
#import "OStoresDetailViewController.h"
#import "OStore.h"

#define VIEWPORT_WIDTH_RADIANS .5
#define VIEWPORT_HEIGHT_RADIANS .7392

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;}
CGFloat RadiansToDegrees(CGFloat radians) {return radians * 180 / M_PI;}

@interface ARViewController ()

<SBStoreARViewDelegate, OStoresDetailViewControllerDelegate>

@property (strong, nonatomic) UIImage *topBarLockImage;
@property (strong, nonatomic) UIImage *topBarBackImage;
@property (strong, nonatomic) UIImageView *topBar;
@property (strong, nonatomic) UIView *decorationView;
@property (strong, nonatomic) UIButton *lockButton;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIImage *lockImageOff;
@property (strong, nonatomic) UIImage *unlockImageOff;
@property (strong, nonatomic) UIImageView *compassArrow;
@property (strong, nonatomic) ARGeoCoordinate *compassCoordinate;
@property (assign, nonatomic) BOOL isCompassEnabled;
@property (assign, nonatomic) BOOL isDismissLockedFromController;
@property (strong, nonatomic) SBStoreARDetailView *storeDetailView;
@property (strong, nonatomic) NSMutableArray *selectedViews;
@property (assign, nonatomic) CGFloat viewHeight;

@end

@implementation ARViewController

@synthesize accelerometerManager;
@synthesize centerCoordinate;
@synthesize scaleViewsBasedOnDistance, rotateViewsBasedOnPerspective;
@synthesize maximumScaleDistance;
@synthesize minimumScaleFactor, maximumRotationAngle;
@synthesize updateFrequency;
@synthesize delegate;


#pragma mark - Lock/Unlock Methods

- (void)lockDismissAR:(BOOL)lock
{
	NSNumber *dismissValue = lock?@NO:@YES;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:dismissValue forKey:kOSLockARDismissKey];
	[defaults synchronize];
}

-(IBAction)handleLocking:(id)sender
{
    self.lockButton.selected = !self.lockButton.selected;
    [self lockDismissAR:self.lockButton.selected];
}

#pragma mark - Custom Setters

- (void)setIsDismissLockedFromController:(BOOL)isDismissLockedFromController
{
	_isDismissLockedFromController = isDismissLockedFromController;	
	NSNumber *dismissValue = _isDismissLockedFromController?@YES:@NO;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:dismissValue forKey:kOSDismissLockFromController];
	[defaults synchronize];
}

#pragma mark - Private Selectors

- (void)tapOverItem:(UITapGestureRecognizer *)tapGesture {
    NSInteger index = [self.coordinateViews indexOfObject:tapGesture.view];
    ARCoordinate *coordinate = [self.coordinates objectAtIndex:index];
    if ([self.delegate respondsToSelector:@selector(didSelectStoreWithObjectID:)])
        [self.delegate didSelectStoreWithObjectID:coordinate.storeID];
}

#pragma mark - Object Lifecycle Methods

- (id)init {
	if (!(self = [super init])) return nil;
	ar_overlayView = nil;	
	self.coordinates = [[NSMutableArray alloc] init];
	self.coordinateViews = [[NSMutableArray alloc] init];
    self.selectedViews = [[NSMutableArray alloc] init];
	
	_updateTimer = nil;
	self.updateFrequency = 1/6.0f; /// 5.0;
		
	self.scaleViewsBasedOnDistance = NO;
	self.maximumScaleDistance = 0.0;
	self.minimumScaleFactor = 1.0;
	self.rotateViewsBasedOnPerspective = NO;
	self.maximumRotationAngle = M_PI / 6.0;
	
	self.wantsFullScreenLayout = YES;
		
	return self;
}

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {	
	[super loadView];
	ar_overlayView = [[UIView alloc] initWithFrame:CGRectZero];
			
	[self.view addSubview:ar_overlayView];
	
	self.decorationView = [[UIView alloc] init];
	self.decorationView.backgroundColor = [UIColor clearColor];
	self.decorationView.frame = ar_overlayView.bounds;
	[self.view addSubview:self.decorationView];
	
	self.lockImageOff = [UIImage imageNamed:@"bt_candadocerrado_off"];
	self.unlockImageOff = [UIImage imageNamed:@"bt_candadoabierto_off"];
		
	self.lockButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.lockButton.frame = CGRectMake(0.0f, 0.0f, 73.0f, 30.0f);	
	[self.lockButton setImage:self.unlockImageOff forState:UIControlStateNormal];
	[self.lockButton setImage:self.lockImageOff forState:UIControlStateSelected];
	[self.lockButton addTarget:self action:@selector(handleLocking:) forControlEvents:UIControlEventTouchDown];
	
	self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.backButton.frame = CGRectMake(0.0f, 0.0f, 50.0f, 30.0f);
	[self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.backButton setImage:[UIImage imageNamed:@"bt_regresar_off"] forState:UIControlStateNormal];
	[self.backButton setImage:[UIImage imageNamed:@"bt_regresar_on"] forState:UIControlStateHighlighted];
	[self.backButton addTarget:self action:@selector(backFromCompass:) forControlEvents:UIControlEventTouchUpInside];
		
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	self.lockButton.selected = ![[defaults objectForKey:kOSLockARDismissKey] boolValue];
	
	self.topBarLockImage = [UIImage imageNamed:@"topbar_candado"];
	self.topBarBackImage = [UIImage imageNamed:@"topbar_compass"];
	
	self.topBar = [[UIImageView alloc] initWithImage:self.topBarLockImage];
		
	[self.decorationView addSubview:self.topBar];
	[self.decorationView addSubview:self.lockButton];
	[self.decorationView addSubview:self.backButton];
	CGRect topBarFrame = self.topBar.bounds;
	CGFloat midYTopBar = CGRectGetMidY(topBarFrame);
	CGFloat leftInset = 5.0f;
	self.lockButton.center = CGPointMake(CGRectGetMidX(topBarFrame), midYTopBar);
	self.backButton.center = CGPointMake(CGRectGetMidX(self.backButton.bounds)+leftInset, midYTopBar);
	self.backButton.alpha = 0.0f;
		
	self.compassArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flecha"]];	
	self.storeDetailView = [SBStoreARDetailView getView];
}


- (void) viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
#pragma mark - Rotation

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)setUpdateFrequency:(double)newUpdateFrequency {
	updateFrequency = newUpdateFrequency;
	
	if (!_updateTimer)
        return;
	
	[_updateTimer invalidate];
	_updateTimer = nil;
	_updateTimer = [NSTimer scheduledTimerWithTimeInterval:self.updateFrequency
													 target:self
												   selector:@selector(updateLocations:)
												   userInfo:nil
													repeats:YES];
}

- (BOOL)viewportContainsCoordinate:(ARCoordinate *)coordinate {
	double centerAzimuth = self.centerCoordinate.azimuth;
	double leftAzimuth = centerAzimuth - VIEWPORT_WIDTH_RADIANS / 2.0;
	
	if (leftAzimuth < 0.0)
		leftAzimuth = 2 * M_PI + leftAzimuth;
	
	double rightAzimuth = centerAzimuth + VIEWPORT_WIDTH_RADIANS / 2.0;
	
	if (rightAzimuth > 2 * M_PI)
		rightAzimuth = rightAzimuth - 2 * M_PI;
	
	BOOL result = (coordinate.azimuth > leftAzimuth && coordinate.azimuth < rightAzimuth);
	
	if(leftAzimuth > rightAzimuth)
		result = (coordinate.azimuth < rightAzimuth || coordinate.azimuth > leftAzimuth);
	
	double centerInclination = self.centerCoordinate.inclination;
	double bottomInclination = centerInclination - VIEWPORT_HEIGHT_RADIANS / 2.0;
	double topInclination = centerInclination + VIEWPORT_HEIGHT_RADIANS / 2.0;
	
	//check the height.
	result = result && (coordinate.inclination > bottomInclination && coordinate.inclination < topInclination);
	return result;
}

- (void)startListening {
				
	//steal back the delegate.
    [self.locationManager startUpdatingHeading];
    
    if(self.selectedViews) {
        self.selectedViews = [[NSMutableArray alloc] init];
    }
		
	if (!self.accelerometerManager) {
		self.accelerometerManager = [UIAccelerometer sharedAccelerometer];
		self.accelerometerManager.updateInterval = 0.01;
	}
	self.accelerometerManager.delegate = self;
	
	self.centerCoordinate = [ARCoordinate coordinateWithRadialDistance:0 inclination:0 azimuth:0];
	
	if (!_updateTimer) {
		_updateTimer = [NSTimer scheduledTimerWithTimeInterval:self.updateFrequency
														target:self
													  selector:@selector(updateLocations:)
													  userInfo:nil
													   repeats:YES];
	}
}

- (void)stopListening
{
	[self.locationManager stopUpdatingHeading];
    self.accelerometerManager.delegate = nil;
    self.accelerometerManager = nil;
    [_updateTimer invalidate];
	_updateTimer = nil;
    [self.selectedViews removeAllObjects];    		
}

- (void)reloadCoordinatesWithCoordinates:(NSArray *)coordinates
{
    self.blockEnumeration = YES;
    [self.coordinates removeAllObjects];
    [[ar_overlayView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.coordinateViews removeAllObjects];
    [self addCoordinates:coordinates];
    self.blockEnumeration = NO;
}

- (CGPoint)pointInView:(UIView *)realityView forCoordinate:(ARCoordinate *)coordinate {
	
	CGPoint point;
	
//  x coordinate.
	
	double pointAzimuth = coordinate.azimuth;
	
//  our x numbers are left based.
	double leftAzimuth = self.centerCoordinate.azimuth - VIEWPORT_WIDTH_RADIANS / 2.0;
	
	if (leftAzimuth < 0.0) {
		leftAzimuth = 2 * M_PI + leftAzimuth;
	}
	
	if (pointAzimuth < leftAzimuth) {
		//it's past the 0 point.
		point.x = ((2 * M_PI - leftAzimuth + pointAzimuth) / VIEWPORT_WIDTH_RADIANS) * realityView.frame.size.width;
	} else {
		point.x = ((pointAzimuth - leftAzimuth) / VIEWPORT_WIDTH_RADIANS) * realityView.frame.size.width;
	}
	
//  y coordinate.
	
	double pointInclination = coordinate.inclination;
	
	double topInclination = self.centerCoordinate.inclination - VIEWPORT_HEIGHT_RADIANS / 2.0;
	
	point.y = realityView.frame.size.height - ((pointInclination - topInclination) / VIEWPORT_HEIGHT_RADIANS) * realityView.frame.size.height;
	
	return point;
}

#define kFilteringFactor 0.05
UIAccelerationValue rollingX, rollingZ;

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {	
	rollingZ  = (acceleration.z * kFilteringFactor) + (rollingZ  * (1.0 - kFilteringFactor));
    rollingX = (acceleration.y * kFilteringFactor) + (rollingX * (1.0 - kFilteringFactor));
	
	if (rollingZ > 0.0) {
		self.centerCoordinate.inclination = atan(rollingX / rollingZ) + M_PI / 2.0;
	} else if (rollingZ < 0.0) {
		self.centerCoordinate.inclination = atan(rollingX / rollingZ) - M_PI / 2.0;// + M_PI;
	} else if (rollingX < 0) {
		self.centerCoordinate.inclination = M_PI/2.0;
	} else if (rollingX >= 0) {
		self.centerCoordinate.inclination = 3 * M_PI/2.0;
	}	
}

#pragma mark - Coordinates Handling

- (void)addCoordinate:(ARCoordinate *)coordinate {
	[self addCoordinate:coordinate animated:YES];
}

- (void)addCoordinate:(ARCoordinate *)coordinate animated:(BOOL)animated {
	[self.coordinates addObject:coordinate];
		
	if (coordinate.radialDistance > self.maximumScaleDistance) {
		self.maximumScaleDistance = coordinate.radialDistance;
	}
	
	SBStoreARView *itemView = (SBStoreARView *)[self.delegate viewForCoordinate:coordinate];
	itemView.delegate = self;
    
    NSUInteger indexOfObject = [self.selectedViews indexOfObject:coordinate.title];
    if(indexOfObject!=NSNotFound)
        [itemView storeSelected];
    
    [self.coordinateViews addObject:itemView];
}

- (void)addCoordinates:(NSArray *)newCoordinates {
	
//  go through and add each coordinate.
	for (ARCoordinate *coordinate in newCoordinates) {
		[self addCoordinate:coordinate animated:NO];
	}
}

- (void)removeCoordinate:(ARCoordinate *)coordinate {
	[self removeCoordinate:coordinate animated:YES];
}

- (void)removeCoordinate:(ARCoordinate *)coordinate animated:(BOOL)animated {
	[self.coordinates removeObject:coordinate];
}

- (void)removeCoordinates:(NSArray *)coordinates {	
	for (ARCoordinate *coordinateToRemove in coordinates) {
		NSUInteger indexToRemove = [self.coordinates indexOfObject:coordinateToRemove];
				
		[self.coordinates removeObjectAtIndex:indexToRemove];
		[self.coordinateViews removeObjectAtIndex:indexToRemove];
	}
}

- (void)removeAllCoordinates
{
	for(UIView *arView in self.coordinateViews)
		[arView removeFromSuperview];
	
	[self.coordinates removeAllObjects];
	[self.coordinateViews removeAllObjects];
}

#pragma mark - Updating Methods

- (void)updateLocations:(NSTimer *)timer
{
//      update locations!
    if (!self.coordinateViews || self.coordinateViews.count == 0 || self.blockEnumeration || self.isShowingDetail || self.isShowingCompass) {
        return;
    }
    
    int index = 0;
    for (ARGeoCoordinate *item in self.coordinates) {
        SBStoreARView *viewToDraw = [self.coordinateViews objectAtIndex:index];
        
        if ([self viewportContainsCoordinate:item]) {
            
            CGPoint loc = [self pointInView:ar_overlayView forCoordinate:item];
            
            CGFloat scaleFactor = 1.0;
            if (self.scaleViewsBasedOnDistance) {
                scaleFactor = 1.0 - self.minimumScaleFactor * (item.radialDistance / self.maximumScaleDistance);
            }
            
            float width = viewToDraw.bounds.size.width * scaleFactor;
            float height = viewToDraw.bounds.size.height * scaleFactor;
            
            CGRect rect = CGRectMake(loc.x - width / 2.0, loc.y - height / 2.0, width, height);
            float y = 0;
            for (UIView *intersectView in ar_overlayView.subviews) {
                if (intersectView != viewToDraw && CGRectIntersectsRect(rect, intersectView.frame)) {
                    // 260
                    y = self.viewHeight - CGRectGetHeight(intersectView.bounds) * index;
                    break;
                }
            }
            
            rect = CGRectMake(loc.x - width / 2.0, loc.y - height / 2.0 + y, width, height);
            viewToDraw.frame = rect;
            CLLocationDistance storeDistance = [self.locationManager.location distanceFromLocation:item.geoLocation];
            [viewToDraw.storeDistanceLabel performSelectorOnMainThread:@selector(setText:) withObject:[self formatForDistance:storeDistance] waitUntilDone:NO];
            
            NSDictionary *storeInfo = [self.delegate inforForStoreWithManagedObjectID:item.storeID];
            viewToDraw.isOpen = [[storeInfo objectForKey:@"isOpen"] boolValue];
            viewToDraw.storeNameLabel = [storeInfo objectForKey:@"name"];
            viewToDraw.storeAddressLabel = [storeInfo objectForKey:@"address"];
            
            if (!(viewToDraw.superview)) {
                [ar_overlayView addSubview:viewToDraw];
                [ar_overlayView sendSubviewToBack:viewToDraw];
            }
            
        } else {
            [viewToDraw removeFromSuperview];
            item.deltaY = 0;
            viewToDraw.transform = CGAffineTransformIdentity;
        }
        
        index++;
    }    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
	self.centerCoordinate.azimuth = fmod(newHeading.magneticHeading, 360.0) * (2 * (M_PI / 360.0));
	
	if(self.isCompassEnabled) {
        CLLocationDistance distanceToCompassCoordinate = [self.locationManager.location distanceFromLocation:self.compassCoordinate.geoLocation];
		[self.storeDetailView.storeDistanceLabel performSelectorOnMainThread:@selector(setText:) withObject:[self formatForDistance:distanceToCompassCoordinate] waitUntilDone:NO];
        
		float lat1 = DegreesToRadians(self.locationManager.location.coordinate.latitude);
        float lng1 = DegreesToRadians(self.locationManager.location.coordinate.longitude);
        float lat2 = DegreesToRadians(self.compassCoordinate.geoLocation.coordinate.latitude);
        float lng2 = DegreesToRadians(self.compassCoordinate.geoLocation.coordinate.longitude);
        float deltalng = lng2 - lng1;
        
        double y = sin(deltalng) * cos(lat2);
        double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(deltalng);
        double bearing = atan2(y, x) + 2 * M_PI;
        
        float bearingDegrees = RadiansToDegrees(bearing);
        bearingDegrees = (int)bearingDegrees % 360;
        double heading = newHeading.trueHeading;
        double rotationInDegree = (bearingDegrees - heading);  //the important line
        rotationInDegree = fmod((rotationInDegree + 360), 360);
        [self performSelectorOnMainThread:@selector(rotateCompassWithRadians:) withObject:@(DegreesToRadians(rotationInDegree)) waitUntilDone:NO];        
	}
}

- (void)rotateCompassWithRadians:(NSNumber *)radians
{
    self.compassArrow.transform = CGAffineTransformMakeRotation([radians floatValue]);
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];    
    self.viewHeight = CGRectGetHeight(self.view.bounds);
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
	CGRect viewFrame = self.view.bounds;
	ar_overlayView.frame = viewFrame;
	
	CGRect decorationFrame = viewFrame;
	decorationFrame.size.height = 44.0;
	self.decorationView.frame = decorationFrame;
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];		
}

- (void)didReceiveMemoryWarning
{
//  Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
//  Release any cached data, images, etc that aren't in use.
}

- (NSString *)formatForDistance:(CLLocationDistance)distance
{
	NSString *distanceFormat;
	if(distance>=1000.0f){
		distanceFormat = [NSString stringWithFormat:@"%.02f km", distance/1000];
	} else {
		distanceFormat = [NSString stringWithFormat:@"%.02f m", distance];
	}
	return distanceFormat;
}

#pragma mark - OStoresDetailViewControllerDelegate Methods

- (CLLocation *)storesDetailControllerRequestLocation:(OStoresDetailViewController *)storesDetailController
{
	return self.locationManager.location;
}

#pragma mark - SBARStoreViewDelegate Methods

- (IBAction)dismissStoreDetail:(id)sender
{
	__weak ARViewController *weakSelf = self;
	[self dismissViewControllerAnimated:YES completion:^{
		if(weakSelf) {
			ARViewController *strongSelf = weakSelf;
			strongSelf.isDismissLockedFromController = NO;
            strongSelf.isShowingDetail = NO;
		}
	}];
}

- (void)storeViewDidSelectShowDetail:(SBStoreARView *)storeView
{
	NSInteger index = [self.coordinateViews indexOfObject:storeView];
    ARCoordinate *coordinate = [self.coordinates objectAtIndex:index];
	OStore *store = [self.delegate objectWithManagedObjectID:coordinate.storeID];
	OStoresDetailViewController *storesDetailController = [[OStoresDetailViewController alloc] initWithNibName:@"OStoresDetailViewController" bundle:nil];
	[storesDetailController setStoreSelected:store];
    storesDetailController.delegate = self;
	storesDetailController.comesFromModal = YES;
	self.isDismissLockedFromController = YES;
    self.isShowingDetail = YES;
	SBNavigationController *navigationController = [[SBNavigationController alloc] initWithRootViewController:storesDetailController];
	UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cerrar", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(dismissStoreDetail:)];
	storesDetailController.navigationItem.leftBarButtonItem = closeButtonItem;
	[self presentViewController:navigationController animated:YES completion:nil];
}

- (void)storeViewDidSelectShowOrientation:(SBStoreARView *)storeView
{
	self.isDismissLockedFromController = YES;
    self.isShowingCompass = YES;
	
	NSInteger index = [self.coordinateViews indexOfObject:storeView];
    ARGeoCoordinate *coordinate = [self.coordinates objectAtIndex:index];
	self.compassCoordinate = coordinate;
	
	CGRect viewFrame = self.view.bounds;
	self.compassArrow.alpha = 0.0f;
	self.compassArrow.transform = CGAffineTransformIdentity;
	self.decorationView.frame = viewFrame;
	[self.decorationView addSubview:self.compassArrow];
	CGRect compassBounds = self.compassArrow.bounds;
	CGFloat compassHeight = CGRectGetHeight(compassBounds);
	self.compassArrow.frame = CGRectMake(CGRectGetMidX(viewFrame)-CGRectGetMidX(compassBounds), CGRectGetHeight(viewFrame)-compassHeight-10.0f,
										 CGRectGetWidth(compassBounds), compassHeight);
		
	self.storeDetailView.alpha = 0.0f;
	[self.decorationView addSubview:self.storeDetailView];
	CGRect storeDetailFrame = self.storeDetailView.bounds;
	storeDetailFrame.origin.y = 54.0f;
	self.storeDetailView.frame = storeDetailFrame;
	
	NSDictionary *storeInfo = [self.delegate inforForStoreWithManagedObjectID:coordinate.storeID];
	self.storeDetailView.storeNameLabel.text = self.compassCoordinate.title;
	self.storeDetailView.storeAddressLabel.text = self.compassCoordinate.subtitle;
	self.storeDetailView.storeDistanceLabel.text = @"0 m";
	self.storeDetailView.isOpen = [[storeInfo objectForKey:@"isOpen"] boolValue];
	
	__weak ARViewController *weakSelf = self;
	[UIView animateWithDuration:0.4f animations:^{
		if(weakSelf) {
			ARViewController *strongSelf = weakSelf;
			strongSelf.compassArrow.alpha = 1.0f;
			strongSelf.storeDetailView.alpha = 1.0;
			strongSelf.topBar.image = strongSelf.topBarBackImage;
			strongSelf.backButton.alpha = 1.0f;
			strongSelf.lockButton.alpha = 0.0;
			ar_overlayView.alpha = 0.0;
		}
	} completion:^(BOOL finished) {
		if(weakSelf) {
			ARViewController *strongSelf = weakSelf;
			strongSelf.isCompassEnabled = YES;
		}
	}];
}

- (void)storeView:(SBStoreARView *)storeView selected:(BOOL)selected
{
    NSInteger index = [self.coordinateViews indexOfObject:storeView];
    ARGeoCoordinate *coordinate = [self.coordinates objectAtIndex:index];
    NSString *title = coordinate.title;
    
    NSUInteger indexOfObject = [self.selectedViews indexOfObject:title];
    if(selected) {
        if(indexOfObject==NSNotFound) {
            [self.selectedViews addObject:coordinate.title];
        }
    } else {
        if(indexOfObject!=NSNotFound) {
            [self.selectedViews removeObjectAtIndex:indexOfObject];
        }
    }
}

#pragma mark - Compass Methods

- (IBAction)backFromCompass:(id)sender
{
	self.isCompassEnabled = NO;
	__weak ARViewController *weakSelf = self;
	[UIView animateWithDuration:0.4f animations:^{
		if(weakSelf) {
			ARViewController *strongSelf = weakSelf;
			strongSelf.compassArrow.alpha = 0.0f;
			strongSelf.storeDetailView.alpha = 0.0;
			strongSelf.topBar.image = strongSelf.topBarLockImage;
			strongSelf.backButton.alpha = 0.0f;
			strongSelf.lockButton.alpha = 1.0;
			ar_overlayView.alpha = 1.0;
		}
	} completion:^(BOOL finished) {
		if(weakSelf) {
			ARViewController *strongSelf = weakSelf;
			strongSelf.compassCoordinate = nil;
			CGRect decorationFrame = strongSelf.view.bounds;
			decorationFrame.size.height = 44.0;
			self.decorationView.frame = decorationFrame;
			strongSelf.isDismissLockedFromController = NO;
            strongSelf.isShowingCompass = NO;
		}
	}];
}

@end
