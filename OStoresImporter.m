//
//  OStoresImporter.m
//  Starbucks
//
//  Created by Adrián Caramés Ramos on 22/08/12.
//  Copyright (c) 2012 Dylvian. All rights reserved.
//

#import "OStoresImporter.h"
#import "OAppDelegate.h"
#import "OStore.h"
#import "Ostore+Extras.h"
#import "OService.h"
#import "OService+Extras.h"
#import "OSchedule.h"

#import <DYCore/operation/DYDispatch.h>

@implementation OStoresImporter



#pragma mark - Management Memory

- (void)dealloc
{
    mCurrentStoreDict = nil;
    mStoreSchedulesArray = nil;
    mStoreServicesArray = nil;
    
}


#pragma mark - Private Methods

- (NSData *) getData
{
    @try{    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kStoresURL]];
	return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/getdata");
    }
    
}

- (void) didStartElement:(NSString *)_elementName attributes:(NSDictionary *)_attributeDict
{
    @try{
    if ([_elementName isEqualToString:@"tiendas"])
    {
        // Remove old stores
        NSArray *stores = [OStore storesInContext:mAddingContext];
        [mAppDelegate removeObjects:stores fromContext:mAddingContext];
        [mAddingContext processPendingChanges];

    }
	else if ([_elementName isEqualToString:@"tienda"])
	{
		mCurrentStoreDict = [[NSMutableDictionary alloc] init];
        
        mIsSchedule = NO;
        mIsService = NO;
	}
    else if ([_elementName isEqualToString:@"servicios"])
    {
        mStoreServicesArray = [[NSMutableArray alloc] init];
        mCurrentServiceDict = [[NSMutableDictionary alloc] init];
        
        mIsService = YES;
    }
    else if ([_elementName isEqualToString:@"servicio"])
    {
        mCurrentServiceDict = [[NSMutableDictionary alloc] init];
       
        mIsService = YES;
    }
    else if ([_elementName isEqualToString:@"horario"])
    {
        mStoreSchedulesArray = [[NSMutableArray alloc] init];
        
        mIsSchedule = YES;
    }
    else if ([_elementName isEqualToString:@"dia"])
    {
        mCurrentScheduleDict = [[NSMutableDictionary alloc] init];
    }
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/StoresImporter/disstarElement");
    }
}

- (void) didEndElement:(NSString *)_elementName withElementValue:(NSString *)_elementValue
{
    @try{
	if ([_elementName isEqualToString:@"tienda"])
	{
        [self insertIntoContext:mCurrentStoreDict];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"servicios"])
    {
        

    }
    else if (_elementValue != nil && [_elementName isEqualToString:@"dia"])
    {
        [mStoreSchedulesArray addObject:mCurrentScheduleDict];
    }
	else if (_elementValue != nil && [_elementName isEqualToString:@"nombre"])
	{
        if (mIsSchedule)
            [mCurrentScheduleDict setObject:_elementValue forKey:@"name"];
        else
            [mCurrentStoreDict setObject:_elementValue forKey:@"name"];
	}
	else if (_elementValue != nil && [_elementName isEqualToString:@"direccion"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"address"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"codigopostal"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"postalCode"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"ciudad"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"city"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"provincia"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"province"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"zona"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"area"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"latitud"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"latitude"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"longitud"])
	{
		[mCurrentStoreDict setObject:_elementValue forKey:@"longitude"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"servicio"])
	{
		[mCurrentServiceDict setObject:_elementValue forKey:@"name"];
        [mStoreServicesArray addObject:mCurrentServiceDict];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"horainicio"])
	{
		[mCurrentScheduleDict setObject:_elementValue forKey:@"startTime"];
	}
    else if (_elementValue != nil && [_elementName isEqualToString:@"horafin"])
	{
		[mCurrentScheduleDict setObject:_elementValue forKey:@"endTime"];
	}
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/didendelement");
    }
}


#pragma mark - CoreData Methods

- (void) insertIntoContext:(NSDictionary *)_item
{
    @try{
    NSEntityDescription *storesEntity = [NSEntityDescription entityForName:@"Stores" inManagedObjectContext:mAddingContext];
    OStore *store = [[OStore alloc] initWithEntity:storesEntity insertIntoManagedObjectContext:mAddingContext];
    
    store.name = [_item objectForKey:@"name"];
    store.address = [_item objectForKey:@"address"];
    store.postalCode = [_item objectForKey:@"postalCode"];
    store.city = [_item objectForKey:@"city"];
    store.province = [_item objectForKey:@"province"];
    store.area = [_item objectForKey:@"area"];
    store.latitude = [NSNumber numberWithDouble:[[_item objectForKey:@"latitude"] doubleValue]];
    store.longitude = [NSNumber numberWithDouble:[[_item objectForKey:@"longitude"] doubleValue]];
    
    for (NSDictionary *serviceDict in mStoreServicesArray)
    {
        if ([serviceDict objectForKey:@"name"] != nil)
        {
            OService *service = [OService serviceWithName:[serviceDict objectForKey:@"name"] context:mAddingContext];
            
            if (service == nil)
            {
                NSEntityDescription *servicesEntity = [NSEntityDescription entityForName:@"Services" inManagedObjectContext:mAddingContext];
                service = [[OService alloc] initWithEntity:servicesEntity insertIntoManagedObjectContext:mAddingContext];
                service.name = [serviceDict objectForKey:@"name"];
                service.isSelected = [NSNumber numberWithBool:NO];
                [store addServicesObject:service];
                
                [mAddingContext processPendingChanges];
            }
            else
                [store addServicesObject:service];

        }
                        
    }
    
    for (NSDictionary *scheduleDict in mStoreSchedulesArray)
    {   
        NSEntityDescription *schedulesEntity = [NSEntityDescription entityForName:@"Schedules" inManagedObjectContext:mAddingContext];
        OSchedule *schedule = [[OSchedule alloc] initWithEntity:schedulesEntity insertIntoManagedObjectContext:mAddingContext];
        schedule.name = [scheduleDict objectForKey:@"name"];
        schedule.startTime = [scheduleDict objectForKey:@"startTime"];
        schedule.endTime = [scheduleDict objectForKey:@"endTime"];
		schedule.order = [NSNumber numberWithInt:[mStoreSchedulesArray indexOfObject:scheduleDict]];
        
        [store addSchedulesObject:schedule];
    }
    
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/insertincontext");
    }
}


#pragma mark - Delegate Methods

- (void) importerDidFinishAllItems:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishAllItems:withData:withIdentifier:)])
		[mDelegate importerDidFinishAllItems:self withData:mAddingContext withIdentifier:@"OStoresImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/importerdidfinish");
    }
}

- (void) importerDidFinishItem:(id)_data
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFinishItem:withData:withIdentifier:)])
		[mDelegate importerDidFinishItem:self withData:mAddingContext withIdentifier:@"OStoresImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/importerdidFinish");
    }
}

- (void) importerDidFail:(NSError *)_error
{
    @try{
	if(mDelegate != nil && [mDelegate respondsToSelector:@selector(importerDidFail:withError:withIdentifier:)])
		[mDelegate importerDidFail:self withError:_error withIdentifier:@"OStoresImporter"];
    }
    
    @catch (NSException *ex) {
        
        NSLog(@"No Disponible - Starbucks/Classes/Data importes/storesimporter/importeDidFail");
    }
}

@end
